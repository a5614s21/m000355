﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using web.Models;
using web.Repository;
//using Web.Controllers;

namespace Web.Service
{
    public class TablesService : Controller
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot, object PrevGuid)
        {
            string currentUserLang = "";
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["iDisplayStart"].ToString());
            int take = int.Parse(requests["iDisplayLength"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }

            if (requests["sSearch"].ToString() != "")
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["sSearch"].ToString());
                sSearch = sSearch[0];
            }

            string orderByKey = requests["mDataProp_" + requests["iSortCol_0"]].ToString();
            string orderByType = requests["sSortDir_0"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            switch (tables)
            {
                #region -- 控制台 --

                ////資源回收桶
                //case "ashcan":
                //    if (model != null)
                //    {
                //        var data = model.Repository<ashcan>();

                //        var models = data.ReadsWhere(m => m.from_guid != "");

                //        if (requests["sSearch"].ToString() != "")
                //        {
                //            if (sSearch["keyword"].ToString() != "")
                //            {
                //                string keywords = sSearch["keyword"].ToString();
                //                models = models.Where(m => m.title.Contains(keywords));
                //            }
                //        }

                //        iTotalDisplayRecords = models.ToList().Count;
                //        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                //    }
                //    break;

                #endregion
                //特色標章
                case "feature":
                    if (model != null)
                    {
                        var data = model.Repository<feature>();
                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品標章
                case "emblem":
                    if (model != null)
                    {
                        var data = model.Repository<emblem>();
                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //首頁BANNER
                case "home_banner":
                    if (model != null)
                    {
                        var data = model.Repository<home_banner>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //CATALOG DOWNLOAD
                case "catalog":
                    if (model != null)
                    {
                        var data = model.Repository<catalog>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //abouts
                case "abouts":
                    if (model != null)
                    {
                        var data = model.Repository<abouts>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //aboutsGroup
                case "group":
                    if (model != null)
                    {
                        var data = model.Repository<group>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //warrantyPolicy
                case "warranty_policy":
                    if (model != null)
                    {
                        var data = model.Repository<warranty_policy>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //warrantyCheck
                case "warranty_register_detail":
                    if (model != null)
                    {
                        var data = model.Repository<warranty_register_detail>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.firstname.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["emailkey"].ToString() != "")
                            {
                                string keywords = sSearch["emailkey"].ToString();
                                models = models.Where(m => m.email.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["telkey"].ToString() != "")
                            {
                                string keywords = sSearch["telkey"].ToString();
                                models = models.Where(m => m.tel.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["compkey"].ToString() != "")
                            {
                                string keywords = sSearch["compkey"].ToString();
                                models = models.Where(m => m.company.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["modelkey"].ToString() != "")
                            {
                                string keywords = sSearch["modelkey"].ToString();
                                models = models.Where(m => m.modelnumber.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["serialkey"].ToString() != "")
                            {
                                string keywords = sSearch["serialkey"].ToString();
                                models = models.Where(m => m.serialnumber.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["channelkey"].ToString() != "")
                            {
                                string keywords = sSearch["channelkey"].ToString();
                                models = models.Where(m => m.buyingchannel.ToUpper().Contains(keywords.ToUpper()));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //contactSubject
                case "contactSubject":
                    if (model != null)
                    {
                        var data = model.Repository<contactSubject>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //contactCountry
                case "contactCountry":
                    if (model != null)
                    {
                        var data = model.Repository<contactCountry>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //warrantyModel
                case "warrantyModel":
                    if (model != null)
                    {
                        var data = model.Repository<warrantyModel>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //service
                case "service":
                    if (model != null)
                    {
                        var data = model.Repository<service>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //contactinfo
                case "contactinfo":
                    if (model != null)
                    {
                        var data = model.Repository<contactinfo>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //newscatagory
                case "news_catalog":
                    if (model != null)
                    {
                        var data = model.Repository<news_catalog>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //news
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //dealercatagory
                case "dealer_catalog":
                    if (model != null)
                    {
                        var data = model.Repository<dealer_catalog>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //dealer
                case "dealer":
                    if (model != null)
                    {
                        var data = model.Repository<dealer>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品BANNER
                case "product_banner":
                    if (model != null)
                    {
                        var data = model.Repository<product_banner>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //
                case "product_category":
                    if (model != null)
                    {
                        var data = model.Repository<product_category>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                //models = models.Where(m => m.title.Contains(keywords));
                                models = models.Where(m => m.title.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //商品分類
                case "product_subclass":
                    if (model != null)
                    {
                        var data = model.Repository<product_subclass>();
                        var dataCategory = model.Repository<product_category>();
                        IQueryable<product_subclass> models;
                        List<product_subclass> subClassList = new List<product_subclass>();

                        if (PrevGuid != null)
                        {
                            models = data.ReadsWhere(m => m.status != "D" && m.lang == currentUserLang && m.category == PrevGuid.ToString());
                        }
                        else
                        {
                            //models = data.ReadsWhere(m => m.status != "D" && m.lang == defLang && m.category == "0");

                            var allCategory = dataCategory.ReadsWhere(m => m.status != "D" && m.lang == currentUserLang);
                            foreach (var itemFirst in allCategory)
                            {
                                var tempFirt = data.ReadsWhere(m => m.status != "D" && m.lang == currentUserLang && m.productsCategory_guid == itemFirst.guid && m.level == 1);
                                foreach (var firstData in tempFirt)
                                {
                                    //subClassList.Add(firstData);
                                    var tempSecond = (from a in DB.product_subclass
                                                      where a.level == 2 && a.lang == currentUserLang && a.status != "D" && a.category == firstData.guid
                                                      select a).ToList();
                                    foreach (var secondData in tempSecond)
                                    {
                                        subClassList.Add(secondData);
                                        var tempThird = (from a in DB.product_subclass
                                                         where a.level == 3 && a.lang == currentUserLang && a.status != "D" && a.category == secondData.guid
                                                         select a).ToList();

                                        foreach (var thirdData in tempThird)
                                        {
                                            var templevel2data = (from a in DB.product_subclass
                                                                  where a.guid == thirdData.category && a.lang == currentUserLang && a.status != "D"
                                                                  select a.category).FirstOrDefault();
                                            thirdData.category = templevel2data;
                                            subClassList.Add(thirdData);
                                            var tempFour = (from a in DB.product_subclass
                                                            where a.level == 4 && a.lang == currentUserLang && a.status != "D" && a.category == thirdData.guid
                                                            select a).ToList();
                                            foreach (var fourData in tempFour)
                                            {
                                                var templevel3data = (from a in DB.product_subclass
                                                                      where a.guid == fourData.category && a.lang == currentUserLang && a.status != "D"
                                                                      select a.category).FirstOrDefault();
                                                fourData.category = templevel3data;
                                                subClassList.Add(fourData);
                                            }
                                        }
                                    }
                                }
                            }
                            //var tempFirt = data.ReadsWhere(m => m.status != "D" && m.lang == currentUserLang && m.level == 1);
                            //foreach (var firstData in tempFirt)
                            //{
                            //    subClassList.Add(firstData);
                            //    var tempSecond = (from a in DB.product_subclass
                            //                      where a.level == 2 && a.lang == currentUserLang && a.status != "D" && a.category == firstData.guid
                            //                      select a).ToList();
                            //    foreach (var secondData in tempSecond)
                            //    {
                            //        subClassList.Add(secondData);
                            //        var tempThird = (from a in DB.product_subclass
                            //                         where a.level == 3 && a.lang == currentUserLang && a.status != "D" && a.category == secondData.guid
                            //                         select a).ToList();

                            //        foreach (var thirdData in tempThird)
                            //        {
                            //            subClassList.Add(thirdData);
                            //            var tempFour = (from a in DB.product_subclass
                            //                            where a.level == 4 && a.lang == currentUserLang && a.status != "D" && a.category == thirdData.guid
                            //                            select a).ToList();
                            //            foreach (var fourData in tempFour)
                            //            {
                            //                subClassList.Add(fourData);
                            //            }
                            //        }
                            //    }
                            //}

                            models = data.ReadsWhere(m => m.status != "D" && m.lang == currentUserLang).OrderByDescending(m => m.productsCategory_guid).ThenBy(m => m.category);
                        }

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["category"].ToString() != "" && sSearch["category"].ToString() != "undefined")
                            {
                                string category = sSearch["category"].ToString();
                                subClassList = (from a in subClassList
                                                where a.productsCategory_guid == category
                                                select a).ToList();
                                //models = models.Where(m => m.productsCategory_guid == category);
                            }

                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                //subClassList = (from a in subClassList
                                //                where a.reTitle.Contains(keywords)
                                //                select a).ToList();
                                //subClassList = subClassList.Where(m => m.title.Contains(keywords)).ToList();
                                subClassList = subClassList.Where(m => m.title.ToUpper().Contains(keywords.ToUpper())).ToList();
                                //models = models.Where(m => m.title.Contains(keywords));
                            }
                        }
                        iTotalDisplayRecords = subClassList.ToList().Count;
                        //listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                        listData = subClassList.OrderBy(orderBy).Skip(skip).Take(take);
                    }
                    break;
                //商品內容
                case "product":
                    if (model != null)
                    {
                        var data = model.Repository<product>();
                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                //models = models.Where(m => m.subtitle.Contains(keywords));
                                models = models.Where(m => m.subtitle.ToUpper().Contains(keywords.ToUpper()));
                            }
                            //if (sSearch["status"].ToString() != "")
                            //{
                            //    string status = sSearch["status"].ToString();
                            //    models = models.Where(m => m.status == status);
                            //}
                            //if (sSearch["category"].ToString() != "")
                            //{
                            //    string category = sSearch["category"].ToString();
                            //    models = models.Where(m => m.productsCategory_guid.Contains(category));
                            //}
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();
                        List<user> usersList = new List<user>();

                        var activeRole = (from a in DB.roles
                                          where a.status == "Y"
                                          select a).ToList();
                        foreach (var item in activeRole)
                        {
                            var tempUser = (from a in DB.user
                                            where a.username != "sysadmin" && a.status != "D" && a.role_guid == item.guid
                                            select a).ToList();
                            foreach (var userData in tempUser)
                            {
                                usersList.Add(userData);
                            }
                        }

                        var tempUserD = (from a in DB.user
                                         where a.username != "sysadmin" && a.status != "D" && a.role_guid == ""
                                         select a).ToList();
                        foreach (var userData in tempUserD)
                        {
                            usersList.Add(userData);
                        }

                        //var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                //models = models.Where(m => m.role_guid.Contains(category));
                                usersList = (from a in usersList
                                             where a.role_guid.Contains(category)
                                             select a).ToList();
                            }
                        }

                        iTotalDisplayRecords = usersList.ToList().Count;
                        listData = usersList.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);
                        //var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == currentUserLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                //models = models.Where(m => m.title.Contains(keywords));
                                models = models.Where(m => m.title.ToUpper().Contains(keywords.ToUpper()));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "").Where(m => m.lang == currentUserLang);

                        //if (!string.IsNullOrEmpty(requests["search[value]"]))
                        //{
                        //    if (sSearch["keyword"].ToString() != "")
                        //    {
                        //        string keywords = sSearch["keyword"].ToString();
                        //        models = models.Where(m => m.title.Contains(keywords));
                        //    }
                        //}

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                    #endregion
            }

            list.Add("data", dataTableListData(listData, tables, urlRoot));

            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 調整DataTable資料輸出
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static List<Object> dataTableListData(dynamic list, string tables, string urlRoot)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentLang = "";

            Model DB = new Model();

            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableRow = dataTableTitle(tables);//資料欄位

            //string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            //轉型(model to Json String) 忽略循環引用
            //string json = JsonConvert.SerializeObject(list, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
            string json = JsonConvert.SerializeObject(list);

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列

            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
            }

            string currentUserLang = "";
            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }


            //取得選單一些基本資訊
            var system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();

            int i = 0;
            foreach (var dataList in Json1)
            {
                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                string thisGuid = dataList["guid"].ToString();

                rowData.Add("DT_RowId", "row_" + thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {
                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值

                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var pic = dataList[dataItem.Key.ToString()].ToString();

                                if (!string.IsNullOrEmpty(pic))
                                {
                                    if (pic.Contains("/Content"))
                                    {
                                        //readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                        readText = readText.Replace("{$value}", FunctionService.getWebUrl() + dataList[dataItem.Key.ToString()].ToString());
                                    }
                                    else
                                    {
                                        //readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                        readText = readText.Replace("{$value}", FunctionService.getWebUrl() + dataList[dataItem.Key.ToString()].ToString());
                                    }
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + "styles/images/default.jpg");
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", urlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "style=\"background-color:#333;\"");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 標題

                        case "title":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/title.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                if (tables == "product_subclass")
                                {
                                    readText = readText.Replace("{$value}", dataList["titleSpace"].ToString() + dataList["title"].ToString());
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", dataList["title"].ToString());
                                }

                                //置頂
                                string sticky = "";
                                if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                {
                                    if (dataList["sticky"].ToString() == "Y")
                                    {
                                        sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                    }
                                }
                                readText = readText.Replace("{$sticky}", sticky);

                                //今日異動
                                string modifydate = "";
                                if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                {
                                    string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                    if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                    {
                                        modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                    }
                                }
                                readText = readText.Replace("{$modifydate}", modifydate);

                                string notes = "";//

                                readText = readText.Replace("{$notes}", notes);

                                //連結
                                readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid);
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 內容摘要

                        case "info":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", dataList["title"].ToString());

                                //置頂
                                string sticky = "";
                                if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                {
                                    if (dataList["sticky"].ToString() == "Y")
                                    {
                                        sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                    }
                                }
                                readText = readText.Replace("{$sticky}", sticky);

                                //今日異動
                                string modifydate = "";
                                if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                {
                                    string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                    if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                    {
                                        modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                    }
                                }
                                readText = readText.Replace("{$modifydate}", modifydate);

                                //簡述
                                string content = "";

                                if (dataList["content"] != null && dataList["content"].ToString() != "")
                                {
                                    content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);
                                    content = Regex.Replace(content, "\r", String.Empty);
                                    content = Regex.Replace(content, "\n", String.Empty);
                                    if (content.Length > 50)
                                    {
                                        content = content.Substring(0, 50) + "...";
                                    }
                                    content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                }
                                readText = readText.Replace("{$content}", content);

                                string notes = "";//

                                readText = readText.Replace("{$notes}", notes);

                                //連結
                                readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid);
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 分類或其他上層對應

                        case "category":

                            if (context.Request.Cookies["sysLogin"] != null)
                            {
                                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                string[] tempAccount = aCookie.Value.Split('&');

                                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                            }
                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                switch (tables)
                                {
                                    //商品分類
                                    case "product_subclass":
                                        string productsID = dataList["category"].ToString();
                                        if (productsID == "0")
                                        {
                                            var t = DB.product_subclass.Where(x => x.guid == thisGuid).FirstOrDefault();
                                            var productscategory = DB.product_category.Where(x => x.guid == t.productsCategory_guid).Where(x => x.lang == currentLang).FirstOrDefault();
                                            readText = productscategory.title;
                                        }
                                        else
                                        {
                                            var products_subclass = DB.product_subclass.Where(x => x.guid == productsID).Where(x => x.lang == currentLang).FirstOrDefault();
                                            readText = products_subclass.reTitle;
                                        }
                                        break;
                                    case "news":
                                        string categoryID = dataList["category"].ToString();
                                        readText = DB.news_catalog.Where(m => m.guid == categoryID && m.lang == currentLang).First().title;
                                        break;
                                    case "dealer":
                                        string dealerCategoryID = dataList["category"].ToString();
                                        readText = DB.dealer_catalog.Where(m => m.guid == dealerCategoryID && m.lang == currentLang).First().title;
                                        break;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    var tempJudgeLang = (from a in DB.user
                                                         where a.guid == thisGuid
                                                         select a.lang).FirstOrDefault();
                                    if (tempJudgeLang == "en")
                                    {
                                        string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                        for (int s = 0; s < categoryArr.Length; s++)
                                        {
                                            string categoryID = categoryArr[s].ToString();
                                            var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                            if (category != null)
                                            {
                                                readText += category.title + "<br>";
                                            }
                                        }
                                    }
                                    else if (tempJudgeLang == "tw")
                                    {
                                        string[] categoryArr = dataList["role2_guid"].ToString().Split(',');
                                        for (int s = 0; s < categoryArr.Length; s++)
                                        {
                                            string categoryID = categoryArr[s].ToString();
                                            var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                            if (category != null)
                                            {
                                                readText += category.title + "<br>";
                                            }
                                        }
                                    }
                                    else if (tempJudgeLang == "cn")
                                    {
                                        string[] categoryArr = dataList["role3_guid"].ToString().Split(',');
                                        for (int s = 0; s < categoryArr.Length; s++)
                                        {
                                            string categoryID = categoryArr[s].ToString();
                                            var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                            if (category != null)
                                            {
                                                readText += category.title + "<br>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                        for (int s = 0; s < categoryArr.Length; s++)
                                        {
                                            string categoryID = categoryArr[s].ToString();
                                            var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                            if (category != null)
                                            {
                                                readText += category.title + "<br>";
                                            }
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory2":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role2_guid"].ToString().Split(',');
                                    for (int s = 0; s < categoryArr.Length; s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 審核者

                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";

                                if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                {
                                    string categoryID = dataList["user_guid"].ToString();
                                    var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                    if (category != null)
                                    {
                                        readText = category.name;
                                    }
                                    else
                                    {
                                        readText = "查無審核者!";
                                    }
                                }
                                else
                                {
                                    readText = "未指定";
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 瀏覽次數

                        case "view":

                            /*  if (thisGuid != null && thisGuid != "")
                              {
                                  int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                  string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                  rowData.Add(dataItem.Key.ToString(), readText);
                              }*/
                            break;

                        #endregion

                        #region 日期

                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期

                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期2

                        case "create_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 修改日期

                        case "modifydate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 修改日期2

                        case "enddate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 狀態

                        case "status":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                if (tables == "contacts" || tables == "contactinfo")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    if (context.Request.Cookies["sysLogin"] != null)
                                    {
                                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                        string[] tempAccount = aCookie.Value.Split('&');

                                        currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                                    }

                                    string style = "danger";
                                    string statusSubject = "已回覆";

                                    if (currentLang == "en")
                                    {
                                        statusSubject = "Reply";
                                    }
                                    else if (currentLang == "tw")
                                    {
                                        statusSubject = "已回覆";
                                    }
                                    else if (currentLang == "cn")
                                    {
                                        statusSubject = "已回覆";
                                    }
                                    else
                                    {
                                        statusSubject = "Reply";
                                    }

                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        if (currentLang == "en")
                                        {
                                            statusSubject = "NoReply";
                                        }
                                        else if (currentLang == "tw")
                                        {
                                            statusSubject = "未回覆";
                                        }
                                        else if (currentLang == "cn")
                                        {
                                            statusSubject = "未回覆";
                                        }
                                        else
                                        {
                                            statusSubject = "NoReply";
                                        }
                                    }

                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else if (tables == "contactinfo")
                                    {
                                        readText = readText.Replace("{$title}", dataList["contactname"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);

                                }
                                else
                                {
                                    if (context.Request.Cookies["sysLogin"] != null)
                                    {
                                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                        string[] tempAccount = aCookie.Value.Split('&');

                                        currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                                    }

                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    string style = "danger";
                                    string statusSubject = "";

                                    if (currentLang == "en")
                                    {
                                        statusSubject = "Enable";
                                    }
                                    else if (currentLang == "tw")
                                    {
                                        statusSubject = "啟用";
                                    }
                                    else if (currentLang == "cn")
                                    {
                                        statusSubject = "启用";
                                    }
                                    else
                                    {
                                        statusSubject = "Enable";
                                    }

                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        if (currentLang == "en")
                                        {
                                            statusSubject = "Disable";
                                        }
                                        else if (currentLang == "tw")
                                        {
                                            statusSubject = "停用";
                                        }
                                        else if (currentLang == "cn")
                                        {
                                            statusSubject = "停用";
                                        }
                                        else
                                        {
                                            statusSubject = "Disable";
                                        }
                                    }

                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "inquiry_form")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else if (tables == "warranty_register_detail")
                                    {
                                        var warrantguid = dataList["warrantyRegister_guid"].ToString();
                                        var temp = (from a in DB.warranty_register
                                                    where a.guid == warrantguid
                                                    select a).FirstOrDefault();
                                        readText = readText.Replace("{$title}", temp.tel.ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        #endregion

                        #region 討論區狀態

                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 回覆狀態

                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;

                        #endregion

                        #region 角色

                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();

                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 屬性

                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                if (tables == "holiday")
                                {
                                    string type = "休假";
                                    if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                    {
                                        type = "補上班";
                                    }
                                    readText = type;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 排序

                        case "sortIndex":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 成員數

                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m => m.status == "Y").Count();
                                string readText = "<div>" + data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 來源資料表名稱

                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;

                        #endregion

                        #region 動作

                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                                //是否修改
                                if (system_menu.can_edit == "Y")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }

                                //是否刪除
                                if (system_menu.can_del == "Y")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid);

                                if (system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {
                                    readText = readText.Replace("{$index_view_url}", "<a href=\"" + urlRoot + system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                //

                                switch (tables)
                                {
                                    case "products_subclass":
                                        if (dataList["level"].ToString() != "3")
                                        {
                                            readText = readText.Replace("{$nextcategory}", urlRoot + "/siteadmin/products_subclass/list/" + thisGuid);
                                        }
                                        else
                                        {
                                            readText = readText.Replace("{$nextHide}", "style=\"display:none;\"");
                                        }
                                        break;

                                    default:
                                        readText = readText.Replace("{$nextHide}", "style=\"display:none;\"");
                                        break;
                                }

                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region -- 商品專區 --

                        //所屬商品類別
                        case "productsCategory_guid":
                            if (context.Request.Cookies["sysLogin"] != null)
                            {
                                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                string[] tempAccount = aCookie.Value.Split('&');

                                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                            }

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string Products_ProductsCategoryID = dataList["productsCategory_guid"].ToString();
                                var Products_ProductsCategory = DB.product_category.Where(x => x.guid == Products_ProductsCategoryID).Where(x => x.lang == currentLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.title;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        //所屬商品分類
                        case "productsSubClass_guid":
                            if (context.Request.Cookies["sysLogin"] != null)
                            {
                                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                string[] tempAccount = aCookie.Value.Split('&');

                                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                            }

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string Products_ProductsSubClassID = dataList["productsSubClass_guid"].ToString();
                                var tempSubclassSplit = Products_ProductsSubClassID.Split(',');
                                string readText = "";
                                foreach (var item in tempSubclassSplit)
                                {
                                    var Products_ProductsSubClass = DB.product_subclass.Where(x => x.guid == item).Where(x => x.lang == currentLang).FirstOrDefault();
                                    //readText = readText + "," + Products_ProductsSubClass.reTitle;



                                    var tempPreTitle = (from a in DB.product_subclass
                                                        where a.guid == Products_ProductsSubClass.category && a.lang == currentLang
                                                        select a).FirstOrDefault();

                                    var tempPreMultiTitle = (from a in DB.product_subclass
                                                             where a.guid == tempPreTitle.category && a.lang == currentLang
                                                             select a).FirstOrDefault();
                                    if (tempPreMultiTitle != null)
                                    {
                                        readText = readText + "<div class=\"\">" + tempPreMultiTitle.reTitle + " - " + tempPreTitle.reTitle + " - " + Products_ProductsSubClass.reTitle + "</div>";
                                    }
                                    else
                                    {
                                        readText = readText + "<div class=\"\">" + tempPreTitle.reTitle + " - " + Products_ProductsSubClass.reTitle + "</div>";
                                    }



                                }
                                //readText = "<div class=\"\">" + readText.Substring(4) + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        //商品
                        case "products_guid":
                            if (context.Request.Cookies["sysLogin"] != null)
                            {
                                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                                string[] tempAccount = aCookie.Value.Split('&');

                                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                            }

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string Products_ID = dataList["groducts_guid"].ToString();
                                var Products = DB.product.Where(x => x.guid == Products_ID).Where(x => x.lang == currentLang).FirstOrDefault();
                                string readText = Products.title;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region -- 保修表單 --

                        //所屬商品類別
                        case "warrantyRegister_guid":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var aaa = thisGuid;
                                string Products_ProductsCategoryID = dataList["warrantyRegister_guid"].ToString();
                                var Products_ProductsCategory = DB.warranty_register.Where(x => x.guid == Products_ProductsCategoryID).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.tel;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "warrantyRegister_guid_g":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var Products_ProductsCategory = DB.growarranty_register_detailup.Where(x => x.guid == thisGuid).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.warrantyRegister_guid;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "warrantyRegister_guid_name":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var aaa = thisGuid;
                                string Products_ProductsCategoryID = dataList["warrantyRegister_guid"].ToString();
                                var Products_ProductsCategory = DB.warranty_register.Where(x => x.guid == Products_ProductsCategoryID).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.firstname + " " + Products_ProductsCategory.lastname;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "downloaddetail":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var Products_ProductsCategory = DB.growarranty_register_detailup.Where(x => x.guid == thisGuid).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.proofofpruchase;
                                readText = "<div class=\"text\"><a target=\"_blank\" href=\"" + readText + "\"><img src=\"/styles/images/warranty/icon-download-yellow.svg\" class=\"icon-dl\" style=\"width:30px;height:30px;\">" + "</a></div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "warrantyRegister_guid_cop":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var aaa = thisGuid;
                                string Products_ProductsCategoryID = dataList["warrantyRegister_guid"].ToString();
                                var Products_ProductsCategory = DB.warranty_register.Where(x => x.guid == Products_ProductsCategoryID).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.company;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "warrantyRegister_guid_addr":

                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                var aaa = thisGuid;
                                string Products_ProductsCategoryID = dataList["warrantyRegister_guid"].ToString();
                                var Products_ProductsCategory = DB.warranty_register.Where(x => x.guid == Products_ProductsCategoryID).Where(x => x.lang == currentUserLang).FirstOrDefault();
                                string readText = Products_ProductsCategory.address;
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;


                        #endregion

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }
                }

                re.Add(rowData);
            }

            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //特色標章
                case "feature":
                    re = featureRepository.useLang();
                    break;
                //產品標章
                case "emblem":
                    re = emblemRepository.useLang();
                    break;
                //首頁BANNER
                case "home_banner":
                    re = homeBannerRepository.useLang();
                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    re = catalogRepository.useLang();
                    break;
                //abouts
                case "abouts":
                    re = aboutsRepository.useLang();
                    break;
                //aboutsGroup
                case "group":
                    re = groupRepository.useLang();
                    break;
                //warrantyPolicy
                case "warranty_policy":
                    re = warrantyPolicyRepository.useLang();
                    break;
                //warrantyCheck
                case "warranty_register_detail":
                    re = warrantyCheckRepository.useLang();
                    break;
                //contactSubject
                case "contactSubject":
                    re = contactSubjectRepository.useLang();
                    break;
                //contactCountry
                case "contactCountry":
                    re = contactCountryRepository.useLang();
                    break;
                //warrantyModel
                case "warrantyModel":
                    re = warrantyModelRepository.useLang();
                    break;
                //service
                case "service":
                    re = serviceRepository.useLang();
                    break;
                //contactinfo
                case "contactinfo":
                    re = contactinfoRepository.useLang();
                    break;
                //newscatalog
                case "news_catalog":
                    re = newsCategoryRepository.useLang();
                    break;
                //news
                case "news":
                    re = newsRepository.useLang();
                    break;
                //dealercatalog
                case "dealer_catalog":
                    re = newsCategoryRepository.useLang();
                    break;
                //dealer
                case "dealer":
                    re = dealerRepository.useLang();
                    break;
                //產品BANNER
                case "product_banner":
                    re = productBannerRepository.useLang();
                    break;
                //商品類別
                case "product_category":
                    re = productCategoryRepository.useLang();
                    break;
                //商品分類
                case "product_subclass":
                    re = productSubclassRepository.useLang();
                    break;
                //商品內容
                case "product":
                    re = productsRepository.useLang();
                    break;
                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    re = webDataRepository.useLang();
                    break;

                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.useLang();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    re = userRepository.useLang();
                    break;
                //群組管理
                case "roles":
                    re = rolesRepository.useLang();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentLang = "";

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }

            dynamic re = null;
            switch (tables)
            {
                //特色標章
                case "feature":
                    re = featureRepository.dataTableTitle();
                    break;
                //產品標章
                case "emblem":
                    re = emblemRepository.dataTableTitle();
                    break;
                //首頁BANNER
                case "home_banner":
                    if (currentLang == "en")
                    {
                        re = homeBannerRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = homeBannerTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = homeBannerCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = homeBannerRepository.dataTableTitle();
                    }

                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    if (currentLang == "en")
                    {
                        re = catalogRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = catalogTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = catalogCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = catalogRepository.dataTableTitle();
                    }

                    break;
                //abouts
                case "abouts":
                    if (currentLang == "en")
                    {
                        re = aboutsRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = aboutsTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = aboutsCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = aboutsRepository.dataTableTitle();
                    }

                    break;
                //aboutsGroup
                case "group":
                    if (currentLang == "en")
                    {
                        re = groupRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = groupTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = groupCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = groupRepository.dataTableTitle();
                    }

                    break;
                //warrantyPolicy
                case "warranty_policy":
                    if (currentLang == "en")
                    {
                        re = warrantyPolicyRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyPolicyTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyPolicyCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = warrantyPolicyRepository.dataTableTitle();
                    }

                    break;
                //warrantyCheck
                case "warranty_register_detail":
                    if (currentLang == "en")
                    {
                        re = warrantyCheckRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyCheckTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyCheckCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = warrantyCheckRepository.dataTableTitle();
                    }

                    break;
                //contactSubject
                case "contactSubject":
                    if (currentLang == "en")
                    {
                        re = contactSubjectRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactSubjectTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactSubjectCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = contactSubjectRepository.dataTableTitle();
                    }

                    break;
                //contactCountry
                case "contactCountry":
                    if (currentLang == "en")
                    {
                        re = contactCountryRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactCountryTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactCountryCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = contactCountryRepository.dataTableTitle();
                    }

                    break;
                //warrantyModel
                case "warrantyModel":
                    if (currentLang == "en")
                    {
                        re = warrantyModelRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyModelTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyModelCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = warrantyModelRepository.dataTableTitle();
                    }

                    break;
                //service
                case "service":
                    if (currentLang == "en")
                    {
                        re = serviceRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = serviceTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = serviceCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = serviceRepository.dataTableTitle();
                    }

                    break;
                //contactinfo
                case "contactinfo":
                    if (currentLang == "en")
                    {
                        re = contactinfoRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactinfoTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactinfoCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = contactinfoRepository.dataTableTitle();
                    }

                    break;
                //newscatalog
                case "news_catalog":
                    if (currentLang == "en")
                    {
                        re = newsCategoryRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = newsCategoryTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = newsCategoryCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = newsCategoryRepository.dataTableTitle();
                    }

                    break;
                //news
                case "news":
                    if (currentLang == "en")
                    {
                        re = newsRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = newsTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = newsCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = newsRepository.dataTableTitle();
                    }

                    break;
                //dealercatalog
                case "dealer_catalog":
                    if (currentLang == "en")
                    {
                        re = dealerCategoryRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = dealerCategoryTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = dealerCategoryCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = dealerCategoryRepository.dataTableTitle();
                    }

                    break;
                //dealer
                case "dealer":
                    if (currentLang == "en")
                    {
                        re = dealerRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = dealerTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = dealerCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = dealerRepository.dataTableTitle();
                    }

                    break;
                //產品BANNER
                case "product_banner":
                    if (currentLang == "en")
                    {
                        re = productBannerRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productBannerTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productBannerCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = productBannerRepository.dataTableTitle();
                    }

                    break;
                //商品類別
                case "product_category":
                    if (currentLang == "en")
                    {
                        re = productCategoryRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productCategoryTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productCategoryCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = productCategoryRepository.dataTableTitle();
                    }

                    break;
                //商品分類
                case "product_subclass":
                    if (currentLang == "en")
                    {
                        re = productSubclassRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productSubclassTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productSubclassCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = productSubclassRepository.dataTableTitle();
                    }

                    break;
                //商品內容
                case "product":
                    if (currentLang == "en")
                    {
                        re = productsRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productsTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productsCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = productsRepository.dataTableTitle();
                    }

                    break;
                #region -- 控制台 --

                //資源回收桶
                case "ashcan":
                    if (currentLang == "en")
                    {
                        re = ashcanRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = ashcanTwRepository.dataTableTitle();
                    }
                    else
                    {
                        re = ashcanRepository.dataTableTitle();
                    }

                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":

                    if (currentLang == "en")
                    {
                        re = userRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = userTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = userCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = userTwRepository.dataTableTitle();
                    }

                    break;

                //群組管理
                case "roles":

                    if (currentLang == "en")
                    {
                        re = rolesRepository.dataTableTitle();
                    }
                    else if (currentLang == "tw")
                    {
                        re = rolesTwRepository.dataTableTitle();
                    }
                    else if (currentLang == "cn")
                    {
                        re = rolesCnRepository.dataTableTitle();
                    }
                    else
                    {
                        re = rolesRepository.dataTableTitle();
                    }

                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentLang = "";
            dynamic re = null;

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }

            switch (tables)
            {
                //特色標章
                case "feature":
                    re = featureRepository.colFrom();
                    break;
                //產品標章
                case "emblem":
                    re = emblemRepository.colFrom();
                    break;
                //首頁BANNER
                case "home_banner":
                    if (currentLang == "en")
                    {
                        re = homeBannerRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = homeBannerTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = homeBannerCnRepository.colFrom();
                    }
                    else
                    {
                        re = homeBannerRepository.colFrom();
                    }

                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    if (currentLang == "en")
                    {
                        re = catalogRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = catalogTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = catalogCnRepository.colFrom();
                    }
                    else
                    {
                        re = catalogRepository.colFrom();
                    }

                    break;
                //abouts
                case "abouts":
                    if (currentLang == "en")
                    {
                        re = aboutsRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = aboutsTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = aboutsCnRepository.colFrom();
                    }
                    else
                    {
                        re = aboutsRepository.colFrom();
                    }

                    break;
                //aboutsGroup
                case "group":
                    if (currentLang == "en")
                    {
                        re = groupRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = groupTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = groupCnRepository.colFrom();
                    }
                    else
                    {
                        re = groupRepository.colFrom();
                    }

                    break;
                //warrantyPolicy
                case "warranty_policy":
                    if (currentLang == "en")
                    {
                        re = warrantyPolicyRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyPolicyTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyPolicyCnRepository.colFrom();
                    }
                    else
                    {
                        re = warrantyPolicyRepository.colFrom();
                    }

                    break;
                //warrantyCheck
                case "warranty_register_detail":
                    if (currentLang == "en")
                    {
                        re = warrantyCheckRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyCheckTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyCheckCnRepository.colFrom();
                    }
                    else
                    {
                        re = warrantyCheckRepository.colFrom();
                    }

                    break;
                //contactSubject
                case "contactSubject":
                    if (currentLang == "en")
                    {
                        re = contactSubjectRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactSubjectTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactSubjectCnRepository.colFrom();
                    }
                    else
                    {
                        re = contactSubjectRepository.colFrom();
                    }

                    break;
                //contactCountry
                case "contactCountry":
                    if (currentLang == "en")
                    {
                        re = contactCountryRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactCountryTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactCountryCnRepository.colFrom();
                    }
                    else
                    {
                        re = contactCountryRepository.colFrom();
                    }

                    break;
                //warrantyModel
                case "warrantyModel":
                    if (currentLang == "en")
                    {
                        re = warrantyModelRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = warrantyModelTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = warrantyModelCnRepository.colFrom();
                    }
                    else
                    {
                        re = warrantyModelRepository.colFrom();
                    }

                    break;
                //service
                case "service":
                    if (currentLang == "en")
                    {
                        re = serviceRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = serviceTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = serviceCnRepository.colFrom();
                    }
                    else
                    {
                        re = serviceRepository.colFrom();
                    }

                    break;
                //contactinfo
                case "contactinfo":
                    if (currentLang == "en")
                    {
                        re = contactinfoRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = contactinfoTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = contactinfoCnRepository.colFrom();
                    }
                    else
                    {
                        re = contactinfoRepository.colFrom();
                    }

                    break;
                //newscatalog
                case "news_catalog":
                    if (currentLang == "en")
                    {
                        re = newsCategoryRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = newsCategoryTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = newsCategoryCnRepository.colFrom();
                    }
                    else
                    {
                        re = newsCategoryRepository.colFrom();
                    }

                    break;
                //news
                case "news":
                    if (currentLang == "en")
                    {
                        re = newsRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = newsTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = newsTwRepository.colFrom();
                    }
                    else
                    {
                        re = newsRepository.colFrom();
                    }

                    break;
                //dealercatalog
                case "dealer_catalog":
                    if (currentLang == "en")
                    {
                        re = dealerCategoryRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = dealerCategoryTwRepository.colFrom();
                    }
                    else
                    {
                        re = dealerCategoryRepository.colFrom();
                    }

                    break;
                //dealer
                case "dealer":
                    if (currentLang == "en")
                    {
                        re = dealerRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = dealerTwRepository.colFrom();
                    }
                    else
                    {
                        re = dealerRepository.colFrom();
                    }

                    break;
                //產品BANNER
                case "product_banner":
                    if (currentLang == "en")
                    {
                        re = productBannerRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productBannerTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productBannerCnRepository.colFrom();
                    }
                    else
                    {
                        re = productBannerRepository.colFrom();
                    }

                    break;
                //商品類別
                case "product_category":
                    if (currentLang == "en")
                    {
                        re = productCategoryRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productCategoryTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productCategoryCnRepository.colFrom();
                    }
                    else
                    {
                        re = productCategoryRepository.colFrom();
                    }

                    break;
                //商品分類
                case "product_subclass":
                    if (currentLang == "en")
                    {
                        re = productSubclassRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productSubclassTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productSubclassCnRepository.colFrom();
                    }
                    else
                    {
                        re = productSubclassRepository.colFrom();
                    }

                    break;
                //商品內容
                case "product":
                    if (currentLang == "en")
                    {
                        re = productsRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = productsTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = productsCnRepository.colFrom();
                    }
                    else
                    {
                        re = productsRepository.colFrom();
                    }

                    break;
                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    if (currentLang == "en")
                    {
                        re = webDataRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = webTwDataRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = webCnDataRepository.colFrom();
                    }
                    else
                    {
                        re = webDataRepository.colFrom();
                    }

                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    if (currentLang == "en")
                    {
                        re = ashcanRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = ashcanTwRepository.colFrom();
                    }
                    else
                    {
                        re = ashcanRepository.colFrom();
                    }
                    break;

                //系統參數
                case "system_data":
                    re = systemDataRepository.colFrom();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //群組管理
                case "roles":
                    if (currentLang == "en")
                    {
                        re = rolesRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = rolesTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = rolesCnRepository.colFrom();
                    }
                    else
                    {
                        re = rolesRepository.colFrom();
                    }

                    break;
                //使用者
                case "user":
                    if (currentLang == "en")
                    {
                        re = userRepository.colFrom();
                    }
                    else if (currentLang == "tw")
                    {
                        re = userTwRepository.colFrom();
                    }
                    else if (currentLang == "cn")
                    {
                        re = userCnRepository.colFrom();
                    }
                    else
                    {
                        re = userRepository.colFrom();
                    }

                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentUserLang = "";
            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }

            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();

            switch (tables)
            {
                //特色標章
                case "feature":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<feature>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                //產品標章
                case "emblem":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<emblem>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                //首頁BANNER
                case "home_banner":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<home_banner>();
                        var tempData = data.ReadsWhere(m => m.guid == guid).Where(m => m.lang == currentUserLang);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                        string convertedJson = Regex.Replace(re, @"(\d+\/\d+)""", "$1\\\"");

                        var res = Newtonsoft.Json.JsonConvert.DeserializeObject(convertedJson);
                    }
                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<catalog>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //abouts
                case "abouts":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<abouts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //aboutsGroup
                case "group":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<group>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //warrantyPolicy
                case "warranty_policy":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<warranty_policy>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //warrantyCheck
                case "warranty_register_detail":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<warranty_register_detail>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //contactSubject
                case "contactSubject":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<contactSubject>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //contactCountry
                case "contactCountry":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<contactCountry>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //warrantyModel
                case "warrantyModel":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<warrantyModel>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //service
                case "service":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<service>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //contactinfo
                case "contactinfo":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<contactinfo>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //newscatalog
                case "news_catalog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<news_catalog>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //news
                case "news":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //dealercatalog
                case "dealer_catalog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<dealer_catalog>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //dealer
                case "dealer":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<dealer>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                    break;
                //產品BANNER
                case "product_banner":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_banner>();
                        var tempData = data.ReadsWhere(m => m.guid == guid).Where(m => m.lang == currentUserLang);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                //商品類別
                case "product_category":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid).Where(m => m.lang == currentUserLang);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                //商品分類
                case "product_subclass":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_subclass>();
                        var tempData = data.ReadsWhere(m => m.guid == guid).Where(m => m.lang == currentUserLang);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                //商品內容
                case "product":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product>();
                        var tempData = data.ReadsWhere(m => m.guid == guid).Where(m => m.lang == currentUserLang);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    break;
                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //smtp
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentUserLang = "";
            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
            }

            Model DB = new Model();
            dynamic re = null;
            //string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
            string defLang = currentUserLang;

            switch (tables)
            {
                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.news_catalog.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;
                //dealer分類
                case "dealer_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.dealer_catalog.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;
                //商品類別
                case "product_category":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.product_category.Where(m => m.status == "Y" && m.lang == defLang).ToList();
                    }
                    break;
                //商品分類
                case "product_subclass":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        ProductsSubClassService ProductsSubClass = new ProductsSubClassService();
                        List<product_subclass> tmp = new List<product_subclass>();
                        if (data != null)
                        {
                            tmp = ProductsSubClass.GetProductsSubClassOrderbyLevel_All(currentUserLang);

                            string pguid = "";
                            Type t = data.GetType();
                            bool isMultiple = t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Dictionary<,>);
                            if (isMultiple == false)
                            {
                                var productsguid = tmp.Where(m => m.guid == data).SingleOrDefault();
                                pguid = productsguid.productsCategory_guid;
                            }
                            else
                            {
                                pguid = data[defLang].category;
                            }
                            tmp = tmp.Where(m => m.productsCategory_guid == pguid && m.level <= 3 && m.guid != data && m.category != data).ToList();
                        }
                        else
                        {
                            //tmp = ProductsSubClass.GetProductsSubClassOrderbyLevel_2("tw");
                            //tmp = tmp.Where(m => m.level <= 2).ToList();
                            tmp = ProductsSubClass.GetProductsSubClassOrderbyLevel_All(currentUserLang);
                        }
                        re = tmp;
                    }
                    break;

                //商品分類(商品內容)
                case "products_subclassByProduct":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        ProductsSubClassService ProductsSubClass = new ProductsSubClassService();
                        ProductsService Products = new ProductsService();
                        List<product_subclass> tmp = new List<product_subclass>();
                        if (data != null)
                        {
                            tmp = ProductsSubClass.GetProductsSubClassOrderbyLevel_All(currentUserLang);

                            string pguid = "";
                            Type t = data.GetType();
                            bool isMultiple = t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Dictionary<,>);
                            //if (isMultiple == false)
                            //{
                            //    var productsguid = Products.GetProductsByGuid("en", data);
                            //    pguid = productsguid.productsCategory_guid;
                            //    tmp = tmp.Where(m => m.productsCategory_guid == pguid).ToList();
                            //}
                        }
                        else
                        {
                            tmp = ProductsSubClass.GetProductsSubClassOrderbyLevel_All(currentUserLang);
                        }

                        tmp = tmp.Where(m => m.level >= 2).ToList();
                        foreach (var preTitle in tmp)
                        {
                            var tempPreTitle = (from a in DB.product_subclass
                                                where a.guid == preTitle.category && a.status == "Y" && a.lang == defLang
                                                select a).FirstOrDefault();

                            var tempPreMultiTitle = (from a in DB.product_subclass
                                                     where a.guid == tempPreTitle.category && a.status == "Y" && a.lang == defLang
                                                     select a).FirstOrDefault();
                            if (tempPreMultiTitle != null)
                            {
                                preTitle.reTitle = tempPreMultiTitle.reTitle + " - " + tempPreTitle.reTitle + " - " + preTitle.reTitle;
                            }
                            else
                            {
                                preTitle.reTitle = tempPreTitle.reTitle + " - " + preTitle.reTitle;
                            }
                        }
                        re = tmp;
                    }
                    break;

                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").Where(m => m.lang == "en").ToList();
                    }
                    break;

                //群組繁中
                case "rolesTw":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").Where(m => m.lang == "tw").ToList();
                    }
                    break;
                //群組簡中
                case "rolesCn":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").Where(m => m.lang == "cn").ToList();
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        re = DB.user.Where(m => m.status == "Y").ToList();
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            /*  switch (tables)
              {
              }*/

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";

            switch (tabels)
            {
                //顏色設定
                //case "products":
                //    var data = DB.products.Where(m => m.guid == forum_guid && m.color.Contains(user_guid)).Any();
                //    if (data == true)
                //    {
                //        selected = " selected";
                //    }
                //    break;
            }

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentUserLang = "";
            string revertLang = "";
            string createUser = "";
            Model DB = new Model();
            Model DB2 = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic Model2 = null;//目前Model
            dynamic Model3 = null;//目前Model
            dynamic Model4 = null;//目前Model
            dynamic FromData = null;//表單資訊
            dynamic FromData2 = null;//表單資訊
            dynamic FromData3 = null;//表單資訊
            dynamic FromData4 = null;//表單資訊
            string lang = "";
            string userlang = "";
            // string guid = "";
            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                createUser = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString());
                currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());

            }

            switch (tables)
            {
                //特色標章
                case "feature":
                    FromData = JsonConvert.DeserializeObject<feature>(form);
                    Model = model.Repository<feature>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.feature.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //產品標章
                case "emblem":
                    FromData = JsonConvert.DeserializeObject<emblem>(form);
                    Model = model.Repository<emblem>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.enblem.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //首頁BANNER
                case "home_banner":
                    FromData = JsonConvert.DeserializeObject<home_banner>(form);
                    Model = model.Repository<home_banner>();

                    var tempCreateUserHomeBanner = (from a in DB.home_banner
                                                    where a.guid == guid && a.lang == currentUserLang
                                                    select a.create_name).FirstOrDefault();
                    if (tempCreateUserHomeBanner != null)
                    {
                        FromData.create_name = tempCreateUserHomeBanner;
                    }

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp.Split(',')[0];
                        }
                    }

                    if (FromData.picmobile != null && FromData.picmobile != "")
                    {
                        var splitindex_urlpic = FromData.picmobile.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.picmobile = index_urltemp.Split(',')[0];
                        }
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.home_banner.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //產品BANNER
                case "product_banner":

                    if (FormObj != null)
                    {
                        FormObj["create_date"] = FormObj["create_date"].ToString().Split(',')[0];
                    }


                    FromData = JsonConvert.DeserializeObject<product_banner>(form);
                    Model = model.Repository<product_banner>();

                    var tempCreateUserProductBanner = (from a in DB.product_banner
                                                       where a.guid == guid && a.lang == currentUserLang
                                                       select a.create_name).FirstOrDefault();
                    if (tempCreateUserProductBanner != null)
                    {
                        FromData.create_name = tempCreateUserProductBanner;
                    }

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp.Split(',')[0];
                        }
                    }

                    if (FromData.picmobile != null && FromData.picmobile != "")
                    {
                        var splitindex_urlpic = FromData.picmobile.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.picmobile = index_urltemp.Split(',')[0];
                        }
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_banner.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    FromData = JsonConvert.DeserializeObject<catalog>(form);
                    Model = model.Repository<catalog>();

                    var tempCreateUserCatalog = (from a in DB.catalog
                                                 where a.guid == guid && a.lang == currentUserLang
                                                 select a.create_name).FirstOrDefault();
                    if (tempCreateUserCatalog != null)
                    {
                        FromData.create_name = tempCreateUserCatalog;
                    }

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp.Split(',')[0];
                        }
                    }

                    if (FromData.file != null && FromData.file != "")
                    {
                        var splitdown_contentfile = FromData.file.Split('/');
                        string down_contenttemp = "";
                        int deletedown_contentindex = 0;
                        if (FromData.lang == "tw" || FromData.lang == "en")
                        {
                            for (var i = 0; i < splitdown_contentfile.Length; i++)
                            {
                                if (splitdown_contentfile[i] == "Content")
                                {
                                    deletedown_contentindex = i;
                                }
                            }

                            for (var j = 0; j < deletedown_contentindex; j++)
                            {
                                splitdown_contentfile[j] = "";
                            }

                            for (var k = 0; k < splitdown_contentfile.Length; k++)
                            {
                                if (splitdown_contentfile[k] != "")
                                {
                                    down_contenttemp = down_contenttemp + "/" + splitdown_contentfile[k];
                                }
                            }

                            FromData.file = down_contenttemp;
                        }
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {
                                
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.catalog.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //abouts
                case "abouts":
                    FromData = JsonConvert.DeserializeObject<abouts>(form);
                    Model = model.Repository<abouts>();

                    var tempCreateUserAbouts = (from a in DB.abouts
                                                 where a.guid == guid && a.lang == currentUserLang
                                                 select a.create_name).FirstOrDefault();
                    if (tempCreateUserAbouts != null)
                    {
                        FromData.create_name = tempCreateUserAbouts;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.abouts.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //aboutsGroup
                case "group":
                    FromData = JsonConvert.DeserializeObject<group>(form);
                    Model = model.Repository<group>();

                    var tempCreateUserGroup = (from a in DB.@group
                                                where a.guid == guid && a.lang == currentUserLang
                                                select a.create_name).FirstOrDefault();
                    if (tempCreateUserGroup != null)
                    {
                        FromData.create_name = tempCreateUserGroup;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.group.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //warrantyPolicy
                case "warranty_policy":
                    FromData = JsonConvert.DeserializeObject<warranty_policy>(form);
                    Model = model.Repository<warranty_policy>();

                    var tempCreateUserWarrantyPolicy = (from a in DB.warranty_policy
                                                where a.guid == guid && a.lang == currentUserLang
                                                select a.create_name).FirstOrDefault();
                    if (tempCreateUserWarrantyPolicy != null)
                    {
                        FromData.create_name = tempCreateUserWarrantyPolicy;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.warranty_policy.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //warrantyCheck
                case "warranty_register_detail":
                    FromData = JsonConvert.DeserializeObject<warranty_register_detail>(form);
                    Model = model.Repository<warranty_register_detail>();

                    var tempCreateUserWarrantyCheck = (from a in DB.growarranty_register_detailup
                                                        where a.guid == guid && a.lang == currentUserLang
                                                        select a.create_name).FirstOrDefault();
                    if (tempCreateUserWarrantyCheck != null)
                    {
                        FromData.create_name = tempCreateUserWarrantyCheck;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.growarranty_register_detailup.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //contactSubject
                case "contactSubject":
                    FromData = JsonConvert.DeserializeObject<contactSubject>(form);
                    Model = model.Repository<contactSubject>();

                    var tempCreateUserContactSubject = (from a in DB.contactsubject
                                                        where a.guid == guid && a.lang == currentUserLang
                                                        select a.create_name).FirstOrDefault();
                    if (tempCreateUserContactSubject != null)
                    {
                        FromData.create_name = tempCreateUserContactSubject;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contactsubject.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //contactCountry
                case "contactCountry":
                    FromData = JsonConvert.DeserializeObject<contactCountry>(form);
                    Model = model.Repository<contactCountry>();

                    var tempCreateUserContactCountry = (from a in DB.contactCountry
                                                        where a.guid == guid && a.lang == currentUserLang
                                                        select a.create_name).FirstOrDefault();
                    if (tempCreateUserContactCountry != null)
                    {
                        FromData.create_name = tempCreateUserContactCountry;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contactCountry.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //warrantyModel
                case "warrantyModel":
                    FromData = JsonConvert.DeserializeObject<warrantyModel>(form);
                    Model = model.Repository<warrantyModel>();

                    var tempCreateUserWarrantyModel = (from a in DB.warrantyModel
                                                        where a.guid == guid && a.lang == currentUserLang
                                                        select a.create_name).FirstOrDefault();
                    if (tempCreateUserWarrantyModel != null)
                    {
                        FromData.create_name = tempCreateUserWarrantyModel;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.warrantyModel.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //service
                case "service":
                    FromData = JsonConvert.DeserializeObject<service>(form);
                    Model = model.Repository<service>();

                    var tempCreateUserService = (from a in DB.service
                                                where a.guid == guid && a.lang == currentUserLang
                                                select a.create_name).FirstOrDefault();
                    if (tempCreateUserService != null)
                    {
                        FromData.create_name = tempCreateUserService;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.service.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //contactinfo
                case "contactinfo":
                    FromData = JsonConvert.DeserializeObject<contactinfo>(form);
                    Model = model.Repository<contactinfo>();

                    var tempCreateUserContactinfo = (from a in DB.contactinfo
                                                where a.guid == guid && a.lang == currentUserLang
                                                select a.create_name).FirstOrDefault();
                    if (tempCreateUserContactinfo != null)
                    {
                        FromData.create_name = tempCreateUserContactinfo;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contactinfo.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //newscatalog
                case "news_catalog":
                    FromData = JsonConvert.DeserializeObject<news_catalog>(form);
                    Model = model.Repository<news_catalog>();

                    var tempCreateUserNewsCatalog = (from a in DB.news_catalog
                                                     where a.guid == guid && a.lang == currentUserLang
                                                     select a.create_name).FirstOrDefault();
                    if (tempCreateUserNewsCatalog != null)
                    {
                        FromData.create_name = tempCreateUserNewsCatalog;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_catalog.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //news
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();

                    var tempCreateUserNews = (from a in DB.news
                                                     where a.guid == guid && a.lang == currentUserLang
                                                     select a.create_name).FirstOrDefault();
                    if (tempCreateUserNews != null)
                    {
                        FromData.create_name = tempCreateUserNews;
                    }

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp.Split(',')[0];
                        }
                    }


                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //dealercatalog
                case "dealer_catalog":
                    FromData = JsonConvert.DeserializeObject<dealer_catalog>(form);
                    Model = model.Repository<dealer_catalog>();

                    var tempCreateUserDealerCatalog = (from a in DB.dealer_catalog
                                                     where a.guid == guid && a.lang == currentUserLang
                                                     select a.create_name).FirstOrDefault();
                    if (tempCreateUserDealerCatalog != null)
                    {
                        FromData.create_name = tempCreateUserDealerCatalog;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.dealer_catalog.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //dealer
                case "dealer":
                    FromData = JsonConvert.DeserializeObject<dealer>(form);
                    Model = model.Repository<dealer>();

                    var tempCreateUserDealer = (from a in DB.dealer
                                                       where a.guid == guid && a.lang == currentUserLang
                                                       select a.create_name).FirstOrDefault();
                    if (tempCreateUserDealer != null)
                    {
                        FromData.create_name = tempCreateUserDealer;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {

                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.dealer.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //商品類別
                case "product_category":
                    FromData = JsonConvert.DeserializeObject<product_category>(form);
                    Model = model.Repository<product_category>();

                    FromData3 = JsonConvert.DeserializeObject<product_subclass>(form);
                    Model3 = model.Repository<product_subclass>();
                    FromData3.guid = Guid.NewGuid().ToString();
                    FromData3.title = FromData.title;
                    FromData3.category = "0";
                    FromData3.sortIndex = 1;
                    FromData3.reTitle = FromData.title;
                    FromData3.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    FromData3.status = "Y";
                    FromData3.productsCategory_guid = guid;
                    FromData3.create_name = createUser;
                    FromData3.level = 1;

                    var tempCreateUserCategory = (from a in DB.product_category
                                                  where a.guid == guid && a.lang == currentUserLang
                                                  select a.create_name).FirstOrDefault();
                    if (tempCreateUserCategory != null)
                    {
                        FromData.create_name = tempCreateUserCategory;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {
                                FromData.status = "D";
                                FromData3.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                                FromData3.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {
                                FromData.status = "D";
                                FromData3.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                                FromData3.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                            FromData3.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                            FromData3.lang = reTemp;
                            FromData3.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                            FromData3.lang = reTemp;
                            FromData3.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //商品分類
                case "product_subclass":
                    ProductsSubClassService ProductsSubClass = new ProductsSubClassService();
                    FromData = JsonConvert.DeserializeObject<product_subclass>(form);

                    var tempCreateUserSubclass = (from a in DB.product_subclass
                                                  where a.guid == guid && a.lang == currentUserLang
                                                  select a.create_name).FirstOrDefault();
                    if (tempCreateUserSubclass != null)
                    {
                        FromData.create_name = tempCreateUserSubclass;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {
                                FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {
                                FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }

                    var GetPLevel = ProductsSubClass.GetLevel(FromData.lang, FromData.category);
                    if (GetPLevel != null)
                    {
                        FromData.level = (GetPLevel.level + 1);
                    }

                    Model = model.Repository<product_subclass>();

                    string tempSpace = "";
                    //FromData.reTitle = FromData.title;
                    FromData.title = FromData.reTitle;
                    for (int i = 0; i < (FromData.level) * 2; i++)
                    {
                        tempSpace = tempSpace + "&nbsp;";
                    }
                    FromData.titleSpace = tempSpace;
                    //FromData.title = tempSpace + FromData.reTitle;

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_subclass.Where(m => m.guid == guid & m.lang == lang).FirstOrDefault();
                    }
                    break;

                //商品內容
                case "product":
                    FromData = JsonConvert.DeserializeObject<product>(form);
                    Model = model.Repository<product>();

                    var tempCreateUserGoods = (from a in DB.product
                                               where a.guid == guid && a.lang == currentUserLang
                                               select a.create_name).FirstOrDefault();
                    if (tempCreateUserGoods != null)
                    {
                        FromData.create_name = tempCreateUserGoods;
                    }

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {
                                FromData.status = "D";
                            }
                            else
                            {
                                if (actType == "add")
                                {
                                    FromData.lang = currentUserLang;
                                }
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {
                                FromData.status = "D";
                            }
                            else
                            {
                                if (actType == "add")
                                {
                                    FromData.lang = currentUserLang;
                                }
                            }
                        }
                        else
                        {
                            FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            if (actType == "add")
                            {
                                var reTemp = (from a in DB.language
                                              where a.lang != currentUserLang
                                              select a.lang).FirstOrDefault();
                                FromData.lang = reTemp;
                                FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }

                        }
                        else if (currentUserLang == "cn")
                        {
                            if (actType == "add")
                            {
                                var reTemp = (from a in DB.language
                                              where a.lang != currentUserLang
                                              select a.lang).FirstOrDefault();
                                FromData.lang = reTemp;
                                FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }

                    if (FromData.featurespic != null && FromData.featurespic != "")
                    {
                        var emblemnumsplic = FromData.featurespic.Split(',');
                        string resultemblem = "";

                        foreach (var item in emblemnumsplic)
                        {
                            var splitindex_urlpic = item.Split('/');
                            string index_urltemp = "";
                            int deleteindex = 0;
                            if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                            {
                                for (var i = 0; i < splitindex_urlpic.Length; i++)
                                {
                                    if (splitindex_urlpic[i] == "Content")
                                    {
                                        deleteindex = i;
                                    }
                                }

                                for (var j = 0; j < deleteindex; j++)
                                {
                                    splitindex_urlpic[j] = "";
                                }

                                for (var k = 0; k < splitindex_urlpic.Length; k++)
                                {
                                    if (splitindex_urlpic[k] != "")
                                    {
                                        index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                    }
                                }

                                resultemblem = resultemblem + index_urltemp + ",";
                            }
                        }

                        FromData.featurespic = resultemblem;
                    }

                    if (FromData.emblem != null && FromData.emblem != "")
                    {
                        var emblemnumsplic = FromData.emblem.Split(',');
                        string resultemblem = "";

                        foreach (var item in emblemnumsplic)
                        {
                            var splitindex_urlpic = item.Split('/');
                            string index_urltemp = "";
                            int deleteindex = 0;
                            if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                            {
                                for (var i = 0; i < splitindex_urlpic.Length; i++)
                                {
                                    if (splitindex_urlpic[i] == "Content")
                                    {
                                        deleteindex = i;
                                    }
                                }

                                for (var j = 0; j < deleteindex; j++)
                                {
                                    splitindex_urlpic[j] = "";
                                }

                                for (var k = 0; k < splitindex_urlpic.Length; k++)
                                {
                                    if (splitindex_urlpic[k] != "")
                                    {
                                        index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                    }
                                }

                                resultemblem = resultemblem + index_urltemp + ",";
                            }
                        }

                        FromData.emblem = resultemblem;
                    }


                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var picnumsplic = FromData.pic.Split(',');
                        string resultpic = "";

                        foreach (var item in picnumsplic)
                        {
                            var splitindex_urlpic = item.Split('/');
                            string index_urltemp = "";
                            int deleteindex = 0;
                            if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                            {
                                for (var i = 0; i < splitindex_urlpic.Length; i++)
                                {
                                    if (splitindex_urlpic[i] == "Content")
                                    {
                                        deleteindex = i;
                                    }
                                }

                                for (var j = 0; j < deleteindex; j++)
                                {
                                    splitindex_urlpic[j] = "";
                                }

                                for (var k = 0; k < splitindex_urlpic.Length; k++)
                                {
                                    if (splitindex_urlpic[k] != "")
                                    {
                                        index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                    }
                                }

                                resultpic = resultpic + index_urltemp + ",";
                            }
                        }

                        FromData.pic = resultpic;
                    }

                    if (FromData.option != null && FromData.option != "")
                    {
                        var splitindex_urlpic = FromData.option.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "en" || FromData.lang == "tw" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.option = index_urltemp.Split(',')[0];
                        }
                    }


                    if (FromData.downloadfile != null && FromData.downloadfile != "")
                    {

                        var picnumsplic = FromData.downloadfile.Split(',');
                        string resultpic = "";

                        string result_alt = "";

                        foreach (var item in picnumsplic)
                        {
                            var splitindex_urlpic = item.Split('/');
                            string index_urltemp = "";
                            int deleteindex = 0;
                            if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                            {
                                for (var i = 0; i < splitindex_urlpic.Length; i++)
                                {
                                    if (splitindex_urlpic[i] == "Content")
                                    {
                                        deleteindex = i;
                                    }
                                }

                                for (var j = 0; j < deleteindex; j++)
                                {
                                    splitindex_urlpic[j] = "";
                                }

                                for (var k = 0; k < splitindex_urlpic.Length; k++)
                                {
                                    if (splitindex_urlpic[k] != "")
                                    {
                                        index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                    }
                                }

                                resultpic = resultpic + index_urltemp + ",";
                            }
                        }

                        FromData.downloadfile_alt = FormObj["downloadfile_alt_"].ToString() + ",";
                        FromData.downloadfile = resultpic;
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();

                    if (FromData.lang != currentUserLang)
                    {
                        if (currentUserLang == "tw")
                        {
                            if (FromData.lang == "cn")
                            {
                                //FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else if (currentUserLang == "cn")
                        {
                            if (FromData.lang == "tw")
                            {
                                //FromData.status = "D";
                            }
                            else
                            {
                                FromData.lang = currentUserLang;
                            }
                        }
                        else
                        {
                            //FromData.status = "D";
                        }
                    }
                    else
                    {
                        if (currentUserLang == "tw")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            //FromData.status = "D";

                        }
                        else if (currentUserLang == "cn")
                        {
                            var reTemp = (from a in DB.language
                                          where a.lang != currentUserLang
                                          select a.lang).FirstOrDefault();
                            FromData.lang = reTemp;
                            //FromData.status = "D";
                        }
                        else if (currentUserLang == "en")
                        {

                        }
                        else
                        {

                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":
                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    //FormObj["create_date"] = FormObj["create_date"].ToString().Split(',')[0];
                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();
                    string tempguid = FromData.guid;
                    var tempid = (from a in DB.user
                                  where a.guid == tempguid
                                  select a).FirstOrDefault();

                    if (tempid != null)
                    {
                        userlang = tempid.lang;
                        FromData.id = tempid.id;
                    }

                    FromData.lang = currentUserLang;

                    var tempCreateUser = (from a in DB.user
                                          where a.guid == guid && a.lang == currentUserLang
                                          select a.create_name).FirstOrDefault();
                    if (tempCreateUser != null)
                    {
                        FromData.create_name = tempCreateUser;
                    }

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //群組管理
                case "roles":
                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();

                    var tempCreateUserRole = (from a in DB.roles
                                              where a.guid == guid && a.lang == currentUserLang
                                              select a.create_name).FirstOrDefault();
                    if (tempCreateUserRole != null)
                    {
                        FromData.create_name = tempCreateUserRole;
                    }

                    string tempguidRole = FromData.guid;
                    int tempidRole = (from a in DB.roles
                                      where a.guid == tempguidRole
                                      select a.id).FirstOrDefault();
                    FromData.id = tempidRole;
                    FromData.lang = currentUserLang;

                    Model3 = model.Repository<user>();
                    Model3 = (from a in DB2.user
                              where a.role_guid == guid
                              select a).ToList();

                    foreach (var item in Model3)
                    {
                        //item.role_guid = "";
                        //DB2.SaveChanges();
                    }


                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                    #endregion
            }
            FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (actType == "add")
            {
                if (tables == "user")
                {
                    var reTemp = (from a in DB.language
                                  where a.lang != currentUserLang
                                  select a.lang).ToList();
                    foreach (var useritem in reTemp)
                    {
                        FromData4 = JsonConvert.DeserializeObject<user>(form);
                        Model4 = model.Repository<user>();
                        FromData4.guid = Guid.NewGuid().ToString();
                        FromData4.username = FromData.username;
                        FromData4.password = FromData.password;
                        FromData4.name = FromData.name;
                        FromData4.email = FromData.email;
                        FromData4.phone = FromData.phone;
                        FromData4.mobile = FromData.mobile;
                        FromData4.address = FromData.address;
                        FromData4.status = FromData.status;
                        FromData4.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        FromData4.lang = useritem;
                        FromData4.role_guid = FromData.role_guid;
                        FromData4.role2_guid = FromData.role2_guid;
                        FromData4.role3_guid = FromData.role3_guid;
                        FromData4.create_name = createUser;
                        Model4.Create(FromData4);
                        Model4.SaveChanges();
                    }
                }

                FromData.create_name = createUser;
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //FromData.role2_guid = "";

                if (FromData.lang == currentUserLang)
                {
                    if (tables == "product")
                    {
                        string tempsamelang = FromData.lang;
                        string tempsamesubtitle = FromData.subtitle;
                        var jsamesubtitle = (from a in DB.product
                                             where a.subtitle == tempsamesubtitle && a.lang == tempsamelang
                                             select a).ToList();
                        if (jsamesubtitle.Count() == 0)
                        {
                            Model.Create(FromData);
                            Model.SaveChanges();
                        }
                    }
                    else
                    {
                        Model.Create(FromData);
                        Model.SaveChanges();
                    }
                }
                else
                {
                    if (tables == "user")
                    {
                        Model.Create(FromData);
                        Model.SaveChanges();
                    }
                }

                if (tables == "product" && FromData.lang == currentUserLang)
                {
                    string currentLang = FromData.lang;
                    if (FromData.specColumn != null)
                    {
                        //spen_detail
                        var delDefaultProductSpecList = (from a in DB.speccolumnvalue_detail
                                                         where a.product_id == guid && a.lang == currentLang
                                                         select a).ToList();
                        foreach (var item in delDefaultProductSpecList)
                        {
                            DB.speccolumnvalue_detail.Remove(item);
                            DB.SaveChanges();
                        }

                        string[] stringSeparators = new string[] { "\r\n" };
                        var tempSpecColumn = FromData.specColumn.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        //var tempSpecValue = FromData.specValue.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        for (var i = 0; i < tempSpecColumn.Length; i++)
                        {
                            string[] stringSpecSeparators = new string[] { "||" };
                            var tempSpecColumnValue = tempSpecColumn[i].Split(stringSpecSeparators, StringSplitOptions.RemoveEmptyEntries);
                            FromData2 = JsonConvert.DeserializeObject<speccolumnvalue_detail>(form);
                            Model2 = model.Repository<speccolumnvalue_detail>();
                            FromData2.guid = Guid.NewGuid().ToString();
                            FromData2.product_id = guid;
                            FromData2.column = tempSpecColumnValue[0];
                            FromData2.value = tempSpecColumnValue[1];
                            FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            FromData2.lang = FromData.lang;
                            Model2.Create(FromData2);
                            Model2.SaveChanges();
                        }
                    }
                    //emblem_detail
                    var delDefaultProductEmblemList = (from a in DB.emblem_detail
                                                       where a.product_id == guid && a.lang == currentLang
                                                       select a).ToList();
                    foreach (var item in delDefaultProductEmblemList)
                    {
                        DB.emblem_detail.Remove(item);
                        DB.SaveChanges();
                    }

                    var splitEmblem = FromData.emblem.Split(',');

                    foreach (var item in splitEmblem)
                    {
                        if (item != "")
                        {
                            FromData2 = JsonConvert.DeserializeObject<emblem_detail>(form);
                            Model2 = model.Repository<emblem_detail>();
                            FromData2.guid = Guid.NewGuid().ToString();
                            FromData2.product_id = guid;
                            FromData2.picUrl = item;
                            FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            FromData2.lang = FromData.lang;
                            Model2.Create(FromData2);
                            Model2.SaveChanges();
                        }
                    }
                    //featurepic_detail
                    var delDefaultProductFeaturepicList = (from a in DB.featurepic_detail
                                                           where a.product_id == guid && a.lang == currentLang
                                                           select a).ToList();
                    foreach (var itemF in delDefaultProductFeaturepicList)
                    {
                        DB.featurepic_detail.Remove(itemF);
                        DB.SaveChanges();
                    }

                    var splitFeaturePic = FromData.featurespic.Split(',');

                    foreach (var itemFpic in splitFeaturePic)
                    {
                        if (itemFpic != "")
                        {
                            FromData2 = JsonConvert.DeserializeObject<featurepic_detail>(form);
                            Model2 = model.Repository<featurepic_detail>();
                            FromData2.guid = Guid.NewGuid().ToString();
                            FromData2.product_id = guid;
                            FromData2.picUrl = itemFpic;
                            FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            FromData2.lang = FromData.lang;
                            Model2.Create(FromData2);
                            Model2.SaveChanges();
                        }
                    }
                }

                if (tables == "product_category")
                {
                    Model3.Create(FromData3);
                    Model3.SaveChanges();
                }
            }
            else
            {
                if (tables != "web_data" && tables != "smtp_data")
                {
                    FromData.modify_name = createUser;
                }

                if (Field == "")
                {
                    if (tables == "user")
                    {
                        //FromData = JsonConvert.DeserializeObject<user>(form);
                        //Model = model.Repository<user>();
                        //Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();

                        //if (FromData.password != null && FromData.password != "")
                        //{
                        //    Model.password = FunctionService.md5(Model.password);
                        //}
                        ////無輸入密碼進入修改情況下
                        //if (FromData.password == "" && FromData.guid != "")
                        //{
                        //    Model.password = FormObj["defPassword"].ToString();
                        //}

                        //DB.SaveChanges();



                        if (userlang == "en")
                        {
                            if (FromData.role_guid == "" || FromData.role_guid == null)
                            {
                                //var tempUserModifyData = (from a in DB.user
                                //                          where a.guid == guid
                                //                          select a).FirstOrDefault();

                                //FromData.role_guid = tempUserModifyData.role_guid;
                            }
                            else
                            {

                            }

                            if (FromData.role2_guid == "" || FromData.role2_guid == null)
                            {

                            }
                            else
                            {
                                //var tempUserModifyData = (from a in DB.user
                                //                          where a.guid == guid
                                //                          select a).FirstOrDefault();

                                //var tempUserModifyTwData = (from a in DB.user
                                //                            where a.username == tempUserModifyData.username && a.lang == "tw"
                                //                            select a).FirstOrDefault();
                                //tempUserModifyTwData.role_guid = FromData.role2_guid;
                                //DB.Entry(tempUserModifyTwData).State = EntityState.Modified;
                                //DB.SaveChanges();
                            }

                            var tempUserModifyData = (from a in DB.user
                                                      where a.guid == guid
                                                      select a).FirstOrDefault();

                            var tempUserModifyTwData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "tw"
                                                        select a).FirstOrDefault();
                            tempUserModifyTwData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyTwData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role2_guid = null;
                            }
                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyTwData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyTwData).State = EntityState.Modified;
                            DB.SaveChanges();

                            var tempUserModifyCnData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "cn"
                                                        select a).FirstOrDefault();
                            tempUserModifyCnData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyCnData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyCnData.role2_guid = null;
                            }

                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyCnData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyCnData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyCnData).State = EntityState.Modified;
                            DB.SaveChanges();


                            var userEntry = (from a in DB.user
                                             where a.guid == guid
                                             select a).FirstOrDefault();

                            userEntry.id = FromData.id;
                            userEntry.username = FromData.username;
                            userEntry.password = FromData.password;
                            userEntry.role_guid = FromData.role_guid;
                            userEntry.role2_guid = FromData.role2_guid;
                            userEntry.name = FromData.name;
                            userEntry.email = FromData.email;
                            userEntry.phone = FromData.phone;
                            userEntry.mobile = FromData.mobile;
                            userEntry.address = FromData.address;
                            userEntry.status = FromData.status;
                            userEntry.modifydate = FromData.modifydate;
                            userEntry.modify_name = FromData.modify_name;
                            userEntry.lang = userlang;

                            DB.Entry(userEntry).State = EntityState.Modified;
                            DB.SaveChanges();
                        }
                        else if (userlang == "tw")
                        {
                            if (FromData.role_guid == "" || FromData.role_guid == null)
                            {
                                //var tempUserModifyData = (from a in DB.user
                                //                          where a.guid == guid
                                //                          select a).FirstOrDefault();

                                //FromData.role_guid = tempUserModifyData.role_guid;
                            }
                            else
                            {
                                //var tempUserModifyData = (from a in DB.user
                                //                          where a.guid == guid
                                //                          select a).FirstOrDefault();

                                //var tempUserModifyTwData = (from a in DB.user
                                //                            where a.username == tempUserModifyData.username && a.lang == "en"
                                //                            select a).FirstOrDefault();
                                //tempUserModifyTwData.role_guid = FromData.role_guid;
                                //DB.Entry(tempUserModifyTwData).State = EntityState.Modified;
                                //DB.SaveChanges();
                            }

                            if (FromData.role2_guid == "" || FromData.role2_guid == null)
                            {

                            }
                            else
                            {
                                //FromData.role_guid = FromData.role2_guid;
                            }

                            var tempUserModifyData = (from a in DB.user
                                                      where a.guid == guid
                                                      select a).FirstOrDefault();

                            var tempUserModifyTwData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "en"
                                                        select a).FirstOrDefault();
                            tempUserModifyTwData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyTwData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role3_guid = null;
                            }

                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyTwData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyTwData).State = EntityState.Modified;
                            DB.SaveChanges();

                            var tempUserModifyCnData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "cn"
                                                        select a).FirstOrDefault();
                            tempUserModifyCnData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyCnData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyCnData.role3_guid = null;
                            }

                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyCnData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyCnData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyCnData).State = EntityState.Modified;
                            DB.SaveChanges();


                            var userEntry = (from a in DB.user
                                             where a.guid == guid
                                             select a).FirstOrDefault();

                            userEntry.id = FromData.id;
                            userEntry.username = FromData.username;
                            userEntry.password = FromData.password;
                            userEntry.role_guid = FromData.role_guid;
                            userEntry.role2_guid = FromData.role2_guid;
                            userEntry.name = FromData.name;
                            userEntry.email = FromData.email;
                            userEntry.phone = FromData.phone;
                            userEntry.mobile = FromData.mobile;
                            userEntry.address = FromData.address;
                            userEntry.status = FromData.status;
                            userEntry.modifydate = FromData.modifydate;
                            userEntry.modify_name = FromData.modify_name;
                            userEntry.lang = userlang;

                            DB.Entry(userEntry).State = EntityState.Modified;
                            DB.SaveChanges();
                        }
                        else if (userlang == "cn")
                        {
                            var tempUserModifyData = (from a in DB.user
                                                      where a.guid == guid
                                                      select a).FirstOrDefault();

                            var tempUserModifyEnData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "en"
                                                        select a).FirstOrDefault();
                            tempUserModifyEnData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyEnData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyEnData.role3_guid = null;
                            }

                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyEnData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyEnData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyEnData).State = EntityState.Modified;
                            DB.SaveChanges();

                            var tempUserModifyTwData = (from a in DB.user
                                                        where a.username == tempUserModifyData.username && a.lang == "tw"
                                                        select a).FirstOrDefault();
                            tempUserModifyTwData.role_guid = FromData.role_guid;
                            if (FromData.role2_guid != "")
                            {
                                tempUserModifyTwData.role2_guid = FromData.role2_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role3_guid = null;
                            }

                            if (FromData.role3_guid != "")
                            {
                                tempUserModifyTwData.role3_guid = FromData.role3_guid;
                            }
                            else
                            {
                                tempUserModifyTwData.role3_guid = null;
                            }

                            DB.Entry(tempUserModifyTwData).State = EntityState.Modified;
                            DB.SaveChanges();


                            var userEntry = (from a in DB.user
                                             where a.guid == guid
                                             select a).FirstOrDefault();

                            userEntry.id = FromData.id;
                            userEntry.username = FromData.username;
                            userEntry.password = FromData.password;
                            userEntry.role_guid = FromData.role_guid;
                            userEntry.role2_guid = FromData.role2_guid;
                            userEntry.role3_guid = FromData.role3_guid;
                            userEntry.name = FromData.name;
                            userEntry.email = FromData.email;
                            userEntry.phone = FromData.phone;
                            userEntry.mobile = FromData.mobile;
                            userEntry.address = FromData.address;
                            userEntry.status = FromData.status;
                            userEntry.modifydate = FromData.modifydate;
                            userEntry.modify_name = FromData.modify_name;
                            userEntry.lang = userlang;

                            DB.Entry(userEntry).State = EntityState.Modified;
                            DB.SaveChanges();
                        }
                        else
                        {

                        }

                        //FromData.lang = userlang;
                        //Model.Update(FromData);
                        //Model.SaveChanges();
                    }
                    else
                    {
                        if (tables == "roles")
                        {
                            Model.Update(FromData);
                            Model.SaveChanges();
                        }
                        else if (tables == "smtp_data")
                        {
                            var temppic = (from a in DB.smtp_data
                                           where a.guid == "test"
                                           select a).FirstOrDefault();

                            temppic.host = FromData.host;
                            temppic.port = FromData.port;
                            temppic.smtp_auth = FromData.smtp_auth;
                            temppic.username = FromData.username;
                            temppic.password = FromData.password;
                            temppic.from_email = FromData.from_email;

                            DB.Entry(temppic).State = EntityState.Modified;
                            DB.SaveChanges();
                        }
                        else
                        {
                            if (FromData.lang != currentUserLang)
                            {

                            }
                            else
                            {
                                if (tables == "catalog")
                                {
                                    int tempid = FromData.id;
                                    var temppic = (from a in DB.catalog
                                                   where a.id == tempid
                                                   select a).FirstOrDefault();
                                    if (FromData.pic != "")
                                    {
                                        temppic.pic = FromData.pic;
                                    }

                                    if (FromData.file != "")
                                    {
                                        temppic.file = FromData.file;
                                    }

                                    if (FromData.pic_alt != "")
                                    {
                                        temppic.pic_alt = FromData.pic_alt;
                                    }

                                    if (FromData.file_alt != "")
                                    {
                                        temppic.file_alt = FromData.file_alt;
                                    }

                                    temppic.status = FromData.status;
                                    temppic.title = FromData.title;

                                    DB.Entry(temppic).State = EntityState.Modified;
                                    DB.SaveChanges();
                                }
                                else if (tables == "product")
                                {
                                    if (currentUserLang == "cn")
                                    {
                                        string cntitle = FromData.title;
                                        string changeCntitle = cntitle.Split(',')[0];
                                        FromData.title = changeCntitle;
                                        Model.Update(FromData);
                                        Model.SaveChanges();
                                    }
                                    else
                                    {
                                        int tempid = FromData.id;
                                        var temppic = (from a in DB.product
                                                       where a.id == tempid
                                                       select a).FirstOrDefault();

                                        if (FromData.title != "")
                                        {
                                            temppic.title = FromData.title;
                                        }

                                        if (FromData.subtitle != "")
                                        {
                                            temppic.subtitle = FromData.subtitle;
                                        }

                                        if (FromData.productsSubClass_guid != "")
                                        {
                                            temppic.productsSubClass_guid = FromData.productsSubClass_guid;
                                        }

                                        if (FromData.product_description != "")
                                        {
                                            temppic.product_description = FromData.product_description;
                                        }

                                        if (FromData.product_features != "")
                                        {
                                            temppic.product_features = FromData.product_features;
                                        }

                                        if (FromData.specColumn != "")
                                        {
                                            temppic.specColumn = FromData.specColumn;
                                        }

                                        if (FromData.isComingSoon != null)
                                        {
                                            temppic.isComingSoon = FromData.isComingSoon;
                                        }

                                        //if (FromData.pic != "")
                                        //{
                                        //    temppic.pic = FromData.pic;
                                        //}

                                        //if (FromData.pic_alt != "")
                                        //{
                                        //    temppic.pic_alt = FromData.pic_alt;
                                        //}
                                        temppic.pic = FromData.pic;
                                        temppic.pic_alt = FromData.pic_alt;

                                        if (FromData.downloadfile != "")
                                        {
                                            temppic.downloadfile = FromData.downloadfile;
                                        }

                                        if (FromData.downloadfile_alt != "")
                                        {
                                            temppic.downloadfile_alt = FromData.downloadfile_alt;
                                        }

                                        //if (FromData.emblem != "")
                                        //{
                                        //    temppic.emblem = FromData.emblem;
                                        //}

                                        //if (FromData.emblem_alt != "")
                                        //{
                                        //    temppic.emblem_alt = FromData.emblem_alt;
                                        //}
                                        temppic.emblem = FromData.emblem;
                                        temppic.emblem_alt = FromData.emblem_alt;

                                        //if (FromData.featurespic != "")
                                        //{
                                        //    temppic.featurespic = FromData.featurespic;
                                        //}

                                        //if (FromData.featurespic_alt != "")
                                        //{
                                        //    temppic.featurespic_alt = FromData.featurespic_alt;
                                        //}
                                        temppic.featurespic = FromData.featurespic;
                                        temppic.featurespic_alt = FromData.featurespic_alt;

                                        //if (FromData.option != "")
                                        //{
                                        //    temppic.option = FromData.option;
                                        //}

                                        //if (FromData.option_alt != "")
                                        //{
                                        //    temppic.option_alt = FromData.option_alt;
                                        //}
                                        temppic.option = FromData.option;
                                        temppic.option_alt = FromData.option_alt;

                                        if (FromData.videoLink != "")
                                        {
                                            temppic.videoLink = FromData.videoLink;
                                        }

                                        temppic.status = FromData.status;
                                        temppic.modify_name = createUser;
                                        temppic.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                                        temppic.keywords = FromData.keywords;
                                        temppic.description = FromData.description;

                                        DB.Entry(temppic).State = EntityState.Modified;
                                        DB.SaveChanges();
                                    }
                                }
                                else
                                {
                                    Model.Update(FromData);
                                    Model.SaveChanges();
                                }


                                if (tables == "product_category")
                                {
                                    var entity = DB.product_subclass.Where(m => m.productsCategory_guid == guid).Where(m => m.lang == currentUserLang).FirstOrDefault();

                                    entity.title = FromData.title;
                                    entity.reTitle = FromData.title;
                                    entity.modify_name = createUser;

                                    DB.Entry(entity).State = EntityState.Modified;
                                    DB.SaveChanges();
                                }

                            }
                        }
                    }

                    switch (tables)
                    {
                        //case "contacts":
                        //    //回覆聯絡表單 發送給客戶與相關管理人員
                        //    if (FromData.replystatus == "Y" && FromData.status == "Y" && string.IsNullOrEmpty(FromData.replied))
                        //    {
                        //        ContactController Contact = new ContactController();
                        //        Contact.SendContactReply(guid);
                        //    }
                        //    break;
                    }

                    if (tables == "product")
                    {
                        string currentLang = FromData.lang;

                        if (FromData.lang == currentUserLang)
                        {
                            if (FromData.specColumn != null)
                            {
                                //spen_detail
                                var delDefaultProductSpecList = (from a in DB.speccolumnvalue_detail
                                                                 where a.product_id == guid && a.lang == currentLang
                                                                 select a).ToList();

                                string[] stringSeparatorsA = new string[] { "\r\n" };
                                var tempSpecColumnA = FromData.specColumn.Split(stringSeparatorsA, StringSplitOptions.RemoveEmptyEntries);
                                if (tempSpecColumnA.Length != delDefaultProductSpecList.Count())
                                {
                                    foreach (var item in delDefaultProductSpecList)
                                    {
                                        DB.speccolumnvalue_detail.Remove(item);
                                        DB.SaveChanges();
                                    }
                                    string[] stringSeparators = new string[] { "\r\n" };
                                    var tempSpecColumn = FromData.specColumn.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                                    //var tempSpecValue = FromData.specValue.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                                    for (var i = 0; i < tempSpecColumn.Length; i++)
                                    {
                                        string[] stringSpecSeparators = new string[] { "||" };
                                        var tempSpecColumnValue = tempSpecColumn[i].Split(stringSpecSeparators, StringSplitOptions.RemoveEmptyEntries);
                                        FromData2 = JsonConvert.DeserializeObject<speccolumnvalue_detail>(form);
                                        Model2 = model.Repository<speccolumnvalue_detail>();
                                        FromData2.guid = Guid.NewGuid().ToString();
                                        FromData2.product_id = guid;
                                        FromData2.column = tempSpecColumnValue[0];
                                        FromData2.value = tempSpecColumnValue[1];
                                        FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                        FromData2.lang = FromData.lang;
                                        Model2.Create(FromData2);
                                        Model2.SaveChanges();
                                    }
                                }
                            }
                        }

                        //emblem_detail
                        var delDefaultProductEmblemList = (from a in DB.emblem_detail
                                                           where a.product_id == guid && a.lang == currentLang
                                                           select a).ToList();
                        foreach (var item in delDefaultProductEmblemList)
                        {
                            DB.emblem_detail.Remove(item);
                            DB.SaveChanges();
                        }

                        var splitEmblem = FromData.emblem.Split(',');

                        foreach (var item in splitEmblem)
                        {
                            if (item != "")
                            {
                                FromData2 = JsonConvert.DeserializeObject<emblem_detail>(form);
                                Model2 = model.Repository<emblem_detail>();
                                FromData2.guid = Guid.NewGuid().ToString();
                                FromData2.product_id = guid;
                                FromData2.picUrl = item;
                                FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                FromData2.lang = FromData.lang;
                                Model2.Create(FromData2);
                                Model2.SaveChanges();
                            }
                        }
                        //featurepic_detail
                        var delDefaultProductFeaturepicList = (from a in DB.featurepic_detail
                                                               where a.product_id == guid && a.lang == currentLang
                                                               select a).ToList();
                        foreach (var itemF in delDefaultProductFeaturepicList)
                        {
                            DB.featurepic_detail.Remove(itemF);
                            DB.SaveChanges();
                        }

                        var splitFeaturePic = FromData.featurespic.Split(',');

                        foreach (var itemFpic in splitFeaturePic)
                        {
                            if (itemFpic != "")
                            {
                                FromData2 = JsonConvert.DeserializeObject<featurepic_detail>(form);
                                Model2 = model.Repository<featurepic_detail>();
                                FromData2.guid = Guid.NewGuid().ToString();
                                FromData2.product_id = guid;
                                FromData2.picUrl = itemFpic;
                                FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                FromData2.lang = FromData.lang;
                                Model2.Create(FromData2);
                                Model2.SaveChanges();
                            }
                        }
                    }
                }
                else
                {

                    if (tables == "roles")
                    {
                        //單一欄位修改
                        switch (Field)
                        {
                            case "status":
                                Model.status = FromData.status;
                                break;

                            case "sortIndex":
                                Model.sortIndex = FromData.sortIndex;
                                break;

                            case "logindate":
                                Model.logindate = FromData.logindate;
                                break;
                            case "modify":
                                Model.inquirytype = FromData.inquirytype;
                                break;
                        }

                        DB.SaveChanges();
                    }
                    else
                    {
                        if (FromData.lang != currentUserLang)
                        {

                        }
                        else
                        {
                            if (FromData.status != null)
                            {
                                if (tables == "product_category")
                                {
                                    var entity = DB.product_subclass.Where(m => m.productsCategory_guid == guid).Where(m => m.lang == currentUserLang).Where(m => m.category == "0").FirstOrDefault();

                                    entity.status = FromData.status;
                                    entity.modify_name = createUser;

                                    DB.Entry(entity).State = EntityState.Modified;
                                    DB.SaveChanges();

                                }
                            }
                            

                            //單一欄位修改
                            switch (Field)
                            {
                                case "status":
                                    Model.status = FromData.status;
                                    break;

                                case "sortIndex":
                                    Model.sortIndex = FromData.sortIndex;
                                    break;

                                case "logindate":
                                    Model.logindate = FromData.logindate;
                                    break;
                                case "modify":
                                    Model.inquirytype = FromData.inquirytype;
                                    break;
                            }

                            DB.SaveChanges();
                        }
                    }
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    //role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //role_permissions_add.guid = Guid.NewGuid();
                        //role_permissions_add.role_guid = guid;
                        //role_permissions_add.permissions_guid = key.ToString();
                        //role_permissions_add.permissions_status = permissions[key].ToString();
                        //DB.role_permissions.Add(role_permissions_add);
                        //DB.SaveChanges();
                        role_permissions role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        if (permissions[key].ToString() == null)
                        {
                            role_permissions_add.permissions_status = "F";
                        }
                        else
                        {
                            role_permissions_add.permissions_status = permissions[key].ToString();
                        }

                        DB.Entry(role_permissions_add).State = EntityState.Added;
                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            return guid;

        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            switch (tables)
            {
                //特色標章
                case "feature":
                    re.Clear();
                    re = featureRepository.defaultOrderBy();
                    break;
                //產品標章
                case "emblem":
                    re.Clear();
                    re = emblemRepository.defaultOrderBy();
                    break;
                //首頁BANNER
                case "home_banner":
                    re.Clear();
                    re = homeBannerRepository.defaultOrderBy();
                    break;
                //CATALOG DOWNLOAD
                case "catalog":
                    re.Clear();
                    re = catalogRepository.defaultOrderBy();
                    break;
                //產品BANNER
                case "product_banner":
                    re.Clear();
                    re = productBannerRepository.defaultOrderBy();
                    break;
                //商品類別
                case "product_category":
                    re.Clear();
                    re = productCategoryRepository.defaultOrderBy();
                    break;

                //商品分類
                case "product_subclass":
                    re.Clear();
                    re = productSubclassRepository.defaultOrderBy();
                    break;

                //商品內容
                case "product":
                    re.Clear();
                    re = productsRepository.defaultOrderBy();
                    break;
                //dealer
                case "dealer":
                    re.Clear();
                    re = dealerRepository.defaultOrderBy();
                    break;
                //dealercatalog
                case "dealer_catalog":
                    re.Clear();
                    re = dealerCategoryRepository.defaultOrderBy();
                    break;

                    #region -- 首頁管理 --

                    ////其他項目
                    //case "index":
                    //    re.Clear();
                    //    re = indexRepository.defaultOrderBy();
                    //    break;

                    #endregion
            }
            return re;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return re;
        }
    }
}