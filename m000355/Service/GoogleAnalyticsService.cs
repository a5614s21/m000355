﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Analytics.v3;
using System.Threading;
using System.Web.Mvc;
using Google.Apis.Services;
using System.Configuration;
using Web.Models;
using Newtonsoft.Json;
using Google.Apis.AnalyticsReporting;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using web.Models.ViewModels;
using System.Globalization;

namespace Web.Service
{
    public class GoogleAnalyticsService : Controller
    {
        private Model DB = new Model();
        public string GaProfileId = ConfigurationManager.ConnectionStrings["GaProfileId"].ConnectionString;//GaProfileId
        private ServiceAccountCredential credential = null;//驗證
        private string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
        private string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
        private string metrics = "";
        private string Dimensions = "";

        private string GaCode = "";
        private GoogleCredential googleCredential;
        private AnalyticsReportingService analyticsReporting = new AnalyticsReportingService();

        public void GetCredential()
        {
            GoogleAnalyticsService GaData = new GoogleAnalyticsService();
            credential = GaData.GoogleAnalytics();
        }


        public object TasksService { get; private set; }

        /// <summary>
        /// 取得登入憑證
        /// </summary>
        /// <returns></returns>
        public ServiceAccountCredential GoogleAnalytics()
        {
            string[] scopes = new string[] {
                AnalyticsService.Scope.Analytics,               // view and manage your Google Analytics data
                AnalyticsService.Scope.AnalyticsEdit,           // Edit and manage Google Analytics Account
                AnalyticsService.Scope.AnalyticsManageUsers,    // Edit and manage Google Analytics Users
                AnalyticsService.Scope.AnalyticsReadonly};      // View Google Analytics Data

            string serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");
            var keyFilePath = serverPath + "Content/Siteadmin/spatial-shore-356108-b3cf0de94db2.p12";
            var serviceAccountEmail = "durofix@spatial-shore-356108.iam.gserviceaccount.com";
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = scopes
            }.FromCertificate(certificate));

            //取得token
            string AuthenticationKey = "";
            if (credential.RequestAccessTokenAsync(CancellationToken.None).Result)
            {   
                AuthenticationKey = credential.Token.AccessToken;

            }
            return credential;
        }


        public dynamic GetGaData(DateRange dateRange, string dimensionStr, string metricStr)
        {
            var aaa = "http://iis.24241872.tw/projects/public/m000355/test/Content/Siteadmin/";
            var bbb = "spatial-shore-356108-c6d16255d828.json";

            var googleCredential = GoogleCredential.FromFile(Path.GetFullPath("spatial-shore-356108-c6d16255d828.json")).CreateScoped(AnalyticsReportingService.Scope.Analytics);

            var analyticsReporting = new AnalyticsReportingService(
                new BaseClientService.Initializer
                {
                    HttpClientInitializer = googleCredential,
                    ApplicationName = "GA Lab"
                });

            var dimension = new Dimension { Name = dimensionStr };

            var dimensionFilter = new DimensionFilter
            {
                DimensionName = dimensionStr,
                Expressions = new List<string> { "^/supershowwei/2016/01/26/145353$" }
            };

            var dimensionFilterClause = new DimensionFilterClause { Filters = new List<DimensionFilter> { dimensionFilter } };

            var metric = new Metric { Expression = metricStr };

            //var january = new DateRange { StartDate = "2019-01-01", EndDate = "2019-01-31" };

            var reportRequest = new ReportRequest
            {
                ViewId = "3573880306",
                Metrics = new List<Metric> { metric },
                Dimensions = new List<Dimension> { dimension },
                DimensionFilterClauses = new List<DimensionFilterClause> { dimensionFilterClause },
                DateRanges = new List<DateRange> { dateRange }
            };

            var reportGetRequest = new GetReportsRequest { ReportRequests = new List<ReportRequest> { reportRequest } };

            return analyticsReporting.Reports.BatchGet(reportGetRequest).Execute();
        }

        /// <summary>
        /// 瀏覽數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaPageCount(DateRange dateRange)
        {
            return GetGaData(dateRange, "ga:pagePath", "ga:pageviews");
        }

        /// <summary>
        /// 搜尋引擎來源數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaOrganicSearches(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:fullReferrer", "ga:organicSearches");

            List<GaModel> gaModel = new List<GaModel>();

            TextInfo textInfo = new CultureInfo("es-ES", false).TextInfo;

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add(new GaModel { source = textInfo.ToTitleCase(item.Dimensions[0]), count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 關鍵字
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaKeywords(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:keyword", "ga:organicSearches");

            List<GaModel> gaModel = new List<GaModel>();

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add(new GaModel { source = item.Dimensions[0], count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 社群網站來源數
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaSocialCount(DateRange dateRange)
        {
            return GetGaData(dateRange, "ga:socialNetwork", "ga:organicSearches");
        }

        /// <summary>
        /// 總瀏覽次數
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaTotalWebViewNum(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:year", "ga:visits");
            int num = 0;
            foreach (var item in response.Reports[0].Data.Rows)
            {
                num = num + int.Parse(item.Metrics[0].Values[0]);
            }

            return num;
        }

        /// <summary>
        /// 新/舊訪問者
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaUserType(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:userType", "ga:users");

            List<GaModel> gaModel = new List<GaModel>();

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add(new GaModel { source = item.Dimensions[0], count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 每月數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaYear(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:month", "ga:visits");

            List<GaMonthModel> gaModel = new List<GaMonthModel>();


            for (int i = 0; i < 12; i++)
            {
                gaModel.Add(new GaMonthModel { age = (i + 1) + "月", visits = "0" });
            }


            int s = 0;
            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel[s].visits = item.Metrics[0].Values[0];
                s++;

            }

            return gaModel;
        }

        /// <summary>
        /// 及時瀏覽人數
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.RealtimeData nowUser()
        {
            try
            {
                GetCredential();//設定驗證
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "RealtimeData",
                });
                string profileId = GaProfileId;
                DataResource.RealtimeResource.GetRequest request = service.Data.Realtime.Get(profileId, "rt:pageviews");

                Google.Apis.Analytics.v3.Data.RealtimeData realtimeData = request.Execute();

                return realtimeData;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// 寫入年度數量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ThisYear(ServiceAccountCredential credential)
        {

            var service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Year",
            });

            string profileId = GaProfileId;
            string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
            string metrics = "ga:visits";
            DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
            request.Dimensions = "ga:month";
            Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

            return GaData;

        }


        //線上使用者人數
        public Google.Apis.Analytics.v3.Data.RealtimeData nowUser(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API Year",
                });

                string profileId = GaProfileId;
                DataResource.RealtimeResource.GetRequest request = service.Data.Realtime.Get(profileId, "rt:pageviews");

                Google.Apis.Analytics.v3.Data.RealtimeData realtimeData = request.Execute();

                return realtimeData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// 回傳年度數量
        /// </summary>
        /// <returns></returns>
        public string ViewYear()
        {
            List<Object> dic = new List<Object>();
            //QQ 並沒有google_analytics這張表
            google_analytics GAThisYear = null;// DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();
            if (GAThisYear != null)
            {
                dynamic mJObj = null;

                if(!string.IsNullOrEmpty(GAThisYear.content.ToString()))
                {
                    mJObj = Newtonsoft.Json.Linq.JArray.Parse(GAThisYear.content.ToString());
                }
             

                for (int i = 0; i <= 11; i++)
                {
                    Dictionary<String, Object> dic2 = new Dictionary<string, object>();

                    var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());

                    dic2.Add("age", (i + 1) + "月");
                    dic2.Add("visits", subObj[1]);
                    dic.Add(dic2);
                }
            }
            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            return json;
        }

        /// <summary>
        /// 回傳年度數量(目前月份)
        /// </summary>
        /// <returns></returns>
        public string ViewYearUseMonth(string month)
        {
            List<Object> dic = new List<Object>();
            //QQ 並沒有google_analytics這張表
            google_analytics google_analytics = null;// DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();

            string visits = "0";
            if (google_analytics != null)
            {

                var mJObj = Newtonsoft.Json.Linq.JArray.Parse(google_analytics.content.ToString());

                for (int i = 0; i <= 11; i++)
                {

                    var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());
                    if ((i + 1).ToString() == month)
                    {
                        visits = subObj[1].ToString();
                    }

                }
            }
            return visits;
        }

        /// <summary>
        /// 瀏覽頁面與數量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ReGaContent(GaParameterModel data)
        {
            startDate = data.startDate;
            endDate = data.endDate;
            metrics = data.metrics;
            Dimensions = data.Dimensions;

            return GetGaContent();
        }

        //瀏覽器使用率
        public string ViewBrowser()
        {
            try
            {
                List<object> dic = new List<object>();
                google_analytics GAThisBrowser = DB.google_analytics.Where(m => m.area == "ThisBrowser").FirstOrDefault();
                if (GAThisBrowser != null && GAThisBrowser.content != "null")
                {
                    var mJObj = Newtonsoft.Json.Linq.JArray.Parse(GAThisBrowser.content.ToString());
                    var total = 0;
                    Dictionary<string, int> num = new Dictionary<string, int>();

                    for (int i = 0; i < mJObj.Count; i++)
                    {
                        var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());
                        switch (subObj[0].ToString())
                        {
                            case "Chrome":
                                num.Add("GoogleChrome", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Firefox":
                                num.Add("MozilaFirefox", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Internet Explorer":
                                num.Add("InterentExplorer", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Safari":
                                num.Add("Safari", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;
                        }
                    }

                    //百分比
                    List<string> list = new List<string>();
                    list.AddRange(num.Keys);
                    foreach (var item in list)
                    {
                        Dictionary<String, object> dic2 = new Dictionary<string, object>();
                        float n = num[item];
                        num[item] = Int32.Parse(((n / total) * 100).ToString("N0"));
                        dic2.Add(item, num[item]);
                        dic.Add(dic2);
                    }
                }

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 共用取得函式
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="metrics"></param>
        /// <param name="Dimensions"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData GetGaContent()
        {
            try
            {
                GetCredential();//設定驗證
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API",
                });

                string profileId = GaProfileId;
                DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
                request.Dimensions = Dimensions;

                //request.Metrics
                Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

                return GaData;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }

    public partial class GaParameterModel
    {
        public string startDate { get; set; }

        public string endDate { get; set; }
        public string metrics { get; set; }
        public string Dimensions { get; set; }
    }
}
