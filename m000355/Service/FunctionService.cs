﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Resources;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.IO;

namespace Web.Service
{
    public class FunctionService : Controller
    {
        // GET: FunctionService

        /// <summary>
        /// 格式化檔案更新日期
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static string FileModifyTime(string files)
        {
            string re = System.DateTime.Parse(System.IO.File.GetLastWriteTime(files).ToString()).ToString("yyyyMMddHHmmss");
            return re;
        }

        /// <summary>
        /// GUID樣式
        /// </summary>
        /// <returns></returns>
        public static string getGuid()
        {
            string GuidType = ConfigurationManager.ConnectionStrings["GuidType"].ConnectionString;

            if (GuidType == "System")
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return DateTime.Now.ToString("yyMMddHHmmssff");
            }
        }

        public static NameValueCollection getLangData(string lang)
        {
            NameValueCollection langData = new NameValueCollection();
            if (lang != "")
            {
                lang = "_" + lang;
            }
            ResourceManager rm = new ResourceManager("m000355.App_GlobalResources.Resource" + lang, Assembly.GetExecutingAssembly());
            ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                langData.Add(entry.Key.ToString(), entry.Value.ToString());
            }

            return langData;
        }

        /// <summary>
        /// 儲存LOG
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="files"></param>
        public static void saveLog(dynamic ex, string files)
        {
            try
            {
                string lines = "";
                string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/" + files;

                if (!System.IO.File.Exists(fileName))
                {
                    System.IO.File.Create(fileName);
                }

                if (System.IO.File.Exists(fileName))
                {
                    StreamReader str = new StreamReader(fileName);
                    str.ReadLine();
                    lines = str.ReadToEnd();
                    str.Close();
                }

                lines += DateTime.Now.ToString() + "：Message => " + ex.Message + "，";
                lines += $"Main exception occurs {ex}.";
                lines += "\r\n";
                System.IO.File.WriteAllText(fileName, lines);
            }
            catch
            {
            }
        }

        /// <summary>
        /// 表單JSON轉換
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static NameValueCollection reSubmitFormDataJson(string jsonText)
        {
            NameValueCollection reData = new NameValueCollection();

            dynamic dynObj = JsonConvert.DeserializeObject(jsonText);
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    if (reData[(string)item.name] == null)
                    {
                        string val = item.value;
                        if (val == null)
                        {
                            reData.Add((string)item.name, "F");
                        }
                        else
                        {
                            reData.Add((string)item.name, val);
                        }
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }
                }
            }

            return reData;
        }
        public static string getWebUrl()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string webURL = "http://" + context.Request.ServerVariables["Server_Name"];
            if (context.Request.ServerVariables["Server_Name"] == "localhost")
            {
                //webURL = webURL + $":{context.Request.Url.Port}";
                webURL = "";
            }
            else if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1)
            {
                webURL = "http://iis.24241872.tw/projects/public/m000355/test";
            }
            else
            {
                webURL = webURL + $":{context.Request.Url.Port}";
            }
            //else if (context.Request.ServerVariables["Server_Name"].IndexOf("south-china.com.tw") != -1)
            //{
            //    webURL = "http://south-china.com.tw";
            //}
            return webURL;
        }

        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string md5(string str)
        {
            string reStr = "";

            byte[] Original = Encoding.Default.GetBytes(str); //將字串來源轉為Byte[]
            MD5 s1 = MD5.Create(); //使用MD5
            byte[] Change = s1.ComputeHash(Original);//進行加密
            reStr = Convert.ToBase64String(Change);//將加密後的字串從byte[]轉回string

            return reStr;
        }

        /// <summary>
        /// 後台登入判斷
        /// </summary>
        /// <param name="sysUsername"></param>
        /// <param name="adID"></param>
        /// <returns></returns>
        public static bool systemUserCheck()
        {
            Model DB = new Model();

            bool re = false;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
            string username = "";

            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (System.Web.HttpContext.Current.Session["sysUsername"] != null && System.Web.HttpContext.Current.Session["sysUsername"].ToString() != "")
                    {
                        re = true;
                        username = System.Web.HttpContext.Current.Session["sysUsername"].ToString();
                    }
                }
                else
                {
                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        re = true;
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');
                        username = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", ""));
                    }
                }

                //更新登入日期
                var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                if (saveData != null)
                {
                    saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    DB.SaveChanges();
                }

                getAdminPermissions(username);//權限
            }

            //IP判斷
            if (re == true && username != "sysadmin")
            {
                /*
                SOGOEntities DB = new SOGOEntities();
                var ipLock = DB.sogo_ip_lock.Where(m => m.status == "Y").ToList();

                if (ipLock.Count > 0)
                {
                    string ip = GetIP();
                    var ipLock2 = DB.sogo_ip_lock.Where(m => m.status == "Y").Where(m => m.ip == ip).ToList();
                    if (ipLock2.Count > 0)
                    {
                        re = true;
                    }
                    else
                    {
                        re = false;
                    }
                }*/
            }

            return re;
        }

        /// <summary>
        /// 回傳登入者帳號
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, String> ReUserData()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie

            Dictionary<String, String> userData = new Dictionary<string, string>();

            if (adminCathType == "Session")
            {
                if (System.Web.HttpContext.Current.Session["sysUsername"] != null && System.Web.HttpContext.Current.Session["sysUsername"].ToString() != "")
                {
                    userData.Add("username", System.Web.HttpContext.Current.Session["sysUsername"].ToString());
                    userData.Add("guid", System.Web.HttpContext.Current.Session["sysUserGuid"].ToString());
                }
            }
            else
            {
                if (context.Request.Cookies["sysLogin"] != null)
                {
                    HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                    string[] tempAccount = aCookie.Value.Split('&');

                    userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                    userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));
                }
            }

            return userData;
        }

        /// <summary>
        /// 角色權限
        /// </summary>
        /// <param name="role_guid"></param>
        /// <returns></returns>
        public static NameValueCollection role_permissions(string role_guid)
        {
            Model DB = new Model();
            string site = ConfigurationManager.ConnectionStrings["site"].ConnectionString;
            var data = DB.role_permissions.Where(m => m.role_guid == role_guid).ToList();

            NameValueCollection re = new NameValueCollection();
            foreach (var item in data)
            {
                re.Add(item.permissions_guid, item.permissions_status);
            }
            return re;
        }

        /// <summary>
        /// 設定權限群組
        /// </summary>
        /// <param name="username"></param>
        public static void getAdminPermissions(string username)
        {
            Model DB = new Model();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentLang = "";

            Dictionary<String, Object> permissions = new Dictionary<String, Object>();
            List<String> permissionsTop = new List<String>();

            //非系統權限
            if (username != "sysadmin")
            {
                if (context.Request.Cookies["sysLogin"] != null)
                {
                    HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                    string[] tempAccount = aCookie.Value.Split('&');

                    if (tempAccount.Length == 2)
                    {
                        currentLang = "en";
                    }
                    else
                    {
                        currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                    }
                }

                //取得權限
                if (currentLang == "en")
                {
                    var tempUser = DB.user.Where(m => m.username == username).Where(m => m.lang == "en").Select(a => new { role_guid = a.role_guid }).FirstOrDefault();
                    if (tempUser != null)
                    {
                        string role_guid = tempUser.role_guid;
                        var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                        var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                        if (perData.Count > 0)
                        {
                            foreach (var item in perData)
                            {
                                string systemMenuGuid = item.permissions_guid;

                                var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                                if (tempData != null)
                                {
                                    permissions.Add(item.permissions_guid, item.permissions_status);
                                    if (permissionsTop.IndexOf(tempData.category) == -1)
                                    {
                                        permissionsTop.Add(tempData.category);
                                    }
                                }
                            }

                            context.Session.Remove("permissions");
                            context.Session.Remove("permissionsTop");

                            context.Session.Add("permissions", permissions);
                            context.Session.Add("permissionsTop", permissionsTop);
                        }
                    }
                }
                else if (currentLang == "cn")
                {
                    var tempUser = DB.user.Where(m => m.username == username).Where(m => m.lang == "cn").Select(a => new { role3_guid = a.role3_guid }).FirstOrDefault();
                    if (tempUser != null)
                    {
                        string role_guid = tempUser.role3_guid;
                        var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                        var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                        if (perData.Count > 0)
                        {
                            foreach (var item in perData)
                            {
                                string systemMenuGuid = item.permissions_guid;

                                var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                                if (tempData != null)
                                {
                                    permissions.Add(item.permissions_guid, item.permissions_status);
                                    if (permissionsTop.IndexOf(tempData.category) == -1)
                                    {
                                        permissionsTop.Add(tempData.category);
                                    }
                                }
                            }

                            context.Session.Remove("permissions");
                            context.Session.Remove("permissionsTop");

                            context.Session.Add("permissions", permissions);
                            context.Session.Add("permissionsTop", permissionsTop);
                        }
                    }
                }
                else if (currentLang == "tw")
                {
                    var tempUser = DB.user.Where(m => m.username == username).Where(m => m.lang == "tw").Select(a => new { role2_guid = a.role2_guid }).FirstOrDefault();
                    if (tempUser != null)
                    {
                        string role_guid = tempUser.role2_guid;
                        var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                        var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                        if (perData.Count > 0)
                        {
                            foreach (var item in perData)
                            {
                                string systemMenuGuid = item.permissions_guid;

                                var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                                if (tempData != null)
                                {
                                    permissions.Add(item.permissions_guid, item.permissions_status);
                                    if (permissionsTop.IndexOf(tempData.category) == -1)
                                    {
                                        permissionsTop.Add(tempData.category);
                                    }
                                }
                            }

                            context.Session.Remove("permissions");
                            context.Session.Remove("permissionsTop");

                            context.Session.Add("permissions", permissions);
                            context.Session.Add("permissionsTop", permissionsTop);
                        }
                    }
                }
                else
                {
                    var tempUser = DB.user.Where(m => m.username == username).Where(m => m.lang == "en").Select(a => new { role_guid = a.role_guid }).FirstOrDefault();
                    if (tempUser != null)
                    {
                        string role_guid = tempUser.role_guid;
                        var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                        var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                        if (perData.Count > 0)
                        {
                            foreach (var item in perData)
                            {
                                string systemMenuGuid = item.permissions_guid;

                                var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                                if (tempData != null)
                                {
                                    permissions.Add(item.permissions_guid, item.permissions_status);
                                    if (permissionsTop.IndexOf(tempData.category) == -1)
                                    {
                                        permissionsTop.Add(tempData.category);
                                    }
                                }
                            }

                            context.Session.Remove("permissions");
                            context.Session.Remove("permissionsTop");

                            context.Session.Add("permissions", permissions);
                            context.Session.Add("permissionsTop", permissionsTop);
                        }
                    }
                }
            }
            else
            {
                var menuData = DB.system_menu.Where(m => m.category != "0").Select(a => new { category = a.category, guid = a.guid }).ToList();

                if (menuData.Count > 0)
                {
                    foreach (var item in menuData)
                    {
                        permissions.Add(item.guid, "F");
                        if (permissionsTop.IndexOf(item.category) == -1)
                        {
                            permissionsTop.Add(item.category);
                        }
                    }

                    context.Session.Remove("permissions");
                    context.Session.Remove("permissionsTop");

                    context.Session.Add("permissions", permissions);
                    context.Session.Add("permissionsTop", permissionsTop);
                }
            }
        }

        /// <summary>
        /// 取得頁碼
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Allpage"></param>
        /// <param name="page"></param>
        /// <param name="inPage"></param>
        /// <param name="totalNum"></param>
        /// <param name="addpach"></param>
        /// <returns></returns>
        public static string getPageNum(string URL, int Allpage, int page, int totalNum, string addpach)
        {
            //addpach 附屬參數

            string PageList = "";

            int countPage = Allpage;
            int startPage = 1;
            int pageSec = Convert.ToInt16(Math.Ceiling((double)page / 5));

            if (Allpage > 1)
            {
                if (Allpage > 5)
                {
                    startPage = pageSec * 5 - 4;
                    if (startPage < Allpage)
                    {
                        countPage = startPage + 4;
                    }

                    if (page >= Allpage)
                    {
                        countPage = Allpage;
                    }
                }

                //上一頁
                if (Allpage > 1)
                {
                    if (page == 1)
                    {
                        PageList += "<a href = '' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        PageList += "<a href = '' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";

                        //PageList += "<li class=\"prev_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-left\"></i></a></li>";
                        //PageList += "<li class=\"prev\"><a href=\"#\"><i class=\"icon-left-open-mini\"></i></a></li>";
                    }
                    else
                    {
                        PageList += "<a href = '" + URL + "&page=1'" + addpach + " data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "&page=" + (page - 1).ToString() + addpach + "' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";

                        //PageList += "<li class=\"prev_all\"><a href=\"" + URL + "&page=1" + addpach + "\"><i class=\"icon-angle-double-left\"></i></a></li>";
                        //PageList += "<li class=\"prev\"><a href=\"" + URL + "&page=" + (page - 1).ToString() + addpach + "\"><i class=\"icon-left-open-mini\"></i></a></li>";
                    }
                }

                for (int p = startPage; p <= countPage; p++)
                {
                    if (p == page)
                    {
                        //  <a href="" class=" active" data-aos="flip-right" data-aos-delay="400"> < span class="pages font-20 kanit font-weight-600">01</span> </a>
                        PageList += "<a href='' class=' active' data-aos='flip-right' data-aos-delay='400'><span class='pages font-20 kanit font-weight-600'>" + p.ToString() + "</span></a>";
                        //PageList += "<li class=\"onPage\"><a href=\"#\"><span>" + p.ToString() + "</span></a></li>";
                    }
                    else
                    {
                        PageList += "<a href='" + URL + "&page=" + p.ToString() + addpach + "' class='' data-aos='flip-right' data-aos-delay='500'><span class='pages font-20 kanit font-weight-600'>" + p.ToString() + "</span></a>";
                        //PageList += "<li class=\"\"><a href=\"" + URL + "&page=" + p.ToString() + addpach + "\"><span>" + p.ToString() + "</span></a></li>";
                    }
                }

                if ((Allpage - ((pageSec - 1) * 5)) >= 5)
                {
                    //PageList += "...... <a href=\"#1\" class=\"_pageLink\" data-rel=\"" + URL + "," + Allpage + "," + inPage + "," + Allpage + "," + totalNum + "," + orderBy + "\"><span>" + Allpage + "</span></a> ";
                }

                //下一頁
                if (Allpage > 1)
                {
                    if (Allpage > page)
                    {
                        PageList += "<a href = '" + URL + "&page=" + (page + 1).ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "&page=" + Allpage.ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        //PageList += "<li class=\"next\"><a href=\"" + URL + "&page=" + (page + 1).ToString() + addpach + "\"><i class=\"icon-right-open-mini\"></i></a></li>";
                        //PageList += "<li class=\"next_all\"><a href=\"" + URL + "&page=" + Allpage.ToString() + addpach + "\"><i class=\"icon-angle-double-right\"></i></a></li>";
                    }
                    else
                    {
                        PageList += "<a href = '" + URL + "&page=" + (page + 1).ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next hidden'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "&page=" + Allpage.ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next-end hidden'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        //PageList += "<li class=\"next\"><a href=\"#\"><i class=\"icon-right-open-mini\"></i></a></li>";
                        //PageList += "<li class=\"next_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-right\"></i></a></li>";
                    }
                }
            }

            return PageList;
        }
        public static string getPageNumNoCatalog(string URL, int Allpage, int page, int totalNum, string addpach)
        {
            //addpach 附屬參數

            string PageList = "";

            int countPage = Allpage;
            int startPage = 1;
            int pageSec = Convert.ToInt16(Math.Ceiling((double)page / 5));

            if (Allpage > 1)
            {
                if (Allpage > 5)
                {
                    startPage = pageSec * 5 - 4;
                    if (startPage < Allpage)
                    {
                        countPage = startPage + 4;
                    }

                    if (page >= Allpage)
                    {
                        countPage = Allpage;
                    }
                }

                //上一頁
                if (Allpage > 1)
                {
                    if (page == 1)
                    {
                        PageList += "<a href = '' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        PageList += "<a href = '' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";

                        //PageList += "<li class=\"prev_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-left\"></i></a></li>";
                        //PageList += "<li class=\"prev\"><a href=\"#\"><i class=\"icon-left-open-mini\"></i></a></li>";
                    }
                    else
                    {
                        PageList += "<a href = '" + URL + "page=1'" + addpach + " data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "page=" + (page - 1).ToString() + addpach + "' data-aos = 'fade-left' data-aos-duration = '300' data-aos-delay = '300' class='prev'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";

                        //PageList += "<li class=\"prev_all\"><a href=\"" + URL + "&page=1" + addpach + "\"><i class=\"icon-angle-double-left\"></i></a></li>";
                        //PageList += "<li class=\"prev\"><a href=\"" + URL + "&page=" + (page - 1).ToString() + addpach + "\"><i class=\"icon-left-open-mini\"></i></a></li>";
                    }
                }

                for (int p = startPage; p <= countPage; p++)
                {
                    if (p == page)
                    {
                        //  <a href="" class=" active" data-aos="flip-right" data-aos-delay="400"> < span class="pages font-20 kanit font-weight-600">01</span> </a>
                        PageList += "<a href='' class=' active' data-aos='flip-right' data-aos-delay='400'><span class='pages font-20 kanit font-weight-600'>" + p.ToString() + "</span></a>";
                        //PageList += "<li class=\"onPage\"><a href=\"#\"><span>" + p.ToString() + "</span></a></li>";
                    }
                    else
                    {
                        PageList += "<a href='" + URL + "page=" + p.ToString() + addpach + "' class='' data-aos='flip-right' data-aos-delay='500'><span class='pages font-20 kanit font-weight-600'>" + p.ToString() + "</span></a>";
                        //PageList += "<li class=\"\"><a href=\"" + URL + "&page=" + p.ToString() + addpach + "\"><span>" + p.ToString() + "</span></a></li>";
                    }
                }

                if ((Allpage - ((pageSec - 1) * 5)) >= 5)
                {
                    //PageList += "...... <a href=\"#1\" class=\"_pageLink\" data-rel=\"" + URL + "," + Allpage + "," + inPage + "," + Allpage + "," + totalNum + "," + orderBy + "\"><span>" + Allpage + "</span></a> ";
                }

                //下一頁
                if (Allpage > 1)
                {
                    if (Allpage > page)
                    {
                        PageList += "<a href = '" + URL + "page=" + (page + 1).ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "page=" + Allpage.ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next-end'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        //PageList += "<li class=\"next\"><a href=\"" + URL + "&page=" + (page + 1).ToString() + addpach + "\"><i class=\"icon-right-open-mini\"></i></a></li>";
                        //PageList += "<li class=\"next_all\"><a href=\"" + URL + "&page=" + Allpage.ToString() + addpach + "\"><i class=\"icon-angle-double-right\"></i></a></li>";
                    }
                    else
                    {
                        PageList += "<a href = '" + URL + "page=" + (page + 1).ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next hidden'><img src = '/projects/public/m000355/test/styles/images/layout/pagination01.svg' alt=''></a>";
                        PageList += "<a href = '" + URL + "page=" + Allpage.ToString() + addpach + "' data-aos = 'fade-right' data-aos-duration = '300' data-aos-delay = '300'  class='next-end hidden'><img src = '/projects/public/m000355/test/styles/images/layout/pagination02.svg' alt=''></a>";
                        //PageList += "<li class=\"next\"><a href=\"#\"><i class=\"icon-right-open-mini\"></i></a></li>";
                        //PageList += "<li class=\"next_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-right\"></i></a></li>";
                    }
                }
            }

            return PageList;
        }

        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="MailList"></param>
        /// <param name="title"></param>
        /// <param name="defLang"></param>
        /// <param name="form"></param>
        public static void sendMailIndex(List<string> MailList, string title, string defLang, string contentUrl, FormCollection form)
        {
            Model DB = new Model();

            string webURL = ConfigurationManager.ConnectionStrings["APP_URL"].ConnectionString;

            web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            Guid sysGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");

            system_data system_data = DB.system_data.Where(model => model.guid == sysGuid).FirstOrDefault();//取得網站基本資訊

            // string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Content/Mail/survey.html";
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + contentUrl;
            string content = System.IO.File.ReadAllText(path);





            content = content.Replace("{[title]}", title);
            content = content.Replace("{[websiteName]}", web_Data.title);
            string info = "";

            try
            {
                info = form["info"].ToString();
            }
            catch { }

            try
            {
                content = content.Replace("{[sysName]}", form["sysName"].ToString());
                form.Remove("sysName");
            }
            catch
            {
                content = content.Replace("{[sysName]}", "管理者");
            }

            content = content.Replace("{[websiteUrl]}", webURL);
            content = content.Replace("{[logo]}", webURL + "/" + system_data.logo);
            content = content.Replace("{[date]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            content = content.Replace("{[address]}", web_Data.address);
            content = content.Replace("{[phone]}", web_Data.phone);

            foreach (var key in form.AllKeys)
            {
                content = content.Replace("{[" + key + "]}", form[key].ToString());

                string comprehensive = "";
                for (int i = 1; i <= 11; i++)
                {
                    if (form.AllKeys.Contains("problem_comprehensive_" + i.ToString()))
                    {
                        comprehensive = comprehensive + form["problem_comprehensive_" + i.ToString()].ToString() + "," ?? "";
                    }

                }

                content = content.Replace("{[comprehensive]}", comprehensive.ToString());

                string env = "";
                for (int i = 1; i <= 6; i++)
                {
                    if (form.AllKeys.Contains("problem_env_" + i.ToString()))
                    {
                        env = env + form["problem_env_" + i.ToString()].ToString() + "," ?? "";
                    }
                }

                content = content.Replace("{[env]}", env.ToString());

            }

            //
            string smtp_use = ConfigurationManager.ConnectionStrings["smtp_use"].ConnectionString;//SMTP 正式/測試

            var smtpData = DB.smtp_data.Where(m => m.guid == smtp_use).FirstOrDefault();

            string re = "";
            MailMessage msg = new MailMessage();
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            msg.To.Add(string.Join(",", MailList.ToArray()));
            msg.From = new MailAddress(smtpData.from_email.ToString(), web_Data.title, System.Text.Encoding.UTF8);
            //郵件標題
            msg.Subject = title.ToString();
            //郵件標題編碼
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            msg.Body = content;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼
            msg.Priority = MailPriority.Normal;//郵件優先級
                                               //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port

            SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.password.ToString());
            //Gmial 的 smtp 使用 SSL
            if (smtpData.smtp_auth.ToString() == "Y")
            {
                MySmtp.EnableSsl = true;
            }
            else
            {
                MySmtp.EnableSsl = false;
            }

            try
            {
                MySmtp.Send(msg);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
            }
        }

        /// <summary>
        /// 發問問題列表參數格式化
        /// </summary>
        public class forumData
        {
            public string guid { get; set; } // or whatever
            public string title { get; set; }  // or whatever

            public string forum_categoryguid { get; set; }  // or whatever
            public string forum_subcategoryguid { get; set; }  // or whatever
            public string ad_id { get; set; }  // or whatever

            public string status { get; set; }  // or whatever

            public DateTime create_date { get; set; }  // or whatever

            public string permission { get; set; }  // or whatever

            public string look { get; set; }  // or whatever

            public string thumbsup { get; set; }  // or whatever

            public string ad_name { get; set; }  // or whatever
            public string ad_mail { get; set; }  // or whatever
            public string comment { get; set; }  // or whatever

            public string ad_imgurl { get; set; }  // or whatever
            public string ad_department { get; set; }  // or whatever

            public string ad_ext_num { get; set; }  // or whatever
            public string top_guid { get; set; }  // or whatever
        }
    }
}