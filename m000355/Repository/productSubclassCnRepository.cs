﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Repository
{
    public class productSubclassCnRepository
    {
        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("reTitle", "[{'subject': '标题','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("productsCategory_guid", "[{'subject': '产品类别','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");
            main.Add("category", "[{'subject': '产品分类','type': 'selects','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_subclass','prev_table':'product_category','prev_field':'productsCategory_guid', 'useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': '建立者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': '建立时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': '编辑者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': '编辑时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");

            other.Add("status", "[{'subject': '状态','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '排序','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            hidden.Add("level", "[{'subject': 'level','type': 'hidden','defaultVal': '1','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("level", "阶层");
            re.Add("category", "产品类别");
            re.Add("title", "标题");
            re.Add("create_name", "建立者");
            re.Add("create_date", "建立时间");
            re.Add("modify_name", "编辑者");
            re.Add("modifydate", "编辑时间");
            re.Add("sortIndex", "排序");
            re.Add("status", "状态");
            re.Add("action", "动作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
    }
}