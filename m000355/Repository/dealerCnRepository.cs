﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Repository
{
    public class dealerCnRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '标题','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("category", "[{'subject': '类别','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'dealer_category'}]");
            main.Add("phone", "[{'subject': '电话','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("fax", "[{'subject': '传真','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("addressmap", "[{'subject': '地图','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("email", "[{'subject': '邮件','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //media.Add("video", "[{'subject': '影片上傳','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'video/mp4','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': '建立者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': '建立时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': '編輯者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': '编辑时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");

            other.Add("status", "[{'subject': '状态','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '排序','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "标题");
            re.Add("category", "类别");
            re.Add("create_name", "建立者");
            re.Add("create_date", "建立时间");
            re.Add("modify_name", "編輯者");
            re.Add("modifydate", "编辑时间");
            re.Add("sortIndex", "排序");
            re.Add("status", "状态");
            re.Add("action", "动作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}