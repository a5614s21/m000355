﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Repository
{
    public class homeBannerCnRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '标题','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': '子标题','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("url", "[{'subject': '连结','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("newBlank", "[{'subject': '连结视窗','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'新視窗/原視窗','Val':'Y/N','useLang':'N'}]");
            //main.Add("category", "[{'subject': 'Product Category','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");
            //main.Add("title_en", "[{'subject': '英文標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("pic", "[{'subject': '图片路径','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("picmobile", "[{'subject': '手机图片路径','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': '图片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高1920 x 970 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("picmobile", "[{'subject': '手机图片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高356 x 356 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:950 x 520 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("banner_pic", "[{'subject': 'BANNER圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:1920 x 465 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("index_pic", "[{'subject': '首頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:800 x 345 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': '建立者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': '建立时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': '編輯者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': '编辑时间','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");

            other.Add("status", "[{'subject': '状态','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '排序','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");


            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "标题");
            re.Add("sortIndex", "排序");
            re.Add("create_name", "建立者");
            re.Add("create_date", "建立时间");
            re.Add("modify_name", "編輯者");
            re.Add("modifydate", "编辑时间");
            re.Add("status", "状态");
            re.Add("action", "动作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}