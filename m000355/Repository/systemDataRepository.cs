﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Models;

namespace Web.Repository
{
    public class systemDataRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': 'Web Title','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("login_title", "[{'subject': 'Logion Title','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

         
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("logo", "[{'subject': 'LOGO','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高158 x 35 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'N'}]");
            media.Add("background", "[{'subject': 'Background','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">1920 x 1280 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'N'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("design_by", "[{'subject': 'Design by','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Minmax/E-creative','Val':'Minmax/E-creative','useLang':'N'}]");


            #endregion






            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

        

            return fromData;

        }
    }
    public class SystemService
    {
        private Model _context = new Model();
        public string InsertWarranty(string lang,string firstname,string lastname,string email,string tel,string company,string address,DateTime create_date)
        {
            try
            {
                warranty_register c = new warranty_register();
                c.guid = Guid.NewGuid().ToString();
                var add_guid = c.guid;
                c.firstname = firstname;
                c.lastname = lastname;
                c.email = email;
                c.tel = tel;
                c.company = company;
                c.address = address;
                c.status = "N";
                c.create_date = create_date;
                c.modifydate = create_date;
                c.lang = lang;
                _context.warranty_register.Add(c);
                _context.SaveChanges();
                return add_guid;
            }
            catch (Exception ex)
            {
                var errormessage = ex;
                return "";
            }
        }

        public bool InsertWarrantyDetail(string fname,string lname,string email,string telphone,string company,string address,string lang,string guid, string modelnumber, string serialnumber, string buyingchannel, string purchasedate,string proofofpruchase)
        {
            try
            {
                warranty_register_detail c = new warranty_register_detail();
                c.guid = Guid.NewGuid().ToString();
                c.warrantyRegister_guid = guid;
                c.modelnumber = modelnumber;
                c.serialnumber = serialnumber;
                c.buyingchannel = buyingchannel;
                c.purchasedate = purchasedate;
                c.proofofpruchase = proofofpruchase;
                c.status = "Y";
                c.create_date = DateTime.Now;
                c.modifydate = DateTime.Now;
                c.lang = lang;
                c.firstname = lname + " " + fname;
                c.email = email;
                c.tel = telphone;
                c.company = company;
                c.address = address;
                _context.Entry(c).State = EntityState.Added;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errormessage = ex;
                return false;
            }
        }
    }
}