﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
namespace web.Repository
{
    public class webTwDataRepository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '網站名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': '網站網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("description", "[{'subject': '網站描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("keywords", "[{'subject': '網站關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("name", "[{'subject': '聯絡人姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': '電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("ext_num", "[{'subject': '分機','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            main.Add("fax", "[{'subject': '傳真','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("servicemail", "[{'subject': '客服信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("staffmail", "[{'subject': '行政信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("managermail", "[{'subject': '主管信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("facebook", "[{'subject': 'Facebook','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("line", "[{'subject': 'Line','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("insgram", "[{'subject': 'Instagram','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("homefooter", "[{'subject': 'HomeFooter','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("privacypolicy", "[{'subject': 'PrivacyPolicy','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("dataLayer", "[{'subject': '行銷碼','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("keywords_products", "[{'subject': '產品首頁SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_products", "[{'subject': '產品首頁SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_productList", "[{'subject': '產品列表SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_productList", "[{'subject': '產品列表SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_news", "[{'subject': '最新消息列表SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_news", "[{'subject': '最新消息列表SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_dealer", "[{'subject': '經銷商SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_dealer", "[{'subject': '經銷商SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_catalogDown", "[{'subject': '目錄下載SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_catalogDown", "[{'subject': '目錄下載SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_contactService", "[{'subject': '服務SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_contactService", "[{'subject': '服務SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_contactinfo", "[{'subject': '聯絡資訊SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_contactinfo", "[{'subject': '聯絡資訊SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantyregister", "[{'subject': '保修註冊SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantyregister", "[{'subject': '保修註冊SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantycheck", "[{'subject': '保修查詢SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantycheck", "[{'subject': '保修查詢SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantyPolicy", "[{'subject': '保修政策SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantyPolicy", "[{'subject': '保修政策SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }
    }
    public class WebDataService
    {
        private Model _context = new Model();

        public web_data GetWebData(string lang)
        {
            var result = _context.web_data.Where(x => x.lang == "tw").SingleOrDefault();
            return result;
        }
    }
}