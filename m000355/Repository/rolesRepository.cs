﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class rolesRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': 'Role Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("rolenote", "[{'subject': 'Role Description','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            //other.Add("forum_status", "[{'subject': '問題回覆檢視','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'全部顯示/僅分派之回覆','Val':'Y/N'}]");
            //other.Add("get_ask_mail", "[{'subject': '接收提問通知信件','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'接收/不接收','Val':'Y/N'}]");
            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");
            other.Add("permissions", "[{'subject': 'Permissions','type': 'permissions','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':''}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "Name");
            re.Add("rolenote", "Description");
            re.Add("user_qty", "Number");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            re.Add("modify_name", "ModifyUser");
            re.Add("modifydate", "ModifyDate");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }
    }
}