﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using web.Models;
using Web.Models;

namespace web.Repository
{
    public class productsRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': 'Product Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': 'Subtitle','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            //main.Add("productsCategory_guid", "[{'subject': 'Product Category','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");

            //main.Add("productsSubClass_guid", "[{'subject': 'Product Subclass','type': 'select','default': '0','class': 'col-lg-10','required': 'required','notes': '','inherit':'products_subclassByProduct','prev_table':'product_subclass','prev_field':'productsCategory_guid', 'useLang':'N'}]");
            main.Add("productsSubClass_guid", "[{'subject': 'Product Subclass','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'','inherit':'products_subclassByProduct'}]");
            #endregion

            #region 內文設定

            Dictionary<String, Object> content = new Dictionary<string, object>();
            //content.Add("color", "[{'subject': '顏色','type': 'selectMultiple','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','inherit':'color','next_table':'products'}]");
            //content.Add("product_model", "[{'subject': '型號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("product_specification", "[{'subject': '產品規格','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("original_price", "[{'subject': '定價','type': 'text_number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("price", "[{'subject': '優惠價','type': 'text_number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("url", "[{'subject': '原廠網站','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("product_description", "[{'subject': 'Description','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            content.Add("emblem", "[{'subject': 'Emblem','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 40 x 40 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");

            //content.Add("product_features", "[{'subject': 'Feature','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("product_features", "[{'subject': 'Feature','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            content.Add("featurespic", "[{'subject': 'Feature Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 32 x 32 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");


            //content.Add("product_specification", "[{'subject': 'Specification','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("specColumn", "[{'subject': 'Specification','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">欄位請用ENTER做區隔</small>'}]");
            //content.Add("specValue", "[{'subject': 'Spec Value','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">欄位值請用ENTER做區隔(數量需與欄位相同)</small>'}]");
            content.Add("isComingSoon", "[{'subject': 'IsComingSoon','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': ''}]");
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("pic", "[{'subject': 'Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 530 x 530 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");
            media.Add("videoLink", "[{'subject': 'Video Link','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">example:https://www.youtube.com/embed/dozAVKrIBBM</small>','useLang':'Y'}]");
            media.Add("option", "[{'subject': 'Video Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 80 x 80 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            media.Add("downloadfile", "[{'subject': 'Download','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">limit PDF</small>','filetype': 'application/pdf','multiple': 'Y','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': 'CreateUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': 'CreateDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': 'ModifyUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': 'ModifyDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");

            main.Add("keywords", "[{'subject': 'SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description", "[{'subject': 'SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': 'SortIndex','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("content", content);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "Name");
            re.Add("subtitle", "Subtitle");
            //re.Add("productsCategory_guid", "Category");
            re.Add("productsSubClass_guid", "Subclass");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            re.Add("modify_name", "ModifyUser");
            re.Add("modifydate", "ModifyDate");
            //re.Add("sortIndex", "SortIndex");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }

    public class ProductsService
    {
        private Model _context = new Model();

        //取 商品內容 By Guid
        public product GetProductsByGuid(string lang, string guid)
        {
            var result = _context.product.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
            return result;
        }

        //取 商品 By lang SubClassGuid
        public List<product> GetProductsBySubClassGuid(string lang, string subclassguid)
        {
            var result = _context.product.Where(x => x.lang == lang && x.status == "Y" && x.productsSubClass_guid == subclassguid).OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            return result;
        }
        public List<product_subclass> GetProductSubClassNumLevel2(string wholeLang)
        {
            List<product_subclass> resultProductSubclass = new List<product_subclass>();
            return resultProductSubclass;
        }
        public List<product_subclass> GetProductSubClass(string wholeLang)
        {
            List<product_subclass> resultProductSubclass = new List<product_subclass>();


            var productData = (from a in _context.product
                               where a.status == "Y" && a.lang == wholeLang
                               select a).OrderBy(x => x.sortIndex).ToList();
            foreach (var judgeCategory in productData)
            {
                //level2
                if (judgeCategory.productsSubClass_guid != null)
                {
                    int countLevel1 = 0;
                    var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                    foreach (var isCategory in categorySplit)
                    {
                        var categoryGuid = (from a in _context.product_subclass
                                            where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                                            select a).FirstOrDefault();

                        resultProductSubclass.Add(categoryGuid);
                    }

                }
            }


            return resultProductSubclass;
        }
    }
}