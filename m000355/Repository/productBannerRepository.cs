﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Web.Models;
namespace web.Repository
{
    public class productBannerRepository
    {
                /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': 'Title','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': 'SubTitle','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("category", "[{'subject': 'Product Category','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");
            main.Add("type", "[{'subject': 'HasPicture','type': 'radio','defaultVal': 'no','classVal': 'col-lg-10','required': '','notes': '','data':'無/有','Val':'no/yes','useLang':'N'}]");
            //main.Add("title_en", "[{'subject': '英文標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("pic", "[{'subject': 'Picture Url','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("picmobile", "[{'subject': 'Mobile Picture Url','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','':'readonly','notes': '','useLang':'Y'}]");
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': 'Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 1920 x 420 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("picmobile", "[{'subject': 'Mobile Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">ex: 356 x 500 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:950 x 520 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("banner_pic", "[{'subject': 'BANNER圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:1920 x 465 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("index_pic", "[{'subject': '首頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:800 x 345 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': 'CreateUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': 'CreateDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': 'ModifyUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': 'ModifyDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");

            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': 'SortIndex','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");


            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "Title");
            re.Add("sortIndex", "SortIndex");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            re.Add("modify_name", "ModifyUser");
            re.Add("modifydate", "ModifyDate");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}