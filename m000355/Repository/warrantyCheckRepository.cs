﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Repository
{
    public class warrantyCheckRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("modelnumber", "[{'subject': 'Model Number','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("serialnumber", "[{'subject': 'Serial Number','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("warrantyRegister_guid", "[{'subject': 'TEL','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("create_date", "Registration Time");
            //re.Add("warrantyRegister_guid_g", "Registration Number");
            //re.Add("warrantyRegister_guid_name", "Name");
            //re.Add("warrantyRegister_guid", "Tel");
            //re.Add("warrantyRegister_guid_cop", "Company");
            //re.Add("warrantyRegister_guid_addr", "Address");
            re.Add("firstname", "Name");
            re.Add("email", "Email");
            re.Add("tel", "Tel");
            re.Add("company", "Company");
            re.Add("address", "Address");
            re.Add("modelnumber", "Model");
            re.Add("serialnumber", "Serial");
            re.Add("purchasedate", "Purchase Date");
            re.Add("buyingchannel", "Buying Channel");
            re.Add("downloaddetail", "Proof");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }
    }
}