﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Repository
{
    public class webDataRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': 'Web Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': 'Url','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("description", "[{'subject': 'Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("keywords", "[{'subject': 'Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("name", "[{'subject': '聯絡人姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': 'Phone','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("ext_num", "[{'subject': '分機','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            main.Add("fax", "[{'subject': 'Fax','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("servicemail", "[{'subject': 'Service Email','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("staffmail", "[{'subject': 'Staff Email','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("managermail", "[{'subject': 'Manager Email','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': 'Address','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("facebook", "[{'subject': 'Facebook','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("line", "[{'subject': 'Line','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("insgram", "[{'subject': 'Instagram','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("homefooter", "[{'subject': 'HomeFooter','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("privacypolicy", "[{'subject': 'PrivacyPolicy','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            main.Add("dataLayer", "[{'subject': 'Marketing Code','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("keywords_products", "[{'subject': 'Products SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_products", "[{'subject': 'Products SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_productList", "[{'subject': 'Product List SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_productList", "[{'subject': 'Product List SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_news", "[{'subject': 'News SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_news", "[{'subject': 'News SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_dealer", "[{'subject': 'Dealer SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_dealer", "[{'subject': 'Dealer SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_catalogDown", "[{'subject': 'Download SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_catalogDown", "[{'subject': 'Download SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_contactService", "[{'subject': 'Service SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_contactService", "[{'subject': 'Service SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_contactinfo", "[{'subject': 'ContactInfo SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_contactinfo", "[{'subject': 'ContactInfo SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantyregister", "[{'subject': 'Warrantyregister SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantyregister", "[{'subject': 'Warrantyregister SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantycheck", "[{'subject': 'WarrantyCheck SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantycheck", "[{'subject': 'WarrantyCheck SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("keywords_warrantyPolicy", "[{'subject': 'WarrantyPolicy SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description_warrantyPolicy", "[{'subject': 'WarrantyPolicy SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }
    }

    public class WebDataService
    {
        private Model _context = new Model();

        public web_data GetWebData(string lang)
        {
            var result = _context.web_data.Where(x => x.lang == "tw").SingleOrDefault();
            return result;
        }
    }
}