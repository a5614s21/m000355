﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Web.Models;

namespace web.Repository
{
    public class productsTwRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            //main.Add("title", "[{'subject': '產品名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("title", "[{'subject': '產品名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': '子標題','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            //main.Add("productsCategory_guid", "[{'subject': 'Product Category','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");

            //main.Add("productsSubClass_guid", "[{'subject': 'Product Subclass','type': 'select','default': '0','class': 'col-lg-10','required': 'required','notes': '','inherit':'products_subclassByProduct','prev_table':'product_subclass','prev_field':'productsCategory_guid', 'useLang':'N'}]");
            main.Add("productsSubClass_guid", "[{'subject': '產品分類','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'','inherit':'products_subclassByProduct'}]");
            #endregion

            #region 內文設定

            Dictionary<String, Object> content = new Dictionary<string, object>();
            //content.Add("color", "[{'subject': '顏色','type': 'selectMultiple','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','inherit':'color','next_table':'products'}]");
            //content.Add("product_model", "[{'subject': '型號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("product_specification", "[{'subject': '產品規格','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("original_price", "[{'subject': '定價','type': 'text_number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("price", "[{'subject': '優惠價','type': 'text_number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //content.Add("url", "[{'subject': '原廠網站','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("product_description", "[{'subject': '產品描述','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            content.Add("emblem", "[{'subject': '標章','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高40 x 40 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");

            //content.Add("product_features", "[{'subject': 'Feature','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("product_features", "[{'subject': '產品特色','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            content.Add("featurespic", "[{'subject': '特色圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高32 x 32 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");


            //content.Add("product_specification", "[{'subject': 'Specification','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            content.Add("specColumn", "[{'subject': '規格','type': 'textarea','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">欄位請用ENTER做區隔</small>'}]");
            //content.Add("specValue", "[{'subject': 'Spec Value','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">欄位值請用ENTER做區隔(數量需與欄位相同)</small>'}]");
            content.Add("isComingSoon", "[{'subject': '上架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': ''}]");
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高530 x 530 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");
            media.Add("videoLink", "[{'subject': '影片連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">example:https://www.youtube.com/embed/dozAVKrIBBM</small>','useLang':'Y'}]");
            media.Add("option", "[{'subject': '影片圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高80 x 80 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("downloadfile", "[{'subject': '下載檔案','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">limit PDF</small>','filetype': 'application/pdf','multiple': 'Y','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': '建立者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': '建立時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': '編輯者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': '編輯時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");

            main.Add("keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description", "[{'subject': 'SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            other.Add("status", "[{'subject': '狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '排序','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("content", content);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "名稱");
            re.Add("subtitle", "子標題");
            //re.Add("productsCategory_guid", "Category");
            re.Add("productsSubClass_guid", "產品分類");
            re.Add("create_name", "建立者");
            re.Add("create_date", "建立時間");
            re.Add("modify_name", "編輯者");
            re.Add("modifydate", "編輯時間");
            //re.Add("modifydate", "Modifydate");
            //re.Add("sortIndex", "SortIndex");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}