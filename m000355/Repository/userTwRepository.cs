﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Repository
{
    public class userTwRepository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("username", "[{'subject': '使用者名稱','type': 'account','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>',}]");
            main.Add("password", "[{'subject': '登入密碼','type': 'password','defaultVal': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請再次輸入密碼!</small>',}]");
            main.Add("role_guid", "[{'subject': '帳戶群組','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'roles' }]");
            main.Add("role2_guid", "[{'subject': '中文群組','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'rolesTw' }]");
            main.Add("role3_guid", "[{'subject': '簡文群组','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'rolesCn' }]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 帳戶詳細資料

            Dictionary<String, Object> userCol = new Dictionary<string, object>();

            userCol.Add("name", "[{'subject': '姓名(暱稱)','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("email", "[{'subject': 'Email','type': 'email','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("phone", "[{'subject': '聯絡電話','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("mobile", "[{'subject': '行動電話','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("address", "[{'subject': '通訊地址','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': '建立者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': '建立時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': '編輯者','type': 'text','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': '編輯時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': 'required','readonly':'readonly','notes': '',}]");

            other.Add("lockout", "[{'subject': '到期時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">設定到期時間後將不可再登入</small>'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("user", userCol);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("username", "使用者帳號");
            re.Add("name", "姓名");
            re.Add("lang", "語系");
            re.Add("pluralCategory", "帳戶群組");
            //re.Add("pluralCategory2", "中文群組");
            //re.Add("logindate", "上次登入");
            re.Add("create_name", "建立者");
            re.Add("create_date", "建立時間");
            //re.Add("modify_name", "編輯者");
            //re.Add("modifydate", "編輯時間");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }
    }
}