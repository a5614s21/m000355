﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Models;

namespace web.Repository
{
    public class productSubclassRepository
    {
        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("reTitle", "[{'subject': 'Title','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("productsCategory_guid", "[{'subject': 'Product Classification','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_category'}]");
            main.Add("category", "[{'subject': 'Subclass','type': 'selects','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'product_subclass','prev_table':'product_category','prev_field':'productsCategory_guid', 'useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': 'CreateUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': 'CreateDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': 'ModifyUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': 'ModifyDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");

            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': 'SortIndex','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            hidden.Add("level", "[{'subject': 'level','type': 'hidden','defaultVal': '1','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("level", "level");
            re.Add("category", "Classification");
            re.Add("title", "Title");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            re.Add("modify_name", "ModifyUser");
            re.Add("modifydate", "ModifyDate");
            re.Add("sortIndex", "SortIndex");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
    }

    public class ProductsSubClassService
    {
        private Model _context = new Model();

        //檢查該GUID的LEVEL
        public product_subclass CheckLevelByGuid(string lang, string guid)
        {
            var result = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
            return result;
        }

        //取 層級 By Category
        public product_subclass GetLevel(string lang, string category)
        {
            var result = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.guid == category).SingleOrDefault();
            return result;
        }

        //取 商品分類 By  CategoryGuid Guid
        public List<product_subclass> GetProductsSubClassByCategoryGuid_Guid(string lang, string categoryguid, string guid)
        {
            List<product_subclass> result = new List<product_subclass>();
            List<product_subclass> r = new List<product_subclass>();
            var all = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.productsCategory_guid == categoryguid).OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            var L1 = all.Where(x => x.level == 1).ToList();
            foreach (var item in L1)
            {
                result.Add(item);
                foreach (var L2 in all.Where(x => x.category == item.guid))
                {
                    result.Add(L2);
                    foreach (var L3 in all.Where(x => x.category == L2.guid))
                    {
                        result.Add(L3);
                    }
                }
            }

            var t = all.Where(x => x.guid == guid).SingleOrDefault();
            if (t != null)
            {
                r = result.Where(x => x.level < t.level).ToList();
            }
            else
            {
                r = result.Where(x => x.level < 5).ToList();
            }

            return r;
        }

        //
        public product_subclass GetProductsSubClassByGuid(string lang, string guid)
        {
            var result = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
            return result;
        }

        //取 商品分類 By CategoryGuid Guid
        public List<product_subclass> GetProductsSubClassByGuid_OrderbyLevel23(string lang, string categoryguid, string guid)
        {
            List<product_subclass> result = new List<product_subclass>();
            List<product_subclass> r = new List<product_subclass>();
            var all = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.productsCategory_guid == categoryguid).OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            var L1 = all.Where(x => x.level == 1).ToList();
            foreach (var item in L1)
            {
                result.Add(item);
                foreach (var L2 in all.Where(x => x.category == item.guid))
                {
                    result.Add(L2);
                    foreach (var L3 in all.Where(x => x.category == L2.guid))
                    {
                        result.Add(L3);
                    }
                }
            }

            var t = all.Where(x => x.guid == guid).SingleOrDefault();
            if (t != null)
            {
                r = result.Where(x => x.level < t.level).ToList();
            }
            else
            {
                r = result.Where(x => x.level >= 2).ToList();
            }

            return r;
        }

        //取 商品分類 By ProductsCategory_Guid  Level
        public List<product_subclass> GetProductsSubClassByLevel(string lang, string cyguid, int level)
        {
            var result = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.productsCategory_guid == cyguid && x.level == level).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            return result;
        }

        //取 產品類別第一層 By ProductCategoryTitle
        public Dictionary<string, string> GetProductsSubClassByPrdocutCategoryTitle(string lang, string productCategorytitle)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var categoryguid = _context.product_category.Where(x => x.lang == lang && x.status == "Y" && x.title == productCategorytitle).First().guid;
            var r = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.productsCategory_guid == categoryguid && x.level == 1).OrderBy(x => x.sortIndex).ThenBy(x => x.modifydate).ToList();

            foreach (var item in r)
            {
                result.Add(item.title, item.guid);
            }

            return result;
        }

        //
        public string GetProductsSubClassL1ByL2Guid(string lang, string L2Guid)
        {
            var result = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.guid == L2Guid).First().category;
            return result;
        }

        //取 產品類別第二層 By 類別第一層
        public Dictionary<string, string> GetProductsSubClassLevel2ByGuid(string lang, string guid)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var r = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.category == guid).OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();

            foreach (var item in r)
            {
                result.Add(item.title, item.guid);
            }
            return result;
        }

        public Dictionary<string, string> GetProductsSubClassLevel2ByType(string lang, string type)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var categoryguid = _context.product_category.Where(x => x.lang == lang && x.status == "Y" && x.title == type).First().guid;
            var r = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y" && x.productsCategory_guid == categoryguid && x.level == 2).OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();

            foreach (var item in r)
            {
                result.Add(item.title, item.guid);
            }

            return result;
        }

        //取 第一層級 商品分類
        public List<product_subclass> GetProductsSubClassOrderbyLeve1(string lang)
        {
            List<product_subclass> result = new List<product_subclass>();
            var all = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            var L2 = all.Where(x => x.level == 1).ToList();
            foreach (var item in L2)
            {
                result.Add(item);
            }
            return result;
        }

        //取 前兩層級 商品分類
        public List<product_subclass> GetProductsSubClassOrderbyLevel_2(string lang)
        {
            List<product_subclass> result = new List<product_subclass>();
            var all = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            var L1 = all.Where(x => x.level == 1).ToList();
            foreach (var item in L1)
            {
                result.Add(item);
                foreach (var L2 in all.Where(x => x.category == item.guid))
                {
                    result.Add(L2);
                }
            }
            return result;
        }

        //取 所有層級 商品分類
        public List<product_subclass> GetProductsSubClassOrderbyLevel_All(string lang)
        {
            List<product_subclass> result = new List<product_subclass>();
            var all = _context.product_subclass.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.modifydate).ToList();
            var L1 = all.Where(x => x.level == 1).ToList();
            foreach (var item in L1)
            {
                result.Add(item);
                foreach (var L2 in all.Where(x => x.category == item.guid))
                {
                    result.Add(L2);
                    foreach (var L3 in all.Where(x => x.category == L2.guid))
                    {
                        result.Add(L3);
                        foreach (var L4 in all.Where(x => x.category == L3.guid))
                        {
                            result.Add(L4);
                        }
                    }
                }
            }
            return result;
        }
    }
}