﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class userRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("username", "[{'subject': 'Username','type': 'account','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>',}]");
            main.Add("password", "[{'subject': 'Password','type': 'password','defaultVal': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請再次輸入密碼!</small>',}]");
            main.Add("role_guid", "[{'subject': 'Role','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'roles' }]");
            main.Add("role2_guid", "[{'subject': 'Role_Tw','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'rolesTw' }]");
            main.Add("role3_guid", "[{'subject': 'Role_Cn','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','inherit':'rolesCn' }]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 帳戶詳細資料

            Dictionary<String, Object> userCol = new Dictionary<string, object>();

            userCol.Add("name", "[{'subject': 'Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("email", "[{'subject': 'Email','type': 'email','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("phone", "[{'subject': 'Phone','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("mobile", "[{'subject': 'Mobile','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("address", "[{'subject': 'Address','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("create_name", "[{'subject': 'CreateUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("create_date", "[{'subject': 'CreateDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modify_name", "[{'subject': 'ModifyUser','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");
            other.Add("modifydate", "[{'subject': 'ModifyDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '',}]");

            other.Add("lockout", "[{'subject': 'EndDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">設定到期時間後將不可再登入</small>'}]");
            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("user", userCol);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("username", "Username");
            re.Add("name", "Name");
            re.Add("lang", "Language");
            re.Add("pluralCategory", "Role");
            //re.Add("pluralCategory2", "Role2");
            //re.Add("logindate", "Login Date");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            //re.Add("modify_name", "ModifyUser");
            //re.Add("modifydate", "ModifyDate");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }
    }
}