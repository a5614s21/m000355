﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using web.Models;
using Web.Models;

namespace web.Repository
{
    public class newsRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': 'Title','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': 'SubTitle','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("homedesc", "[{'subject': 'Home SubTitle','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("category", "[{'subject': 'Category','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'news_category'}]");
            main.Add("content", "[{'subject': 'Content','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("line", "[{'subject': 'LINE','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("facebook", "[{'subject': 'FaceBook','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("twitter", "[{'subject': 'Twitter','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("keywords", "[{'subject': 'SEO Keywords','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("description", "[{'subject': 'SEO Description','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': 'Picture','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高1024 x 280 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("video", "[{'subject': '影片上傳','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'video/mp4','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("startdate", "[{'subject': 'StartDate','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("enddate", "[{'subject': 'EndDate','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("sticky", "[{'subject': 'Sticky','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到最前面</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("index_sticky", "[{'subject': 'Index Sticky','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到首頁</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "Picture");
            re.Add("title", "Title");
            re.Add("category", "Category");
            re.Add("startdate", "StartDate");
            re.Add("enddate", "EndDate");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "startdate");
            re.Add("orderByType", "desc");

            return re;
        }
    }
}