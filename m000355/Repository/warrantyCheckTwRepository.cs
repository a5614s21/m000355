﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Repository
{
    public class warrantyCheckTwRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("modelnumber", "[{'subject': 'Model Number','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("serialnumber", "[{'subject': 'Serial Number','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("warrantyRegister_guid", "[{'subject': 'TEL','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Enable/Disable','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("create_date", "註冊時間");
            //re.Add("warrantyRegister_guid_g", "註冊序號");
            //re.Add("warrantyRegister_guid_name", "姓名");
            //re.Add("warrantyRegister_guid", "電話");
            //re.Add("warrantyRegister_guid_cop", "公司");
            //re.Add("warrantyRegister_guid_addr", "地址");
            re.Add("firstname", "姓名");
            re.Add("email", "郵件");
            re.Add("tel", "電話");
            re.Add("company", "公司");
            re.Add("address", "地址");
            re.Add("modelnumber", "型號");
            re.Add("serialnumber", "序號");
            re.Add("purchasedate", "購買日期");
            re.Add("buyingchannel", "購買通路");
            re.Add("downloaddetail", "購買證明");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }
    }
}