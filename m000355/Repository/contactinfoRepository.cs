﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using web.Models;
using Web.Models;

namespace web.Repository
{
    public class contactinfoRepository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("businesstype", "[{'subject': 'Business Type','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("companyname", "[{'subject': 'Company Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("contactname", "[{'subject': 'Contact Name','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("email", "[{'subject': 'E-mail','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("tel", "[{'subject': 'TEL','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("country", "[{'subject': 'Country','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': 'Address','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("website", "[{'subject': 'Web Site','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("subject", "[{'subject': 'Subject','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("message", "[{'subject': 'Leave Your Message','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'Reply/NoReply','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("contactname", "Contact Name");
            re.Add("create_name", "CreateUser");
            re.Add("create_date", "CreateDate");
            re.Add("modify_name", "ModifyUser");
            re.Add("modifydate", "ModifyDate");
            re.Add("status", "Status");
            re.Add("action", "Action");

            return re;
        }
    }
}