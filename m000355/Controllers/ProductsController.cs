﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Repository;
using Web.Models;
using Web.Service;

namespace m000355.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_products;
                ViewBag.keywords = privacyData.keywords_products;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            var productBanner = (from a in DB.product_banner
                                 where a.status == "Y" && a.lang == wholeLang && a.type == "yes"
                                 select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productBanner = productBanner;
            var productBannerNoPic = (from a in DB.product_banner
                                      where a.status == "Y" && a.lang == wholeLang && a.type == "no"
                                      select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productBannerNoPic = productBannerNoPic;

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //ViewBag.productSubclassFirst = productSubclassFirst;

            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }
        public ActionResult ProductList(string category, int? page)
        {
            Session["firstLevelId"] = category;
            Session["secondLevelId"] = "1";
            Session["thirdLevelId"] = "1";
            Session["fourLevelId"] = "1";

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Session["breadCategory"] = category;

            Model DB = new Model();
            product_category categoryTitleAll = new product_category();
            //類別名稱
            var tempCategoryGuidAll = (from a in DB.product_subclass
                                       where a.guid == category && a.status == "Y"
                                       select a.productsCategory_guid).FirstOrDefault();
            if (tempCategoryGuidAll == null)
            {
                categoryTitleAll = (from a in DB.product_category
                                    where a.guid == category && a.status == "Y"
                                    select a).FirstOrDefault();
            }
            else
            {
                categoryTitleAll = (from a in DB.product_category
                                    where a.guid == tempCategoryGuidAll && a.status == "Y"
                                    select a).FirstOrDefault();
            }
            //var categoryTitleAll = (from a in DB.product_category
            //                        where a.guid == tempCategoryGuidAll && a.status == "Y"
            //                        select a).FirstOrDefault();


            wholeLang = categoryTitleAll.lang;
            Session["WordLang"] = categoryTitleAll.lang;
            if (categoryTitleAll.lang == "tw")
            {
                Session["WholeLang"] = "TW";
            }
            else if (categoryTitleAll.lang == "en")
            {
                Session["WholeLang"] = "EN";
            }
            else
            {
                Session["WholeLang"] = "EN";
            }


            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var tempSeoCatalogData = (from a in DB.product_subclass
                                      where a.lang == wholeLang && a.guid == category && a.status == "Y"
                                      select a).FirstOrDefault();
            if (tempSeoCatalogData == null)
            {
                var seoCatalogData = (from a in DB.product_category
                                      where a.lang == wholeLang && a.guid == category && a.status == "Y"
                                      select a).FirstOrDefault();

                if (seoCatalogData != null)
                {
                    ViewBag.description = seoCatalogData.description;
                    ViewBag.keywords = seoCatalogData.keywords;
                }
                else
                {
                    ViewBag.description = "";
                    ViewBag.keywords = "";
                }
            }
            else
            {
                var seoCatalogData = (from a in DB.product_category
                                      where a.lang == wholeLang && a.guid == tempSeoCatalogData.productsCategory_guid && a.status == "Y"
                                      select a).FirstOrDefault();

                if (seoCatalogData != null)
                {
                    ViewBag.description = seoCatalogData.description;
                    ViewBag.keywords = seoCatalogData.keywords;
                }
                else
                {
                    ViewBag.description = "";
                    ViewBag.keywords = "";
                }
            }

            List<product_subclass> isSecondList = new List<product_subclass>();
            List<product_subclass> isThirdList = new List<product_subclass>();
            List<product_subclass> isFourList = new List<product_subclass>();
            List<product> resultProduct = new List<product>();

            List<product> resultProductFirst = new List<product>();
            List<product> resultProductSecond = new List<product>();
            List<product> resultProductThird = new List<product>();
            List<product> resultProductFour = new List<product>();

            ProductsService products = new ProductsService();
            //var getProductSubclassLevel2Num = products.GetProductSubClassNumLevel2(wholeLang);
            var GetProductSubClass = products.GetProductSubClass(wholeLang);
            int level1count = 0;
            int level2count = 0;
            int level3count = 0;
            int level4count = 0;

            List<product_subclass> psLeftFirst = new List<product_subclass>();
            var productCatalogLeftFirst = (from a in DB.product_category
                                           where a.status == "Y" && a.lang == wholeLang
                                           select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogLeftFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psLeftFirst.Add(tempProductSubclassFirst);
            }


            var productCategoryFirst = (from a in DB.product_subclass
                                        where a.category == "0" && a.lang == wholeLang && a.status == "Y"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productCategoryFirst = productCategoryFirst;
            ViewBag.productCategoryFirst = psLeftFirst;

            foreach (var item in productCategoryFirst)
            {
                foreach (var productItem1 in GetProductSubClass)
                {
                    if (productItem1.level == 2)
                    {
                        var templevel1guid = (from a in DB.product_subclass
                                              where a.guid == productItem1.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        if (item.guid == templevel1guid.guid)
                        {
                            level1count = level1count + 1;
                        }
                    }
                    else if (productItem1.level == 3)
                    {
                        var templevel2guid = (from a in DB.product_subclass
                                              where a.guid == productItem1.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        var templevel1guid = (from a in DB.product_subclass
                                              where a.guid == templevel2guid.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        if (item.guid == templevel1guid.guid)
                        {
                            level1count = level1count + 1;
                        }
                    }
                    else if (productItem1.level == 4)
                    {
                        var templevel3guid = (from a in DB.product_subclass
                                              where a.guid == productItem1.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        var templevel2guid = (from a in DB.product_subclass
                                              where a.guid == templevel3guid.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        var templevel1guid = (from a in DB.product_subclass
                                              where a.guid == templevel2guid.category && a.lang == wholeLang
                                              select a).FirstOrDefault();
                        if (item.guid == templevel1guid.guid)
                        {
                            level1count = level1count + 1;
                        }
                    }
                    else
                    {

                    }

                }
                //var judgeLevel = (from a in DB.product_subclass
                //                  where a.guid == item.guid && a.status == "Y" && a.lang == wholeLang
                //                  select a.level).FirstOrDefault();
                //if (judgeLevel == 1)
                //{
                //    //對應產品(第一層)
                //    var productData = (from a in DB.product
                //                       where a.status == "Y" && a.lang == wholeLang
                //                       select a).OrderBy(x => x.sortIndex).ToList();
                //    foreach (var judgeCategory in productData)
                //    {
                //        //level1
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            int countLevel1 = 0;
                //            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //            foreach (var isCategory in categorySplit)
                //            {
                //                var categoryGuid = (from a in DB.product_subclass
                //                                    where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                //                                    select a.productsCategory_guid).FirstOrDefault();

                //                if (categoryGuid == item.guid)
                //                {
                //                    countLevel1 = 1;
                //                }
                //            }
                //            if (countLevel1 == 1)
                //            {
                //                resultProductFirst.Add(judgeCategory);
                //            }
                //            countLevel1 = 0;
                //        }
                //        //level2
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            var level2countData = (from a in DB.product_subclass
                //                                   where a.category == item.guid && a.lang == wholeLang
                //                                   select a).ToList();
                //            foreach (var itemLevel2 in level2countData)
                //            {
                //                int countLevel2 = 0;
                //                var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //                foreach (var isCategory in categorySplit)
                //                {
                //                    if (isCategory == itemLevel2.guid)
                //                    {
                //                        countLevel2 = 1;
                //                    }
                //                }
                //                if (countLevel2 == 1)
                //                {
                //                    resultProductSecond.Add(judgeCategory);
                //                }
                //                countLevel2 = 0;
                //            }

                //        }
                //        //level3
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            var level2countData = (from a in DB.product_subclass
                //                                   where a.category == item.guid && a.lang == wholeLang
                //                                   select a).ToList();
                //            foreach (var itemLevel2 in level2countData)
                //            {
                //                var level3countData = (from a in DB.product_subclass
                //                                       where a.category == itemLevel2.guid && a.lang == wholeLang
                //                                       select a).ToList();
                //                foreach (var itemLevel3 in level3countData)
                //                {
                //                    int countLevel3 = 0;
                //                    var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //                    foreach (var isCategory in categorySplit)
                //                    {
                //                        if (isCategory == itemLevel3.guid)
                //                        {
                //                            countLevel3 = 1;
                //                        }
                //                    }
                //                    if (countLevel3 == 1)
                //                    {
                //                        resultProductThird.Add(judgeCategory);
                //                    }
                //                    countLevel3 = 0;
                //                }
                //            }

                //        }
                //        //level4
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            var level2countData = (from a in DB.product_subclass
                //                                   where a.category == item.guid && a.lang == wholeLang
                //                                   select a).ToList();
                //            foreach (var itemLevel2 in level2countData)
                //            {
                //                var level3countData = (from a in DB.product_subclass
                //                                       where a.category == itemLevel2.guid && a.lang == wholeLang
                //                                       select a).ToList();
                //                foreach (var itemLevel3 in level3countData)
                //                {
                //                    var level4countData = (from a in DB.product_subclass
                //                                           where a.category == itemLevel3.guid && a.lang == wholeLang
                //                                           select a).ToList();
                //                    foreach (var itemLevel4 in level4countData)
                //                    {
                //                        int countLevel4 = 0;
                //                        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //                        foreach (var isCategory in categorySplit)
                //                        {
                //                            if (isCategory == itemLevel4.guid)
                //                            {
                //                                countLevel4 = 1;
                //                            }
                //                        }
                //                        if (countLevel4 == 1)
                //                        {
                //                            resultProductFour.Add(judgeCategory);
                //                        }
                //                        countLevel4 = 0;
                //                    }
                //                }
                //            }

                //        }
                //    }
                //    item.create_name = (resultProductFirst.Count() + resultProductSecond.Count() + resultProductThird.Count() + resultProductFour.Count()).ToString();
                //    resultProductFirst = new List<product>();
                //    resultProductSecond = new List<product>();
                //    resultProductThird = new List<product>();
                //    resultProductFour = new List<product>();
                //}
                item.create_name = level1count.ToString();
                level1count = 0;

                var isSecond = (from a in DB.product_subclass
                                where a.level == 2 && a.lang == wholeLang && a.status == "Y" && a.category == item.guid
                                select a).ToList();
                isSecondList.AddRange(isSecond);
                ViewBag.isSecondList = isSecondList;
            }

            var productCategorySecond = (from a in DB.product_subclass
                                         where a.level == 2 && a.lang == wholeLang && a.status == "Y"
                                         select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productCategorySecond = productCategorySecond;

            List<product> resultProductSecond2 = new List<product>();
            foreach (var item in productCategorySecond)
            {
                foreach (var productItem in GetProductSubClass)
                {
                    if (item.guid == productItem.guid)
                    {
                        level2count = level2count + 1;
                    }
                }
                //var productData = (from a in DB.product
                //                   where a.status == "Y" && a.lang == wholeLang
                //                   select a).OrderBy(x => x.sortIndex).ToList();
                //foreach (var judgeCategory in productData)
                //{
                //    //level2
                //    if (judgeCategory.productsSubClass_guid != null)
                //    {
                //        int countLevel1 = 0;
                //        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //        foreach (var isCategory in categorySplit)
                //        {
                //            var categoryGuid = (from a in DB.product_subclass
                //                                where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                //                                select a).FirstOrDefault();

                //            if (categoryGuid.guid == item.guid)
                //            {
                //                countLevel1 = 1;
                //            }
                //        }
                //        if (countLevel1 == 1)
                //        {
                //            resultProductSecond2.Add(judgeCategory);
                //        }
                //        countLevel1 = 0;

                //    }
                //}
                //item.create_name = resultProductSecond2.Count().ToString();
                //resultProductSecond2 = new List<product>();
                item.create_name = level2count.ToString();
                level2count = 0;

                var isThird = (from a in DB.product_subclass
                               where a.level == 3 && a.lang == wholeLang && a.status == "Y" && a.category == item.guid
                               select a).ToList();
                isThirdList.AddRange(isThird);
                ViewBag.isThirdList = isThirdList;
            }

            var productCategoryThird = (from a in DB.product_subclass
                                        where a.level == 3 && a.lang == wholeLang && a.status == "Y"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productCategoryThird = productCategoryThird;

            List<product> resultProductThird2 = new List<product>();
            foreach (var item in productCategoryThird)
            {
                foreach (var productItem3 in GetProductSubClass)
                {
                    if (item.guid == productItem3.guid)
                    {
                        level3count = level3count + 1;
                    }
                }
                //var productData = (from a in DB.product
                //                   where a.status == "Y" && a.lang == wholeLang
                //                   select a).OrderBy(x => x.sortIndex).ToList();
                //foreach (var judgeCategory in productData)
                //{
                //    //level3
                //    if (judgeCategory.productsSubClass_guid != null)
                //    {
                //        int countLevel1 = 0;
                //        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //        foreach (var isCategory in categorySplit)
                //        {
                //            var categoryGuid = (from a in DB.product_subclass
                //                                where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                //                                select a).FirstOrDefault();

                //            if (categoryGuid.guid == item.guid)
                //            {
                //                countLevel1 = 1;
                //            }
                //        }
                //        if (countLevel1 == 1)
                //        {
                //            resultProductThird2.Add(judgeCategory);
                //        }
                //        countLevel1 = 0;

                //    }
                //}
                //item.create_name = resultProductThird2.Count().ToString();
                //resultProductThird2 = new List<product>();
                item.create_name = level3count.ToString();
                level3count = 0;

                var isFour = (from a in DB.product_subclass
                              where a.level == 4 && a.lang == wholeLang && a.status == "Y" && a.category == item.guid
                              select a).ToList();
                isFourList.AddRange(isFour);
                ViewBag.isFourList = isFourList;
            }

            var productCategoryFour = (from a in DB.product_subclass
                                       where a.level == 4 && a.lang == wholeLang && a.status == "Y"
                                       select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productCategoryFour = productCategoryFour;

            List<product> resultProductFour2 = new List<product>();
            foreach (var item in productCategoryFour)
            {
                foreach (var productItem4 in GetProductSubClass)
                {
                    if (item.guid == productItem4.guid)
                    {
                        level4count = level4count + 1;
                    }
                }
                //var productData = (from a in DB.product
                //                   where a.status == "Y" && a.lang == wholeLang
                //                   select a).OrderBy(x => x.sortIndex).ToList();
                //foreach (var judgeCategory in productData)
                //{
                //    //level4
                //    if (judgeCategory.productsSubClass_guid != null)
                //    {
                //        int countLevel1 = 0;
                //        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //        foreach (var isCategory in categorySplit)
                //        {
                //            var categoryGuid = (from a in DB.product_subclass
                //                                where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                //                                select a).FirstOrDefault();

                //            if (categoryGuid.guid == item.guid)
                //            {
                //                countLevel1 = 1;
                //            }
                //        }
                //        if (countLevel1 == 1)
                //        {
                //            resultProductFour2.Add(judgeCategory);
                //        }
                //        countLevel1 = 0;

                //    }
                //}
                //item.create_name = resultProductFour2.Count().ToString();
                //resultProductFour2 = new List<product>();
                item.create_name = level4count.ToString();
                level4count = 0;
            }


            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var currentCategory = (from a in DB.product_category
                                   where a.guid == category && a.lang == wholeLang
                                   select a).FirstOrDefault();
            if (currentCategory == null)
            {
                var judgeLevel = (from a in DB.product_subclass
                                  where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                  select a.level).FirstOrDefault();
                if (judgeLevel == 1)
                {
                    //類別名稱
                    var tempCategoryGuid = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                            select a.productsCategory_guid).FirstOrDefault();
                    var categoryTitle = (from a in DB.product_category
                                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                                         select a).FirstOrDefault();
                    ViewBag.currentCategory = categoryTitle;
                    Session["firstLevelId"] = categoryTitle.guid;
                    Session["secondLevelId"] = "1";
                    Session["thirdLevelId"] = "1";
                    Session["fourLevelId"] = "1";
                    //對應產品(第一層)
                    var productData = (from a in DB.product
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();
                    foreach (var judgeCategory in productData)
                    {
                        if (judgeCategory.productsSubClass_guid != null)
                        {
                            int count = 0;
                            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                            foreach (var isCategory in categorySplit)
                            {
                                var categoryGuid = (from a in DB.product_subclass
                                                    where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                                                    select a.productsCategory_guid).FirstOrDefault();

                                if (categoryGuid == categoryTitle.guid)
                                {
                                    count = 1;
                                }
                            }
                            if (count == 1)
                            {
                                resultProduct.Add(judgeCategory);
                            }
                            count = 0;
                        }
                    }
                    ViewBag.productData = resultProduct;
                }
                else if (judgeLevel == 2)
                {
                    //類別名稱
                    var tempCategoryGuid = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                            select a.productsCategory_guid).FirstOrDefault();
                    var categoryTitle = (from a in DB.product_category
                                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                                         select a).FirstOrDefault();
                    ViewBag.currentCategory = categoryTitle;
                    Session["firstLevelId"] = categoryTitle.guid;

                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;
                    Session["secondLevelId"] = currentCategorySecond.guid;
                    Session["thirdLevelId"] = "1";
                    Session["fourLevelId"] = "1";
                    //對應產品(第二層)
                    var productData = (from a in DB.product
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();
                    foreach (var judgeCategory in productData)
                    {
                        if (judgeCategory.productsSubClass_guid != null)
                        {
                            int count = 0;
                            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                            foreach (var isCategory in categorySplit)
                            {
                                if (isCategory == category)
                                {
                                    count = 1;
                                }
                            }
                            if (count == 1)
                            {
                                resultProduct.Add(judgeCategory);
                            }
                            count = 0;
                        }
                    }
                    ViewBag.productData = resultProduct;
                }
                else if (judgeLevel == 3)
                {
                    //類別名稱
                    var tempCategoryGuid = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                            select a.productsCategory_guid).FirstOrDefault();
                    var categoryTitle = (from a in DB.product_category
                                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                                         select a).FirstOrDefault();
                    ViewBag.currentCategory = categoryTitle;
                    Session["firstLevelId"] = categoryTitle.guid;

                    var tempCategorySecond = (from a in DB.product_subclass
                                              where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                              select a.category).FirstOrDefault();
                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == tempCategorySecond && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;
                    Session["secondLevelId"] = currentCategorySecond.guid;

                    var currentCategoryThird = (from a in DB.product_subclass
                                                where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategoryThird = currentCategoryThird;
                    Session["thirdLevelId"] = currentCategoryThird.guid;
                    Session["fourLevelId"] = "1";

                    //對應產品(第三層)
                    var productData = (from a in DB.product
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();
                    foreach (var judgeCategory in productData)
                    {
                        if (judgeCategory.productsSubClass_guid != null)
                        {
                            int count = 0;
                            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                            foreach (var isCategory in categorySplit)
                            {
                                if (isCategory == category)
                                {
                                    count = 1;
                                }
                            }
                            if (count == 1)
                            {
                                resultProduct.Add(judgeCategory);
                            }
                            count = 0;
                        }
                    }
                    ViewBag.productData = resultProduct;
                }
                else if (judgeLevel == 4)
                {
                    //類別名稱
                    var tempCategoryGuid = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                            select a.productsCategory_guid).FirstOrDefault();
                    var categoryTitle = (from a in DB.product_category
                                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                                         select a).FirstOrDefault();
                    ViewBag.currentCategory = categoryTitle;
                    Session["firstLevelId"] = categoryTitle.guid;

                    var tempCategoryFour = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                            select a).FirstOrDefault();
                    ViewBag.currentCategoryFour = tempCategoryFour;
                    Session["fourLevelId"] = tempCategoryFour.guid;

                    var currentCategoryThird = (from a in DB.product_subclass
                                                where a.guid == tempCategoryFour.category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategoryThird = currentCategoryThird;
                    Session["thirdLevelId"] = currentCategoryThird.guid;

                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == currentCategoryThird.category && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;
                    Session["secondLevelId"] = currentCategorySecond.guid;

                    //對應產品(第四層)
                    var productData = (from a in DB.product
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();
                    foreach (var judgeCategory in productData)
                    {
                        if (judgeCategory.productsSubClass_guid != null)
                        {
                            int count = 0;
                            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                            foreach (var isCategory in categorySplit)
                            {
                                if (isCategory == category)
                                {
                                    count = 1;
                                }
                            }
                            if (count == 1)
                            {
                                resultProduct.Add(judgeCategory);
                            }
                            count = 0;
                        }
                    }
                    ViewBag.productData = resultProduct;
                }
                else
                {
                    //類別名稱
                    var tempCategoryGuid = (from a in DB.product_subclass
                                            where a.guid == category && a.status == "Y"
                                            select a.productsCategory_guid).FirstOrDefault();
                    var categoryTitle = (from a in DB.product_category
                                         where a.guid == tempCategoryGuid && a.status == "Y"
                                         select a).FirstOrDefault();
                    //ViewBag.currentCategory = categoryTitle.title;
                    ViewBag.currentCategory = categoryTitle;

                    wholeLang = categoryTitle.lang;
                    Session["WordLang"] = categoryTitle.lang;
                    if (categoryTitle.lang == "tw")
                    {
                        Session["WholeLang"] = "TW";
                    }
                    else if (categoryTitle.lang == "en")
                    {
                        Session["WholeLang"] = "EN";
                    }
                    else
                    {
                        Session["WholeLang"] = "EN";
                    }

                    //對應產品(第一層)
                    var productData = (from a in DB.product
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();
                    foreach (var judgeCategory in productData)
                    {
                        if (judgeCategory.productsSubClass_guid != null)
                        {
                            int count = 0;
                            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                            foreach (var isCategory in categorySplit)
                            {
                                var categoryGuid = (from a in DB.product_subclass
                                                    where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                                                    select a.productsCategory_guid).FirstOrDefault();

                                if (categoryGuid == categoryTitle.guid)
                                {
                                    count = 1;
                                }
                            }
                            if (count == 1)
                            {
                                resultProduct.Add(judgeCategory);
                            }
                            count = 0;
                        }
                    }
                    ViewBag.productData = resultProduct;
                }


                ViewBag.productData = resultProduct;
                if (resultProduct != null && resultProduct.Count != 0)
                {
                    var pageNumber = page ?? 1;
                    var pageSize = 12;
                    ViewBag.ProductPageList = resultProduct.ToPagedList(pageNumber, pageSize);
                    int Allpage = Convert.ToInt16(Math.Ceiling((double)resultProduct.Count / pageSize));
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + "products/productlist?category=" + category + "&", Allpage, pageNumber, resultProduct.Count, "");
                }

            }
            else
            {
                var productData = (from a in DB.product
                                   where a.status == "Y" && a.lang == wholeLang
                                   select a).OrderBy(x => x.sortIndex).ToList();
                foreach (var judgeCategory in productData)
                {
                    if (judgeCategory.productsSubClass_guid != null)
                    {
                        int count = 0;
                        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                        foreach (var isCategory in categorySplit)
                        {
                            var categoryGuid = (from a in DB.product_subclass
                                                where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                                                select a.productsCategory_guid).FirstOrDefault();

                            if (categoryGuid == category)
                            {
                                count = 1;
                            }
                        }
                        if (count == 1)
                        {
                            resultProduct.Add(judgeCategory);
                        }
                        count = 0;
                    }
                }
                ViewBag.productData = resultProduct;
                if (resultProduct != null && resultProduct.Count != 0)
                {
                    var pageNumber = page ?? 1;
                    var pageSize = 12;
                    ViewBag.ProductPageList = resultProduct.ToPagedList(pageNumber, pageSize);
                    int Allpage = Convert.ToInt16(Math.Ceiling((double)resultProduct.Count / pageSize));
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + "products/productlist?category=" + category + "&", Allpage, pageNumber, resultProduct.Count, "");
                }

                ViewBag.currentCategory = currentCategory;
            }

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }

        public ActionResult ProductDetial(string id)
        {
            ViewBag.dateNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            string category = "";
            if (Session["breadCategory"] != null)
            {
                category = Session["breadCategory"].ToString();
            }

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            List<string> splitFeature = new List<string>();
            List<string> splitColumn = new List<string>();
            List<string> splitValue = new List<string>();
            List<product> resultProduct = new List<product>();

            var productDetail = (from a in DB.product
                                 where a.lang == wholeLang && a.guid == id && a.status == "Y"
                                 select a).FirstOrDefault();

            if (productDetail != null)
            {
                ViewBag.description = productDetail.description;
                ViewBag.keywords = productDetail.keywords;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            //if (productDetail == null)
            //{
            //    if (wholeLang == "en")
            //    {
            //        Session["WordLang"] = "tw";
            //        Session["WholeLang"] = "TW";

            //        wholeLang = Session["WordLang"].ToString();
            //        productDetail = (from a in DB.product
            //                             where a.lang == wholeLang && a.guid == id && a.status == "Y"
            //                             select a).FirstOrDefault();

            //    }
            //    else
            //    {
            //        Session["WordLang"] = "en";
            //        Session["WholeLang"] = "EN";

            //        wholeLang = Session["WordLang"].ToString();
            //        productDetail = (from a in DB.product
            //                         where a.lang == wholeLang && a.guid == id && a.status == "Y"
            //                         select a).FirstOrDefault();
            //    }
            //}

            var productDetaillang = (from a in DB.product
                                     where a.guid == id && a.status == "Y"
                                     select a.lang).FirstOrDefault();
            if (productDetaillang == "en")
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";

                wholeLang = Session["WordLang"].ToString();
                productDetail = (from a in DB.product
                                 where a.lang == wholeLang && a.guid == id && a.status == "Y"
                                 select a).FirstOrDefault();

            }
            else
            {
                Session["WordLang"] = "tw";
                Session["WholeLang"] = "TW";

                wholeLang = Session["WordLang"].ToString();
                productDetail = (from a in DB.product
                                 where a.lang == wholeLang && a.guid == id && a.status == "Y"
                                 select a).FirstOrDefault();
            }


            ViewBag.productDetail = productDetail;

            if (productDetail != null)
            {
                string[] stringSeparators = new string[] { "\r\n" };
                var tempFeature = productDetail.product_features.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in tempFeature)
                {
                    splitFeature.Add(item);
                }
                ViewBag.splitFeature = splitFeature;

                var tempColumn = productDetail.specColumn.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in tempColumn)
                {
                    string[] stringSpecSeparators = new string[] { "||" };
                    var tempSpecColumnValue = item.Split(stringSpecSeparators, StringSplitOptions.RemoveEmptyEntries);
                    splitColumn.Add(tempSpecColumnValue[0]);
                    splitValue.Add(tempSpecColumnValue[1]);
                }
                ViewBag.splitColumn = splitColumn;
                ViewBag.splitValue = splitValue;

                //var tempValue = productDetail.specValue.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                //foreach (var item in tempValue)
                //{
                //    splitValue.Add(item);
                //}
                //ViewBag.splitValue = splitValue;

                var productEmblem = (from a in DB.enblem
                                     where a.lang == wholeLang && a.status == "Y"
                                     select a).ToList();
                ViewBag.productEmblem = productEmblem;

                var productFeature = (from a in DB.feature
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).ToList();
                ViewBag.productFeature = productFeature;

                List<product_subclass> psFirst = new List<product_subclass>();
                var productCatalogFirst = (from a in DB.product_category
                                           where a.status == "Y" && a.lang == wholeLang
                                           select a).OrderBy(x => x.sortIndex).ToList();

                foreach (var tempItem in productCatalogFirst)
                {
                    var tempProductSubclassFirst = (from a in DB.product_subclass
                                                    where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                    select a).FirstOrDefault();
                    psFirst.Add(tempProductSubclassFirst);
                }
                var productSubclassFirst = (from a in DB.product_subclass
                                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                            select a).ToList();
                ViewBag.productSubclassFirst = psFirst;
                //var productSubclassFirst = (from a in DB.product_subclass
                //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                //                            select a).OrderBy(x => x.sortIndex).ToList();
                //ViewBag.productSubclassFirst = productSubclassFirst;

                var productSubclassMultiLevel = (from a in DB.product_subclass
                                                 where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                                 select a).OrderBy(x => x.sortIndex).ToList();

                ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;


            }

            List<Dictionary<string, string>> Inquiry = new List<Dictionary<string, string>>();

            Dictionary<string, string> InquiryData = new Dictionary<string, string>();
            int samecount = 0;

            if (wholeLang == "tw")
            {
                if (Session["InquiryTw"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryTw"];
                }
            }
            else if (wholeLang == "en")
            {
                if (Session["InquiryEn"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                }
            }
            else
            {
                if (Session["InquiryEn"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                }
            }

            foreach (var inqData in Inquiry)
            {
                var tempProductGuid = inqData["ProductGuid"];
                if (tempProductGuid == productDetail.guid)
                {
                    samecount = 1;
                }
            }

            if (samecount == 1)
            {
                ViewBag.isCheckActive = 1;
            }
            else
            {
                ViewBag.isCheckActive = 0;
            }

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var currentCategory = (from a in DB.product_category
                                   where a.guid == category && a.lang == wholeLang
                                   select a).FirstOrDefault();
            if (currentCategory == null)
            {
                var subclassid = (from a in DB.product
                                  where a.guid == id && a.status == "Y" && a.lang == wholeLang
                                  select a.productsSubClass_guid).FirstOrDefault();
                var splitsubclassid = subclassid.Split(',');
                var level1subclassid = subclassid.Split(',')[0];
                product_subclass currentsubclass = new product_subclass();
                if (splitsubclassid.Length > 1)
                {
                    var tempcurrentsubclass = (from a in DB.product_subclass
                                               where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                               select a).FirstOrDefault();
                    if (tempcurrentsubclass == null)
                    {
                        currentsubclass = (from a in DB.product_subclass
                                           where a.guid == level1subclassid && a.status == "Y" && a.lang == wholeLang
                                           select a).FirstOrDefault();
                    }
                    else
                    {
                        if (tempcurrentsubclass.level == 1)
                        {
                            currentsubclass = (from a in DB.product_subclass
                                               where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                               select a).FirstOrDefault();
                        }
                        else
                        {
                            currentsubclass = (from a in DB.product_subclass
                                               where a.guid == category && a.status == "Y" && a.lang == wholeLang
                                               select a).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    currentsubclass = (from a in DB.product_subclass
                                       where a.guid == subclassid && a.status == "Y" && a.lang == wholeLang
                                       select a).FirstOrDefault();
                }


                if (currentsubclass.level == 1)
                {
                    //類別名稱
                    var currentCategoryFirst = (from a in DB.product_subclass
                                                where a.guid == currentsubclass.guid && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategory = currentCategoryFirst;
                }
                else if (currentsubclass.level == 2)
                {
                    //類別名稱
                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == currentsubclass.guid && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;

                    var currentCategoryFirst = (from a in DB.product_subclass
                                                where a.guid == currentCategorySecond.category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategory = currentCategoryFirst;
                }
                else if (currentsubclass.level == 3)
                {
                    //類別名稱
                    var currentCategoryThird = (from a in DB.product_subclass
                                                where a.guid == currentsubclass.guid && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategoryThird = currentCategoryThird;

                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == currentCategoryThird.category && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;

                    var currentCategoryFirst = (from a in DB.product_subclass
                                                where a.guid == currentCategorySecond.category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategory = currentCategoryFirst;
                }
                else if (currentsubclass.level == 4)
                {
                    //類別名稱
                    var tempCategoryFour = (from a in DB.product_subclass
                                            where a.guid == currentsubclass.guid && a.status == "Y" && a.lang == wholeLang
                                            select a).FirstOrDefault();
                    ViewBag.currentCategoryFour = tempCategoryFour;

                    var currentCategoryThird = (from a in DB.product_subclass
                                                where a.guid == tempCategoryFour.category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategoryThird = currentCategoryThird;

                    var currentCategorySecond = (from a in DB.product_subclass
                                                 where a.guid == currentCategoryThird.category && a.status == "Y" && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                    ViewBag.currentCategorySecond = currentCategorySecond;

                    var currentCategoryFirst = (from a in DB.product_subclass
                                                where a.guid == currentCategorySecond.category && a.status == "Y" && a.lang == wholeLang
                                                select a).FirstOrDefault();
                    ViewBag.currentCategory = currentCategoryFirst;
                }
                //var judgeLevel = (from a in DB.product_subclass
                //                  where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                  select a.level).FirstOrDefault();

                //if (judgeLevel == 1)
                //{
                //    //類別名稱
                //    var tempCategoryGuid = (from a in DB.product_subclass
                //                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                            select a.productsCategory_guid).FirstOrDefault();
                //    var categoryTitle = (from a in DB.product_category
                //                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                //                         select a).FirstOrDefault();
                //    ViewBag.currentCategory = categoryTitle;
                //    //對應產品(第一層)
                //    var productData = (from a in DB.product
                //                       where a.status == "Y" && a.lang == wholeLang
                //                       select a).OrderBy(x => x.sortIndex).ToList();
                //    foreach (var judgeCategory in productData)
                //    {
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            int count = 0;
                //            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //            foreach (var isCategory in categorySplit)
                //            {
                //                var categoryGuid = (from a in DB.product_subclass
                //                                    where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                //                                    select a.productsCategory_guid).FirstOrDefault();

                //                if (categoryGuid == categoryTitle.guid)
                //                {
                //                    count = 1;
                //                }
                //            }
                //            if (count == 1)
                //            {
                //                resultProduct.Add(judgeCategory);
                //            }
                //            count = 0;
                //        }
                //    }
                //    ViewBag.productData = resultProduct;
                //}
                //else if (judgeLevel == 2)
                //{
                //    //類別名稱
                //    var tempCategoryGuid = (from a in DB.product_subclass
                //                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                            select a.productsCategory_guid).FirstOrDefault();
                //    var categoryTitle = (from a in DB.product_category
                //                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                //                         select a).FirstOrDefault();
                //    ViewBag.currentCategory = categoryTitle;

                //    var currentCategorySecond = (from a in DB.product_subclass
                //                                 where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                                 select a).FirstOrDefault();
                //    ViewBag.currentCategorySecond = currentCategorySecond;
                //    //對應產品(第二層)
                //    var productData = (from a in DB.product
                //                       where a.status == "Y" && a.lang == wholeLang
                //                       select a).OrderBy(x => x.sortIndex).ToList();
                //    foreach (var judgeCategory in productData)
                //    {
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            int count = 0;
                //            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //            foreach (var isCategory in categorySplit)
                //            {
                //                if (isCategory == category)
                //                {
                //                    count = 1;
                //                }
                //            }
                //            if (count == 1)
                //            {
                //                resultProduct.Add(judgeCategory);
                //            }
                //            count = 0;
                //        }
                //    }
                //    ViewBag.productData = resultProduct;
                //}
                //else if (judgeLevel == 3)
                //{
                //    //類別名稱
                //    var tempCategoryGuid = (from a in DB.product_subclass
                //                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                            select a.productsCategory_guid).FirstOrDefault();
                //    var categoryTitle = (from a in DB.product_category
                //                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                //                         select a).FirstOrDefault();
                //    ViewBag.currentCategory = categoryTitle;

                //    var tempCategorySecond = (from a in DB.product_subclass
                //                              where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                              select a.category).FirstOrDefault();
                //    var currentCategorySecond = (from a in DB.product_subclass
                //                                 where a.guid == tempCategorySecond && a.status == "Y" && a.lang == wholeLang
                //                                 select a).FirstOrDefault();
                //    ViewBag.currentCategorySecond = currentCategorySecond;

                //    var currentCategoryThird = (from a in DB.product_subclass
                //                                where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                                select a).FirstOrDefault();
                //    ViewBag.currentCategoryThird = currentCategoryThird;
                //    //對應產品(第三層)
                //    var productData = (from a in DB.product
                //                       where a.status == "Y" && a.lang == wholeLang
                //                       select a).OrderBy(x => x.sortIndex).ToList();
                //    foreach (var judgeCategory in productData)
                //    {
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            int count = 0;
                //            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //            foreach (var isCategory in categorySplit)
                //            {
                //                if (isCategory == category)
                //                {
                //                    count = 1;
                //                }
                //            }
                //            if (count == 1)
                //            {
                //                resultProduct.Add(judgeCategory);
                //            }
                //            count = 0;
                //        }
                //    }
                //    ViewBag.productData = resultProduct;
                //}
                //else if (judgeLevel == 4)
                //{
                //    //類別名稱
                //    var tempCategoryGuid = (from a in DB.product_subclass
                //                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                            select a.productsCategory_guid).FirstOrDefault();
                //    var categoryTitle = (from a in DB.product_category
                //                         where a.guid == tempCategoryGuid && a.status == "Y" && a.lang == wholeLang
                //                         select a).FirstOrDefault();
                //    ViewBag.currentCategory = categoryTitle;

                //    var tempCategoryFour = (from a in DB.product_subclass
                //                            where a.guid == category && a.status == "Y" && a.lang == wholeLang
                //                            select a).FirstOrDefault();
                //    ViewBag.currentCategoryFour = tempCategoryFour;

                //    var currentCategoryThird = (from a in DB.product_subclass
                //                                where a.guid == tempCategoryFour.category && a.status == "Y" && a.lang == wholeLang
                //                                select a).FirstOrDefault();
                //    ViewBag.currentCategoryThird = currentCategoryThird;

                //    var currentCategorySecond = (from a in DB.product_subclass
                //                                 where a.guid == currentCategoryThird.category && a.status == "Y" && a.lang == wholeLang
                //                                 select a).FirstOrDefault();
                //    ViewBag.currentCategorySecond = currentCategorySecond;
                //    //對應產品(第四層)
                //    var productData = (from a in DB.product
                //                       where a.status == "Y" && a.lang == wholeLang
                //                       select a).OrderBy(x => x.sortIndex).ToList();
                //    foreach (var judgeCategory in productData)
                //    {
                //        if (judgeCategory.productsSubClass_guid != null)
                //        {
                //            int count = 0;
                //            var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                //            foreach (var isCategory in categorySplit)
                //            {
                //                if (isCategory == category)
                //                {
                //                    count = 1;
                //                }
                //            }
                //            if (count == 1)
                //            {
                //                resultProduct.Add(judgeCategory);
                //            }
                //            count = 0;
                //        }
                //    }
                //    ViewBag.productData = resultProduct;
                //}
                //else
                //{
                //}
            }
            else
            {
                var productData = (from a in DB.product
                                   where a.status == "Y" && a.lang == wholeLang
                                   select a).OrderBy(x => x.sortIndex).ToList();
                foreach (var judgeCategory in productData)
                {
                    if (judgeCategory.productsSubClass_guid != null)
                    {
                        int count = 0;
                        var categorySplit = judgeCategory.productsSubClass_guid.Split(',');
                        foreach (var isCategory in categorySplit)
                        {
                            var categoryGuid = (from a in DB.product_subclass
                                                where a.guid == isCategory && a.status == "Y" && a.lang == wholeLang
                                                select a.productsCategory_guid).FirstOrDefault();

                            if (categoryGuid == category)
                            {
                                count = 1;
                            }
                        }
                        if (count == 1)
                        {
                            resultProduct.Add(judgeCategory);
                        }
                        count = 0;
                    }
                }
                ViewBag.productData = resultProduct;

                ViewBag.currentCategory = currentCategory;
            }

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }
        public ActionResult DeleteInquiry(string guid)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var success = true;

            ViewBag.InquiryCount = 0;
            var count = 0;

            List<Dictionary<string, string>> Inquiry = new List<Dictionary<string, string>>();
            List<Dictionary<string, string>> InquiryR = new List<Dictionary<string, string>>();

            Dictionary<string, string> InquiryData = new Dictionary<string, string>();
            int samecount = 0;

            if (wholeLang == "en")
            {
                Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                if (Inquiry != null)
                {
                    Session.Remove("InquiryEn");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid != guid)
                    {
                        InquiryData.Add("ProductGuid", inqData["ProductGuid"]);
                        InquiryData.Add("Pic", inqData["Pic"]);
                        InquiryData.Add("ProductName", inqData["ProductName"]);
                        InquiryData.Add("ProductSubName", inqData["ProductSubName"]);
                        InquiryData.Add("lang", wholeLang);

                        InquiryR.Add(InquiryData);
                    }
                }

                ViewBag.InquiryCount = InquiryR.Count();
                count = InquiryR.Count();

                Session.Add("InquiryEn", InquiryR);
                Session.Add("InquiryNumEn", count);
            }
            else if (wholeLang == "tw")
            {
                Inquiry = (List<Dictionary<string, string>>)Session["InquiryTw"];
                if (Inquiry != null)
                {
                    Session.Remove("InquiryTw");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid != guid)
                    {
                        InquiryData.Add("ProductGuid", inqData["ProductGuid"]);
                        InquiryData.Add("Pic", inqData["Pic"]);
                        InquiryData.Add("ProductName", inqData["ProductName"]);
                        InquiryData.Add("ProductSubName", inqData["ProductSubName"]);
                        InquiryData.Add("lang", wholeLang);

                        InquiryR.Add(InquiryData);
                    }
                }

                ViewBag.InquiryCount = InquiryR.Count();
                count = InquiryR.Count();

                Session.Add("InquiryTw", InquiryR);
                Session.Add("InquiryNumTw", count);
            }
            else
            {
                Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                if (Inquiry != null)
                {
                    Session.Remove("InquiryEn");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid != guid)
                    {
                        InquiryData.Add("ProductGuid", inqData["ProductGuid"]);
                        InquiryData.Add("Pic", inqData["Pic"]);
                        InquiryData.Add("ProductName", inqData["ProductName"]);
                        InquiryData.Add("ProductSubName", inqData["ProductSubName"]);
                        InquiryData.Add("lang", wholeLang);

                        InquiryR.Add(InquiryData);
                    }
                }

                ViewBag.InquiryCount = InquiryR.Count();
                count = InquiryR.Count();

                Session.Add("InquiryEn", InquiryR);
                Session.Add("InquiryNumEn", count);
            }

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;


            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return RedirectToAction("RFQ", "Products");
        }
        public ActionResult AddInquiry(string ProductGuid)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var success = true;

            ViewBag.InquiryCount = 0;
            var count = 0;

            var productDetail = (from a in DB.product
                                 where a.lang == wholeLang && a.guid == ProductGuid
                                 select a).FirstOrDefault();

            List<Dictionary<string, string>> Inquiry = new List<Dictionary<string, string>>();

            Dictionary<string, string> InquiryData = new Dictionary<string, string>();
            int samecount = 0;

            if (wholeLang == "en")
            {
                if (Session["InquiryEn"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                    ViewBag.InquiryCount = Inquiry.Count();
                    count = Inquiry.Count();
                    Session.Remove("InquiryEn");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid == productDetail.guid)
                    {
                        samecount = 1;
                    }
                }

                if (samecount == 0)
                {
                    InquiryData.Add("ProductGuid", productDetail.guid);
                    InquiryData.Add("Pic", productDetail.pic.Split(',')[0]);
                    InquiryData.Add("ProductName", productDetail.title);
                    InquiryData.Add("ProductSubName", productDetail.subtitle);
                    InquiryData.Add("lang", wholeLang);

                    Inquiry.Add(InquiryData);

                    ViewBag.InquiryCount = ViewBag.InquiryCount + 1;
                    count = count + 1;
                }


                Session.Add("InquiryEn", Inquiry);
                Session.Add("InquiryNumEn", count);
            }
            else if (wholeLang == "tw")
            {
                if (Session["InquiryTw"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryTw"];
                    ViewBag.InquiryCount = Inquiry.Count();
                    count = Inquiry.Count();
                    Session.Remove("InquiryTw");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid == productDetail.guid)
                    {
                        samecount = 1;
                    }
                }

                if (samecount == 0)
                {
                    InquiryData.Add("ProductGuid", productDetail.guid);
                    InquiryData.Add("Pic", productDetail.pic.Split(',')[0]);
                    InquiryData.Add("ProductName", productDetail.title);
                    InquiryData.Add("ProductSubName", productDetail.subtitle);
                    InquiryData.Add("lang", wholeLang);

                    Inquiry.Add(InquiryData);

                    ViewBag.InquiryCount = ViewBag.InquiryCount + 1;
                    count = count + 1;
                }


                Session.Add("InquiryTw", Inquiry);
                Session.Add("InquiryNumTw", count);
            }
            else
            {
                if (Session["InquiryEn"] != null)
                {
                    Inquiry = (List<Dictionary<string, string>>)Session["InquiryEn"];
                    ViewBag.InquiryCount = Inquiry.Count();
                    count = Inquiry.Count();
                    Session.Remove("InquiryEn");
                }

                foreach (var inqData in Inquiry)
                {
                    var tempProductGuid = inqData["ProductGuid"];
                    if (tempProductGuid == productDetail.guid)
                    {
                        samecount = 1;
                    }
                }

                if (samecount == 0)
                {
                    InquiryData.Add("ProductGuid", productDetail.guid);
                    InquiryData.Add("Pic", productDetail.pic.Split(',')[0]);
                    InquiryData.Add("ProductName", productDetail.title);
                    InquiryData.Add("ProductSubName", productDetail.subtitle);
                    InquiryData.Add("lang", wholeLang);

                    Inquiry.Add(InquiryData);

                    ViewBag.InquiryCount = ViewBag.InquiryCount + 1;
                    count = count + 1;
                }


                Session.Add("InquiryEn", Inquiry);
                Session.Add("InquiryNumEn", count);
            }

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RFQ()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var contactSubjectData = (from a in DB.contactsubject
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a.title).ToList();
            ViewBag.contactSubjectData = contactSubjectData;

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }
        public ActionResult Search(string keyword)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            if (keyword != null && keyword != "")
            {
                var productData = DB.product.Where(m => m.title.Contains(keyword.Trim()) || m.subtitle.Contains(keyword.Trim())).Where(m => m.status == "Y").Where(m => m.lang == wholeLang);

                Session["productData"] = productData;
                Session["productDataCount"] = productData.Count();
                Session["productKeyword"] = keyword;
                var data = Session["productData"];

                if (data != null)
                {
                    ViewBag.productData = data;
                }
            }
            else
            {
                var data = Session["productData"];
                var count = Session["productDataCount"];
                var dataKeyword = Session["productKeyword"];

                if (data != null)
                {
                    ViewBag.productData = data;
                    ViewBag.productDataCount = count;
                    ViewBag.productKeyword = dataKeyword;
                }
            }

            //var productData = (from a in DB.product
            //                   where a.status == "Y" && a.lang == "en"
            //                   select a).OrderBy(x => x.sortIndex).ToList();
            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }
        public ActionResult SearchProduct(string keyword)
        {
            Model DB = new Model();

            var productData = (from a in DB.product
                               where a.status == "Y" && a.lang == "en"
                               select a).OrderBy(x => x.sortIndex).ToList();

            Session["productData"] = productData;

            //return RedirectToAction("SearchList", "Products");
            return Redirect("SearchList");
        }
        public ActionResult SearchList(string keyword)
        {
            var data = Session["productData"];
            Model DB = new Model();

            if (data != null)
            {
                ViewBag.productData = data;
            }

            return View();
        }
        public ActionResult RFQSend()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;
            //var productSubclassFirst = (from a in DB.product_subclass
            //                            where a.status == "Y" && a.lang == wholeLang && a.category == "0"
            //                            select a).OrderBy(x => x.sortIndex).ToList();
            //ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productsSearch = "產品搜尋";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productsSearch = "Products Search";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Send(FormCollection form)
        {
            Model DB = new Model();

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Session.Remove("form");
            Session.Add("form", form);

            if (form["vcode"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", "驗證碼比對錯誤");
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + "/products/RFQ"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                //return RedirectPermanent(Url.Content("~/" + "/Home/Alert"));
                return RedirectToAction("RFQ", "Products");
            }
            else
            {
                inquiry data = new inquiry();
                web_data webData = new web_data();


                data.guid = Guid.NewGuid().ToString();
                data.business_type = form["bType"].ToString();
                data.company_name = form["corp"].ToString();
                data.contact_name = form["name"].ToString();
                data.email = form["email"].ToString();
                data.tel = form["tel"].ToString();
                data.country = form["country"].ToString();
                data.address = form["adress"].ToString();
                data.website = form["web"].ToString();
                data.subject = form["subject"].ToString();
                data.message = form["content"].ToString();
                data.status = "N";
                data.lang = wholeLang;

                data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                Session.Remove("form");
                DB.inquiry_form.Add(data);
                DB.SaveChanges();

                var webdatamail = (from a in DB.web_data
                                   where a.lang == wholeLang
                                   select a).FirstOrDefault();
                List<string> MailList = new List<string>();
                MailList.Add(form["email"].ToString());
                if (webdatamail.staffmail != "")
                {
                    MailList.Add(webdatamail.staffmail);
                }
                if (webdatamail.managermail != "")
                {
                    MailList.Add(webdatamail.managermail);
                }

                FunctionService.sendMailIndex(MailList, "產品詢問表單", wholeLang, "/Content/Mail/contact.html", form);

                return RedirectToAction("RFQSend", "Products");
            }
        }
    }
}