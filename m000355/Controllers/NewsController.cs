﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Models;
using Web.Service;

namespace m000355.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index(int? page)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_news;
                ViewBag.keywords = privacyData.keywords_news;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            var now = DateTime.Now;
            //var newsData = DB.news.Where(x => x.lang == wholeLang && x.status == "Y" && x.category == "22021014324219"
            //                                  && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
            //                                  .OrderByDescending(x => x.sticky).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();
            var newsData = DB.news.Where(x => x.lang == wholeLang && x.status == "Y"
                                  && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                  .OrderByDescending(x => x.sticky).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();

            ViewBag.newsData = newsData;
            if (newsData != null && newsData.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 8;
                ViewBag.NewsPageList = newsData.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)newsData.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNumNoCatalog(Url.Content("~/") + "news?", Allpage, pageNumber, newsData.Count, "");
            }

            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.previousbtn = "上一則";
                //ViewBag.nextbtn = "下一則";
                //ViewBag.backbtn = "回前頁";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.previousbtn = "Previous";
                //ViewBag.nextbtn = "Next";
                //ViewBag.backbtn = "Back";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.previousbtn = "Previous";
                //ViewBag.nextbtn = "Next";
                //ViewBag.backbtn = "Back";
            }

            return View();
        }

        public ActionResult New(string guid)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;


            var now = DateTime.Now;
            var newsData = DB.news.Where(x => x.lang == wholeLang && x.status == "Y"
                                              && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                              .OrderByDescending(x => x.sticky).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();
            Dictionary<int, news> D = new Dictionary<int, news>();
            var num = 1;
            foreach (var item in newsData)
            {
                D.Add(num, item);
                num++;
            }
            var key = D.Where(x => x.Value.guid == guid).Select(x => x.Key).SingleOrDefault();
            var prev = D.Where(x => x.Key == (key - 1)).Select(x => x.Value).SingleOrDefault();
            if (prev == null)
            {
                prev = D.OrderByDescending(x => x.Key).Select(x => x.Value).FirstOrDefault();
            }

            var next = D.Where(x => x.Key == (key + 1)).Select(x => x.Value).SingleOrDefault();
            if (next == null)
            {
                next = D.OrderBy(x => x.Key).Select(x => x.Value).FirstOrDefault();
            }
            Dictionary<string, news> PrevNext = new Dictionary<string, news>();
            PrevNext.Add("Prev", prev);
            PrevNext.Add("Next", next);

            ViewBag.prevNews = PrevNext["Prev"];
            ViewBag.nextNews = PrevNext["Next"];

            DateTime datenow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var newData = (from a in DB.news
                           where a.guid == guid && a.lang == wholeLang && a.status == "Y"
                           select a).FirstOrDefault();
            ViewBag.newData = newData;

            if (newData != null)
            {
                ViewBag.description = newData.description;
                ViewBag.keywords = newData.keywords;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            if (newData != null)
            {
                if (newData.enddate < datenow)
                {
                    return RedirectToAction("Index", "News");
                }
            }
            else
            {
                return RedirectToAction("Index", "News");
            }


            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.previousbtn = "上一則";
                //ViewBag.nextbtn = "下一則";
                //ViewBag.backbtn = "回前頁";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.previousbtn = "Previous";
                //ViewBag.nextbtn = "Next";
                //ViewBag.backbtn = "Back";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.previousbtn = "Previous";
                //ViewBag.nextbtn = "Next";
                //ViewBag.backbtn = "Back";
            }

            return View();
        }
    }
}