﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Newtonsoft.Json;
using Web.Service;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI;
using Web.Repository;
using System.Configuration;
using Google.Apis.Auth.OAuth2;
using System.Data.Entity;
using System.Linq.Dynamic;
using web.Repository;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using web.Models;
using System.IO;
using Google.Apis.AnalyticsReporting.v4.Data;
//using Web.ViewModels;

namespace m000355.Controllers
{
    public class SiteadminController : Controller
    {
        DateRange dateRange = new DateRange { StartDate = DateTime.Now.ToString("yyyy") + "-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" };

        public string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        public string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString;

        private Model DB = new Model();
        //後台登入記錄方式 Session or Cookie

        /// <summary>
        /// 登入
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult login()
        {
            ViewBag.username = "";
            if (Request.Cookies["keepMeAccount"] != null)
            {
                HttpCookie aCookie = Request.Cookies["keepMeAccount"];

                string keepAdminID = Server.HtmlEncode(aCookie.Value.Replace("keepMeAccount=", ""));

                if (keepAdminID != null && keepAdminID != "")
                {
                    ViewBag.account = keepAdminID;
                }
            }

            //bool isLogin = FunctionService.systemUserCheck();
            //if (isLogin)
            //{
            //    return RedirectPermanent(Url.Content("~/siteadmin"));
            //}

            return View();
        }

        /// <summary>
        /// 後台首頁
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                //userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                //userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));
            }

            try
            {
                GoogleAnalyticsService GaData = new GoogleAnalyticsService();
                //ServiceAccountCredential credential = GaData.GoogleAnalytics();
                GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用

                //即時人數
                //var nowUser = GaData.nowUser(credential);
                //ViewBag.nowUser = nowUser.TotalsForAllResults["rt:pageviews"].ToString();
                try
                {
                    ////即時人數
                    //Google.Apis.Analytics.v3.Data.RealtimeData RealtimeData = GaData.nowUser();
                    //ViewBag.RealtimeData = RealtimeData.TotalsForAllResults["rt:pageviews"].ToString();

                    //GaParameter.endDate = DateTime.Now.ToString("yyyy") + "-12-31";//預設結束日
                    ViewBag.RealtimeData = GaData.GaTotalWebViewNum(new DateRange { StartDate = "2019-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" });
                }
                catch (Exception ex)
                {
                    ViewBag.IsGA = "N";

                    #region 產生LOG

                    FunctionService.saveLog(ex, "Ga.txt");

                    #endregion
                }

                //本月流量次數
                string thisMonth = int.Parse(DateTime.Now.ToString("MM")).ToString();
                ViewBag.ViewYearUseMonth = JsonConvert.DeserializeObject(GaData.ViewYearUseMonth(thisMonth));

                ////聯絡我們
                //ContactService Contact = new ContactService();
                //ViewBag.ContactList = Contact.GetBackEndIndexContact("tw");
                //ViewBag.ContactRole = Contact.CheckContactRole(ViewBag.permissionsTop);

                ////維修表單
                //ServiceService Service = new ServiceService();
                //ViewBag.ServiceList = Service.GetBackEndIndexService("tw");
                //ViewBag.ServiceRole = Service.CheckServiceRole(ViewBag.permissionsTop);
            }
            catch (Exception ex)
            {
            }

            return View();
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult list()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            //
            ViewBag.NextCategory = RouteData.Values["id"];
            ViewBag.PrevCategory = Session["dataTableCategory"];
            //

            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            Session.Remove("dataTableCategory");//移除暫存細項

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                //userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                //userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));
            }

            ViewBag.RetuenToList = "N";
            ViewBag.statusData = null;//狀態篩選顯示
            ViewBag.statusVal = null;//狀態篩選值

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();

                ViewBag.Tables = tables;

                ViewBag.addTitle = "";
                //DataTable用額外參數
                ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': ''}";
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': '" + RouteData.Values["id"].ToString() + "'}";

                    Session.Add("dataTableCategory", RouteData.Values["id"].ToString());//紀錄傳值

                    //取得標題

                    string reTables = ViewBag.Tables;

                    ViewBag.addTitle = TablesService.getDataTitle(reTables, RouteData.Values["id"].ToString());

                    ViewBag.RetuenToList = "Y";
                }
                /*if(ViewBag.forum_status != null)
                {
                    ViewBag.dataTableAddPostVal += ",{'name': 'forum_status', 'value': '" + ViewBag.forum_status.ToString() + "'}";
                }*/

                ViewBag.listMessage = TablesService.listMessage(tables);//取得列表說明

                //取得表頭
                ViewBag.dataTableTitle = TablesService.dataTableTitle(tables);
                ViewBag.columns = "";
                ViewBag.columnsLength = 0;//移除動作排序

                Dictionary<String, Object> defaultOrderBy = TablesService.defaultOrderBy(tables);//取得預設排續欄位名稱
                if (!defaultOrderBy.ContainsKey("orderByKey"))
                {
                    defaultOrderBy.Add("orderByKey", "create_date");
                    defaultOrderBy.Add("orderByType", "desc");
                }

                ViewBag.defaultOrderBy = 1;//預設排續欄位
                ViewBag.defaultOrderType = defaultOrderBy["orderByType"].ToString();//預設排續(asc或desc)

                foreach (KeyValuePair<string, object> item in ViewBag.dataTableTitle)
                {
                    ViewBag.columns += "{\"data\": \"" + item.Key + "\" },";
                    if (item.Key == defaultOrderBy["orderByKey"].ToString())
                    {
                        ViewBag.defaultOrderBy = ViewBag.columnsLength;
                    }
                    ViewBag.columnsLength++;
                }

                ViewBag.columnsLength = ViewBag.columnsLength - 1;//移除動作排序

                #region 取得檢視篩選

                Dictionary<String, Object> tempData = new Dictionary<string, object>();
                tempData.Add("nums", 1);

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), tempData);

                if (colData.ContainsKey("other") && ((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    Dictionary<String, Object> otherCol = (Dictionary<String, Object>)colData["other"];

                    if (otherCol["status"] != null && otherCol["status"].ToString() != "")
                    {
                        var mJObj = Newtonsoft.Json.Linq.JArray.Parse(otherCol["status"].ToString());
                        string[] statusData = mJObj[0]["data"].ToString().Split('/');//狀態篩選顯示
                        string[] statusVal = mJObj[0]["Val"].ToString().Split('/');//狀態篩選值

                        ViewBag.statusData = statusData;//狀態篩選顯示
                        ViewBag.statusVal = statusVal;//狀態篩選值
                    }
                }

                #endregion
            }
            return View();
        }

        /// <summary>
        /// 新增/修改
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult edit()
        {
            ViewBag.PrevCategory = RouteData.Values["id"];

            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            //權限
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            ViewBag.mainCol = null;
            ViewBag.contentCol = null;
            ViewBag.mediaCol = null;
            ViewBag.otherCol = null;
            ViewBag.guid = "";
            ViewBag.data = null;

            if (RouteData.Values["tables"] != null)
            {
                ViewBag.useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                //修改資料
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.guid = RouteData.Values["id"].ToString();
                    string Data = TablesService.getPresetData(RouteData.Values["tables"].ToString(), ViewBag.guid);

                    var obj = Newtonsoft.Json.Linq.JArray.Parse(Data.ToString());

                    if (ViewBag.useLang == "Y")
                    {
                        Dictionary<String, Object> langValData = new Dictionary<string, object>();
                        foreach (var item in obj)
                        {
                            langValData.Add(item["lang"].ToString(), item);
                        }

                        ViewBag.data = langValData;
                    }
                    else
                    {
                        ViewBag.data = obj[0];
                    }
                }

                ViewBag.Tables = RouteData.Values["tables"].ToString();

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), ViewBag.data);

                #region 基本欄位設定

                if (((Dictionary<String, Object>)colData["main"]).Count > 0)
                {
                    ViewBag.mainCol = colData["main"];
                }
                if (((Dictionary<String, Object>)colData).ContainsKey("content") == true && ((Dictionary<String, Object>)colData["content"]).Count > 0)
                {
                    ViewBag.contentCol = colData["content"];
                }
                if (((Dictionary<String, Object>)colData["media"]).Count > 0)
                {
                    ViewBag.mediaCol = colData["media"];
                }
                if (((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    ViewBag.otherCol = colData["other"];
                }
                if (colData.ContainsKey("content") == true && ((Dictionary<String, Object>)colData["content"]).Count > 0)
                {
                    ViewBag.contentCol = colData["content"];
                }

                #endregion

                #region 延伸欄位設定

                if (colData.ContainsKey("user") && ((Dictionary<String, Object>)colData["user"]).Count > 0)
                {
                    ViewBag.userCol = colData["user"];
                }

                if (colData.ContainsKey("review") && ((Dictionary<String, Object>)colData["review"]).Count > 0)
                {
                    ViewBag.reviewCol = colData["review"];
                }
                if (colData.ContainsKey("verify") && ((Dictionary<String, Object>)colData["verify"]).Count > 0)
                {
                    ViewBag.verifyCol = colData["verify"];
                }
                if (colData.ContainsKey("verifyForum") && ((Dictionary<String, Object>)colData["verifyForum"]).Count > 0)
                {
                    ViewBag.verifyColForum = colData["verifyForum"];
                }

                if (colData.ContainsKey("reInfo") && ((Dictionary<String, Object>)colData["reInfo"]).Count > 0)
                {
                    ViewBag.reInfoCol = colData["reInfo"];
                }
                if (colData.ContainsKey("reUser") && ((Dictionary<String, Object>)colData["reUser"]).Count > 0)
                {
                    ViewBag.reUserCol = colData["reUser"];
                }
                if (colData.ContainsKey("modifyer") && ((Dictionary<String, Object>)colData["modifyer"]).Count > 0)
                {
                    ViewBag.modifyerCol = colData["modifyer"];
                }

                if (colData.ContainsKey("delay") && ((Dictionary<String, Object>)colData["delay"]).Count > 0)
                {
                    ViewBag.delayCol = colData["delay"];
                }

                if (colData.ContainsKey("delay2") && ((Dictionary<String, Object>)colData["delay2"]).Count > 0)
                {
                    ViewBag.delay2Col = colData["delay2"];
                }

                if (colData.ContainsKey("viewSet") && ((Dictionary<String, Object>)colData["viewSet"]).Count > 0)
                {
                    ViewBag.viewSetCol = colData["viewSet"];
                }

                if (colData.ContainsKey("hidden") && ((Dictionary<String, Object>)colData["hidden"]).Count > 0)
                {
                    ViewBag.hiddenCol = colData["hidden"];
                }

                if (colData.ContainsKey("services") && ((Dictionary<String, Object>)colData["services"]).Count > 0)
                {
                    ViewBag.serviceCol = colData["services"];
                }

                #endregion
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult save(FormCollection form)
        {
            string retempid = "";
            if (RouteData.Values["tables"] != null)
            {
                string useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                //呼叫寫入或修改
                string guid = FunctionService.getGuid();
                string actType = "add";
                if (form["guid"].ToString() != "")
                {
                    guid = form["guid"].ToString();
                    actType = "edit";
                }

                Dictionary<String, Object> dic = new Dictionary<string, object>();
                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            Dictionary<String, Object> subForm = new Dictionary<string, object>();
                            subForm.Add("lang", item.lang);
                            foreach (var key in form.AllKeys)
                            {
                                if (key.Split('_')[0] == "id" && form[key].ToString() != "")
                                {
                                    retempid = form[key].ToString();
                                }
                            }

                            foreach (var key in form.AllKeys)
                            {
                                if (key.Replace("_" + item.lang, "") == "id" && form[key].ToString() == "")
                                {
                                    subForm.Add(key.Replace("_" + item.lang, ""), retempid);
                                }
                                else
                                {
                                    if (key == "create_date")
                                    {
                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString().Split(',')[0]);
                                    }
                                    else
                                    {
                                        if (RouteData.Values["tables"].ToString() == "product")
                                        {
                                            var tempemblemen = form["emblem_en"].ToString();
                                            var tempemblemtw = form["emblem_tw"].ToString();
                                            var tempemblemcn = form["emblem_cn"].ToString();
                                            var tempemblemall = form["emblem_en"].ToString() + "," + form["emblem_tw"].ToString();
                                            //emblem_tw
                                            if ((key == "emblem_tw") && item.lang == "tw")
                                            {
                                                if (tempemblemen != "")
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "emblem_cn") && item.lang == "cn")
                                            {
                                                if (tempemblemen != "")
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (key != "downloadfile_tw" && key != "title_tw" && key != "emblem_alt_tw" && key != "featurespic_alt_tw" && key != "featurespic_tw" && key != "pic_alt_tw" && key != "pic_tw" && key != "option_alt_tw" && key != "option_tw" && key != "videoLink_tw")
                                                {
                                                    if (item.lang != "cn")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key, form[key].ToString());
                                                    }
                                                }
                                            }

                                            var temptitleen = form["title_en"].ToString();
                                            var temptitletw = form["title_tw"].ToString();
                                            var temptitleall = form["title_en"].ToString() + "," + form["title_tw"].ToString();
                                            //emblem_alt_tw
                                            if ((key == "title_tw") && item.lang == "tw")
                                            {
                                                if (temptitleen != "")
                                                {
                                                    if (temptitletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), temptitleall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), temptitleen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (temptitletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "title_cn") && item.lang == "cn")
                                            {
                                                if (temptitleen != "")
                                                {
                                                    if (temptitletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), temptitleall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), temptitleen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (temptitletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            var tempemblemalten = form["emblem_alt_en"].ToString();
                                            var tempemblemalttw = form["emblem_alt_tw"].ToString();
                                            var tempemblemaltall = form["emblem_alt_en"].ToString() + "," + form["emblem_alt_tw"].ToString();
                                            //emblem_alt_tw
                                            if ((key == "emblem_alt_tw") && item.lang == "tw")
                                            {
                                                if (tempemblemen != "")
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "emblem_alt_cn") && item.lang == "cn")
                                            {
                                                if (tempemblemen != "")
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempemblemalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempemblemtw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            //特色圖片
                                            var tempfeaturesen = form["featurespic_en"].ToString();
                                            var tempfeaturestw = form["featurespic_tw"].ToString();
                                            var tempfeaturesall = form["featurespic_en"].ToString() + "," + form["featurespic_tw"].ToString();
                                            //featurespic_tw
                                            if ((key == "featurespic_tw") && item.lang == "tw")
                                            {
                                                if (tempfeaturesen != "")
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "featurespic_cn") && item.lang == "cn")
                                            {
                                                if (tempfeaturesen != "")
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            var tempfeaturespicalten = form["featurespic_alt_en"].ToString();
                                            var tempfeaturespicalttw = form["featurespic_alt_tw"].ToString();
                                            var tempfeaturespicaltall = form["featurespic_alt_en"].ToString() + "," + form["featurespic_alt_tw"].ToString();
                                            //featurespic_alt_tw
                                            if ((key == "featurespic_alt_tw") && item.lang == "tw")
                                            {
                                                if (tempfeaturesen != "")
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "featurespic_alt_cn") && item.lang == "cn")
                                            {
                                                if (tempfeaturesen != "")
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempfeaturesen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempfeaturestw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            //產品圖片
                                            var tempproducten = form["pic_en"].ToString();
                                            var tempproducttw = form["pic_tw"].ToString();
                                            var tempproductall = form["pic_en"].ToString() + "," + form["pic_tw"].ToString();
                                            //pic_tw
                                            if ((key == "pic_tw") && item.lang == "tw")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_cn") && item.lang == "cn")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            var tempproductalten = form["pic_alt_en"].ToString();
                                            var tempproductalttw = form["pic_alt_tw"].ToString();
                                            var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();
                                            //pic_alt_tw
                                            if ((key == "pic_alt_tw") && item.lang == "tw")
                                            {
                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_alt_cn") && item.lang == "cn")
                                            {
                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            //影片圖片
                                            var tempoptionen = form["option_en"].ToString();
                                            var tempoptiontw = form["option_tw"].ToString();
                                            var tempoptionall = form["option_en"].ToString() + "," + form["option_tw"].ToString();
                                            //option_tw
                                            if ((key == "option_tw") && item.lang == "tw")
                                            {
                                                if (tempoptionen != "")
                                                {
                                                    if (tempoptiontw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempoptiontw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "option_cn") && item.lang == "cn")
                                            {
                                                if (tempoptionen != "")
                                                {
                                                    if (tempoptiontw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempoptiontw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            var tempoptionalten = form["option_alt_en"].ToString();
                                            var tempoptionalttw = form["option_alt_tw"].ToString();
                                            var tempoptionaltall = form["option_alt_en"].ToString() + "," + form["option_alt_tw"].ToString();
                                            //option_alt_tw
                                            if ((key == "option_alt_tw") && item.lang == "tw")
                                            {
                                                if (tempoptionalten != "")
                                                {
                                                    if (tempoptionalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempoptionalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "option_alt_cn") && item.lang == "cn")
                                            {
                                                if (tempoptionalten != "")
                                                {
                                                    if (tempoptionalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempoptionalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempoptionalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            if ((key == "downloadfile_tw") && item.lang == "tw")
                                            {
                                                var tempproductfileen = form["downloadfile_en"].ToString();
                                                var tempproductfiletw = form["downloadfile_tw"].ToString();
                                                var tempproductfileall = form["downloadfile_en"].ToString() + "," + form["downloadfile_tw"].ToString();

                                                if (tempproductfileen != "")
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "downloadfile_cn") && item.lang == "cn")
                                            {
                                                var tempproductfileen = form["downloadfile_en"].ToString();
                                                var tempproductfiletw = form["downloadfile_tw"].ToString();
                                                var tempproductfileall = form["downloadfile_en"].ToString() + "," + form["downloadfile_tw"].ToString();

                                                if (tempproductfileen != "")
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            if ((key == "videoLink_tw") && item.lang == "tw")
                                            {
                                                var tempproductfileen = form["videoLink_en"].ToString();
                                                var tempproductfiletw = form["videoLink_tw"].ToString();
                                                var tempproductfileall = form["videoLink_en"].ToString() + "," + form["videoLink_tw"].ToString();

                                                if (tempproductfileen != "")
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "videoLink_cn") && item.lang == "cn")
                                            {
                                                var tempproductfileen = form["videoLink_en"].ToString();
                                                var tempproductfiletw = form["videoLink_tw"].ToString();
                                                var tempproductfileall = form["videoLink_en"].ToString() + "," + form["videoLink_tw"].ToString();

                                                if (tempproductfileen != "")
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductfiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductfiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                        }
                                        else if (RouteData.Values["tables"].ToString() == "catalog")
                                        {
                                            //產品圖片
                                            //pic_tw
                                            if ((key == "pic_en") && item.lang == "en")
                                            {
                                                var tempproducten = form["pic_en"].ToString();
                                                var tempproducttw = form["pic_tw"].ToString();
                                                var tempproductall = form["pic_en"].ToString() + "," + form["pic_tw"].ToString();

                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            //pic_alt_tw
                                            else if ((key == "pic_alt_en") && item.lang == "en")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "file_en") && item.lang == "en")
                                            {
                                                var tempproducten = form["file_en"].ToString();
                                                var tempproducttw = form["file_tw"].ToString();
                                                var tempproductall = form["file_en"].ToString() + "," + form["file_tw"].ToString();

                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            //pic_alt_tw
                                            else if ((key == "file_alt_en") && item.lang == "en")
                                            {
                                                var tempproductalten = form["file_alt_en"].ToString();
                                                var tempproductalttw = form["file_alt_tw"].ToString();
                                                var tempproductaltall = form["file_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                            }
                                        }
                                        else if (RouteData.Values["tables"].ToString() == "news")
                                        {
                                            //產品圖片
                                            var tempproducten = form["pic_en"].ToString();
                                            var tempproducttw = form["pic_tw"].ToString();
                                            var tempproductall = form["pic_en"].ToString() + "," + form["pic_tw"].ToString();
                                            //pic_tw
                                            if ((key == "pic_en") && item.lang == "en")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_cn") && item.lang == "cn")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (key != "pic_alt_en" && key != "pic_en")
                                                {
                                                    if (item.lang != "cn")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key, form[key].ToString());
                                                    }
                                                }
                                            }

                                            //pic_alt_tw
                                            if ((key == "pic_alt_en") && item.lang == "en")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_alt_cn") && item.lang == "cn")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            //else
                                            //{
                                            //    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                            //}
                                        }
                                        else if (RouteData.Values["tables"].ToString() == "home_banner")
                                        {
                                            //產品mobile圖片
                                            var tempproductmobileen = form["picmobile_en"].ToString();
                                            var tempproductmobiletw = form["picmobile_tw"].ToString();
                                            var tempproductmobileall = form["picmobile_en"].ToString() + "," + form["picmobile_tw"].ToString();
                                            //pic_tw
                                            if ((key == "picmobile_en") && item.lang == "en")
                                            {
                                                if (tempproductmobileen != "")
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "picmobile_cn") && item.lang == "cn")
                                            {
                                                if (tempproductmobileen != "")
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (key != "picmobile_alt_en" && key != "picmobile_en" && key != "pic_alt_en" && key != "pic_en")
                                                {
                                                    if (item.lang != "cn")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key, form[key].ToString());
                                                    }
                                                }
                                            }

                                            //picmobile_alt_tw
                                            if ((key == "picmobile_alt_en") && item.lang == "en")
                                            {
                                                var tempproductaltmobileen = form["picmobile_alt_en"].ToString();
                                                var tempproductaltmobiletw = form["picmobile_alt_tw"].ToString();
                                                var tempproductaltmobileall = form["picmobile_alt_en"].ToString() + "," + form["picmobile_alt_tw"].ToString();

                                                if (tempproductaltmobileen != "")
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "picmobile_alt_cn") && item.lang == "cn")
                                            {
                                                var tempproductaltmobileen = form["picmobile_alt_en"].ToString();
                                                var tempproductaltmobiletw = form["picmobile_alt_tw"].ToString();
                                                var tempproductaltmobileall = form["picmobile_alt_en"].ToString() + "," + form["picmobile_alt_tw"].ToString();

                                                if (tempproductaltmobileen != "")
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }

                                            //產品圖片
                                            var tempproducten = form["pic_en"].ToString();
                                            var tempproducttw = form["pic_tw"].ToString();
                                            var tempproductall = form["pic_en"].ToString() + "," + form["pic_tw"].ToString();
                                            //pic_tw
                                            if ((key == "pic_en") && item.lang == "en")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_cn") && item.lang == "cn")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }


                                            //pic_alt_tw
                                            if ((key == "pic_alt_en") && item.lang == "en")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_alt_cn") && item.lang == "cn")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            //else
                                            //{
                                            //    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                            //}
                                        }
                                        else if (RouteData.Values["tables"].ToString() == "product_banner")
                                        {
                                            //產品mobile圖片
                                            var tempproductmobileen = form["picmobile_en"].ToString();
                                            var tempproductmobiletw = form["picmobile_tw"].ToString();
                                            var tempproductmobilecn = form["picmobile_cn"].ToString();
                                            var tempproductmobileall = form["picmobile_en"].ToString() + "," + form["picmobile_tw"].ToString();
                                            //pic_tw
                                            if ((key == "picmobile_en") && item.lang == "en")
                                            {
                                                if (tempproductmobileen != "")
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        if (tempproductmobilecn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobilecn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "picmobile_cn") && item.lang == "cn")
                                            {
                                                if (tempproductmobileen != "")
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        if (tempproductmobilecn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobilecn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductmobilecn);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (key != "picmobile_alt_en" && key != "picmobile_en" && key != "pic_alt_en" && key != "pic_en")
                                                {
                                                    if (item.lang != "cn")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key, form[key].ToString());
                                                    }
                                                }
                                            }

                                            //picmobile_alt_tw
                                            if ((key == "picmobile_alt_en") && item.lang == "en")
                                            {
                                                var tempproductaltmobileen = form["picmobile_alt_en"].ToString();
                                                var tempproductaltmobiletw = form["picmobile_alt_tw"].ToString();
                                                var tempproductaltmobilealtcn = form["picmobile_alt_cn"].ToString();
                                                var tempproductaltmobileall = form["picmobile_alt_en"].ToString() + "," + form["picmobile_alt_tw"].ToString();

                                                if (tempproductaltmobileen != "")
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        if (tempproductaltmobilealtcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobilealtcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "picmobile_alt_cn") && item.lang == "cn")
                                            {
                                                var tempproductaltmobileen = form["picmobile_alt_en"].ToString();
                                                var tempproductaltmobiletw = form["picmobile_alt_tw"].ToString();
                                                var tempproductaltmobilealtcn = form["picmobile_alt_cn"].ToString();
                                                var tempproductaltmobileall = form["picmobile_alt_en"].ToString() + "," + form["picmobile_alt_tw"].ToString();

                                                if (tempproductaltmobileen != "")
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        if (tempproductaltmobilealtcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobilealtcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobileen);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductaltmobiletw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobiletw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltmobilealtcn);
                                                    }
                                                }
                                            }

                                            //產品圖片
                                            var tempproducten = form["pic_en"].ToString();
                                            var tempproducttw = form["pic_tw"].ToString();
                                            var tempproductcn = form["pic_cn"].ToString();
                                            var tempproductall = form["pic_en"].ToString() + "," + form["pic_tw"].ToString();
                                            //pic_tw
                                            if ((key == "pic_en") && item.lang == "en")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        if (tempproductcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_cn") && item.lang == "cn")
                                            {
                                                if (tempproducten != "")
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        if (tempproductcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproducttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproducttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductcn);
                                                    }
                                                }
                                            }


                                            //pic_alt_tw
                                            if ((key == "pic_alt_en") && item.lang == "en")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltcn = form["pic_alt_cn"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        if (tempproductaltcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                        }                                                        
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                                    }
                                                }
                                            }
                                            else if ((key == "pic_alt_cn") && item.lang == "cn")
                                            {
                                                var tempproductalten = form["pic_alt_en"].ToString();
                                                var tempproductalttw = form["pic_alt_tw"].ToString();
                                                var tempproductaltcn = form["pic_alt_cn"].ToString();
                                                var tempproductaltall = form["pic_alt_en"].ToString() + "," + form["pic_alt_tw"].ToString();

                                                if (tempproductalten != "")
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        if (tempproductaltcn.Split(',')[0] != "")
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltcn);
                                                        }
                                                        else
                                                        {
                                                            subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltall);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalten);
                                                    }
                                                }
                                                else
                                                {
                                                    if (tempproductalttw != "")
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductalttw);
                                                    }
                                                    else
                                                    {
                                                        subForm.Add(key.Replace("_" + item.lang, ""), tempproductaltcn);
                                                    }
                                                }
                                            }
                                            //else
                                            //{
                                            //    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                            //}
                                        }
                                        else
                                        {
                                            subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                        }
                                    }
                                }
                            }

                            if (actType == "add")
                            {
                                subForm.Remove("id");
                            }

                            dic.Add(item.lang, subForm);
                        }
                    }
                }
                else
                {
                    foreach (var key in form.AllKeys)
                    {
                        if (key == "create_date")
                        {
                            dic.Add(key, form[key].ToString().Split(',')[0]);
                        }
                        else
                        {
                            dic.Add(key, form[key].ToString());
                        }
                    }
                }

                string Field = "";
                if (RouteData.Values["tables"].ToString() == "forum_message")
                {
                    Field = "forum_message";
                }

                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic[item.lang]);
                            TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic[item.lang]);
                        }
                    }
                }
                else
                {
                    string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic);
                    TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic);
                }

                //完成轉跳
                TempData["success"] = true;
                switch (RouteData.Values["tables"].ToString())
                {
                    case "web_data":
                    case "smtp_data":
                    case "terms":
                    case "privacy":
                    case "contact_content":
                    case "service_content":
                    case "recruiting_content":
                    case "pdf_setting":
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid);
                        break;

                    default:
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/list/");
                        break;
                }
                if (RouteData.Values["tables"].ToString() == "product")
                {
                    var tempisaddguid = (from a in DB.product
                                         where a.guid == guid
                                         select a).ToList();
                    if (tempisaddguid.Count() == 0)
                    {
                        Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/list/") + "';</script>");
                    }
                    else
                    {
                        Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid) + "';</script>");
                    }
                }
                else
                {
                    Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid) + "';</script>");
                }
            }
            return null;
        }

        /// <summary>
        /// DataTable
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string dataTable()
        {
            //
            var PrevGuid = Session["dataTableCategory"];
            //

            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> listData = new Dictionary<string, object>();

            string json = "";

            dynamic data = null;

            if (RouteData.Values["tables"] != null)
            {
                NameValueCollection requests = Request.Params;

                //data = TablesService.getListData(RouteData.Values["tables"].ToString(), requests, Url.Content("~/"));
                data = TablesService.getListData(RouteData.Values["tables"].ToString(), requests, Url.Content("~/"), PrevGuid);
            }

            listData = data;

            json = JsonConvert.SerializeObject(listData, Formatting.Indented);

            return json;
        }
        public ActionResult excel()
        {
            //取得資料
            List<warranty_register_detail> result = DB.growarranty_register_detailup.OrderByDescending(m => m.create_date).ToList();

            //建立Excel
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(); //建立活頁簿
            ISheet sheet = hssfworkbook.CreateSheet("sheet"); //建立sheet

            //設定樣式
            ICellStyle headerStyle = hssfworkbook.CreateCellStyle();
            IFont headerfont = hssfworkbook.CreateFont();
            headerStyle.Alignment = HorizontalAlignment.Center; //水平置中
            headerStyle.VerticalAlignment = VerticalAlignment.Center; //垂直置中

            headerfont.FontName = "微軟正黑體";
            headerfont.FontHeightInPoints = 12;
            headerfont.Boldweight = (short)FontBoldWeight.Bold;
            headerStyle.SetFont(headerfont);


            //新增標題列
            sheet.CreateRow(0); //需先用CreateRow建立,才可通过GetRow取得該欄位
                                // sheet.AddMergedRegion(new CellRangeAddress(0, 1, 0, 2)); //合併1~2列及A~C欄儲存格
                                // sheet.GetRow(0).CreateCell(0).SetCellValue("昕力大學");
                                //sheet.GetRow(0).GetCell(0).CellStyle = headerStyle; //套用樣式


            List<string> titles = new List<string>
            {
                "註冊時間",
                "姓名",
                "E-mail",
                "電話",
                "公司",
                "地址",
                "型號",
                "序號",
                "購買日期",
                "購買通路",
            };

            for (int i = 0; i < titles.Count; i++)
            {
                sheet.GetRow(0).CreateCell(i).SetCellValue(titles[i].ToString());
                sheet.GetRow(0).GetCell(i).CellStyle = headerStyle;
                sheet.AutoSizeColumn(i);

            }

            //填入資料         
            int rowIndex = 1;
            foreach (warranty_register_detail item in result)
            {
                sheet.CreateRow(rowIndex).CreateCell(0).SetCellValue((DateTime)item.create_date);
                sheet.GetRow(rowIndex).CreateCell(1).SetCellValue(item.firstname);
                sheet.GetRow(rowIndex).CreateCell(2).SetCellValue(item.email);
                sheet.GetRow(rowIndex).CreateCell(3).SetCellValue(item.tel);
                sheet.GetRow(rowIndex).CreateCell(4).SetCellValue(item.company);
                sheet.GetRow(rowIndex).CreateCell(5).SetCellValue(item.address);
                sheet.GetRow(rowIndex).CreateCell(6).SetCellValue(item.modelnumber);
                sheet.GetRow(rowIndex).CreateCell(7).SetCellValue(item.serialnumber);
                sheet.GetRow(rowIndex).CreateCell(8).SetCellValue(item.purchasedate);
                sheet.GetRow(rowIndex).CreateCell(9).SetCellValue(item.buyingchannel);


                rowIndex++;
            }


            //for (int i = 0; i <= result.Count; i++)
            //{
            //    for (int a = 0; a <= 15; a++)
            //    {
            //        sheet.GetRow(i).GetCell(a).CellStyle = headerStyle;
            //    }
            //}


            var excelDatas = new MemoryStream();
            hssfworkbook.Write(excelDatas);

            return File(excelDatas.ToArray(), "application/vnd.ms-excel", string.Format($"保修查詢" + DateTime.Now.ToString("yyyyMMdd") + ".xls"));

        }
        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {
                #region 徹底刪除

                case "removeData":
                    System.Web.HttpContext contextRemove = System.Web.HttpContext.Current;
                    string currentUserLangRemove = "";
                    if (contextRemove.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = contextRemove.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');

                        currentUserLangRemove = contextRemove.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                    }

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).Where(m => m.lang == currentUserLangRemove).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "' and lang = '" + currentUserLangRemove + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "' and lang = '" + currentUserLangRemove + "'");
                                    break;
                            }

                            if (getData["type"].ToString() == "del")
                            {
                                DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "' and lang = '" + currentUserLangRemove + "'");
                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "' and lang = '" + currentUserLangRemove + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "' and lang = '" + currentUserLangRemove + "'");
                        }
                    }
                    break;

                #endregion

                #region 還原

                case "recoverData":

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "'");
                                    break;
                            }
                            if (getData["type"].ToString() == "del")
                            {
                                switch (ashcan.tables)
                                {
                                    case "insurance_content_add":
                                    case "insurance_content_main":
                                        DB.Database.ExecuteSqlCommand("delete from [" + "insurance_content" + "] where guid = '" + ashcan.from_guid + "'");
                                        break;
                                    case "insurance_company_content_add":
                                    case "insurance_company_content_main":
                                        DB.Database.ExecuteSqlCommand("delete from [" + "insurance_company_content" + "] where guid = '" + ashcan.from_guid + "'");
                                        break;
                                    default:
                                        DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                                        break;
                                }



                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "'");
                        }
                    }
                    break;

                #endregion

                #region 修改狀態

                case "changeStatus":
                    System.Web.HttpContext contextChange = System.Web.HttpContext.Current;
                    string currentUserLangChange = "";
                    if (contextChange.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = contextChange.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');

                        currentUserLangChange = contextChange.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                    }

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系

                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("status", getData["status"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("status", getData["status"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                            }
                        }

                        //資源回收桶
                        if (getData["status"].ToString() == "D")
                        {
                            web.Models.ashcan ashcan = new web.Models.ashcan();
                            for (int i = 0; i < guidArr.Length; i++)
                            {
                                ashcan.title = titleArr[i].ToString();
                                ashcan.tables = getData["tables"].ToString();
                                ashcan.from_guid = guidArr[i].ToString();
                                ashcan.create_date = DateTime.Now;
                                ashcan.modifydate = DateTime.Now;
                                ashcan.lang = currentUserLangChange;
                                DB.ashcan.Add(ashcan);

                                DB.SaveChanges();
                            }
                        }

                        dic["status"] = getData["status"].ToString();
                    }
                    break;

                #endregion

                #region 修改排序

                case "cahngeSortIndex":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系
                        string[] guidArr = getData["guid"].ToString().Split(',');

                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            /*Dictionary<String, Object> temp = new Dictionary<string, object>();
                            temp.Add("guid", guidArr[i].ToString());
                            temp.Add("sortIndex", getData["sortIndex"].ToString());
                            string jsonData = JsonConvert.SerializeObject(temp);
                            //呼叫寫入或修改
                            string guid = TablesService.saveData(getData["tables"].ToString(), guidArr[i].ToString(), "edit", jsonData, "sortIndex");
                            */
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("sortIndex", getData["sortIndex"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("sortIndex", getData["sortIndex"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                            }
                        }
                    }
                    break;

                #endregion

                #region 登入

                case "sysLogin":

                    if (getData != null)
                    {
                        dic["re"] = "OK";

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["re"] = "codeError";
                        }
                        else
                        {
                            string username = getData["username"].ToString();
                            string selectlang = getData["language"].ToString();

                            //var users = DB.user.Where(m => m.username == username).Where(m => m.lang == selectlang);
                            var users = DB.user.Where(m => m.username == username).Where(m => m.lang == selectlang);
                            if (users.Count() <= 0)
                            {
                                dic["re"] = "usernameError";
                            }
                            else
                            {
                                string password = FunctionService.md5(getData["password"].ToString());

                                users = users.Where(m => m.password == password);
                                if (users.Count() <= 0)
                                {
                                    dic["re"] = "passwordError";
                                }
                                else
                                {
                                    users = users.Where(m => m.status == "Y");
                                    if (users.Count() <= 0)
                                    {
                                        dic["re"] = "statusError";
                                    }
                                    else
                                    {
                                        if (adminCathType == "Session")
                                        {
                                            Session.Add("sysUsername", username);
                                            Session.Add("sysUserGuid", users.FirstOrDefault().guid);
                                            Session.Add("sysLanguage", selectlang);
                                        }
                                        else
                                        {
                                            //產生一個Cookie
                                            HttpCookie cookie = new HttpCookie("sysLogin");
                                            //設定過期日
                                            cookie.Expires = DateTime.Now.AddDays(365);
                                            cookie.Values.Add("sysUsername", username);//增加属性
                                            cookie.Values.Add("sysUserGuid", users.FirstOrDefault().guid);
                                            cookie.Values.Add("sysLanguage", selectlang);//增加属性
                                            Response.AppendCookie(cookie);//确定写入cookie中
                                        }

                                        FunctionService.getAdminPermissions(username);//權限
                                        //更新登入日期
                                        //var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").Where(m => m.lang == selectlang).FirstOrDefault();
                                        var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").Where(m => m.lang == selectlang).FirstOrDefault();
                                        saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                        DB.SaveChanges();
                                    }

                                    if (getData["keepMe"] != null)
                                    {
                                        if (getData["keepMe"] == "Y")
                                        {
                                            //產生一個Cookie
                                            HttpCookie cookie2 = new HttpCookie("keepMeAccount");
                                            //設定單值
                                            cookie2.Value = getData["username"].ToString();
                                            //設定過期日
                                            cookie2.Expires = DateTime.Now.AddDays(365);
                                            //寫到用戶端
                                            Response.Cookies.Add(cookie2);
                                        }
                                    }
                                }
                            }

                            dic["url"] = Url.Content("~/siteadmin");
                        }
                    }
                    break;

                #endregion

                case "changeproducts":
                    System.Web.HttpContext context = System.Web.HttpContext.Current;
                    string currentUserLang = "";
                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');

                        currentUserLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                    }
                    if (getData != null)
                    {
                        ProductsSubClassService ProductsSubClass = new ProductsSubClassService();
                        var categoryguid = getData["categoryguid"].ToString();
                        var guid = getData["guid"].ToString();
                        if (getData["table"].ToString() == "products" || getData["table"].ToString() == "item_number")
                        {
                            var subclass = ProductsSubClass.GetProductsSubClassByGuid_OrderbyLevel23(currentUserLang, categoryguid, guid);
                            //dic["select"] = "<option value=\"\">請選擇</option>";
                            dic["select"] = "<option value=\"\">--Select--</option>";
                            foreach (var item in subclass)
                            {
                                var pic = "";
                                for (var i = 1; i <= item.level; i++)
                                {
                                    pic += "∟";
                                }

                                if (guid != item.guid)
                                {
                                    dic["select"] += "<option value=\"" + item.guid + "\">" + pic + item.title + "</option>";
                                }
                            }
                        }
                        else
                        {
                            var subclass = ProductsSubClass.GetProductsSubClassByCategoryGuid_Guid(currentUserLang, categoryguid, guid);
                            //dic["select"] = "<option value=\"0\">根目錄</option>";
                            dic["select"] = "<option value=\"\">--Select--</option>";
                            foreach (var item in subclass)
                            {
                                var pic = "";
                                for (var i = 1; i <= item.level; i++)
                                {
                                    pic += "∟";
                                }

                                if (guid != item.guid)
                                {
                                    dic["select"] += "<option value=\"" + item.guid + "\">" + pic + item.title + "</option>";
                                }
                            }
                        }
                    }
                    break;

                case "changeproductsByitem":
                    System.Web.HttpContext context2 = System.Web.HttpContext.Current;
                    string currentUserLang2 = "";
                    if (context2.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = context2.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');

                        currentUserLang2 = context2.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                    }
                    if (getData != null)
                    {
                        ProductsService products = new ProductsService();
                        var subclassguid = getData["subclassguid"].ToString();
                        var p = products.GetProductsBySubClassGuid(currentUserLang2, subclassguid);

                        //dic["select"] = "<option value=\"\">請選擇</option>";
                        dic["select"] = "<option value=\"\">--Select--</option>";
                        foreach (var item in p)
                        {
                            dic["select"] += "<option value=\"" + item.guid + "\">" + item.title + "</option>";
                        }
                    }
                    break;
            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }

        [HttpGet]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public void CkAccount()
        {
            string username = Request["username"].ToString();
            var data = DB.user.Where(m => m.username == username).Count();
            if (data > 0)
            {
                Response.Write("false");
            }
            else
            {
                Response.Write("true");
            }
        }

        public ActionResult Logout()
        {
            Response.Cookies.Clear();

            //FormsAuthentication.SignOut();

            HttpCookie c = new HttpCookie("sysLogin");
            c.Values.Remove("adminDataID");
            c.Values.Remove("adminCategory");
            c.Values.Remove("adminUsername");
            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            Session.Clear();

            return RedirectPermanent(Url.Content("~/siteadmin/login"));
        }

        public string analytics()
        {
            var GaData = new GoogleAnalyticsService();
            string json = "";
            //switch (RouteData.Values["key"].ToString())
            //{
            //    case "year":
            //        json = GaData.ViewYear();
            //        break;

            //    case "browser":
            //        json = GaData.ViewBrowser();
            //        break;
            //}

            return json;
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string currentLang = "";
            //登入判斷
            ViewBag.username = "";
            ViewBag.role_guid = null;

            //ViewBag.defLang = defLang;

            ViewBag.permissions = Session["permissions"];
            ViewBag.permissionsTop = Session["permissionsTop"];

            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                if (tempAccount.Length == 2)
                {
                    currentLang = "en";
                }
                else
                {
                    currentLang = context.Server.HtmlEncode(tempAccount[2].Replace("sysLanguage=", "").ToString());
                }

                //userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                //userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));

                //ViewBag.sysUseraccount = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString());
                string tempSysUserGuid = context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString());
                var siteadminInfo = (from a in DB.user
                                     where a.guid == tempSysUserGuid && a.lang == currentLang
                                     select a).FirstOrDefault();

                string temproleguid1 = siteadminInfo.role_guid;
                string temproleguid2 = siteadminInfo.role2_guid;
                string temproleguid3 = siteadminInfo.role3_guid;

                if (currentLang == "en")
                {
                    ViewBag.sysLang = currentLang;
                }
                else if (currentLang == "tw")
                {
                    ViewBag.sysLang = "繁中";
                }
                else if (currentLang == "cn")
                {
                    ViewBag.sysLang = "簡中";
                }
                else
                {
                    ViewBag.sysLang = currentLang;
                }

                ViewBag.sysUseraccount = siteadminInfo.username;
                ViewBag.sysName = siteadminInfo.name;

                if (temproleguid1 != null)
                {
                    var roleInfo1 = (from a in DB.roles
                                     where a.guid == temproleguid1
                                     select a).FirstOrDefault();
                    ViewBag.sysGroup1 = roleInfo1.title;
                }

                if (temproleguid2 != null)
                {
                    var roleInfo2 = (from a in DB.roles
                                     where a.guid == temproleguid2
                                     select a).FirstOrDefault();
                    if (ViewBag.sysGroup2 == null)
                    {
                        ViewBag.sysGroup2 = "";
                    }
                    else
                    {
                        ViewBag.sysGroup2 = roleInfo2.title;
                    }
                }

                if (temproleguid3 != null)
                {
                    var roleInfo3 = (from a in DB.roles
                                     where a.guid == temproleguid3
                                     select a).FirstOrDefault();
                    if (ViewBag.sysGroup3 == null)
                    {
                        ViewBag.sysGroup3 = "";
                    }
                    else
                    {
                        ViewBag.sysGroup3 = roleInfo3.title;
                    }
                }
            }

            ViewBag.defLang = currentLang;
            ViewBag.currentLang = currentLang;

            ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").Where(m => m.lang == currentLang).ToList();
            ViewBag.system_menu_data = null;
            ViewBag.tables = "";

            NameValueCollection langData = new NameValueCollection();
            if (currentLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.loginlang = "語系";
                //ViewBag.loginaccount = "帳號";
                //ViewBag.loginname = "姓名";
                //ViewBag.logingroup = "權限群組1";
                //ViewBag.logingroup2 = "權限群組2";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.iconHome = "系統首頁";
                //ViewBag.onlineUser = "線上使用者";
                //ViewBag.browser = "瀏覽器使用";
                //ViewBag.yearCount = "語系及帳號資訊";
                //ViewBag.addButton = "新增";
                //ViewBag.batchButton = "批次";
                //ViewBag.batchDelButton = "刪除";
                //ViewBag.keywordSearchButton = "搜尋";
                //ViewBag.batchEnableButton = "啟用";
                //ViewBag.batchDisableButton = "停用";
                //ViewBag.batchRevertButton = "還原";
                //ViewBag.returnButton = "返回";
                //ViewBag.mainSetting = "主要設定";
                //ViewBag.accountDetail = "帳戶詳細資料";
                //ViewBag.media = "多媒體檔案";
                //ViewBag.inspectorSetting = "審核者設定";
                //ViewBag.verifyManage = "審核管理";
                //ViewBag.replyContent = "回覆內容";
                //ViewBag.respondentSetting = "回覆者設定";
                //ViewBag.advancedOption = "進階選項";
                //ViewBag.saveButton = "儲存";
                //ViewBag.submitButton = "送出";
                //ViewBag.clearButton = "重填";
                //ViewBag.logout = "登出";
                //ViewBag.mediaBtn = "媒體庫";
                //ViewBag.pictureAlt = "圖片敘述";
                //ViewBag.selectMulti1 = "選取全部";
                //ViewBag.selectMulti2 = "清除選取";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
            }
            else if (currentLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.loginlang = "Lang";
                //ViewBag.loginaccount = "Account";
                //ViewBag.loginname = "Name";
                //ViewBag.logingroup = "group1";
                //ViewBag.logingroup2 = "group2";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.iconHome = "System Home";
                //ViewBag.onlineUser = "Online User";
                //ViewBag.browser = "Browser";
                //ViewBag.yearCount = "Information";
                //ViewBag.addButton = "Add";
                //ViewBag.batchButton = "Batch";
                //ViewBag.batchDelButton = "Delete";
                //ViewBag.keywordSearchButton = "Search";
                //ViewBag.batchEnableButton = "Enable";
                //ViewBag.batchDisableButton = "Disable";
                //ViewBag.batchRevertButton = "Revert";
                //ViewBag.returnButton = "Return";
                //ViewBag.mainSetting = "Main Setting";
                //ViewBag.accountDetail = "Account Detail Info";
                //ViewBag.media = "Media";
                //ViewBag.inspectorSetting = "Inspector Setting";
                //ViewBag.verifyManage = "Verify Manage";
                //ViewBag.replyContent = "Reply Content";
                //ViewBag.respondentSetting = "Respondent Setting";
                //ViewBag.advancedOption = "Advanced Option";
                //ViewBag.saveButton = "Save";
                //ViewBag.submitButton = "Submit";
                //ViewBag.clearButton = "Clear";
                //ViewBag.logout = "LogOut";
                //ViewBag.mediaBtn = "Media";
                //ViewBag.pictureAlt = "Pic Alt";
                //ViewBag.selectMulti1 = "Select All";
                //ViewBag.selectMulti2 = "Clear Select";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }
            else if (currentLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.loginlang = "Lang";
                //ViewBag.loginaccount = "Account";
                //ViewBag.loginname = "Name";
                //ViewBag.logingroup = "group1";
                //ViewBag.logingroup2 = "group2";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.iconHome = "System Home";
                //ViewBag.onlineUser = "Online User";
                //ViewBag.browser = "Browser";
                //ViewBag.yearCount = "Information";
                //ViewBag.addButton = "Add";
                //ViewBag.batchButton = "Batch";
                //ViewBag.batchDelButton = "Delete";
                //ViewBag.keywordSearchButton = "Search";
                //ViewBag.batchEnableButton = "Enable";
                //ViewBag.batchDisableButton = "Disable";
                //ViewBag.batchRevertButton = "Revert";
                //ViewBag.returnButton = "Return";
                //ViewBag.mainSetting = "Main Setting";
                //ViewBag.accountDetail = "Account Detail Info";
                //ViewBag.media = "Media";
                //ViewBag.inspectorSetting = "Inspector Setting";
                //ViewBag.verifyManage = "Verify Manage";
                //ViewBag.replyContent = "Reply Content";
                //ViewBag.respondentSetting = "Respondent Setting";
                //ViewBag.advancedOption = "Advanced Option";
                //ViewBag.saveButton = "Save";
                //ViewBag.submitButton = "Submit";
                //ViewBag.clearButton = "Clear";
                //ViewBag.logout = "LogOut";
                //ViewBag.mediaBtn = "Media";
                //ViewBag.pictureAlt = "Pic Alt";
                //ViewBag.selectMulti1 = "Select All";
                //ViewBag.selectMulti2 = "Clear Select";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
            }

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();
                ViewBag.system_menu_data = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).Where(m => m.lang == currentLang).FirstOrDefault();
                string topCategory = ViewBag.system_menu_data.category;
                ViewBag.system_menu_top_data = DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault();
                ViewBag.tables = RouteData.Values["tables"].ToString();
            }

            ViewBag.language = null;
            ViewBag.viewLanguage = "語系";
            var language = DB.language.OrderBy(m => m.sortIndex).Where(m => m.status == "Y").ToList();
            if (language.Count > 0)
            {
                ViewBag.language = language;
                ViewBag.viewLanguage = language[0].title;
            }

            ViewBag.userData = FunctionService.ReUserData();//回傳使用者資訊

            //系統資訊
            Guid systemGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");
            ViewBag.systemData = DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault();
        }
    }
}