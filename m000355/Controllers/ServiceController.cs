﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.Models.ViewModels;
using Web.Models;
using Web.Service;

namespace m000355.Controllers
{
    public class ServiceController : Controller
    {
        // GET: Service
        public ActionResult Index()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.contactinfo = "聯絡資訊";
                //ViewBag.contactservice = "服務";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            return View();
        }
        public ActionResult Contactus()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_contactService;
                ViewBag.keywords = privacyData.keywords_contactService;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            contactServiceViewModel contactServiceVm = new contactServiceViewModel();
            List<contactServiceViewModel> contactServiceList = new List<contactServiceViewModel>();

            var contactusservice = (from a in DB.service
                                where a.lang == wholeLang && a.status == "Y"
                                select a).OrderByDescending(a=>a.create_date).ToList();
            for (var i = 0; i< contactusservice.Count(); i++)
            {
                contactServiceVm.title = contactusservice[i].title;
                contactServiceVm.subtitle = contactusservice[i].subtitle;
                contactServiceVm.email = contactusservice[i].email;
                contactServiceVm.telphone = contactusservice[i].tel;
                contactServiceVm.fax = contactusservice[i].fax;
                contactServiceVm.address = contactusservice[i].address;
                contactServiceVm.maparea = contactusservice[i].maparea;
                contactServiceVm.mapsitex = double.Parse(contactusservice[i].mapsitex);
                contactServiceVm.mapsitey = double.Parse(contactusservice[i].mapsitey);
                contactServiceVm.jmaparea = i+1;
                if (i+1 == 1)
                {
                    if (contactusservice[i].mapsitex != "")
                    {
                        ViewBag.mapx1 = double.Parse(contactusservice[i].mapsitex);
                    }
                    if (contactusservice[i].mapsitey != "")
                    {
                        ViewBag.mapy1 = double.Parse(contactusservice[i].mapsitey);
                    }                    
                }
                else if (i + 1 == 2)
                {
                    if (contactusservice[i].mapsitex != "")
                    {
                        ViewBag.mapx2 = double.Parse(contactusservice[i].mapsitex);
                    }
                    if (contactusservice[i].mapsitey != "")
                    {
                        ViewBag.mapy2 = double.Parse(contactusservice[i].mapsitey);
                    }
                }
                else if (i + 1 == 3)
                {
                    if (contactusservice[i].mapsitex != "")
                    {
                        ViewBag.mapx3 = double.Parse(contactusservice[i].mapsitex);
                    }
                    if (contactusservice[i].mapsitey != "")
                    {
                        ViewBag.mapy3 = double.Parse(contactusservice[i].mapsitey);
                    }
                }
                else if (i + 1 == 4)
                {
                    if (contactusservice[i].mapsitex != "")
                    {
                        ViewBag.mapx4 = double.Parse(contactusservice[i].mapsitex);
                    }
                    if (contactusservice[i].mapsitey != "")
                    {
                        ViewBag.mapy4 = double.Parse(contactusservice[i].mapsitey);
                    }
                }
                else
                {

                }
                contactServiceList.Add(contactServiceVm);
                contactServiceVm = new contactServiceViewModel();
            }


            ViewBag.contactusservice = contactServiceList;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.contactinfo = "聯絡資訊";
                //ViewBag.contactservice = "服務";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            return View();
        }
        public ActionResult Contactinfo()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var contactSubjectData = (from a in DB.contactsubject
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a.title).ToList();
            ViewBag.contactSubjectData = contactSubjectData;

            var contactCountryData = (from a in DB.contactCountry
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a.title).ToList();
            ViewBag.contactCountryData = contactCountryData;

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_contactinfo;
                ViewBag.keywords = privacyData.keywords_contactinfo;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassFirst = productSubclassFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.contactinfo = "聯絡資訊";
                //ViewBag.contactservice = "服務";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.contactinfo = "CONTACT INFO.";
                //ViewBag.contactservice = "SERVICE";
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Send(FormCollection form)
        {
            Model DB = new Model();

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Session.Remove("form");
            Session.Add("form", form);

            if (form["vcode"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", "驗證碼比對錯誤");
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);
                TempData["alertmessage"] = "驗證碼比對錯誤";

                //return RedirectPermanent(Url.Content("~/" + "/Home/Alert"));
                return RedirectToAction("Contactinfo", "Service");
            }
            else
            {
                contactinfo data = new contactinfo();
                web_data webData = new web_data();


                data.guid = Guid.NewGuid().ToString();
                if (wholeLang == "en")
                {
                    data.businesstype = form["bType"].ToString();
                    data.companyname = form["corp"].ToString();
                    data.country = form["country"].ToString();
                    data.address = form["adress"].ToString();
                    data.website = form["web"].ToString();
                }
                else
                {
                    data.businesstype = wholeLang;
                    data.companyname = wholeLang;
                    data.country = wholeLang;
                    data.address = wholeLang;
                    data.website = wholeLang;
                }

                data.contactname = form["name"].ToString();
                data.email = form["email"].ToString();
                data.tel = form["tel"].ToString();

                data.subject = form["subject"].ToString();
                data.message = form["content"].ToString();
                data.status = "N";
                data.lang = wholeLang;

                data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                Session.Remove("form");
                DB.contactinfo.Add(data);
                DB.SaveChanges();

                var webdatamail = (from a in DB.web_data
                                   where a.lang == wholeLang
                                   select a).FirstOrDefault();
                List<string> MailList = new List<string>();
                MailList.Add(form["email"].ToString());
                if (webdatamail.staffmail != "")
                {
                    MailList.Add(webdatamail.staffmail);
                }
                if (webdatamail.managermail != "")
                {
                    MailList.Add(webdatamail.managermail);
                }

                FunctionService.sendMailIndex(MailList, "客戶聯絡表單", wholeLang, "/Content/Mail/contact.html", form);

                TempData["alertmessage"] = "已成功寄出";

                return RedirectToAction("Contactinfo", "Service");
            }
        }
    }
}