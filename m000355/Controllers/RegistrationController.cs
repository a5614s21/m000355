﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using Web.Models;
using Web.Repository;
using Web.Service;

namespace m000355.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration
        public ActionResult Index()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            if (webBasicData != null)
            {
                ViewBag.description = webBasicData.description_warrantyregister;
                ViewBag.keywords = webBasicData.keywords_warrantyregister;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            List<string> warrantyModelMixData = new List<string>();
            var warrantyModelData = (from a in DB.product
                           where a.lang == wholeLang && a.status == "Y"
                           select a.subtitle).ToList();

            var warrantyModelData2 = (from a in DB.warrantyModel
                                     where a.lang == wholeLang && a.status == "Y"
                                     select a.title).ToList();
            foreach (var mixitem1 in warrantyModelData)
            {
                warrantyModelMixData.Add(mixitem1);
            }
            foreach (var mixitem2 in warrantyModelData2)
            {
                warrantyModelMixData.Add(mixitem2);
            }

            //var warrantyModelData = (from a in DB.warrantyModel
            //                    where a.lang == wholeLang && a.status == "Y"
            //                    select a.title).ToList();
            ViewBag.defWarrantyModelData = warrantyModelData[0];
            ViewBag.warrantyModelData = warrantyModelMixData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.registerproduct = "註冊您的產品";
                //ViewBag.warrantycheck = "保修查詢";
                //ViewBag.warrantypolicy = "保修政策";
                //ViewBag.firstname = "名";
                //ViewBag.lastname = "姓";
                //ViewBag.modelnumber = "型號";
                //ViewBag.serialnumber = "序號";
                //ViewBag.buyingchannel = "購買通路";
                //ViewBag.purchasedate = "購買日期";
                //ViewBag.proofofpurchase = "購買明細";
                //ViewBag.addnewitem = "新增新項目";
                //ViewBag.termscheck = "我理解、接受並同意";
                //ViewBag.fileuploadsize = "每個文檔的最大大小不得超過 5MB";
                //ViewBag.regsuccessful = "註冊成功";
                //ViewBag.regtime = "註冊時間";
                //ViewBag.regname = "姓名";
                //ViewBag.regcompany = "公司";
                //ViewBag.regproducts = "產品";
                //ViewBag.regaddress = "地址";
                //ViewBag.regbuychannel = "購買通路";
                //ViewBag.regpurchasedate = "購買日期";
                //ViewBag.regnumber = "註冊序號";
                //ViewBag.repproductname = "產品";
                //ViewBag.repmodnum = "型號";
                //ViewBag.repserialnum = "序號";
                //ViewBag.reppudate = "購買日期";
                //ViewBag.repdownload = "下載";
                //ViewBag.checkagain = "重新查詢";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }

            return View();
        }
        public ActionResult Repairs()
        {

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_warrantycheck;
                ViewBag.keywords = privacyData.keywords_warrantycheck;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            List<string> warrantyModelMixData = new List<string>();
            var warrantyModelData = (from a in DB.product
                                     where a.lang == wholeLang && a.status == "Y"
                                     select a.subtitle).ToList();

            var warrantyModelData2 = (from a in DB.warrantyModel
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a.title).ToList();
            foreach (var mixitem1 in warrantyModelData)
            {
                warrantyModelMixData.Add(mixitem1);
            }
            foreach (var mixitem2 in warrantyModelData2)
            {
                warrantyModelMixData.Add(mixitem2);
            }

            //var warrantyModelData = (from a in DB.warrantyModel
            //                    where a.lang == wholeLang && a.status == "Y"
            //                    select a.title).ToList();
            ViewBag.defWarrantyModelData = warrantyModelData[0];
            ViewBag.warrantyModelData = warrantyModelMixData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.registerproduct = "註冊您的產品";
                //ViewBag.warrantycheck = "保修查詢";
                //ViewBag.warrantypolicy = "保修政策";
                //ViewBag.firstname = "名";
                //ViewBag.lastname = "姓";
                //ViewBag.modelnumber = "型號";
                //ViewBag.serialnumber = "序號";
                //ViewBag.buyingchannel = "購買通路";
                //ViewBag.purchasedate = "購買日期";
                //ViewBag.proofofpurchase = "購買明細";
                //ViewBag.addnewitem = "新增新項目";
                //ViewBag.termscheck = "我理解、接受並同意";
                //ViewBag.fileuploadsize = "每個文檔的最大大小不得超過 5MB";
                //ViewBag.regsuccessful = "註冊成功";
                //ViewBag.regtime = "註冊時間";
                //ViewBag.regname = "姓名";
                //ViewBag.regcompany = "公司";
                //ViewBag.regproducts = "產品";
                //ViewBag.regaddress = "地址";
                //ViewBag.regbuychannel = "購買通路";
                //ViewBag.regpurchasedate = "購買日期";
                //ViewBag.regnumber = "註冊序號";
                //ViewBag.repproductname = "產品";
                //ViewBag.repmodnum = "型號";
                //ViewBag.repserialnum = "序號";
                //ViewBag.reppudate = "購買日期";
                //ViewBag.repdownload = "下載";
                //ViewBag.checkagain = "重新查詢";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }

            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult RepairsSend(FormCollection form)
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.registerproduct = "註冊您的產品";
                //ViewBag.warrantycheck = "保修查詢";
                //ViewBag.warrantypolicy = "保修政策";
                //ViewBag.firstname = "名";
                //ViewBag.lastname = "姓";
                //ViewBag.modelnumber = "型號";
                //ViewBag.serialnumber = "序號";
                //ViewBag.buyingchannel = "購買通路";
                //ViewBag.purchasedate = "購買日期";
                //ViewBag.proofofpurchase = "購買明細";
                //ViewBag.addnewitem = "新增新項目";
                //ViewBag.termscheck = "我理解、接受並同意";
                //ViewBag.fileuploadsize = "每個文檔的最大大小不得超過 5MB";
                //ViewBag.regsuccessful = "註冊成功";
                //ViewBag.regtime = "註冊時間";
                //ViewBag.regname = "姓名";
                //ViewBag.regcompany = "公司";
                //ViewBag.regproducts = "產品";
                //ViewBag.regaddress = "地址";
                //ViewBag.regbuychannel = "購買通路";
                //ViewBag.regpurchasedate = "購買日期";
                //ViewBag.regnumber = "註冊序號";
                //ViewBag.repproductname = "產品";
                //ViewBag.repmodnum = "型號";
                //ViewBag.repserialnum = "序號";
                //ViewBag.reppudate = "購買日期";
                //ViewBag.repdownload = "下載";
                //ViewBag.checkagain = "重新查詢";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }

            Session.Remove("form");
            Session.Add("form", form);

            if (form["vcode"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", "驗證碼比對錯誤");
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);
                Session.Add("alertTitle", "驗證碼比對錯誤");
                TempData["alertmessage"] = "驗證碼比對錯誤";

                return RedirectToAction("repairs", "Registration");
            }
            else
            {
                string modelnumber = form["mn"].ToString();
                string serialnumber = form["sn"].ToString();
                string tel = form["tel"].ToString();

                var searchDetailData = (from a in DB.growarranty_register_detailup
                                  where a.lang == wholeLang && a.modelnumber == modelnumber && a.serialnumber == serialnumber
                                  select a).ToList();

                foreach (var detailItem in searchDetailData)
                {
                    var searchData = (from a in DB.warranty_register
                                      where a.lang == wholeLang && a.guid == detailItem.warrantyRegister_guid && a.tel == tel
                                      select a).FirstOrDefault();
                    if (searchData != null)
                    {
                        var repairProductData = (from a in DB.product
                                                 where a.subtitle == modelnumber && a.lang == wholeLang
                                                 select a).FirstOrDefault();
                        if (repairProductData != null)
                        {
                            ViewBag.repairProductName = repairProductData.title;
                        }
                        else
                        {
                            ViewBag.repairProductName = "";
                        }
                        ViewBag.repairModelnumber = modelnumber;
                        ViewBag.repairRegTime = searchData.create_date;
                        ViewBag.repairPuchasedate = detailItem.purchasedate;
                        ViewBag.repairRegNumber = serialnumber;
                        ViewBag.repairRegNumberGuid = searchData.guid;
                        ViewBag.repairName = searchData.firstname + " " + searchData.lastname;
                        ViewBag.repairTel = searchData.tel;
                        ViewBag.repairCompany = searchData.company;
                        ViewBag.repairAddress = searchData.address;
                        ViewBag.repairBuyingChannel = detailItem.buyingchannel;
                        ViewBag.downloadfile = detailItem.proofofpruchase;
                    }
                    else
                    {
                        ViewBag.repairModelnumber = "";
                        ViewBag.repairProductName = "";
                        ViewBag.repairPuchasedate = "";
                        ViewBag.repairRegTime = "";
                        ViewBag.repairRegNumber = "";
                        ViewBag.repairName = "";
                        ViewBag.repairTel = "";
                        ViewBag.repairCompany = "";
                        ViewBag.repairAddress = "";
                        ViewBag.repairBuyingChannel = "";
                    }
                }

                if (searchDetailData.Count() == 0)
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", "查無產品資料");
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);
                    TempData["alertmessage"] = "查無產品，請輸入正確資料";

                    return RedirectToAction("repairs", "Registration");
                }
                else
                {
                    return View();
                }
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationSend(FormCollection form,IEnumerable<HttpPostedFileBase> MultiFile1)
        {
            List<HttpPostedFileBase> fileList2 = MultiFile1.ToList();
            List<string> resultFilename = new List<string>();

            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.registerproduct = "註冊您的產品";
                //ViewBag.warrantycheck = "保修查詢";
                //ViewBag.warrantypolicy = "保修政策";
                //ViewBag.firstname = "名";
                //ViewBag.lastname = "姓";
                //ViewBag.modelnumber = "型號";
                //ViewBag.serialnumber = "序號";
                //ViewBag.buyingchannel = "購買通路";
                //ViewBag.purchasedate = "購買日期";
                //ViewBag.proofofpurchase = "購買明細";
                //ViewBag.addnewitem = "新增新項目";
                //ViewBag.termscheck = "我理解、接受並同意";
                //ViewBag.fileuploadsize = "每個文檔的最大大小不得超過 5MB";
                //ViewBag.regsuccessful = "註冊成功";
                //ViewBag.regtime = "註冊時間";
                //ViewBag.regname = "姓名";
                //ViewBag.regcompany = "公司";
                //ViewBag.regproducts = "產品";
                //ViewBag.regaddress = "地址";
                //ViewBag.regbuychannel = "購買通路";
                //ViewBag.regpurchasedate = "購買日期";
                //ViewBag.regnumber = "註冊序號";
                //ViewBag.repproductname = "產品";
                //ViewBag.repmodnum = "型號";
                //ViewBag.repserialnum = "序號";
                //ViewBag.reppudate = "購買日期";
                //ViewBag.repdownload = "下載";
                //ViewBag.checkagain = "重新查詢";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }

            Session.Remove("form");
            Session.Add("form", form);

            var checkterm = form["term"];
            int isAttachFile = 0;

            if (checkterm != "on")
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", "沒有勾選條款");
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                return RedirectToAction("Index", "Registration");
            }
            else
            {
                if (form["vcode"].ToString() != Session["_ValCheckCode"].ToString())
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", "驗證碼比對錯誤");
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);
                    TempData["alertmessage"] = "驗證碼比對錯誤";

                    return RedirectToAction("Index", "Registration");
                }
                else
                {
                    List<HttpPostedFileBase> fileList = MultiFile1.ToList();
                    foreach (var fileitem in fileList)
                    {
                        if (fileitem != null)
                        {
                            if (fileitem.ContentLength > 0)
                            {
                                var nguid = Guid.NewGuid();
                                isAttachFile = 1;
                                string _filename = Path.GetFileName(fileitem.FileName);
                                resultFilename.Add("/Content/Upload/warranty/" + wholeLang +"_" + nguid + "_" + _filename);

                                string _path = Path.Combine(Server.MapPath("~/Content/Upload/warranty"),wholeLang + "_" + nguid + "_" + _filename);
                                fileitem.SaveAs(_path);
                            }
                        }
                    }

                    if (isAttachFile == 0)
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", "沒有上傳檔案");
                        alertData.Add("text", "");
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + "/service/Contactinfo"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        return RedirectToAction("Index", "Registration");
                    }
                    else
                    {
                        warranty_register data = new warranty_register();
                        warranty_register_detail dataDetail = new warranty_register_detail();
                        List<warrantyProduct> warrantyProduct = new List<warrantyProduct>();
                        warrantyProduct regWarrtyProduct = new warrantyProduct();
                        SystemService warrantyContent = new SystemService();

                        data.guid = Guid.NewGuid().ToString();
                        data.firstname = form["fname"].ToString();
                        data.lastname = form["lname"].ToString();
                        data.email = form["email"].ToString();
                        data.tel = form["tel"].ToString();
                        data.company = form["corp"].ToString();
                        data.address = form["adress"].ToString();

                        //regWarrtyProduct.modelNumber = form["mn01"].ToString();
                        //regWarrtyProduct.serialNumber = form["sn01"].ToString();
                        //regWarrtyProduct.buyingChannel = form["channel01"].ToString();
                        //regWarrtyProduct.purchaseDate = form["purchase01"].ToString();
                        //regWarrtyProduct.proofPurchase = form["MultiFile1"].ToString();
                        //warrantyProduct.Add(regWarrtyProduct);

                        //data.products = JsonConvert.SerializeObject(warrantyProduct);

                        data.status = "Y";
                        data.lang = wholeLang;

                        var create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        string alertString = "";
                        int isAddMain = 0;

                        for (int i = 0; i < resultFilename.Count(); i++)
                        {
                            if (i == 0)
                            {
                                dataDetail.modelnumber = form["mn01"].ToString();
                                dataDetail.serialnumber = form["sn01"].ToString();
                                dataDetail.buyingchannel = form["channel01"].ToString();
                                dataDetail.purchasedate = form["purchase01"].ToString();
                                dataDetail.proofofpruchase = resultFilename[0];

                                var judgeSameSerial = (from a in DB.growarranty_register_detailup
                                                       where a.lang == wholeLang && a.modelnumber == dataDetail.modelnumber && a.serialnumber == dataDetail.serialnumber
                                                       select a).ToList();

                                if (judgeSameSerial.Count() == 0)
                                {
                                    isAddMain = 1;
                                }
                                else
                                {
                                    alertString = alertString+ "下列序號已重複無法註冊:" + dataDetail.serialnumber + "、";
                                }
                            }
                            else
                            {
                                dataDetail.modelnumber = form["mn_" + (i + 1)].ToString();
                                dataDetail.serialnumber = form["sn_" + (i + 1)].ToString();
                                dataDetail.buyingchannel = form["channel_" + (i + 1)].ToString();
                                dataDetail.purchasedate = form["purchase_" + (i + 1)].ToString();
                                dataDetail.proofofpruchase = resultFilename[i];

                                var judgeSameSerial = (from a in DB.growarranty_register_detailup
                                                       where a.lang == wholeLang && a.modelnumber == dataDetail.modelnumber && a.serialnumber == dataDetail.serialnumber
                                                       select a).ToList();

                                if (judgeSameSerial.Count() == 0)
                                {
                                    isAddMain = 1;
                                }
                                else
                                {
                                    if (alertString == "")
                                    {
                                        alertString = alertString + "下列序號已重複無法註冊:" + dataDetail.serialnumber + "、";
                                    }
                                    else
                                    {
                                        alertString = alertString + dataDetail.serialnumber + "、";
                                    }
                                }
                            }
                        }

                        if (isAddMain == 0)
                        {
                            TempData["alertmessage"] = alertString;

                            return RedirectToAction("Index", "Registration");
                        }
                        else
                        {
                            var add_guid = warrantyContent.InsertWarranty(wholeLang, form["fname"].ToString(), form["lname"].ToString(), form["email"].ToString(), form["tel"].ToString(), form["corp"].ToString(), form["adress"].ToString(), create_date);

                            for (int i = 0; i < resultFilename.Count(); i++)
                            {
                                if (i == 0)
                                {
                                    dataDetail.modelnumber = form["mn01"].ToString();
                                    dataDetail.serialnumber = form["sn01"].ToString();
                                    dataDetail.buyingchannel = form["channel01"].ToString();
                                    dataDetail.purchasedate = form["purchase01"].ToString();
                                    dataDetail.proofofpruchase = resultFilename[0];

                                    var judgeSameSerial = (from a in DB.growarranty_register_detailup
                                                           where a.lang == wholeLang && a.modelnumber == dataDetail.modelnumber && a.serialnumber == dataDetail.serialnumber
                                                           select a).ToList();

                                    if (judgeSameSerial.Count() == 0)
                                    {
                                        warrantyContent.InsertWarrantyDetail(form["fname"].ToString(), form["lname"].ToString(), form["email"].ToString(), form["tel"].ToString(), form["corp"].ToString(), form["adress"].ToString(),wholeLang, add_guid, form["mn01"].ToString(), form["sn01"].ToString(), form["channel01"].ToString(), form["purchase01"].ToString(), resultFilename[0]);
                                    }
                                }
                                else
                                {
                                    dataDetail.modelnumber = form["mn_" + (i + 1)].ToString();
                                    dataDetail.serialnumber = form["sn_" + (i + 1)].ToString();
                                    dataDetail.buyingchannel = form["channel_" + (i + 1)].ToString();
                                    dataDetail.purchasedate = form["purchase_" + (i + 1)].ToString();
                                    dataDetail.proofofpruchase = resultFilename[i];

                                    var judgeSameSerial = (from a in DB.growarranty_register_detailup
                                                           where a.lang == wholeLang && a.modelnumber == dataDetail.modelnumber && a.serialnumber == dataDetail.serialnumber
                                                           select a).ToList();

                                    if (judgeSameSerial.Count() == 0)
                                    {
                                        warrantyContent.InsertWarrantyDetail(form["fname"].ToString(), form["lname"].ToString(), form["email"].ToString(), form["tel"].ToString(), form["corp"].ToString(), form["adress"].ToString(),wholeLang, add_guid, form["mn_" + (i + 1)].ToString(), form["sn_" + (i + 1)].ToString(), form["channel_" + (i + 1)].ToString(), form["purchase_" + (i + 1)].ToString(), resultFilename[i]);
                                    }
                                }
                            }

                            var searchAddProduct = (from a in DB.growarranty_register_detailup
                                                    where a.warrantyRegister_guid == add_guid
                                                    select a).ToList();
                            List<string> resultSuccessfulProduct = new List<string>();
                            foreach (var itemsap in searchAddProduct)
                            {
                                resultSuccessfulProduct.Add("Model number:" + itemsap.modelnumber + "   |   " + "Serial Number:" + itemsap.serialnumber + "   |   " + "Buying Channel:" + itemsap.buyingchannel + "   |   " + "Purchase Date:" + itemsap.purchasedate);
                            }

                            ViewBag.registrationtime = create_date;
                            ViewBag.registrationnumber = add_guid;
                            ViewBag.successfulname = form["fname"].ToString() + " " + form["lname"].ToString();
                            ViewBag.successfultel = form["tel"].ToString();
                            ViewBag.successfulcompany = form["corp"].ToString();
                            //ViewBag.successfulproducts = "Model number:" + form["mn01"].ToString() + "   |   " + "Serial Number:" + form["sn01"].ToString();
                            ViewBag.successfulproducts = resultSuccessfulProduct;
                            ViewBag.successfuladdress = form["adress"].ToString();
                            ViewBag.successfulbuyingchannel = form["channel01"].ToString();
                            ViewBag.successfulpurchasedate = form["purchase01"].ToString();

                            Session.Remove("form");
                            //DB.warranty_register.Add(data);
                            //DB.growarranty_register_detailup.Add(dataDetail);
                            //DB.SaveChanges();

                            if (alertString != "")
                            {
                                TempData["alertmessage"] = alertString;
                            }

                            return View();
                        }
                       
                    }
                }
            }
        }
        public ActionResult Repair()
        {
            if (Session["WordLang"] == null)
            {
                Session["WordLang"] = "en";
                Session["WholeLang"] = "EN";
            }

            string wholeLang = Session["WordLang"].ToString();
            if (wholeLang == null)
            {
                wholeLang = "en";
            }

            Model DB = new Model();

            var warrantyPolicyData = (from a in DB.warranty_policy
                               where a.lang == wholeLang && a.status =="Y"
                               select a).FirstOrDefault();
            if (warrantyPolicyData != null)
            {
                ViewBag.warrantyPolicyData = warrantyPolicyData.content;
            }

            var privacyData = (from a in DB.web_data
                               where a.lang == wholeLang
                               select a).FirstOrDefault();
            ViewBag.privacyData = privacyData.privacypolicy;

            if (privacyData != null)
            {
                ViewBag.description = privacyData.description_warrantyPolicy;
                ViewBag.keywords = privacyData.keywords_warrantyPolicy;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            List<product_subclass> psFirst = new List<product_subclass>();
            var productCatalogFirst = (from a in DB.product_category
                                       where a.status == "Y" && a.lang == wholeLang
                                       select a).OrderBy(x => x.sortIndex).ToList();

            foreach (var tempItem in productCatalogFirst)
            {
                var tempProductSubclassFirst = (from a in DB.product_subclass
                                                where a.productsCategory_guid == tempItem.guid && a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                                select a).FirstOrDefault();
                psFirst.Add(tempProductSubclassFirst);
            }
            var productSubclassFirst = (from a in DB.product_subclass
                                        where a.status == "Y" && a.lang == wholeLang && a.category == "0"
                                        select a).ToList();
            ViewBag.productSubclassFirst = psFirst;

            var productSubclassMultiLevel = (from a in DB.product_subclass
                                             where a.status == "Y" && a.lang == wholeLang && a.category != "0"
                                             select a).OrderBy(x => x.sortIndex).ToList();
            ViewBag.productSubclassMultiLevel = productSubclassMultiLevel;

            var webBasicData = (from a in DB.web_data
                                where a.lang == wholeLang
                                select a).FirstOrDefault();
            ViewBag.webBasicData = webBasicData;

            var defaultcatalogguid = (from a in DB.dealer_catalog
                                      where a.lang == wholeLang && a.status == "Y"
                                      select a).OrderBy(x => x.sortIndex).ToList();

            NameValueCollection langData = new NameValueCollection();
            if (wholeLang == "tw")
            {
                langData = FunctionService.getLangData("tw");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "特色";
                //ViewBag.specificationsTab = "規格";
                //ViewBag.downloadTab = "下載";
                //ViewBag.searchkeyword = "關鍵字...";
                //ViewBag.searchResults = "個結果被找到關於";
                //ViewBag.searchAbout = "大約";
                //ViewBag.openRfq = "前往詢價";
                //ViewBag.saveRfq = "儲存詢價";
                //ViewBag.productsSearch = "產品搜尋";
                ViewBag.lang = "tw";
                //ViewBag.reqRfq = "詢價";
                //ViewBag.aboutus = "關於我們";
                //ViewBag.productpage = "產品專區";
                //ViewBag.warranty = "保修專區";
                //ViewBag.contactus = "聯絡我們";
                //ViewBag.catalogdownload = "型錄下載";
                //ViewBag.dealer = "經銷商區";
                //ViewBag.latestnews = "最新消息";
                //ViewBag.readmore = "瀏覽更多";
                //ViewBag.privacyPolicy = "隱私政策";
                //ViewBag.quotation = "詢問表單";
                //ViewBag.productDesc = "DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。";
                //ViewBag.homepage = "首頁";
                //ViewBag.productCategoryBtn = "產品類別";
                //ViewBag.productListBtn = "產品列表";
                //ViewBag.productSearchBtn = "產品搜尋";
                //ViewBag.terms = "條款";
                //ViewBag.registered = "商標";
                //ViewBag.inquire = "詢問";
                //ViewBag.information = "資訊";
                //ViewBag.contactform = "聯絡表單";
                //ViewBag.footerbrand = "品牌";
                //ViewBag.footergroup = "群組";
                //ViewBag.whychose = "為何選擇";
                //ViewBag.offlinepage = "網站維護中";
                //ViewBag.offlinemobilepage = "頁面維護中，請於稍後重整頁面";
                //ViewBag.completeall = "請填寫所有項目";
                //ViewBag.assure = "以確保正確回復";
                //ViewBag.businessType = "商業類型";
                //ViewBag.companyName = "公司名稱";
                //ViewBag.contactName = "聯絡人";
                //ViewBag.email = "電子郵件";
                //ViewBag.tel = "電話";
                //ViewBag.country = "國家";
                //ViewBag.address = "地址";
                //ViewBag.website = "網址";
                //ViewBag.subject = "主題";
                //ViewBag.leaveMessage = "留下您的訊息";
                //ViewBag.verification = "驗證碼";
                //ViewBag.submit = "提交";
                //ViewBag.registerproduct = "註冊您的產品";
                //ViewBag.warrantycheck = "保修查詢";
                //ViewBag.warrantypolicy = "保修政策";
                //ViewBag.firstname = "名";
                //ViewBag.lastname = "姓";
                //ViewBag.modelnumber = "型號";
                //ViewBag.serialnumber = "序號";
                //ViewBag.buyingchannel = "購買通路";
                //ViewBag.purchasedate = "購買日期";
                //ViewBag.proofofpurchase = "購買明細";
                //ViewBag.addnewitem = "新增新項目";
                //ViewBag.termscheck = "我理解、接受並同意";
                //ViewBag.fileuploadsize = "每個文檔的最大大小不得超過 5MB";
                //ViewBag.regsuccessful = "註冊成功";
                //ViewBag.regtime = "註冊時間";
                //ViewBag.regname = "姓名";
                //ViewBag.regcompany = "公司";
                //ViewBag.regproducts = "產品";
                //ViewBag.regaddress = "地址";
                //ViewBag.regbuychannel = "購買通路";
                //ViewBag.regpurchasedate = "購買日期";
                //ViewBag.regnumber = "註冊序號";
                //ViewBag.repproductname = "產品";
                //ViewBag.repmodnum = "型號";
                //ViewBag.repserialnum = "序號";
                //ViewBag.reppudate = "購買日期";
                //ViewBag.repdownload = "下載";
                //ViewBag.checkagain = "重新查詢";
            }
            else if (wholeLang == "en")
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }
            else if (wholeLang == "cn")
            {
                langData = FunctionService.getLangData("cn");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                if (defaultcatalogguid.Count() == 0)
                {
                    ViewBag.defaultcatalogguid = "";
                }
                else
                {
                    ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                }

                ViewBag.lang = "cn";
            }
            else
            {
                langData = FunctionService.getLangData("en");//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案

                ViewBag.defaultcatalogguid = defaultcatalogguid[0].guid;
                //ViewBag.featuresTab = "Features";
                //ViewBag.specificationsTab = "Specifications";
                //ViewBag.downloadTab = "Download";
                //ViewBag.searchkeyword = "Keyword...";
                //ViewBag.searchResults = " results found for ";
                //ViewBag.searchAbout = "About";
                //ViewBag.openRfq = "OPEN RFQ";
                //ViewBag.saveRfq = "Saved to Request for Quotation";
                //ViewBag.productsSearch = "Products Search";
                ViewBag.lang = "en";
                //ViewBag.reqRfq = "REQUEST FOR QUOTATION";
                //ViewBag.aboutus = "ABOUT US";
                //ViewBag.productpage = "PRODUCTS";
                //ViewBag.warranty = "WARRANTY";
                //ViewBag.contactus = "CONTACT US";
                //ViewBag.catalogdownload = "CATALOG DOWNLOAD";
                //ViewBag.dealer = "DEALER";
                //ViewBag.latestnews = "LATEST NEWS";
                //ViewBag.readmore = "READ MORE";
                //ViewBag.privacyPolicy = "PRIVACY POLICY";
                //ViewBag.quotation = "Request for Quotation";
                //ViewBag.productDesc = "DUROFIX™ is a new brand for the 21st century, and is currently being promoted in Europe, America, Australia, and Asia.";
                //ViewBag.homepage = "HOME";
                //ViewBag.productCategoryBtn = "Product Category";
                //ViewBag.productListBtn = "Product List";
                //ViewBag.productSearchBtn = "PRODUCT SEARCH";
                //ViewBag.terms = "TERMS";
                //ViewBag.registered = "REGISTERED";
                //ViewBag.inquire = "INQUIRE";
                //ViewBag.information = "INFORMATION";
                //ViewBag.contactform = "CONTACT FORM";
                //ViewBag.footerbrand = "BRAND";
                //ViewBag.footergroup = "GROUP";
                //ViewBag.whychose = "WHY CHOOSE DUROFIX";
                //ViewBag.offlinepage = "The Website is Under Maintenance";
                //ViewBag.offlinemobilepage = "This page is down for maintenance. We are working to get it back up and running as soon as possible. Please check back!";
                //ViewBag.completeall = "Please complete all item with";
                //ViewBag.assure = "to assure proper response";
                //ViewBag.businessType = "Business Type";
                //ViewBag.companyName = "Company Name";
                //ViewBag.contactName = "Contact Name";
                //ViewBag.email = "E-mail";
                //ViewBag.tel = "TEL";
                //ViewBag.country = "Country";
                //ViewBag.address = "Address";
                //ViewBag.website = "Web Site";
                //ViewBag.subject = "Subject";
                //ViewBag.leaveMessage = "Leave Your Message";
                //ViewBag.verification = "Verification code";
                //ViewBag.submit = "SUBMIT";
                //ViewBag.registerproduct = "REGISTER YOUR PRODUCT";
                //ViewBag.warrantycheck = "WARRANTY CHECK";
                //ViewBag.warrantypolicy = "WARRANTY POLICY";
                //ViewBag.firstname = "First Name";
                //ViewBag.lastname = "Last Name";
                //ViewBag.modelnumber = "Model Number";
                //ViewBag.serialnumber = "Serial Naumber";
                //ViewBag.buyingchannel = "Buying Channel";
                //ViewBag.purchasedate = "Purchase Date";
                //ViewBag.proofofpurchase = "Proof of purchase";
                //ViewBag.addnewitem = "Add a New Item";
                //ViewBag.termscheck = "I understand,accept,and agree to the";
                //ViewBag.fileuploadsize = "The maximum allowable size per document may not be more than 5MB";
                //ViewBag.regsuccessful = "REGISTRATION SUCCESSFUL";
                //ViewBag.regtime = "REGISTRATION TIME";
                //ViewBag.regname = "NAME";
                //ViewBag.regcompany = "COMPANY";
                //ViewBag.regproducts = "PRODUCTS";
                //ViewBag.regaddress = "ADDRESS";
                //ViewBag.regbuychannel = "BUYING CHANNEL";
                //ViewBag.regpurchasedate = "PURCHASE DATE";
                //ViewBag.regnumber = "REGISTRATION NUMBER";
                //ViewBag.repproductname = "PRODUCT NAME";
                //ViewBag.repmodnum = "MODEL NUMBER";
                //ViewBag.repserialnum = "SERIAL NUMBER";
                //ViewBag.reppudate = "PURCHASE DATE";
                //ViewBag.repdownload = "DOWNLOAD";
                //ViewBag.checkagain = "Check again";
            }

            return View();
        }
    }
}