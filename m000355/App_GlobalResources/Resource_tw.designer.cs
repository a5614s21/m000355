//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.42000
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   用於查詢當地語系化字串等的強型別資源類別。
    /// </summary>
    // 這個類別是自動產生的，是利用 StronglyTypedResourceBuilder
    // 類別透過 ResGen 或 Visual Studio 這類工具產生。
    // 若要加入或移除成員，請編輯您的 .ResX 檔，然後重新執行 ResGen
    // (利用 /str 選項)，或重建 Visual Studio 專案。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource_tw {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource_tw() {
        }
        
        /// <summary>
        ///   傳回這個類別使用的快取的 ResourceManager 執行個體。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Resource_tw", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   覆寫目前執行緒的 CurrentUICulture 屬性，對象是所有
        ///   使用這個強型別資源類別的資源查閱。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查詢類似 企業沿革 的當地語系化字串。
        /// </summary>
        internal static string aboutmoregroup {
            get {
                return ResourceManager.GetString("aboutmoregroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 關於我們 的當地語系化字串。
        /// </summary>
        internal static string aboutus {
            get {
                return ResourceManager.GetString("aboutus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 帳戶詳細資料 的當地語系化字串。
        /// </summary>
        internal static string accountDetail {
            get {
                return ResourceManager.GetString("accountDetail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 新增 的當地語系化字串。
        /// </summary>
        internal static string addButton {
            get {
                return ResourceManager.GetString("addButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 新增新項目 的當地語系化字串。
        /// </summary>
        internal static string addnewitem {
            get {
                return ResourceManager.GetString("addnewitem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 地址 的當地語系化字串。
        /// </summary>
        internal static string address {
            get {
                return ResourceManager.GetString("address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 進階選項 的當地語系化字串。
        /// </summary>
        internal static string advancedOption {
            get {
                return ResourceManager.GetString("advancedOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 以確保正確回復 的當地語系化字串。
        /// </summary>
        internal static string assure {
            get {
                return ResourceManager.GetString("assure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 回前頁 的當地語系化字串。
        /// </summary>
        internal static string backbtn {
            get {
                return ResourceManager.GetString("backbtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 批次 的當地語系化字串。
        /// </summary>
        internal static string batchButton {
            get {
                return ResourceManager.GetString("batchButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 刪除 的當地語系化字串。
        /// </summary>
        internal static string batchDelButton {
            get {
                return ResourceManager.GetString("batchDelButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 停用 的當地語系化字串。
        /// </summary>
        internal static string batchDisableButton {
            get {
                return ResourceManager.GetString("batchDisableButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 啟用 的當地語系化字串。
        /// </summary>
        internal static string batchEnableButton {
            get {
                return ResourceManager.GetString("batchEnableButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 還原 的當地語系化字串。
        /// </summary>
        internal static string batchRevertButton {
            get {
                return ResourceManager.GetString("batchRevertButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 瀏覽器使用 的當地語系化字串。
        /// </summary>
        internal static string browser {
            get {
                return ResourceManager.GetString("browser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 詢問類型 的當地語系化字串。
        /// </summary>
        internal static string businessType {
            get {
                return ResourceManager.GetString("businessType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買通路 的當地語系化字串。
        /// </summary>
        internal static string buyingchannel {
            get {
                return ResourceManager.GetString("buyingchannel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型錄下載 的當地語系化字串。
        /// </summary>
        internal static string catalogdownload {
            get {
                return ResourceManager.GetString("catalogdownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 重新查詢 的當地語系化字串。
        /// </summary>
        internal static string checkagain {
            get {
                return ResourceManager.GetString("checkagain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 重填 的當地語系化字串。
        /// </summary>
        internal static string clearButton {
            get {
                return ResourceManager.GetString("clearButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 公司名稱 的當地語系化字串。
        /// </summary>
        internal static string companyName {
            get {
                return ResourceManager.GetString("companyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 請填寫所有項目 的當地語系化字串。
        /// </summary>
        internal static string completeall {
            get {
                return ResourceManager.GetString("completeall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 聯絡表單 的當地語系化字串。
        /// </summary>
        internal static string contactform {
            get {
                return ResourceManager.GetString("contactform", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 聯絡資訊 的當地語系化字串。
        /// </summary>
        internal static string contactinfo {
            get {
                return ResourceManager.GetString("contactinfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 聯絡人 的當地語系化字串。
        /// </summary>
        internal static string contactName {
            get {
                return ResourceManager.GetString("contactName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 服務 的當地語系化字串。
        /// </summary>
        internal static string contactservice {
            get {
                return ResourceManager.GetString("contactservice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 聯絡我們 的當地語系化字串。
        /// </summary>
        internal static string contactus {
            get {
                return ResourceManager.GetString("contactus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 縣市 的當地語系化字串。
        /// </summary>
        internal static string country {
            get {
                return ResourceManager.GetString("country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 經銷商區 的當地語系化字串。
        /// </summary>
        internal static string dealer {
            get {
                return ResourceManager.GetString("dealer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 下載 的當地語系化字串。
        /// </summary>
        internal static string downloadTab {
            get {
                return ResourceManager.GetString("downloadTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 電子郵件 的當地語系化字串。
        /// </summary>
        internal static string email {
            get {
                return ResourceManager.GetString("email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 特色 的當地語系化字串。
        /// </summary>
        internal static string featuresTab {
            get {
                return ResourceManager.GetString("featuresTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 每個文檔的最大大小不得超過 5MB 的當地語系化字串。
        /// </summary>
        internal static string fileuploadsize {
            get {
                return ResourceManager.GetString("fileuploadsize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 名 的當地語系化字串。
        /// </summary>
        internal static string firstname {
            get {
                return ResourceManager.GetString("firstname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 品牌 的當地語系化字串。
        /// </summary>
        internal static string footerbrand {
            get {
                return ResourceManager.GetString("footerbrand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 群組 的當地語系化字串。
        /// </summary>
        internal static string footergroup {
            get {
                return ResourceManager.GetString("footergroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 首頁 的當地語系化字串。
        /// </summary>
        internal static string homepage {
            get {
                return ResourceManager.GetString("homepage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 系統首頁 的當地語系化字串。
        /// </summary>
        internal static string iconHome {
            get {
                return ResourceManager.GetString("iconHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 資訊 的當地語系化字串。
        /// </summary>
        internal static string information {
            get {
                return ResourceManager.GetString("information", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 詢問 的當地語系化字串。
        /// </summary>
        internal static string inquire {
            get {
                return ResourceManager.GetString("inquire", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 審核者設定 的當地語系化字串。
        /// </summary>
        internal static string inspectorSetting {
            get {
                return ResourceManager.GetString("inspectorSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 搜尋 的當地語系化字串。
        /// </summary>
        internal static string keywordSearchButton {
            get {
                return ResourceManager.GetString("keywordSearchButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 姓 的當地語系化字串。
        /// </summary>
        internal static string lastname {
            get {
                return ResourceManager.GetString("lastname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 最新消息 的當地語系化字串。
        /// </summary>
        internal static string latestnews {
            get {
                return ResourceManager.GetString("latestnews", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 留下您的訊息 的當地語系化字串。
        /// </summary>
        internal static string leaveMessage {
            get {
                return ResourceManager.GetString("leaveMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 帳號 的當地語系化字串。
        /// </summary>
        internal static string loginaccount {
            get {
                return ResourceManager.GetString("loginaccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 權限群組1 的當地語系化字串。
        /// </summary>
        internal static string logingroup {
            get {
                return ResourceManager.GetString("logingroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 權限群組2 的當地語系化字串。
        /// </summary>
        internal static string logingroup2 {
            get {
                return ResourceManager.GetString("logingroup2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 語系 的當地語系化字串。
        /// </summary>
        internal static string loginlang {
            get {
                return ResourceManager.GetString("loginlang", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 姓名 的當地語系化字串。
        /// </summary>
        internal static string loginname {
            get {
                return ResourceManager.GetString("loginname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 登出 的當地語系化字串。
        /// </summary>
        internal static string logout {
            get {
                return ResourceManager.GetString("logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 主要設定 的當地語系化字串。
        /// </summary>
        internal static string mainSetting {
            get {
                return ResourceManager.GetString("mainSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 多媒體檔案 的當地語系化字串。
        /// </summary>
        internal static string media {
            get {
                return ResourceManager.GetString("media", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 媒體庫 的當地語系化字串。
        /// </summary>
        internal static string mediaBtn {
            get {
                return ResourceManager.GetString("mediaBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型號 的當地語系化字串。
        /// </summary>
        internal static string modelnumber {
            get {
                return ResourceManager.GetString("modelnumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 12 六月, 2021 的當地語系化字串。
        /// </summary>
        internal static string newsData1 {
            get {
                return ResourceManager.GetString("newsData1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 durofix 工具 在 2020 WOC (4-7 2月.) 展位號碼.: C4059, 拉斯維加斯, 內華達州, 美國 的當地語系化字串。
        /// </summary>
        internal static string newsData2 {
            get {
                return ResourceManager.GetString("newsData2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 電子扭矩和速度控制 更長的運行時間和生命週期 最大 2,050 Nm。 反向扭矩 耐用的金屬變速箱外殼... 的當地語系化字串。
        /// </summary>
        internal static string newsData3 {
            get {
                return ResourceManager.GetString("newsData3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 下一則 的當地語系化字串。
        /// </summary>
        internal static string nextbtn {
            get {
                return ResourceManager.GetString("nextbtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 頁面維護中，請於稍後重整頁面 的當地語系化字串。
        /// </summary>
        internal static string offlinemobilepage {
            get {
                return ResourceManager.GetString("offlinemobilepage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 網站維護中 的當地語系化字串。
        /// </summary>
        internal static string offlinepage {
            get {
                return ResourceManager.GetString("offlinepage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 線上使用者 的當地語系化字串。
        /// </summary>
        internal static string onlineUser {
            get {
                return ResourceManager.GetString("onlineUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 前往詢價 的當地語系化字串。
        /// </summary>
        internal static string openRfq {
            get {
                return ResourceManager.GetString("openRfq", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 圖片敘述 的當地語系化字串。
        /// </summary>
        internal static string pictureAlt {
            get {
                return ResourceManager.GetString("pictureAlt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 上一則 的當地語系化字串。
        /// </summary>
        internal static string previousbtn {
            get {
                return ResourceManager.GetString("previousbtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 隱私政策 的當地語系化字串。
        /// </summary>
        internal static string privacyPolicy {
            get {
                return ResourceManager.GetString("privacyPolicy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品類別 的當地語系化字串。
        /// </summary>
        internal static string productCategoryBtn {
            get {
                return ResourceManager.GetString("productCategoryBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 DUROFIX是21世紀的新品牌，目前正在歐洲、美洲、澳大利亞和亞洲推廣。 的當地語系化字串。
        /// </summary>
        internal static string productDesc {
            get {
                return ResourceManager.GetString("productDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品列表 的當地語系化字串。
        /// </summary>
        internal static string productListBtn {
            get {
                return ResourceManager.GetString("productListBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品專區 的當地語系化字串。
        /// </summary>
        internal static string productpage {
            get {
                return ResourceManager.GetString("productpage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品搜尋 的當地語系化字串。
        /// </summary>
        internal static string productSearchBtn {
            get {
                return ResourceManager.GetString("productSearchBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品搜尋 的當地語系化字串。
        /// </summary>
        internal static string productsSearch {
            get {
                return ResourceManager.GetString("productsSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買明細 的當地語系化字串。
        /// </summary>
        internal static string proofofpurchase {
            get {
                return ResourceManager.GetString("proofofpurchase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買日期 的當地語系化字串。
        /// </summary>
        internal static string purchasedate {
            get {
                return ResourceManager.GetString("purchasedate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 詢問表單 的當地語系化字串。
        /// </summary>
        internal static string quotation {
            get {
                return ResourceManager.GetString("quotation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 瀏覽更多 的當地語系化字串。
        /// </summary>
        internal static string readmore {
            get {
                return ResourceManager.GetString("readmore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 地址 的當地語系化字串。
        /// </summary>
        internal static string regaddress {
            get {
                return ResourceManager.GetString("regaddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買通路 的當地語系化字串。
        /// </summary>
        internal static string regbuychannel {
            get {
                return ResourceManager.GetString("regbuychannel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 公司 的當地語系化字串。
        /// </summary>
        internal static string regcompany {
            get {
                return ResourceManager.GetString("regcompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 商標 的當地語系化字串。
        /// </summary>
        internal static string registered {
            get {
                return ResourceManager.GetString("registered", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 註冊您的產品 的當地語系化字串。
        /// </summary>
        internal static string registerproduct {
            get {
                return ResourceManager.GetString("registerproduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 姓名 的當地語系化字串。
        /// </summary>
        internal static string regname {
            get {
                return ResourceManager.GetString("regname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 註冊序號 的當地語系化字串。
        /// </summary>
        internal static string regnumber {
            get {
                return ResourceManager.GetString("regnumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品 的當地語系化字串。
        /// </summary>
        internal static string regproducts {
            get {
                return ResourceManager.GetString("regproducts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買日期 的當地語系化字串。
        /// </summary>
        internal static string regpurchasedate {
            get {
                return ResourceManager.GetString("regpurchasedate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 註冊成功 的當地語系化字串。
        /// </summary>
        internal static string regsuccessful {
            get {
                return ResourceManager.GetString("regsuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 註冊時間 的當地語系化字串。
        /// </summary>
        internal static string regtime {
            get {
                return ResourceManager.GetString("regtime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 下載 的當地語系化字串。
        /// </summary>
        internal static string repdownload {
            get {
                return ResourceManager.GetString("repdownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 回覆內容 的當地語系化字串。
        /// </summary>
        internal static string replyContent {
            get {
                return ResourceManager.GetString("replyContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型號 的當地語系化字串。
        /// </summary>
        internal static string repmodnum {
            get {
                return ResourceManager.GetString("repmodnum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 產品 的當地語系化字串。
        /// </summary>
        internal static string repproductname {
            get {
                return ResourceManager.GetString("repproductname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 購買日期 的當地語系化字串。
        /// </summary>
        internal static string reppudate {
            get {
                return ResourceManager.GetString("reppudate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 序號 的當地語系化字串。
        /// </summary>
        internal static string repserialnum {
            get {
                return ResourceManager.GetString("repserialnum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 詢價 的當地語系化字串。
        /// </summary>
        internal static string reqRfq {
            get {
                return ResourceManager.GetString("reqRfq", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 回覆者設定 的當地語系化字串。
        /// </summary>
        internal static string respondentSetting {
            get {
                return ResourceManager.GetString("respondentSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 返回 的當地語系化字串。
        /// </summary>
        internal static string returnButton {
            get {
                return ResourceManager.GetString("returnButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 儲存 的當地語系化字串。
        /// </summary>
        internal static string saveButton {
            get {
                return ResourceManager.GetString("saveButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 儲存詢價 的當地語系化字串。
        /// </summary>
        internal static string saveRfq {
            get {
                return ResourceManager.GetString("saveRfq", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 大約 的當地語系化字串。
        /// </summary>
        internal static string searchAbout {
            get {
                return ResourceManager.GetString("searchAbout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 關鍵字... 的當地語系化字串。
        /// </summary>
        internal static string searchkeyword {
            get {
                return ResourceManager.GetString("searchkeyword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 個結果被找到關於 的當地語系化字串。
        /// </summary>
        internal static string searchResults {
            get {
                return ResourceManager.GetString("searchResults", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 選取全部 的當地語系化字串。
        /// </summary>
        internal static string selectMulti1 {
            get {
                return ResourceManager.GetString("selectMulti1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 清除選取 的當地語系化字串。
        /// </summary>
        internal static string selectMulti2 {
            get {
                return ResourceManager.GetString("selectMulti2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 序號 的當地語系化字串。
        /// </summary>
        internal static string serialnumber {
            get {
                return ResourceManager.GetString("serialnumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 規格 的當地語系化字串。
        /// </summary>
        internal static string specificationsTab {
            get {
                return ResourceManager.GetString("specificationsTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 主題 的當地語系化字串。
        /// </summary>
        internal static string subject {
            get {
                return ResourceManager.GetString("subject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 提交 的當地語系化字串。
        /// </summary>
        internal static string submit {
            get {
                return ResourceManager.GetString("submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 送出 的當地語系化字串。
        /// </summary>
        internal static string submitButton {
            get {
                return ResourceManager.GetString("submitButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 電話 的當地語系化字串。
        /// </summary>
        internal static string tel {
            get {
                return ResourceManager.GetString("tel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 條款 的當地語系化字串。
        /// </summary>
        internal static string terms {
            get {
                return ResourceManager.GetString("terms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 我理解、接受並同意 的當地語系化字串。
        /// </summary>
        internal static string termscheck {
            get {
                return ResourceManager.GetString("termscheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 驗證碼 的當地語系化字串。
        /// </summary>
        internal static string verification {
            get {
                return ResourceManager.GetString("verification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 審核管理 的當地語系化字串。
        /// </summary>
        internal static string verifyManage {
            get {
                return ResourceManager.GetString("verifyManage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 保修專區 的當地語系化字串。
        /// </summary>
        internal static string warranty {
            get {
                return ResourceManager.GetString("warranty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 保修查詢 的當地語系化字串。
        /// </summary>
        internal static string warrantycheck {
            get {
                return ResourceManager.GetString("warrantycheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 保修政策 的當地語系化字串。
        /// </summary>
        internal static string warrantypolicy {
            get {
                return ResourceManager.GetString("warrantypolicy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 網址 的當地語系化字串。
        /// </summary>
        internal static string website {
            get {
                return ResourceManager.GetString("website", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 為何選擇 的當地語系化字串。
        /// </summary>
        internal static string whychose {
            get {
                return ResourceManager.GetString("whychose", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 語系及帳號資訊 的當地語系化字串。
        /// </summary>
        internal static string yearCount {
            get {
                return ResourceManager.GetString("yearCount", resourceCulture);
            }
        }
    }
}
