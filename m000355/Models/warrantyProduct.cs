﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class warrantyProduct
    {
        public string modelNumber { get; set; }
        public string serialNumber { get; set; }
        public string buyingChannel { get; set; }
        public string purchaseDate { get; set; }
        public string proofPurchase { get; set; }
    }
}