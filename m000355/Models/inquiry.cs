﻿namespace web.Models{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class inquiry
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string product { get; set; }

        public string business_type { get; set; }

        public string company_name { get; set; }

        public string contact_name { get; set; }
        public string email { get; set; }
        public string tel { get; set; }

        public string country { get; set; }

        public string address { get; set; }
        public string website { get; set; }
        public string subject { get; set; }
        public string message { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}