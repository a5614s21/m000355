﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class GaModel
    {
        public string source { get; set; }
        public int? count { get; set; }
    }

    public class GaMonthModel
    {
        public string age { get; set; }
        public string visits { get; set; }

    }
}