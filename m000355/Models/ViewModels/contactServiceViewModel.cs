﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class contactServiceViewModel
    {
        public string title { get; set; }
        public string subtitle { get; set; }
        public string email { get; set; }
        public string telphone { get; set; }
        public string fax { get; set; }
        public string address { get; set; }
        public string maparea { get; set; }
        public int jmaparea { get; set; }
        public double mapsitex { get; set; }
        public double mapsitey { get; set; }
    }
}