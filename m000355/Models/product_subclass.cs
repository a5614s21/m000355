﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class product_subclass
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        public string reTitle { get; set; }
        public string titleSpace { get; set; }
        public string category { get; set; }

        public int? sortIndex { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }
        public int? level { get; set; }
        [StringLength(64)]
        public string productsCategory_guid { get; set; }
        [StringLength(30)]
        public string lang { get; set; }
    }
}