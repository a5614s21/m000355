﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class warranty_register_detail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string modelnumber { get; set; }
        public string serialnumber { get; set; }
        public string buyingchannel { get; set; }
        public string purchasedate { get; set; }
        public string proofofpruchase { get; set; }
        [StringLength(64)]
        public string warrantyRegister_guid { get; set; }

        [StringLength(1)]
        public string status { get; set; }
        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string tel { get; set; }
        public string company { get; set; }
        public string address { get; set; }
    }
}