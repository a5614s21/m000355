﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class ashcan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string tables { get; set; }

        [StringLength(64)]
        public string from_guid { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        [StringLength(30)]
        public string lang { get; set; }
    }
}