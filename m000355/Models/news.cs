﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class news
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(64)]
        public string category { get; set; }

        public string title { get; set; }

        public string subtitle { get; set; }

        public string content { get; set; }
        public string homedesc { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }

        [StringLength(1)]
        public string sticky { get; set; }

        [StringLength(1)]
        public string index_sticky { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        public string facebook { get; set; }
        public string line { get; set; }
        public string twitter { get; set; }
        public string keywords { get; set; }

        public string description { get; set; }
    }
}