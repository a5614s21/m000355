﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        [Key]
        [Column(Order = 0)]
        public string subtitle { get; set; }

        public DateTime? isComingSoon { get; set; }

        public string product_description { get; set; }

        public string emblem { get; set; }
        public string emblem_alt { get; set; }
        public string product_features { get; set; }
        public string featurespic { get; set; }

        public string featurespic_alt { get; set; }
        public string downloadfile { get; set; }

        public string downloadfile_alt { get; set; }

        public string product_specification { get; set; }

        public string download { get; set; }
        public string pic { get; set; }

        public string pic_alt { get; set; }
        public string videoLink { get; set; }
        public int? sortIndex { get; set; }
        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string lang { get; set; }
        public string productsCategory_guid { get; set; }
        public string productsSubClass_guid { get; set; }
        public string option { get; set; }
        public string option_alt { get; set; }
        public string specColumn { get; set; }
        public string specValue { get; set; }
        public string keywords { get; set; }

        public string description { get; set; }
    }
}