﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class abouts
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string notes { get; set; }

        public string content { get; set; }


        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }


        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }
    }
}