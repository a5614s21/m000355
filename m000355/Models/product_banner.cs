﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class product_banner
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }
        public string category { get; set; }
        public string title { get; set; }
        public string subtitle { get; set; }
        public string type { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public string picmobile { get; set; }

        public string picmobile_alt { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
        public string create_name { get; set; }
        public string modify_name { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public int? sortIndex { get; set; }
    }
}