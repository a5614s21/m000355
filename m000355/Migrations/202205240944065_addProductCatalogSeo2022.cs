namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProductCatalogSeo2022 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_category", "keywords", c => c.String());
            AddColumn("dbo.product_category", "description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_category", "description");
            DropColumn("dbo.product_category", "keywords");
        }
    }
}
