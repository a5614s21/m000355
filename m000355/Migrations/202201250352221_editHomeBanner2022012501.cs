namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editHomeBanner2022012501 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.home_banner", "newBlank", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.home_banner", "newBlank");
        }
    }
}
