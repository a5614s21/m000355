namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProductKey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "subtitle", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.product", "status", c => c.String(nullable: false, maxLength: 1));
            AlterColumn("dbo.product", "lang", c => c.String(nullable: false, maxLength: 30));
            AddPrimaryKey("dbo.product", new[] { "id", "guid", "subtitle", "status", "lang" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "lang", c => c.String(maxLength: 30));
            AlterColumn("dbo.product", "status", c => c.String(maxLength: 1));
            AlterColumn("dbo.product", "subtitle", c => c.String());
            AddPrimaryKey("dbo.product", new[] { "id", "guid" });
        }
    }
}
