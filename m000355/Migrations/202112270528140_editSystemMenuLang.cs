namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editSystemMenuLang : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.system_menu", "lang", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.system_menu", "lang");
        }
    }
}
