namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editWebData20220110 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "facebook", c => c.String());
            AddColumn("dbo.web_data", "line", c => c.String());
            AddColumn("dbo.web_data", "insgram", c => c.String());
            AddColumn("dbo.web_data", "homefooter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "homefooter");
            DropColumn("dbo.web_data", "insgram");
            DropColumn("dbo.web_data", "line");
            DropColumn("dbo.web_data", "facebook");
        }
    }
}
