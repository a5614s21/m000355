namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editSubclass20211221 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_subclass", "reTitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_subclass", "reTitle");
        }
    }
}
