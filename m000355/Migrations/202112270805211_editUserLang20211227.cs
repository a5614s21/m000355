namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUserLang20211227 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.user", "lang", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.user", "lang");
        }
    }
}
