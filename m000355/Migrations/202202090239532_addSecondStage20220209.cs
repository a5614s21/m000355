namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSecondStage20220209 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.contactinfo",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        businesstype = c.String(),
                        companyname = c.String(),
                        contactname = c.String(),
                        email = c.String(),
                        tel = c.String(),
                        country = c.String(),
                        address = c.String(),
                        website = c.String(),
                        subject = c.String(),
                        message = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.warranty_register_detail",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        modelnumber = c.String(),
                        serialnumber = c.String(),
                        buyingchannel = c.String(),
                        purchasedate = c.String(),
                        proofofpruchase = c.String(),
                        warrantyRegister_guid = c.String(maxLength: 64),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.service",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subtitle = c.String(),
                        email = c.String(),
                        tel = c.String(),
                        fax = c.String(),
                        address = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.warranty_policy",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.warranty_register",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        firstname = c.String(),
                        lastname = c.String(),
                        email = c.String(),
                        tel = c.String(),
                        company = c.String(),
                        address = c.String(),
                        products = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.warranty_register");
            DropTable("dbo.warranty_policy");
            DropTable("dbo.service");
            DropTable("dbo.warranty_register_detail");
            DropTable("dbo.contactinfo");
        }
    }
}
