namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProduct20211216 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.product", "productsCategory_guid", c => c.String());
            AlterColumn("dbo.product", "productsSubClass_guid", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.product", "productsSubClass_guid", c => c.String(maxLength: 64));
            AlterColumn("dbo.product", "productsCategory_guid", c => c.String(maxLength: 64));
        }
    }
}
