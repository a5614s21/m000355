namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUser20220119 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.product", "isComingSoon", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.product", "isComingSoon", c => c.String());
        }
    }
}
