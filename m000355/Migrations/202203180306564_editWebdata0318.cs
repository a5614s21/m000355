namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editWebdata0318 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "staffmail", c => c.String(maxLength: 255));
            AddColumn("dbo.web_data", "managermail", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "managermail");
            DropColumn("dbo.web_data", "staffmail");
        }
    }
}
