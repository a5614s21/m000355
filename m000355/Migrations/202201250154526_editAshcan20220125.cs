namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editAshcan20220125 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ashcan", "lang", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ashcan", "lang");
        }
    }
}
