namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_productTable20211129 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.inquiry",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        product = c.String(),
                        business_type = c.String(),
                        company_name = c.String(),
                        contact_name = c.String(),
                        email = c.String(),
                        tel = c.String(),
                        country = c.String(),
                        address = c.String(),
                        website = c.String(),
                        subject = c.String(),
                        message = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.product",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subtitle = c.String(),
                        isComingSoon = c.String(),
                        product_description = c.String(),
                        emblem = c.String(),
                        product_features_pic = c.String(),
                        product_features_picalt = c.String(),
                        product_specification = c.String(),
                        download = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        videoLink = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                        productsCategory_guid = c.String(maxLength: 64),
                        productsSubClass_guid = c.String(maxLength: 64),
                        option = c.String(),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.product_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.product_subclass",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        level = c.Int(),
                        productsCategory_guid = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.guid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.product_subclass");
            DropTable("dbo.product_category");
            DropTable("dbo.product");
            DropTable("dbo.inquiry");
        }
    }
}
