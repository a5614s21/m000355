namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editDealer20220315 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.dealer", "addressmap", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.dealer", "addressmap");
        }
    }
}
