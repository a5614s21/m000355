namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUser20220117 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.user", "role2_guid", c => c.String(maxLength: 64));
            AddColumn("dbo.user", "role3_guid", c => c.String(maxLength: 64));
            AddColumn("dbo.user", "role4_guid", c => c.String(maxLength: 64));
            AddColumn("dbo.user", "role5_guid", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            DropColumn("dbo.user", "role5_guid");
            DropColumn("dbo.user", "role4_guid");
            DropColumn("dbo.user", "role3_guid");
            DropColumn("dbo.user", "role2_guid");
        }
    }
}
