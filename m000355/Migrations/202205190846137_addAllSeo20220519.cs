namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAllSeo20220519 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "keywords", c => c.String());
            AddColumn("dbo.news", "description", c => c.String());
            AddColumn("dbo.product", "keywords", c => c.String());
            AddColumn("dbo.product", "description", c => c.String());
            AddColumn("dbo.web_data", "keywords_warrantyregister", c => c.String());
            AddColumn("dbo.web_data", "description_warrantyregister", c => c.String());
            AddColumn("dbo.web_data", "keywords_warrantycheck", c => c.String());
            AddColumn("dbo.web_data", "description_warrantycheck", c => c.String());
            AddColumn("dbo.web_data", "keywords_warrantyPolicy", c => c.String());
            AddColumn("dbo.web_data", "description_warrantyPolicy", c => c.String());
            AddColumn("dbo.web_data", "keywords_contactService", c => c.String());
            AddColumn("dbo.web_data", "description_contactService", c => c.String());
            AddColumn("dbo.web_data", "keywords_contactinfo", c => c.String());
            AddColumn("dbo.web_data", "description_contactinfo", c => c.String());
            AddColumn("dbo.web_data", "keywords_catalogDown", c => c.String());
            AddColumn("dbo.web_data", "description_catalogDown", c => c.String());
            AddColumn("dbo.web_data", "keywords_dealer", c => c.String());
            AddColumn("dbo.web_data", "description_dealer", c => c.String());
            AddColumn("dbo.web_data", "keywords_news", c => c.String());
            AddColumn("dbo.web_data", "description_news", c => c.String());
            AddColumn("dbo.web_data", "keywords_products", c => c.String());
            AddColumn("dbo.web_data", "description_products", c => c.String());
            AddColumn("dbo.web_data", "keywords_productList", c => c.String());
            AddColumn("dbo.web_data", "description_productList", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "description_productList");
            DropColumn("dbo.web_data", "keywords_productList");
            DropColumn("dbo.web_data", "description_products");
            DropColumn("dbo.web_data", "keywords_products");
            DropColumn("dbo.web_data", "description_news");
            DropColumn("dbo.web_data", "keywords_news");
            DropColumn("dbo.web_data", "description_dealer");
            DropColumn("dbo.web_data", "keywords_dealer");
            DropColumn("dbo.web_data", "description_catalogDown");
            DropColumn("dbo.web_data", "keywords_catalogDown");
            DropColumn("dbo.web_data", "description_contactinfo");
            DropColumn("dbo.web_data", "keywords_contactinfo");
            DropColumn("dbo.web_data", "description_contactService");
            DropColumn("dbo.web_data", "keywords_contactService");
            DropColumn("dbo.web_data", "description_warrantyPolicy");
            DropColumn("dbo.web_data", "keywords_warrantyPolicy");
            DropColumn("dbo.web_data", "description_warrantycheck");
            DropColumn("dbo.web_data", "keywords_warrantycheck");
            DropColumn("dbo.web_data", "description_warrantyregister");
            DropColumn("dbo.web_data", "keywords_warrantyregister");
            DropColumn("dbo.product", "description");
            DropColumn("dbo.product", "keywords");
            DropColumn("dbo.news", "description");
            DropColumn("dbo.news", "keywords");
        }
    }
}
