// <auto-generated />
namespace m000355.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addSecondStagePage120220127 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addSecondStagePage120220127));
        
        string IMigrationMetadata.Id
        {
            get { return "202201270951007_addSecondStagePage120220127"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
