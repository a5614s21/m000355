namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_backSystem_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "options", c => c.String(maxLength: 1));
        }

        public override void Down()
        {
            DropColumn("dbo.web_data", "options");
        }
    }
}
