namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editContactSubject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.contactSubject", "title", c => c.String());
            DropColumn("dbo.contactSubject", "subject");
        }
        
        public override void Down()
        {
            AddColumn("dbo.contactSubject", "subject", c => c.String());
            DropColumn("dbo.contactSubject", "title");
        }
    }
}
