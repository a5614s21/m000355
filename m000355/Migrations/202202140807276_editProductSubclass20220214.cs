namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProductSubclass20220214 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_subclass", "titleSpace", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_subclass", "titleSpace");
        }
    }
}
