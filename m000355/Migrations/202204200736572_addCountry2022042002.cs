namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCountry2022042002 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.contactCountry",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.contactCountry");
        }
    }
}
