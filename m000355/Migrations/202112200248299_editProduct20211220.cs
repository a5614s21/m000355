namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProduct20211220 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product", "specColumn", c => c.String());
            AddColumn("dbo.product", "specValue", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product", "specValue");
            DropColumn("dbo.product", "specColumn");
        }
    }
}
