namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editRole20220113 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.roles", "lang", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.roles", "lang");
        }
    }
}
