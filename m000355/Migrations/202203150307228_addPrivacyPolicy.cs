namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPrivacyPolicy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "privacypolicy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "privacypolicy");
        }
    }
}
