namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editNews0318 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "facebook", c => c.String());
            AddColumn("dbo.news", "line", c => c.String());
            AddColumn("dbo.news", "twitter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "twitter");
            DropColumn("dbo.news", "line");
            DropColumn("dbo.news", "facebook");
        }
    }
}
