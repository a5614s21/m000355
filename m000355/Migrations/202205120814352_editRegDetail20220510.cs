namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editRegDetail20220510 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.warranty_register_detail", "firstname", c => c.String());
            AddColumn("dbo.warranty_register_detail", "lastname", c => c.String());
            AddColumn("dbo.warranty_register_detail", "email", c => c.String());
            AddColumn("dbo.warranty_register_detail", "tel", c => c.String());
            AddColumn("dbo.warranty_register_detail", "company", c => c.String());
            AddColumn("dbo.warranty_register_detail", "address", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.warranty_register_detail", "address");
            DropColumn("dbo.warranty_register_detail", "company");
            DropColumn("dbo.warranty_register_detail", "tel");
            DropColumn("dbo.warranty_register_detail", "email");
            DropColumn("dbo.warranty_register_detail", "lastname");
            DropColumn("dbo.warranty_register_detail", "firstname");
        }
    }
}
