namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editDealer20220216 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.dealer", "category", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.dealer", "category");
        }
    }
}
