namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProduct20211213 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product", "featurespic", c => c.String());
            AddColumn("dbo.product", "featurespic_alt", c => c.String());
            AddColumn("dbo.product", "downloadfile", c => c.String());
            AddColumn("dbo.product", "downloadfile_alt", c => c.String());
            DropColumn("dbo.product", "product_features_pic");
            DropColumn("dbo.product", "product_features_picalt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.product", "product_features_picalt", c => c.String());
            AddColumn("dbo.product", "product_features_pic", c => c.String());
            DropColumn("dbo.product", "downloadfile_alt");
            DropColumn("dbo.product", "downloadfile");
            DropColumn("dbo.product", "featurespic_alt");
            DropColumn("dbo.product", "featurespic");
        }
    }
}
