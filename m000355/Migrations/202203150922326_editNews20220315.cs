namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editNews20220315 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "homedesc", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "homedesc");
        }
    }
}
