namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProduct20211207 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product", "emblem_alt", c => c.String());
            AddColumn("dbo.product", "product_features", c => c.String());
            AddColumn("dbo.product", "option_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product", "option_alt");
            DropColumn("dbo.product", "product_features");
            DropColumn("dbo.product", "emblem_alt");
        }
    }
}
