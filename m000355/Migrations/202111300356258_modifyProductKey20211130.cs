namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProductKey20211130 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.product");
            DropPrimaryKey("dbo.product_category");
            DropPrimaryKey("dbo.product_subclass");
            AddPrimaryKey("dbo.product", new[] { "id", "guid" });
            AddPrimaryKey("dbo.product_category", new[] { "id", "guid" });
            AddPrimaryKey("dbo.product_subclass", new[] { "id", "guid" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.product_subclass");
            DropPrimaryKey("dbo.product_category");
            DropPrimaryKey("dbo.product");
            AddPrimaryKey("dbo.product_subclass", "guid");
            AddPrimaryKey("dbo.product_category", "guid");
            AddPrimaryKey("dbo.product", "guid");
        }
    }
}
