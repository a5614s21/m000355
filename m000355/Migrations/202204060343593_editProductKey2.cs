namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProductKey2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "guid", c => c.String(maxLength: 64));
            AddPrimaryKey("dbo.product", new[] { "subtitle", "status", "lang" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "guid", c => c.String(nullable: false, maxLength: 64));
            AddPrimaryKey("dbo.product", new[] { "id", "guid", "subtitle", "status", "lang" });
        }
    }
}
