namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProductKey3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "status", c => c.String(maxLength: 1));
            AddPrimaryKey("dbo.product", new[] { "subtitle", "lang" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.product");
            AlterColumn("dbo.product", "status", c => c.String(nullable: false, maxLength: 1));
            AddPrimaryKey("dbo.product", new[] { "subtitle", "status", "lang" });
        }
    }
}
