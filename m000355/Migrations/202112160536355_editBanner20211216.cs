namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editBanner20211216 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.home_banner", "category", c => c.String());
            AddColumn("dbo.product_banner", "category", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_banner", "category");
            DropColumn("dbo.home_banner", "category");
        }
    }
}
