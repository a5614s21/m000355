namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editAccount20220106 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.roles", "create_name", c => c.String());
            AddColumn("dbo.roles", "modify_name", c => c.String());
            AddColumn("dbo.user", "create_name", c => c.String());
            AddColumn("dbo.user", "modify_name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.user", "modify_name");
            DropColumn("dbo.user", "create_name");
            DropColumn("dbo.roles", "modify_name");
            DropColumn("dbo.roles", "create_name");
        }
    }
}
