namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAshcan20220124 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ashcan",
                c => new
                    {
                        guid = c.Guid(nullable: false, identity: true),
                        title = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        from_guid = c.String(maxLength: 64),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                    })
                .PrimaryKey(t => t.guid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ashcan");
        }
    }
}
