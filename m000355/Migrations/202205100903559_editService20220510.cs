namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editService20220510 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.service", "mapsitex", c => c.String());
            AddColumn("dbo.service", "mapsitey", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.service", "mapsitey");
            DropColumn("dbo.service", "mapsitex");
        }
    }
}
