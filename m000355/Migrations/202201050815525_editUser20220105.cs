namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUser20220105 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.home_banner", "createuser", c => c.String());
            AddColumn("dbo.home_banner", "modifyuser", c => c.String());
            AddColumn("dbo.product_banner", "createuser", c => c.String());
            AddColumn("dbo.product_banner", "modifyuser", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_banner", "modifyuser");
            DropColumn("dbo.product_banner", "createuser");
            DropColumn("dbo.home_banner", "modifyuser");
            DropColumn("dbo.home_banner", "createuser");
        }
    }
}
