namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUser202201052 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.home_banner", "create_name", c => c.String());
            AddColumn("dbo.home_banner", "modify_name", c => c.String());
            AddColumn("dbo.product_banner", "create_name", c => c.String());
            AddColumn("dbo.product_banner", "modify_name", c => c.String());
            DropColumn("dbo.home_banner", "createuser");
            DropColumn("dbo.home_banner", "modifyuser");
            DropColumn("dbo.product_banner", "createuser");
            DropColumn("dbo.product_banner", "modifyuser");
        }
        
        public override void Down()
        {
            AddColumn("dbo.product_banner", "modifyuser", c => c.String());
            AddColumn("dbo.product_banner", "createuser", c => c.String());
            AddColumn("dbo.home_banner", "modifyuser", c => c.String());
            AddColumn("dbo.home_banner", "createuser", c => c.String());
            DropColumn("dbo.product_banner", "modify_name");
            DropColumn("dbo.product_banner", "create_name");
            DropColumn("dbo.home_banner", "modify_name");
            DropColumn("dbo.home_banner", "create_name");
        }
    }
}
