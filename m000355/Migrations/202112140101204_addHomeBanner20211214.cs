namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addHomeBanner20211214 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.home_banner",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subtitle = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        picmobile = c.String(),
                        picmobile_alt = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.home_banner");
        }
    }
}
