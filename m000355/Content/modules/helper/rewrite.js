/**
 * URL 重寫類別
 * 
 * @version v 1.0.3 2011/08/24 00:00
 * @author youweb <youweb@youweb.com.tw>
 * @copyright youweb Design Studio. (youweb.com.tw)
 */
;(function ($)
{
    if (!__rewriteConfigs) return;

    $.rewrite = (
    {
        /**
         * 伺服器設定
         */
        _configs: {
            hasOccurred: false,
            isIntl: false,
            parsedQuerys: {},
            URL_PATTERN_USER: '',
            URL_PATTERN_THUMB: '',
            PARAMS_DEFAULT_NAME_USER: '',
            PARAMS_DEFAULT_NAME_THUMB: '',
            INTL_FIELD_NAME: '',
            UNIT_DEFAULT_NAME: ''
        },
        
        /**
         * 初始化
         */
        init: function() 
        {
            this._configs = __rewriteConfigs;
            return this;
        },

        //***********************************************************************************************************************//
        // 動態網址
        //***********************************************************************************************************************//

        /**
         * 將 URL 轉換為動態網址
         *
         * @param string $url
         * @param boolean $onlyOnHasOccurred - 只在已發生了重寫事件才轉換
         * @return string
         *
         */
        urlToDynamic: function($url, $onlyOnHasOccurred)
        {
            if (!this._isNotSelfHostname($url) && !($onlyOnHasOccurred && !this.hasOccurred())) 
            {
                var $parseUrl = youweb.url.parse($url);
                $parseUrl['path'] = $parseUrl['path'] || '';

                if ($parseUrl['path']) 
                {
                    //拆解路徑內容
                    var matches = $parseUrl['path'].match(new RegExp('^[\\./]*(' + youweb.quoteRegex(this._getBase().replace(/^\/*|\/*$/g, '')) + ')?', 'i'));
                    var $base = matches ? matches[0] : '';
                    var $path = $parseUrl['path'].replace(new RegExp('^' + youweb.quoteRegex($base) + '/?'), '');
                    
                    //修正實體路徑
                    $parseUrl['path'] = $base;

                    //解出 query string
                    var $queryString = {};
                    if ($parseUrl['query']) 
                        $queryString = youweb.url.query.parse($parseUrl['query'], $queryString);

                    //樣式比對符合
                    //thumb/params...
                    if ($matches = $path.match(this._configs.URL_PATTERN_THUMB)) {
                        $url = this._urlToDynamicOfThumb($url, $parseUrl, $queryString, $matches);
                    }

                    //樣式比對符合
                    //unit/[params]... OR
                    //intl/lang/[unit]/[params]...
                    else if ($matches = $path.match(this._configs.URL_PATTERN_USER)) {
                        $url = this._urlToDynamicOfUser($url, $parseUrl, $queryString, $matches);
                    }
                }
            }
            return $url;
        },
        
        /**
         * 將 URL[user] 轉換為動態網址
         *
         * @param string $url
         * @param array $parseUrl
         * @param array $queryString
         * @param array $matches
         * @return string
         *
         */
        _urlToDynamicOfUser: function($url, $parseUrl, $queryString, $matches) 
        {
            //初始化集成
            var $result = {};
            if ($matches[2]) $result[this._configs.INTL_FIELD_NAME] = $matches[2];
            if ($matches[5]) $result['unit'] = $matches[5];
            if ($queryString) youweb.extend($result, $queryString);

            //解析 params
            if ($params = ($matches[6] || '').replace(/^\/*|\/*$/g, '')) {
                this._parseParams(function($key, $value, $index) {
                    $result[$key] = $value;
                }, this._configs.PARAMS_DEFAULT_NAME_USER, $params);
            }

            //集成結果
            if ($result) {
                $parseUrl['query'] = youweb.url.query.build($result);
                $url = this._makerUrl($parseUrl);
            }
            return $url;
        },
        
        /**
         * 將 URL[thumb] 轉換為動態網址
         *
         * @param string $url
         * @param array $parseUrl
         * @param array $queryString
         * @param array $matches
         * @return string
         *
         */
        _urlToDynamicOfThumb: function($url, $parseUrl, $queryString, $matches) 
        {
            var self = this;

            //初始化集成
            var $result = {action: 'thumb'};
            if ($queryString) youweb.extend($result, $queryString);

            //解析 params
            if ($params = ($matches[1] || '').replace(/^\/*|\/*$/g, '')) {
                var self = this;
                this._parseParams(function($key, $value, $index) {
                    if ($key == self._configs.PARAMS_DEFAULT_NAME_THUMB) 
                        $value = self.base64_decode($value);
                    $result[$key] = $value;
                }, this._configs.PARAMS_DEFAULT_NAME_THUMB, $params);
            }

            //集成結果
            if ($result) {
                $parseUrl['query'] = youweb.url.query.build($result);
                $url = this._makerUrl($parseUrl);
            }
            return $url;
        },
        
        /**
         * 解析 params 內容
         *
         * @param string $callback
         * @param string $defaultFieldName
         * @param string $params
         * @return string
         *
         */
        _parseParams: function ($callback, $defaultFieldName, $params)
        {
            if ($params) 
            {
                var $isFoundDefault = false;
                $params = $params.split('/');
                for (var $index in $params) 
                {
                    var $param = $params[$index], $key = null, $value;
                    var $explode = $param.split('-');
                    if ($explode.length >= 2) 
                    {
                        //超過三組結果以上附加到第二組
                        if ($explode.length > 2) {
                            for (var $k = 2; $k < $explode.length; $k++)
                                $explode[1] += '-' + $explode[$k];
                        }
                        $key = $explode[0];
                        $value = $explode[1];
                    } else $value = $explode[0];
                    $value = youweb.url.decode($value);

                    //當找到第一個沒有指定名稱的參數時使用預設名稱
                    if ($key == null && $defaultFieldName && !$isFoundDefault) {
                        $key = $defaultFieldName;
                        $isFoundDefault = true;
                    }

                    //無指定名稱時直接使用參數索引
                    if ($key == null) $key = $index;

                    //callback
                    if (typeof($callback) == 'function')
                        $callback($key, $value, $index);
                }
            }
        },

        //***********************************************************************************************************************//
        // 靜態網址
        //***********************************************************************************************************************//
        
        /**
         * 將 URL 轉換為靜態網址
         *
         * @param string $url
         * @param boolean $onlyOnHasOccurred - 只在已發生了重寫事件才轉換
         * @return string
         *
         */
        urlToStatic: function($url, $onlyOnHasOccurred)
        {
            if (!this._isNotSelfHostname($url) && !($onlyOnHasOccurred && !this.hasOccurred())) 
            {
                //轉換 HTML 實體
                var $text2Html = false;
                if ($url.match(/&amp;/)) {
                    $url = $url.replace(/&amp;/g, '&');
                    $text2Html = true;
                }

                var $parseUrl = youweb.url.parse($url);
                $parseUrl['path'] = $parseUrl['path'] || '';

                //如果包含 index.php 的修正處理
                $parseUrl['path'] = $parseUrl['path'].replace(new RegExp('^(([\\../]|(' + youweb.quoteRegex(this._getBase()) + '))*/)?index.php', 'i'), '$1');

                //沒有 ? 符號開始的 query string 的修正處理
                if (!$parseUrl['query'] && 
                    !$parseUrl['path'].match(/\//) &&
                    $parseUrl['path'].match(/[=&]/)) 
                {
                    $parseUrl['query'] = $parseUrl['path'];
                    $parseUrl['path'] = '';
                }
                
                if (!$parseUrl['path'] || 
                    !$parseUrl['path'].match(/\w+/i) || 
                     $parseUrl['path'].match(new RegExp('^[\\../]*' + youweb.quoteRegex(this._getBase()) + '/?$', 'i'))) 
                {
                    var $queryString = {};
                    if ($parseUrl['query']) 
                        $queryString = youweb.url.query.parse($parseUrl['query'], $queryString);

                    //to user
                    if ($queryString['action'] == 'user' || !$queryString['action']) {
                        $url = this._urlToStaticOfUser($url, $parseUrl, $queryString);
                    }
                    //to thumb
                    else if ($queryString['action'] == 'thumb') {
                        $url = this._urlToStaticOfThumb($url, $parseUrl, $queryString);
                    }
                }

                //還原 HTML 實體
                if ($text2Html) $url = $url.replace(/&/g, '&amp;');
            }
            return $url;
        },

        /**
         * 將 URL[user] 轉換為靜態網址
         *
         * @param string $url
         * @param array $parseUrl
         * @param array $queryString
         * @return string
         *
         */
        _urlToStaticOfUser: function($url, $parseUrl, $queryString) 
        {
            //如果包含一個以上的 query string
            for (var $qs in $queryString) 
            {
                //必須強制包含 unit 參數
                //否則無法符合靜態網址格式
                if (!$queryString['unit'])
                    $queryString['unit'] = this._configs.UNIT_DEFAULT_NAME;
                break;
            }
            
            //在多語系頁面的例外處理
            if (this.isIntl()) 
            {
                //強制包含 intl 參數
                if (!$queryString[this._configs.INTL_FIELD_NAME])
                    $queryString[this._configs.INTL_FIELD_NAME] = this._configs.parsedQuerys[this._configs.INTL_FIELD_NAME];
            }

            //預先準備好關鍵參數
            //稍候直接參考使用
            var $prepared = {};
            $prepared['intl'] =  null;
            $prepared['unit'] =  null;
            $prepared[this._configs.PARAMS_DEFAULT_NAME_USER] = null;
            for(var $key in $prepared) {
                if ($queryString[$key]) {
                    $prepared[$key] = $queryString[$key];
                    delete $queryString[$key];
                } else delete $prepared[$key];
            }
            
            //至少需要 intl 或 unit 參數才可轉靜態
            if ($prepared['intl'] || $prepared['unit']) 
            {
                var $parts = [];
                var $querys = {};

                //預設參數集成
                if ($prepared['intl']) $parts.push(this._configs.INTL_FIELD_NAME, $prepared['intl']);
                if ($prepared['unit']) $parts.push($prepared['unit']);

                //一般參數集成
                for (var $key in $queryString) {
                    var $value = $queryString[$key];
                    if (this._isWithSpecialChars($value)) {
                        $querys[$key] = $value;
                        continue;
                    }
                    $value = youweb.url.encode($value);
                    $part = $key + '-' + $value;
                    $parts.push($part);
                }
 
                //特殊參數集成
                if ($prepared[this._configs.PARAMS_DEFAULT_NAME_USER]) 
                    $parts.push(youweb.url.encode($prepared[this._configs.PARAMS_DEFAULT_NAME_USER]));

                $parseUrl['query'] = youweb.url.query.build($querys);
                $url = this._makerUrl($parseUrl, $parts.join('/'));
            }
            return $url;
        },

        /**
         * 將 URL[thumb] 轉換為靜態網址
         *
         * @param string $url
         * @param array $parseUrl
         * @param array $queryString
         * @return string
         *
         */
        _urlToStaticOfThumb: function($url, $parseUrl, $queryString) 
        {
            var self = this;

            //預先準備好關鍵參數
            //稍候直接參考使用
            var $prepared = {};
            $prepared['action'] =  null;
            $prepared[this._configs.PARAMS_DEFAULT_NAME_THUMB] = null;
            for (var $key in $prepared) {
                if ($queryString[$key]) {
                    $prepared[$key] = $queryString[$key];
                    delete $queryString[$key];
                } else delete $prepared[$key];
            }

            //必須有 action、src 參數才可轉靜態
            if ($prepared['action'] && $prepared[this._configs.PARAMS_DEFAULT_NAME_THUMB]) 
            {
                var $parts = [];
                var $querys = {};

                //預設參數集成
                $parts.push($prepared['action']);

                //一般參數集成
                for (var $key in $queryString) {
                    var $value = $queryString[$key];
                    if (this._isWithSpecialChars($value)) {
                        $querys[$key] = $value;
                        continue;
                    }
                    $value = youweb.url.encode($value);
                    $part = $key + '-' + $value;
                    $parts.push($part);
                }

                //特殊參數集成
                $parts.push(self.base64_encode($prepared[this._configs.PARAMS_DEFAULT_NAME_THUMB]));
                
                $parseUrl['query'] = youweb.url.query.build($querys);
                $url = this._makerUrl($parseUrl, $parts.join('/'));
            }
            return $url;
        },

        /**
         * 從 parse_url() 結果還原成 URL 格式
         *
         * @param array $parseUrl
         * @param string $suffix
         * @return string
         *
         */
        _makerUrl: function ($parseUrl, $suffix)
        {
            var $sorted = {
                scheme: $parseUrl['scheme'],
                host: $parseUrl['host'],
                port: $parseUrl['port'],
                user: $parseUrl['user'],
                pass: $parseUrl['pass'],
                path: $parseUrl['path'],
                query: $parseUrl['query'],
                fragment: $parseUrl['fragment']
            };

            var $url = '';
            for (var $key in $sorted) 
            {
                var $part = $parseUrl[$key];
                if ($part) {
                    if ($key == 'scheme') $url = $part + '://';
                    else if ($key == 'port') $url += ':' + $part;
                    else if ($key == 'path') {
                        $url += $part;
                        if (!$url.match(new RegExp('/$')))  $url += '/';
                    }
                    else if ($key == 'query' || $key == 'fragment') continue;
                    else $url += $part;
                }
            }
            if ($suffix) $url += $suffix;
            if ($parseUrl['query']) $url += '?' + $parseUrl['query'];
            if ($parseUrl['fragment']) $url += '#' + $parseUrl['fragment'];
            return $url;
        },

        //***********************************************************************************************************************//
        // 公用方法
        //***********************************************************************************************************************//

        /**
         * 傳回查詢字串內容
         */
        _queryStringCache: null,
        getQueryString: function ()
        {
            //if (this._queryStringCache) return this._queryStringCache;
            var $queryString = location.search.replace(/^\?/, '');
            if (this.hasOccurred()) {
                var split = this.urlToDynamic(location.href).split('?');
                $queryString = split.length >= 2 ? split[1] : '';
            }
            this._queryStringCache = $queryString;
            return $queryString;
        },
        
        /**
         * 傳回網站根目錄的路徑
         *
         * @see this._getBase()
         * @return string
         *
         */
        getBase: function()
        {
            return this._getBase();
        },

        /**
         * 傳回是否已經發生重寫事件
         *
         * @return boolean
         *
         */
        hasOccurred: function() 
        {
            return this._configs.hasOccurred;
        },
        
        /**
         * 傳回現在是否在多語系頁面
         *
         * @return boolean
         *
         */  
        isIntl: function() 
        {
            return this._configs.isIntl;
        },

        //***********************************************************************************************************************//
        // 私有方法
        //***********************************************************************************************************************//

        /**
         * 傳回 URL 是否非本台主機
         *
         * @param mixed $url
         * @return boolean
         *
         */
        _isNotSelfHostname: function($url) 
        {
            if ($url.match(new RegExp('^\\w+://.+\..+', 'i')) && 
               !$url.match(new RegExp('^https?://(www.)?' + youweb.quoteRegex(document.domain), 'i'))) {
                return true;
            }
            return false;
        },
        
        /**
         * 傳回是否包含特殊字元
         *
         * @param string $chars
         * @return string
         *
         */
        _isWithSpecialChars: function($chars)
        {
            return $chars.match(/%2F|%5C|\/|\\/);
        },
        
        /**
         * 傳回以網站根目錄開始的路徑
         *
         * @param string $currentUri
         * @return string
         *
         */
        _getPath: function()
        {
            return location.pathname.replace(new RegExp('^' + this._getBase()), '');
        },
        
        /**
         * 傳回網站根目錄的路徑
         *
         * @return string
         *
         */
        _getBase: function()
        {
            return __base.replace(/\w+:\/\/[^\/]+/, '');
        },
        
        utf8_encode: function (argString) {
            if (argString === null || typeof argString === "undefined") {
                return "";
            }

            var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
            var utftext = "",
                start, end, stringl = 0;

            start = end = 0;
            stringl = string.length;
            for (var n = 0; n < stringl; n++) {
                var c1 = string.charCodeAt(n);
                var enc = null;

                if (c1 < 128) {
                    end++;
                } else if (c1 > 127 && c1 < 2048) {
                    enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
                } else {
                    enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
                }
                if (enc !== null) {
                    if (end > start) {
                        utftext += string.slice(start, end);
                    }
                    utftext += enc;
                    start = end = n + 1;
                }
            }
            if (end > start) {
                utftext += string.slice(start, stringl);
            }
            return utftext;
        },
        utf8_decode: function (str_data) {
            var tmp_arr = [],
                i = 0,
                ac = 0,
                c1 = 0,
                c2 = 0,
                c3 = 0;

            str_data += '';

            while (i < str_data.length) {
                c1 = str_data.charCodeAt(i);
                if (c1 < 128) {
                    tmp_arr[ac++] = String.fromCharCode(c1);
                    i++;
                } else if (c1 > 191 && c1 < 224) {
                    c2 = str_data.charCodeAt(i + 1);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = str_data.charCodeAt(i + 1);
                    c3 = str_data.charCodeAt(i + 2);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }

            return tmp_arr.join('');
        },
        base64_encode: function (data) {
            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                ac = 0,
                enc = "",
                tmp_arr = [];

            if (!data) {
                return data;
            }

            data = this.utf8_encode(data + '');

            do { // pack three octets into four hexets
                o1 = data.charCodeAt(i++);
                o2 = data.charCodeAt(i++);
                o3 = data.charCodeAt(i++);

                bits = o1 << 16 | o2 << 8 | o3;

                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;

                // use hexets to index into b64, and append result to encoded string
                tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
            } while (i < data.length);

            enc = tmp_arr.join('');
            var r = data.length % 3;
            return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
        },
        base64_decode: function (data) {
            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                ac = 0,
                dec = "",
                tmp_arr = [];

            if (!data) {
                return data;
            }

            data += '';

            do { // unpack four hexets into three octets using index points in b64
                h1 = b64.indexOf(data.charAt(i++));
                h2 = b64.indexOf(data.charAt(i++));
                h3 = b64.indexOf(data.charAt(i++));
                h4 = b64.indexOf(data.charAt(i++));

                bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

                o1 = bits >> 16 & 0xff;
                o2 = bits >> 8 & 0xff;
                o3 = bits & 0xff;

                if (h3 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1);
                } else if (h4 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < data.length);

            dec = tmp_arr.join('');
            dec = this.utf8_decode(dec);
            return dec;
        },
    })
    .init();

    // Overriding
    (function () {

        function toUrl(url) {
            if ($.rewrite.hasOccurred()) {
                if (url && url.toUrl) {
                    url = url.toUrl();
                }

                if (!url.match(new RegExp('^\\w+://.+\\..+'))) {
                    if (url.match(/^[\.].+/)) {
                        if (url.match(/^\.\.\/.*/)) {
                            url = $.rewrite.urlToDynamic(url);
                            url = $.rewrite.urlToStatic(url);
                        }
                    }
                }
                else {
                    url = $.rewrite.urlToDynamic(url);
                    url = $.rewrite.urlToStatic(url);
                }
            }
            return url;
        };

        youweb.url.query.getNativeQuery = function(uri) { 
            uri = youweb.stringify(uri || '?' + $.rewrite.getQueryString());
            var match = uri.match(/\?(.*)$/);
            return match ? match[1] : '';
        };

        youweb.url.query.toUrl = function () {
            var url = $.rewrite.getBase(),
                query = this.toString();
                
            if (query) url += '?' + query;
            
            if ($.rewrite.hasOccurred()) {
                url = $.rewrite.urlToDynamic(url);
                url = $.rewrite.urlToStatic(url);
            }
            return url;
        };

        youweb.helper.redirect = function (url, target) {
            url = toUrl(url);
            if (youweb.env.browser.msie) {
                var base = document.getElementsByTagName('base');
                if (base && base[0].href) {
                    if (!/^\//.test(url) && !/^(\w+:\/\/)/.test(url)) {
                        url = base[0].href + url; 
                    }
                }
            }
            if (target) window.open(url, target);
            else location.href = url;
        };

        youweb.history.navigate = (function () {
            var toNavigate = youweb.history.navigate;
            return function (url, state) {
                url = toUrl(url);
                toNavigate(url, state);
            };
        }());
    }());

})(jQuery);