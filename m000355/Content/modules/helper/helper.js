/*!
 * Helper
 * 
 * @version 1.0.0 2017/01/01 00:00
 * @author JOE <joe@youweb.tw>
 * @copyright (c) youweb.tw
 * @link http://youweb.tw/
 */

; (function ()
{
    /**
     * 傳回元素節點物件
     *
     * @return string
     */
    $.fn.node = function () {
        return this.get(0);
    };

    /**
     * 傳回選取器是否包含任何元素
     *
     * @return boolean
     */
    $.fn.any = function () {
        return this.length > 0;
    };

    /**
     * 傳回選取器的元素是否存在
     *
     * @return boolean
     * @see $.fn.any()
     */
    $.fn.exists = function () {
        return this.any();
    };

    /**
     * 傳回包含外部 HTML 的元素內容
     *
     * @return string
     */
    $.fn.outer = function () {
        return this.node().outerHTML;
        // return $('<div/>').html(this.clone()).html();
    };

    /**
     * 反轉元素的排列順序
     *
     * @return string
     */
    $.fn.reverse = function () {
        return $(this.get().reverse());
    };

    /**
     * 打亂元素的排列順序
     *
     * @return string
     */
    $.fn.shuffle = function () {
        return $(this.get().sort(function () {
            return Math.random() - 0.5;
        }));
    };

    /**
     * 將元素的能見度設定為可見
     *
     * @return jQuery
     */
    $.fn.visible = function () {
        return this.css('visibility', 'visible');
    };

    /**
     * 將元素的能見度設定為不可見
     *
     * @return jQuery
     */
    $.fn.invisible = function () {
        return this.css('visibility', 'hidden');
    };

    /**
     * 傳回 / 設定元素的透明度
     *
     * @param float value
     * @return mixed
     */
    $.fn.opacity = function (value) {
        if (value !== undefined && value !== null) {
            return this.css('opacity', value);
        }
        return parseFloat(this.css('opacity')) || 1;
    };

    /**
     * 設定元素為全透明
     *
     * @return jQuery
     */
    $.fn.transparent = function () {
        return this.opacity(0);
    };

    /**
     * 設定元素為不透明
     *
     * @return jQuery
     */
    $.fn.opaque = function () {
        return this.opacity(1);
    };

    /**
     * 移除 CSS 屬性
     *
     * @param string propertyName
     * @return jQuery
     */
    $.fn.removeCss = function (propertyName) {
        var properties = propertyName.split(/\s+/);

        return this.each(function () {
            for (var i = 0, length = properties.length, that = $(this); i < length; i++) {
                that.css(properties[i], '');
            }
        });
    };

    /**
     * 以高優先級別設定 CSS 規則
     *
     * @param string property
     * @param string value
     * @return jQuery
     */
    $.fn.importantCss = function (property, value) {
        var content = this.attr('style') || '';
        content.replace(new RegExp(property + ':[^;]+;', 'g'), '');
        content += ' ' + property + ': ' + value + ' !important;';
        return this.css('cssText', content);
    };

    /**
     * 停用元素的選取功能
     *
     * @return jQuery
     */
    $.fn.disableSelection = function () {
        return this
            .attr('unselectable', 'on')
            .css('-moz-user-select', 'none')
            .css('-ms-user-select', 'none')
            .css('-webkit-user-select', 'none')
            .css('user-select', 'none')
            .on('selectstart.selection', function (event) {
                event.preventDefault();
            });
    };

    /**
     * 啟用元素的選取功能
     *
     * @return jQuery
     */
    $.fn.enableSelection = function () {
        return this
            .removeAttr('unselectable')
            .css('-moz-user-select', 'auto')
            .css('-ms-user-select', 'auto')
            .css('-webkit-user-select', 'auto')
            .css('user-select', 'auto')
            .off('selectstart.selection');
    };

    /**
     * 傳回物件的尺寸資訊
     *
     * @todo 放到 scrollTo?
     * @return object
     */
    $.fn.sizing = function () {
        var info = undefined;

        this.each(function () {
            var nodeName = (this.nodeName || '').toLowerCase(),
                specialNodes = ['html', 'body', 'iframe', '#document'],
                isWindow = !nodeName || $.inArray(nodeName, specialNodes) > -1,
                container = isWindow ? this.contentWindow || window : this;

            info = { that: container };

            $.each(['scroll', 'client', 'offset'], function (index, key) {

                if (isWindow) {
                    var document = container.ownerDocument || container.document,
                        html = document.documentElement,
                        body = document.body;

                    info[key] = {
                        left: Math.max(html[key + 'Left'], body[key + 'Left']),
                        top: Math.max(html[key + 'Top'], body[key + 'Top']),
                        width: Math.max(html[key + 'Width'], body[key + 'Width']),
                        height: Math.max(html[key + 'Height'], body[key + 'Height']),
                    };
                }
                else {
                    info[key] = {
                        left: container[key + 'Left'],
                        top: container[key + 'Top'],
                        width: container[key + 'Width'],
                        height: container[key + 'Height']
                    };
                }
            });
        });

        return info;
    };

    /**
     * 設定元素以原始比例填滿父元素的的可視範圍
     *
     * @todo auto mode?
     * @function
     */
    !function () {
        var plugin = 'fit',
            configs = {
                mode: 'text',
                mode: 'contain',
                mode: 'cover',
                marker: 'fitted',
                focus: null,
                size: null,
                visible: true,
                resize: true,
                container: null,
            };

        $.fn[plugin] = function (focus, options)
        {
            // TODO: settings do not need to place the inside each.
            return this.each(function () {
                var item = $(this),
                    inited = item.data(plugin),
                    settings;

                if (inited) {
                    settings = inited.settings;

                    var clientWidth = inited.clientWidth,
                        clientHeight = inited.clientHeight,
                        hasSizeChanged = clientWidth !== settings.container.width()
                            || clientHeight !== settings.container.height();

                    if (!hasSizeChanged) {
                        return true;
                    }
                }
                else if (!settings) {
                    if (typeof focus === 'object') {
                        options = focus;
                        focus = options.focus;
                    }

                    if (typeof focus === 'string') {
                        options = options || {};
                        options.focus = focus;
                    }

                    settings = $.extend({}, configs, options);
                    settings.mode = youweb.string(item.data('mode') || settings.mode).toLowerCase();
                    settings.focus = youweb.stringify(item.data('focus') || settings.focus).toLowerCase();
                    settings.size = youweb.stringify(item.data('size') || settings.size);
                    settings.container = settings.container ? $(settings.container) : item.parent();
                }

                getSize.call(item, settings, function (size) {
                    var clientWidth = settings.container.width(),
                        clientHeight = settings.container.height(),
                        focus = getFocus(settings, item),
                        styled = {
                            position: 'absolute',
                            left: '0',
                            top: '0',
                            right: 'auto',
                            bottom: 'auto',
                            margin: '0',
                            width: 'auto',
                            height: 'auto',
                            minWidth: '0',
                            minHeight: '0',
                            maxWidth: 'none',
                            maxHeight: 'none',
                        };

                    switch (settings.mode) {
                        case 'cover':
                        default:
                            var scaled = clientWidth / size.width,
                                width = size.width * scaled,
                                height = size.height * scaled,
                                ratioX = size.width / clientWidth,
                                ratioY = size.height / clientHeight;

                            if (height < clientHeight) {
                                scaled = clientHeight / size.height;
                                width = size.width * scaled;
                                height = size.height * scaled;
                            }

                            styled.width = width;
                            styled.height = height;

                            $.each({
                                left: {
                                    ratio: ratioY,
                                    client: clientWidth,
                                    size: size.width,
                                    focus: focus[0]
                                },
                                top: {
                                    ratio: ratioX,
                                    client: clientHeight,
                                    size: size.height,
                                    focus: focus[1]
                                }
                            },
                            function (key, value) {
                                var center = Math.floor(value.client / 2),
                                    scaled = Math.floor(value.size / value.ratio),
                                    focus = Math.floor(value.focus * scaled),
                                    offset = focus - center,
                                    half = value.client - center,
                                    remainder = scaled - focus;

                                if (remainder < half) {
                                    offset -= half - remainder;
                                }

                                offset = Math.max(offset, 0);
                                offset = offset * -100 / value.client;
                                styled[key] = offset + '%';
                            });

                            break;

                        case 'contain':
                            var scaled = clientWidth / size.width,
                                width = size.width * scaled,
                                height = size.height * scaled;

                            if (height > clientHeight) {
                                scaled = clientHeight / size.height;
                                width = size.width * scaled;
                                height = size.height * scaled;
                            }

                            styled.width = width;
                            styled.height = height;
                            styled.left = clientWidth * focus[0] - styled.width * focus[0];
                            styled.top = clientHeight * focus[1] - styled.height * focus[1];
                            break;

                        case 'text':
                            var scaled = clientWidth / size.width,
                                width = size.width * scaled,
                                height = size.height * scaled;

                            if (height > clientHeight) {
                                scaled = clientHeight / size.height;
                                width = size.width * scaled;
                                height = size.height * scaled;
                            }

                            styled.display = null;
                            styled.padding = 0;
                            styled.transformOrigin = '0 0';
                            styled.transform = 'scale(' + scaled + ')';
                            styled.left = clientWidth * focus[0] - width * focus[0];
                            styled.top = clientHeight * focus[1] - height * focus[1];

                            break;
                    }

                    item.css(styled);

                    if (inited) {
                        inited.settings = settings;
                        inited.clientWidth = clientWidth;
                        inited.clientHeight = clientHeight;
                        inited.styled = styled;
                    }
                    else {

                        if (!/^hidden$/i.test(settings.container.css('overflow'))) {
                            settings.container.css('overflow', 'hidden');
                        }

                        if (/^static$/i.test(settings.container.css('position'))) {
                            settings.container.css('position', 'relative');
                        }

                        if (settings.visible) {
                            if (/^hidden$/i.test(item.css('visibility'))
                                && settings.container.is(':visible')) {
                                item.css('visibility', 'visible');
                            }
                        }

                        var resize = youweb.ifSet(item.data('resize'), settings.resize);
                        if (youweb.isSet(resize) && resize !== false) {
                            $(window).resized({
                                delay: youweb.integer(resize),
                                namespace: plugin,
                                callback: function () {
                                    item[plugin](options);
                                }
                            });
                        }

                        item.data(plugin, {
                            settings: settings,
                            clientWidth: clientWidth,
                            clientHeight: clientHeight,
                            styled: styled,
                            mode: function (value) {
                                if (youweb.isSet(value)) {
                                    item.data('mode', value);
                                    item.data(plugin, null);
                                    item[plugin](options);
                                    return this;
                                }
                                return item.data('mode');
                            },
                            focus: function (value) {
                                if (youweb.isSet(value)) {
                                    item.data('focus', value);
                                    item.data('focused', null);
                                    item.data(plugin, null);
                                    item[plugin](options);
                                    return this;
                                }
                                return item.data('focus');
                            }
                        });

                        item.addClass(settings.marker);
                    }
                });
            });
        };

        // Gets the item focus.
        function getFocus(settings, item) {
            var focused = item.data('focused');

            if (!focused) {
                focused = settings.focus.replace(/,+/g, ' ').split(/\s+/);
                focused[1] = focused.length === 1 ? 'center' : focused[1];
                focused[0] = youweb.isBlank(focused[0]) ? 'center' : focused[0];

                $.each(focused, function (index, value) {
                    switch (value) {
                        case 'left':
                        case 'top':
                            focused[index] = 0;
                            break;
                        case 'right':
                        case 'bottom':
                            focused[index] = 1;
                            break;
                        default:
                            focused[index] = /^[\-\+]?\d+/.test(value)
                                ? youweb.float(focused[index]) / 100
                                : 0.5;
                            break;
                    }
                });

                focused[0] = youweb.float(focused[0]);
                focused[0] = Math.min(focused[0], 1);
                focused[0] = Math.max(focused[0], 0);
                focused[1] = youweb.float(focused[1]);
                focused[1] = Math.min(focused[1], 1);
                focused[1] = Math.max(focused[1], 0);

                item.data('focused', focused);
            }

            return focused;
        }

        // Gets the item size.
        function getSize(settings, callback) {
            var element = $(this),
                size = { width: 0, height: 0 },
                originalWidth = element.data('originalWidth'),
                originalHeight = element.data('originalHeight');

            function onDone() {
                if (originalWidth === undefined) {
                    element.data('originalWidth', size.width);
                }

                if (originalHeight === undefined) {
                    element.data('originalHeight', size.height);
                }

                if ($.isFunction(callback)) {
                    callback.call(element, size);
                }
            };

            if (originalWidth && originalHeight) {
                size.width = originalWidth;
                size.height = originalHeight;
            }
            else {
                var display = element.css('display');

                if (/^none$/i.test(display)) {
                    display = element.show().css('display');
                }

                switch (settings.mode) {
                    case 'text':
                        if (!/^(\-(\w+)\-)?inline/i.test(display)) {
                            var matches = display.match(/^(\-\w+\-)?(.+)/i),
                                vender = matches[1] || '',
                                type = matches[2];

                            switch (type) {
                                case 'block':
                                case 'box':
                                case 'table':
                                case 'flex':
                                case 'grid':
                                case 'list-item':
                                    element.css('display', vender + 'inline-' + type);
                                    break;
                            }
                        }
                        break;

                    default:
                        if (/^inline$/i.test(display)) {
                            element.css('display', 'inline-block');
                        }
                        break;
                }

                if (settings.size) {
                    var setting = settings.size.replace(/,+/g, ' ').split(/\s+/);

                    setting[1] = setting.length === 1 ? setting[0] : setting[1];
                    setting[1] = youweb.integer(setting[1]);
                    setting[0] = youweb.isBlank(setting[0]) ? settings.align : setting[0];
                    setting[0] = youweb.integer(setting[0]);

                    if (setting[0] && setting[1]) {
                        size.width = setting[0];
                        size.height = setting[1];
                    }
                    else {
                        element.data('size', null);
                        return arguments.callee.call(element, callback);
                    }
                }
                else if (element.is('img')) {
                    if (element.prop('complete')
                        || element.prop('readyState') === 4
                        || (element.prop('width') && element.prop('height'))) {
                        size.width = element.prop('naturalWidth');
                        size.height = element.prop('naturalHeight');
                    }
                    else {
                        var image = new Image();

                        image.onload = image.onerror = function (event) {
                            this.onload = this.onerror = null;

                            if (event.type === 'load') {
                                size.width = this.naturalWidth;
                                size.height = this.naturalHeight;
                            }

                            onDone();
                        };

                        return (image.src = element.attr('src'));
                    }
                }
                else {
                    size.width = element.width();
                    size.height = element.height();
                }
            }
            onDone();
        }

        // Default settings
        $.fn[plugin].configs = configs;
    }();

    /**
     * 影片播放器
     *
     * @todo poster for streaming
     * @todo video/audio using div
     * @function
     */
    !function () {
        var plugin = 'player',
            library = 'plyr',
            configs = {
                mode: 'custom',
                mode: 'background',
                padColor: 'black',
                volume: 5,
                autoplay: false,
                loop: false,
                debug: false,
                callback: null
            };

        $.fn[plugin] = function (options) {

            if ($.isFunction(options)) {
                options = { callback: options };
            }

            var settings = $.extend({}, configs, options);

            return this.each(function () {
                var element = $(this),
                    wrap = $('<div/>'),
                    attrs = Array.prototype.slice.call(element.prop('attributes')),
                    source = youweb.string(element.data('src') || settings.src),
                    isVideo = element.is('video'),
                    isAudio = element.is('audio'),
                    isMedia = isAudio || isVideo,
                    isYoutube = /(youtube\.com|youtu\.be)/.test(source),
                    isVimeo = /(vimeo\.com)/.test(source),
                    isStreaming = isYoutube || isVimeo;

                if (isStreaming && !source) return true;

                // Handle the atteributes
                $.each(attrs, function () {
                    switch (this.name) {
                        case 'controls':
                            settings[this.name] = [
                                'play-large',
                                'play',
                                'progress',
                                'current-time',
                                'mute', 'volume',
                                'captions',
                                'fullscreen'
                            ];
                            break;
                        case 'autoplay':
                            settings[this.name] = true;
                            break;
                        case 'muted':
                            settings[this.name] = true;
                            break;
                        case 'src':
                            settings[this.name] = this.value;
                            break;
                        case 'poster':
                            settings[this.name] = this.value;
                            break;
                        case 'loop':
                            settings[this.name] = this.value;
                            break;
                        case 'width':
                        case 'height':
                        case 'preload':
                            break;
                        default:
                            wrap.attr(this.name, this.value);
                            element.removeAttr(this.name);
                            break;
                    }
                });

                // Modify the new wrapper as root
                wrap.insertAfter(element);
                element.appendTo(wrap);

                // Sets the attrs of streaming
                isYoutube && element.attr('data-type', 'youtube');
                isVimeo && element.attr('data-type', 'vimeo');
                isStreaming && element.attr('data-video-id', source);

                // Users settings from html
                settings.autoplay = youweb.ifSet(wrap.data('autoplay'), settings.autoplay);
                settings.start = youweb.ifSet(wrap.data('start'), settings.start);
                settings.volume = youweb.ifSet(wrap.data('volume'), settings.volume);
                settings.muted = youweb.ifSet(wrap.data('muted'), settings.muted);
                settings.poster = youweb.ifSet(wrap.data('poster'), settings.poster);
                settings.loop = youweb.ifSet(wrap.data('loop'), settings.loop);

                // Sets the background mode 
                if (settings.mode.toLowerCase() === 'background') {
                    settings.controls = youweb.ifSet(settings.controls, []);
                    settings.loadSprite = youweb.ifSet(settings.loadSprite, false);
                    settings.autoplay = youweb.ifSet(settings.autoplay, true);
                    settings.volume = youweb.ifSet(settings.volume, 0);
                    settings.clickToPlay = youweb.ifSet(settings.clickToPlay, true);
                    settings.fit = youweb.ifSet(settings.fit, true);
                }

                // Initialize
                $[plugin].preinit(function () {
                    var instance = plyr.setup(element, settings)[0],
                        container = $(instance.getContainer()).invisible();

                    instance.on('ready', function () {

                        // Hide the controls when it is empty.
                        if (settings.controls && !settings.controls.length) {
                            container.children().last().hide();
                        }

                        // Set initial volume
                        if (youweb.isSet(settings.volume)) {
                            instance.setVolume(youweb.integer(settings.volume));
                        }

                        // Set to muted
                        if (youweb.isSet(settings.muted) && settings.muted) {
                            instance.isMuted() || instance.toggleMute();
                        }

                        // Set initial time
                        // Note: Vimeo have to wait until after it playing.
                        if (settings.start) {
                            if (isVimeo) {
                                instance.on('play', function (event) {
                                    if (!arguments.callee.done) {
                                        arguments.callee.done = true;
                                        instance.seek(youweb.integer(settings.start));
                                    }
                                });
                            }
                            else {
                                instance.seek(youweb.integer(settings.start));
                            }
                        }

                        // Loop for Youtube because it was not supported.
                        // Note: Vimeo/video/audio does not occur this event if `loop` is enabled.
                        instance.on('ended', function (event) {
                            if (settings.loop && isYoutube) {
                                instance.play();
                            }
                        });

                        // Fit the wrapper to parent
                        if (youweb.isSet(settings.fit)) {
                            wrap.fit(settings.fit);

                            // Pad the background color
                            if (settings.padColor && (isStreaming || isVideo)) {
                                wrap.css('background', settings.padColor);
                            }

                            // Improve some issues.
                            container
                                .css({
                                    transform: 'translateY(-50%)',
                                    top: '50%'
                                })
                                .find('[class*="video-wrapper"]').css({
                                    transform: 'scale(1.02)',
                                    cursor: 'default'
                                });
                        }

                        // To visible it
                        container.visible();

                        // Callback
                        if ($.isFunction(settings.callback)) {
                            instance.isAudio = isAudio;
                            instance.isVideo = isVideo;
                            instance.isMedia = isMedia;
                            instance.isYoutube = isYoutube;
                            instance.isVimeo = isVimeo;
                            instance.isStreaming = isStreaming;
                            settings.callback.call(instance, settings);
                        }
                    });
                });
            });
        };

        // Preinitialize
        $[plugin] = {
            preinit: function (callback) {
                youweb.loader.include.library(library).success(function () {
                    $.isFunction(callback) && callback();
                });
            }
        };

        // Default settings
        $.fn[plugin].configs = configs;
    }();

    /**
     * 將影像切割成為拼圖
     *
     * @param object options
     * @return jQuery
     */
    $.fn.sprites = function (options) {
        var settings = {
            cols: 10,
            rows: 10,
            name: 'sprite-tile',
            resize: true,
        },
        config = $.extend({}, settings, options);

        config.rows = config.rows || 1;
        config.cols = config.cols || 1;

        function Plugin(image, index) {
            var plugin = this,
                wrapper = image.parent(),
                wrapperWidth = wrapper.width(),
                imageUri = image.attr('src'),
                imageWidth = image.width(),
                imageHeight = image.height(),
                imageRatio = imageWidth / imageHeight,
                imagePercent = imageWidth / wrapperWidth,
                tileWidth = Math.round(imageWidth / config.cols),
                tileHeight = Math.round(imageHeight / config.rows),
                deficitX = imageWidth - Math.round(config.cols * tileWidth),
                deficitY = imageHeight - Math.round(config.rows * tileHeight),
                id = config.name + '-' + parseInt(Math.random() * 100000) + '-' + (index || 0),
                container = $('<div/>', {
                    'class': config.name,
                    id: id,
                    css: {
                        display: 'inline-block',
                        position: 'relative',
                        width: imageWidth,
                        height: imageHeight,
                    }
                });

            (function initialize() {
                create();
                if (config.resize) {
                    $(window).bind('resize', function () {
                        plugin.resize();
                    });
                }
            }());

            function create() {
                var tiles = '';

                for (var row = 0; row < config.rows; row++) {
                    for (var col = 0; col < config.cols; col++) {
                        var tile = info(row, col),
                            tileCss = 'position: absolute; overflow: hidden; left: ' + tile.left + 'px; \
                                top: ' + tile.top + 'px; width: ' + tile.width + 'px; height: ' + tile.height + 'px',
                            innerCss = 'position: absolute; overflow: hidden; left: 0px; top: 0px; \
                                width: ' + tile.width + 'px; height: ' + tile.height + 'px',
                            imageCss = 'position: absolute; display: block; left: ' + (-tile.left) + 'px; \
                                top: ' + (-tile.top) + 'px; width: ' + imageWidth + 'px; height: ' + imageHeight + 'px';

                        tiles += '<div id="' + tile.id + '" style="' + tileCss + '">';
                        tiles += '<div class="inner" style="' + innerCss + '">';
                        tiles += '<img src="' + imageUri + '" style="' + imageCss + '" />';
                        tiles += '</div>';
                        tiles += '</div>';
                    }
                }

                image.replaceWith(container.html(tiles));
                //wrapper.html(container.html(tiles));
            }

            this.container = container;

            this.resize = function () {
                imageWidth = wrapper.width() * imagePercent;
                imageHeight = imageWidth / imageRatio;
                tileWidth = Math.round(imageWidth / config.cols);
                tileHeight = Math.round(imageHeight / config.rows);
                deficitX = imageWidth - Math.round(config.cols * tileWidth);
                deficitY = imageHeight - Math.round(config.rows * tileHeight);

                container.css({
                    width: imageWidth,
                    height: imageHeight
                });

                var callee = arguments.callee,
                    $tiles = callee.tiles || {},
                    $inners = callee.inners || {},
                    $images = callee.images || {};

                for (var row = 0; row < config.rows; row++) {
                    for (var col = 0; col < config.cols; col++) {
                        var tile = info(row, col),
                            $tile = $tiles[tile.id] || ($tiles[tile.id] = container.find('#' + tile.id)),
                            $inner = $inners[tile.id] || ($inners[tile.id] = $tile.find('.inner')),
                            $image = $images[tile.id] || ($images[tile.id] = $tile.find('img'));

                        $tile.css({
                            width: tile.width,
                            height: tile.height,
                            left: tile.left,
                            top: tile.top
                        });

                        $inner.css({
                            width: tile.width,
                            height: tile.height,
                        });

                        $image.css({
                            width: imageWidth,
                            height: imageHeight,
                            left: -tile.left,
                            top: -tile.top
                        });
                    }
                }
            };

            function info(row, col) {
                var info = {
                    id: id + '-' + row + '-' + col,
                    width: tileWidth,
                    height: tileHeight,
                    left: Math.round(col * tileWidth),
                    top: Math.round(row * tileHeight)
                };

                info.width += (col === config.cols - 1) ? deficitX : 0;
                info.height += (row === config.rows - 1) ? deficitY : 0;

                return info;
            }
        }

        return this.each(function (index) {
            var name = 'plugin_spriteImage';
            if (!$.data(this, name)) {
                var self = $(this),
                    instance = new Plugin(self, index);
                $.data(this, name, instance);
            }
        });
    };

    /**
     * 鎖定畫面
     *
     * @return object
     */
    this.overlay = (function () {
        var defaults = {
            zIndex: 10000000,
            opacity: .5,
            background: '#000000',
            delay: 0
        },
        fadeSpeed = 300,
        className = 'sw-overlay',
        selector = '.' + className,
        overlay = $('<div/>');

        overlay.css({
            position: 'fixed',
            left: '0',
            top: '0',
            width: '100%',
            height: '100%',
            display: 'none',
            cursor: 'wait',
            zIndex: defaults.zIndex,
            background: defaults.background,
            opacity: defaults.opacity,
        });

        $(window).bind('mousedown mouseup keydown keypress', function (event) {
            if ($(selector).length) {
                event.preventDefault();
                return false;
            }
        });

        return {
            show: function (options) {
                var settings = $.extend({}, defaults, options);
                overlay
                    .clone()
                    .addClass(className)
                    .appendTo('body')
                    .css({
                        zIndex: settings.zIndex,
                        background: settings.background,
                        opacity: settings.opacity,
                    })
                    .delay(settings.delay)
                    .fadeIn(fadeSpeed);
            },
            hide: function () {
                $(selector).stop(true).fadeOut(fadeSpeed, function () {
                    $(this).remove();
                });
            },
        };
    }());

    /**
     * 動畫控制器
     *
     * @static
     */
    this.tweener =
    {
        init: function () {
            var self = this;
            
            self.native = TweenMax;
            self.override();
        },
        override: function () {
            var self = this;

            youweb.each(self.native, function (name, member) {
                if (youweb.isFunction(member) && name.match(/^[a-z]+/i)) {
                    self[name] || (self[name] = function () {
                        var args = Array.prototype.slice.call(arguments), map = [];
                        if (args.length === 1 && youweb.isPlainObject(args[0])) {
                            youweb.each(args[0], function (key, val) {
                                map.push(val);
                            });
                            args = map;
                        }
                        var returned = self.native[name].apply(self.native, args);
                        return (returned === self.native) ? self : returned;
                    });
                }
            });
        },
        timeline: function (options) {
            return new function () {
                var self = this,
                    settings = youweb.extend({}, options);

                self.native = new TimelineMax(settings);
                youweb.helper.tweener.override.call(self);
            };
        },
        frame: {
            request: function (callback) {
                var self = this,
                    requestMethod = function (callback) {
                        return window.setTimeout(callback, 1000 / 60);
                    };

                if (window.requestAnimationFrame) {
                    requestMethod = window.requestAnimationFrame;
                }
                else if (window.webkitRequestAnimationFrame) {
                    requestMethod = window.webkitRequestAnimationFrame;
                }
                else if (window.mozRequestAnimationFrame) {
                    requestMethod = window.mozRequestAnimationFrame;
                }
                else if (window.window.oRequestAnimationFrame) {
                    requestMethod = window.window.oRequestAnimationFrame;
                }
                else if (window.msRequestAnimationFrame) {
                    requestMethod = window.msRequestAnimationFrame;
                }

                return requestMethod(callback);
            },
            cancel: function (handle) {
                var self = this,
                    cancelMethod = function (timer) {
                        return window.clearTimeout(timer);
                    };

                if (window.cancelAnimationFrame) {
                    cancelMethod = window.cancelAnimationFrame;
                }
                else if (window.webkitCancelAnimationFrame) {
                    cancelMethod = window.webkitCancelAnimationFrame;
                }
                else if (window.webkitCancelRequestAnimationFrame) {
                    cancelMethod = window.webkitCancelRequestAnimationFrame;
                }
                else if (window.mozCancelRequestAnimationFrame) {
                    cancelMethod = window.mozCancelRequestAnimationFrame;
                }
                else if (window.oCancelRequestAnimationFrame) {
                    cancelMethod = window.oCancelRequestAnimationFrame;
                }
                else if (window.msCancelRequestAnimationFrame) {
                    cancelMethod = window.msCancelRequestAnimationFrame;
                }

                return cancelMethod(handle);
            },
        },
    };

    /**
     * 音效控制器
     *
     * @static
     */
    this.sound = (function () {
        var isSupported = !!window.Audio,
            cache = {},
            audio = {},
            muted = false,
            settings = {
                preload: true,
                path: null,
                list: null
            };

        return {
            init: function (options) {
                var self = this;

                if (isSupported) {
                    $.extend(settings, options);
                    settings.preload && self.preload();
                }
            },
            preload: function () {
                var self = this;

                isSupported && youweb.helper.load(function () {
                    youweb.each(settings.list, function (name) {
                        self.load(name, function () {
                            // console.log(name);
                        });
                    });
                });
            },
            load: function (name, callback) {
                var self = this;

                if (isSupported && !audio[name]) {
                    var format = youweb.env.browser.msie ? 'mp3' : 'wav',
                        uri = settings.path + '/' + name + '.' + format,
                        info = settings.list[name] || {};

                    audio[name] = new Audio();
                    audio[name].src = uri;
                    audio[name].volume = info.volume || 1;
                    audio[name].oncanplay = function () {
                        audio[name].oncanplay = null;
                        audio[name].canPlay = true;
                        callback && callback();

                    };

                    audio[name].load();
                }
            },
            play: function (name) {
                var self = this;

                if (!isSupported) return;

                // Alias
                if (cache.alias && cache.alias[name]) {
                    name = cache.alias[name];
                }
                else if (settings.list && !settings.list[name]) {
                    youweb.each(settings.list, function (key, value) {
                        value = value || {};
                        if (value.alias === name) {
                            cache.alias = cache.alias || {};
                            cache.alias[name] = name = key;
                            return false;
                        }
                    });
                }

                if (!audio[name]) {
                    return self.load(name, function () {
                        self.play(name);
                    });
                }

                if (audio[name].canPlay) {
                    audio[name].currentTime = 0;
                    audio[name].muted = muted;
                    audio[name].play();
                }
            },
            mute: function () {
                var self = this;
                muted = true;
            },
            unmute: function () {
                var self = this;
                muted = false;
            },
            isMuted: function () {
                var self = this;
                return muted;
            }
        };
    }());

    /**
     * 全螢幕
     * 
     * @function
     */
    this.fullScreen = (function ()
    {
        var eventChanged = 'youweb.helper.fullScreen.changed';

        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function () {
            youweb.event.fire(eventChanged, {
                isActive: isActive()
            });
        });

        function isActive() {
            var active = false;

            if (document.documentElement.webkitRequestFullscreen) {
                active = document.webkitIsFullScreen;
            }
            else if (document.documentElement.mozRequestFullScreen) {
                active = document.mozFullScreen;
            }
            else if (document.documentElement.msRequestFullscreen) {
                active = !!document.msFullscreenElement;
            }

            return active;
        }

        return {
            toggle: function () {
                if (document.documentElement.requestFullscreen) {
                    document.fullScreenElement
                        ? document.cancelFullScreen()
                        : document.documentElement.requestFullscreen();
                }
                else if (document.documentElement.webkitRequestFullscreen) {
                    document.webkitFullscreenElement
                        ? document.webkitCancelFullScreen()
                        : document.documentElement.webkitRequestFullscreen();
                }
                else if (document.documentElement.msRequestFullscreen) {
                    document.msFullscreenElement
                        ? document.msExitFullscreen()
                        : document.documentElement.msRequestFullscreen();
                }
                else if (document.documentElement.mozRequestFullScreen) {
                    document.mozFullScreenElement
                        ? document.mozCancelFullScreen()
                        : document.documentElement.mozRequestFullScreen();
                }
            },
            changed: function (handler) {
                youweb.event.on(eventChanged, handler);
            },
            isActive: function () {
                return isActive();
            },
        }
    }());

    /**
     * 數字輸入
     * 
     * @function
     */
    !function () {
        var plugin = 'numericInput';

        $.fn[plugin] = function (options) {

            return this.each(function () {
                var that = $(this);

                that
                    .keypress(function (event) {
                        var keyCode = (event.keyCode ? event.keyCode : event.which);
                        if (keyCode && keyCode != 8 && keyCode != 13 && (keyCode < 48 || keyCode > 57)) {
                            if ($(this).hasClass('double')) {
                                if (keyCode != 46) {
                                    event.preventDefault();
                                    return false;
                                }
                            } else {
                                event.preventDefault();
                                return false;
                            }
                        }
                    })
                    .keyup(function (event) {
                        var before = $(this).val(),
                            after = before.replace(/^\./g, '')
                                .replace(/\.{2,}/g, '.')
                                .replace(/(\d+\.\d+)[\.]/g, '$1')
                                .replace(/[^\d]+/g, '');

                        before != after && $(this).val(after);
                    })
                    .focus(function (event) {
                        $(this).one('mouseup', function () {
                            $(this).select();
                        });
                    })
                    .on('paste drop dragenter dragover', function (event) {
                        event.preventDefault();
                        return false;
                    });
            });
        };
    }();

    /**
     * 平滑滾輪
     * 
     * @function
     */
    !function () {
        var plugin = 'smoothWheel',
            configs = {
                axis: 'y',
                easing: 'easeOutCirc',
                duration: 600,
                scroll: 40,
                accelerated: .6,
                lock: false,
                ignore: '.unwheel'
            };

        $.fn[plugin] = function (options) {
            var settings = $.extend({}, configs, options);

            return this.each(function () {
                var that = $(this),
                    listen = 'mousewheel.' + plugin,
                    last = undefined;

                that.on(listen, function (event, delta) {
                    var target = $(event.target),
                        specialNodes = /^(textarea|select[size]|embed|object)$/i,
                        specialInputs = /^(date|datetime-local|month|week|time|number)$/i;

                    // TODO: target != this 
                    if (event.defaultPrevented
                        || (event.ctrlKey || event.altKey || event.metaKey)
                        || (target.prop('isContentEditable'))
                        || (specialNodes.test(target.prop('nodeName')))
                        || (target.is('input') && specialInputs.test(target.prop('type')))
                        || (target.is(settings.ignore))
                        || (target.parents(settings.ignore).any())) {
                        return true;
                    }

                    var sizing = that.sizing(),
                        overflowX = sizing.scroll.width > sizing.client.width,
                        overflowY = sizing.scroll.height > sizing.client.height,
                        overflows = overflowX && overflowY,
                        isHorizontal = /^x$/i.test(settings.axis),
                        axis = isHorizontal ? 'left' : 'top',
                        size = isHorizontal ? 'width' : 'height',
                        dest = overflows ? axis : overflowX ? 'left' : 'top',
                        scrollTo = youweb.ifSet(last, sizing.scroll[dest]),
                        speed = Math.abs((scrollTo - sizing.scroll[dest]) / settings.scroll),
                        accelerated = Math.max(speed * settings.accelerated, 1),
                        multiply = event.deltaMode === 1 ? settings.scroll : 1,
                        properties = {};

                    scrollTo -= settings.scroll * delta * accelerated;
                    scrollTo = Math.max(0, scrollTo);
                    scrollTo = Math.min(sizing.scroll[size] - sizing.client[size], scrollTo);
                    properties[dest] = last = scrollTo;

                    that.scrollTo(properties, $.extend({}, settings, {
                        queue: false,
                        easing: $.easing[settings.easing] ? settings.easing : 'linear',
                        duration: settings.duration,
                        complete: function () {
                            last = undefined;
                            youweb.callback(settings, 'complete');
                        }
                    }));


                    if (settings.lock || scrollTo !== Math.round(sizing.scroll[dest])) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                });
            });
        };

        // Global function
        $[plugin] = function () {
            var target = $(window);
            return target[plugin].apply(target, arguments);
        };

        // Default settings
        $[plugin].configs = $.fn[plugin].configs = configs;
    }();

    /**
     * 捲動到目標
     * 
     * @function
     */
    !function ()
    {
        var plugin = 'scrollTo',
            configs = {
                axis: 'x,y',
                easing: 'easeInOutCirc',
                duration: 600,
                offset: null,
                position: null,
                paddingless: false,
                marginless: false,
                borderless: false,
                interrupt: true,
                queue: false,
            };

        $.fn[plugin] = function (target, duration, options) {

            // Duration as options
            if (typeof duration === 'object') {
                options = duration;
                duration = undefined;
            }

            // Duration as function
            if (typeof duration === 'function') {
                options = { complete: duration };
                duration = undefined;
            }

            // Options as function
            if (typeof options === 'function') {
                options = { complete: duration };
            }

            // Options as string
            if (typeof options === 'string') {
                options = { easing: options };
            }

            // Settings
            var settings = $.extend({}, configs, options);
            settings.duration = parseInt(duration || settings.duration, 10) || 0;

            // Starting
            return this.each(function () {
                var that = this,
                    sizing = $(that).sizing(),
                    container = $(sizing.that),
                    destination = {left: 0, top: 0},
                    properties = {},
                    hasTarget = false;

                // Parse the target
                switch (youweb.type(target)) {
                    case 'null':
                    case 'underfined':
                        target = 0;

                    case 'function':
                        target = target();

                    case 'number':
                    case 'string':
                        if (/^([\+\-]\=?)?\d+(\.\d+)?(px|%)?$/.test(target)) {
                            destination.left = target;
                            destination.top = target;
                            break;
                        }
                        target = $.isWindow(sizing.that) ? $(target) : $(target, container);

                    case 'object':
                        if ($.isPlainObject(target) && (target.left || target.top)) {
                            destination = target;
                        }
                        else if (target instanceof jQuery || target instanceof HTMLElement) {
                            if (!(target = $(target)).exists()) return;
                            destination = target.offset();
                            hasTarget = true;
                        }
                        break;
                }

                // Parse the destination
                $.each(settings.axis.split(/\s*,\s*/), function (i, part) {
                    var isHorizontal = /^x$/i.test(part),
                        axis = isHorizontal ? 'left' : 'top',
                        size = isHorizontal ? 'width' : 'height',
                        direction = youweb.stringify(destination[axis]),
                        value = parseFloat(direction.replace(/^[\+\-]\=/, '')),
                        scrollMax = sizing.scroll[size] - sizing.client[size];

                    // Handle percent value
                    if (/%$/.test(direction)) {
                        value = value / 100 * scrollMax;
                    }

                    // Handle relative value
                    if (/^[\-\+]\=/.test(direction)) {
                        value = /^\-\=/.test(direction) ? sizing.scroll[axis] - value : value;
                        value = /\+\=/.test(direction)  ? sizing.scroll[axis] + value : value;
                    }

                    // Handler offset left / top
                    if (settings.offset) {
                        if ($.isFunction(settings.offset)) {
                            settings.offset = settings.offset();
                        }

                        if (settings.offset[axis]) {
                            var offsetValue = parseFloat(settings.offset[axis]);
                            if (/%$/.test(settings.offset[axis])) {
                                offsetValue = offsetValue / 100 * scrollMax;
                            }
                            value += offsetValue;
                        }
                    }

                    // Handler position left / top
                    if (hasTarget && settings.position) {
                        if ($.isFunction(settings.position)) {
                            settings.position = settings.position();
                        }

                        if (settings.position[axis]) {
                            var positionValue = parseFloat(settings.position[axis]),
                                targetSize = target[size]();

                            if (/%$/.test(settings.position[axis])) {
                                positionValue = positionValue / 100 * targetSize;
                            }

                            value += Math.min(positionValue, targetSize);
                        }
                    }

                    // Handler border / padding / margin
                    if (hasTarget) {
                        if (settings.borderless) {
                            var prop = youweb.toCamelCase('border', axis, 'width');
                            value += parseInt(target.css(prop), 10) || 0;
                        }

                        if (settings.paddingless) {
                            var prop = youweb.toCamelCase('padding', axis);
                            value += parseInt(target.css(prop), 10) || 0;
                        }

                        if (settings.marginless) {
                            var prop = youweb.toCamelCase('margin', axis);
                            value -= parseInt(target.css(prop), 10) || 0;
                        }
                    }

                    // Handle final value
                    direction = Math.max(0, Math.min(value, scrollMax));
                    properties[youweb.toCamelCase('scroll', axis)] = direction;
                });

                // Scroll the container
                $.each(settings.queue ? properties : [properties], function (key, value) {
                    var listen = 'touchmove.' + plugin,
                        easing = $.easing[settings.easing] ? settings.easing : 'linear',
                        duration = settings.queue ? settings.duration / 2 : duration,
                        animate = false,
                        animation = {};

                    // To direct / queue
                    if (!settings.queue) animation = value;
                    else animation[key] = value;

                    // Handle the duration
                    $.each(animation, function (key, value) {
                        if (container[key]() !== value) {
                            return !(animate = true);
                        }
                    });

                    // Scroll with animation
                    settings.queue || container.stop(settings.queue);
                    container.animate(animation, $.extend({}, settings, {
                        queue: true,
                        easing: easing,
                        duration: animate ? duration : 0,
                        start: function (animation) {

                            // Interrupt the scrolling
                            youweb.env.support.touchable
                                && settings.interrupt
                                && container.one(listen, function (e) {
                                container.stop(settings.queue);
                            });

                            youweb.callback(settings, 'start', animation);
                        },
                        always: function () {

                            // Cancel the interrupting
                            youweb.env.support.touchable
                                && settings.interrupt
                                && container.off(listen);

                            youweb.callback(settings, 'always');
                        }
                    }));
                });
            });
        };

        // Tweener hooks for scrollLeft/Top
        $.Tween.propHooks.scrollLeft =
        $.Tween.propHooks.scrollTop = {
            get: function (tween) {
                return $(tween.elem)[tween.prop]();
            },
            set: function (tween) {
                var current = this.get(tween),
                    next = Math.round(tween.now);

                // Stop the animation if the scrolling has been changed.
                if (!youweb.env.support.touchable
                    && tween.options.interrupt
                    && tween.last !== current
                    && tween.last) {
                    return $(tween.elem).stop(tween.options.queue);
                }

                if (current !== next) {
                    $(tween.elem)[tween.prop](next);
                    tween.last = this.get(tween);
                }
            }
        };

        // Global function
        $[plugin] = function () {
            var target = $(window);
            return target[plugin].apply(target, arguments);
        };

        // Default settings
        $[plugin].configs = $.fn[plugin].configs = configs;
    }();

    /**
     * 緩動效果
     * 
     * @function
     */
    !function () {
        var easings = {};

        if ($.easing["easeOutExpo"]) {
            return false;
        }

        $.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (i, name) {
            easings[name] = function (p) {
                return Math.pow(p, i + 2);
            };
        });

        $.extend(easings, {
            Sine: function (p) {
                return 1 - Math.cos(p * Math.PI / 2);
            },
            Circ: function (p) {
                return 1 - Math.sqrt(1 - p * p);
            },
            Elastic: function (p) {
                return p === 0 || p === 1
                    ? p : -Math.pow(2, 8 * (p - 1)) * Math.sin(((p - 1) * 80 - 7.5) * Math.PI / 15);
            },
            Back: function (p) {
                return p * p * (3 * p - 2);
            },
            Bounce: function (p) {
                var pow2, bounce = 4;
                while (p < ((pow2 = Math.pow(2, --bounce)) - 1) / 11) { }
                return 1 / Math.pow(4, 3 - bounce) - 7.5625 * Math.pow((pow2 * 3 - 2) / 22 - p, 2);
            }
        });

        $.each(easings, function (name, easeIn) {
            $.easing["easeIn" + name] = easeIn;
            $.easing["easeOut" + name] = function (p) {
                return 1 - easeIn(1 - p);
            };
            $.easing["easeInOut" + name] = function (p) {
                return p < 0.5 ? easeIn(p * 2) / 2 : 1 - easeIn(p * -2 + 2) / 2;
            };
        });
    }();

    /**
     * 尺寸事件
     * 
     * @todo element resizing
     * @function
     */
    !function () {
        var plugin = 'resized',
            configs = { delay: 60, now: false, one: false };

        function getOrigEventType(handleObj) {
            var type = 'resize.' + plugin;
            if (handleObj.namespace) {
                type += '.' + handleObj.namespace;
            }
            return type;
        }

        $.event.special[plugin] = (function () {
            return {
                add: function (handleObj) {
                    var that = this,
                        $that = $(that),
                        timer = null,
                        delay = 0,
                        settings = $.extend({}, configs, handleObj.data),
                        type = getOrigEventType(handleObj),
                        args = [type];

                    if (handleObj.selector !== undefined && handleObj.selector !== null) {
                        args.push(handleObj.selector);
                    }

                    if (handleObj.data !== undefined && handleObj.data !== null) {
                        args.push(handleObj.data);
                    }

                    if (handleObj.handler && $.isFunction(handleObj.handler)) {
                        args.push(handleObj.wrapper = function (event) {
                            delay = $.isFunction(settings.delay) ? settings.delay() : settings.delay;
                            delay = (delay === true) ? configs.delay : delay;
                            delay = (delay === false) ? 0 : delay;
                            delay = parseInt(delay) || 0;

                            // TODO: event.type 內容不能直接修改 會影響全域結果 resize 也變 resized
                            event.type = plugin;
                            event.handleObj = handleObj;

                            if (delay > 0) {
                                clearTimeout(timer);
                                timer = setTimeout(function () {
                                    handleObj.handler.call(that, event);
                                }, delay);
                            }
                            else {
                                handleObj.handler.call(that, event);
                            }

                            if (settings.one) {
                                var params = [type];

                                if (handleObj.selector !== undefined && handleObj.selector !== null) {
                                    params.push(handleObj.selector);
                                }

                                params.push(handleObj.wrapper);
                                $that.off.apply($that, params);
                            }
                        });

                        if (settings.now) {
                            // TODO: has no event
                            handleObj.handler.call(that, { type: plugin, handleObj: handleObj });
                            if (settings.one) return;
                        }
                    }

                    $that.on.apply($that, args);
                },
                remove: function (handleObj) {
                    var that = this,
                        $that = $(that),
                        args = [getOrigEventType(handleObj)];

                    if (handleObj.selector !== undefined && handleObj.selector !== null) {
                        args.push(handleObj.selector);
                    }

                    if (handleObj.handler && $.isFunction(handleObj.handler)) {
                        args.push(handleObj.wrapper);
                    }

                    $that.off.apply($that, args);
                },
            };
        }());

        $.fn[plugin] = function (callback, options) {

            if ($.isPlainObject(callback)) {
                options = $.extend({}, callback);
                callback = options.callback;
            }

            var args = [options && options.namespace ? plugin + '.' + options.namespace : plugin];

            if (options && options.selector !== undefined && options.selector !== null) {
                args.push(options.selector);
            }

            if (options !== undefined && options !== null) {
                args.push(options);
            }

            if (callback && $.isFunction(callback)) {
                args.push(callback);
            }

            return this.on.apply(this, args);
        };
    }();

    /**
     * 觸碰事件
     *
     * @function
     */
    !function () {
        var plugin = 'tap';

        function Tap(el) {
            this.el = typeof el === 'object' ? el : document.getElementById(el);
            this.moved = false;
            this.startX = 0;
            this.startY = 0;
            this.hasTouchEventOccured = false;
            this.el.addEventListener('touchstart', this, false);
            this.el.addEventListener('mousedown', this, false);
        }

        Tap.prototype.leftButton = function (e) {
            if ('buttons' in e) return e.buttons === 1;
            return 'which' in e ? e.which === 1 : e.button === 1;
        };

        Tap.prototype.start = function (e) {
            if (e.type === 'touchstart') {
                this.hasTouchEventOccured = true;
                this.el.addEventListener('touchmove', this, false);
                this.el.addEventListener('touchend', this, false);
                this.el.addEventListener('touchcancel', this, false);

            }
            else if (e.type === 'mousedown' && this.leftButton(e)) {
                this.el.addEventListener('mousemove', this, false);
                this.el.addEventListener('mouseup', this, false);
            }

            this.moved = false;
            this.startX = e.type === 'touchstart' ? e.touches[0].clientX : e.clientX;
            this.startY = e.type === 'touchstart' ? e.touches[0].clientY : e.clientY;
        };

        Tap.prototype.move = function (e) {
            var x = e.type === 'touchmove' ? e.touches[0].clientX : e.clientX;
            var y = e.type === 'touchmove' ? e.touches[0].clientY : e.clientY;

            if (Math.abs(x - this.startX) > 10
                || Math.abs(y - this.startY) > 10) {
                this.moved = true;
            }
        };

        Tap.prototype.end = function (e) {
            var evt;

            this.el.removeEventListener('touchmove', this, false);
            this.el.removeEventListener('touchend', this, false);
            this.el.removeEventListener('touchcancel', this, false);
            this.el.removeEventListener('mouseup', this, false);
            this.el.removeEventListener('mousemove', this, false);

            if (!this.moved) {
                try {
                    evt = new window.CustomEvent(plugin, {
                        bubbles: true,
                        cancelable: true
                    });
                }
                catch (e) {
                    evt = document.createEvent('Event');
                    evt.initEvent(plugin, true, true);
                }

                e.stopPropagation();

                if (!e.target.dispatchEvent(evt)) {
                    e.preventDefault();
                }
            }
        };

        Tap.prototype.cancel = function () {
            this.hasTouchEventOccured = false;
            this.moved = false;
            this.startX = 0;
            this.startY = 0;
        };

        Tap.prototype.destroy = function () {
            this.el.removeEventListener('touchstart', this, false);
            this.el.removeEventListener('touchmove', this, false);
            this.el.removeEventListener('touchend', this, false);
            this.el.removeEventListener('touchcancel', this, false);
            this.el.removeEventListener('mousedown', this, false);
            this.el.removeEventListener('mouseup', this, false);
            this.el.removeEventListener('mousemove', this, false);
        };

        Tap.prototype.handleEvent = function (e) {
            switch (e.type) {
                case 'touchstart': this.start(e); break;
                case 'touchmove': this.move(e); break;
                case 'touchend': this.end(e); break;
                case 'touchcancel': this.cancel(e); break;
                case 'mousedown': this.start(e); break;
                case 'mouseup': this.end(e); break;
                case 'mousemove': this.move(e); break;
            }
        };

        $.event.special[plugin] = (function () {
            return {
                setup: function () {
                    $.data(this, plugin, new Tap(this));
                    return false;
                },
                teardown: function () {
                    var tap = $.data(this, plugin);
                    if (tap && tap.destroy) {
                        tap.destroy();
                        $.removeData(this, plugin);
                    }
                    return false;
                }
            };
        }());

        $.fn[plugin] = function (callback) {
            return this.on(plugin, callback);
        };
    }();

    /**
     * 滾輪事件
     *
     * @function
     */
    !function () {
        var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
            toBind = ('onwheel' in document || document.documentMode >= 9)
                ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
            slice = Array.prototype.slice,
            nullLowestDeltaTimeout, lowestDelta;

        if ($.event.fixHooks) {
            for (var i = toFix.length; i;) {
                $.event.fixHooks[toFix[--i]] = $.event.mouseHooks;
            }
        }

        var special = $.event.special.mousewheel = {
            setup: function () {
                if (this.addEventListener) {
                    for (var i = toBind.length; i;) {
                        this.addEventListener(toBind[--i], handler, false);
                    }
                }
                else {
                    this.onmousewheel = handler;
                }

                $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
                $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
            },

            teardown: function () {
                if (this.removeEventListener) {
                    for (var i = toBind.length; i;) {
                        this.removeEventListener(toBind[--i], handler, false);
                    }
                }
                else {
                    this.onmousewheel = null;
                }
                $.removeData(this, 'mousewheel-line-height');
                $.removeData(this, 'mousewheel-page-height');
            },

            getLineHeight: function (elem) {
                var $elem = $(elem),
                    $parent = $elem['offsetParent' in $.fn ? 'offsetParent' : 'parent']();

                if (!$parent.length) {
                    $parent = $('body');
                }

                return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
            },

            getPageHeight: function (elem) {
                return $(elem).height();
            },

            settings: {
                adjustOldDeltas: true,
                normalizeOffset: true
            }
        };

        $.fn.extend({
            mousewheel: function (fn) {
                return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
            },
            unmousewheel: function (fn) {
                return this.unbind('mousewheel', fn);
            }
        });

        function handler(event) {
            var orgEvent = event || window.event,
                args = slice.call(arguments, 1),
                delta = 0,
                deltaX = 0,
                deltaY = 0,
                absDelta = 0,
                offsetX = 0,
                offsetY = 0;

            event = $.event.fix(orgEvent);
            event.type = 'mousewheel';

            if ('detail' in orgEvent) deltaY = orgEvent.detail * -1;
            if ('wheelDelta' in orgEvent) deltaY = orgEvent.wheelDelta;
            if ('wheelDeltaY' in orgEvent) deltaY = orgEvent.wheelDeltaY;
            if ('wheelDeltaX' in orgEvent) deltaX = orgEvent.wheelDeltaX * -1;

            if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
                deltaX = deltaY * -1;
                deltaY = 0;
            }

            delta = deltaY === 0 ? deltaX : deltaY;

            if ('deltaY' in orgEvent) {
                deltaY = orgEvent.deltaY * -1;
                delta = deltaY;
            }

            if ('deltaX' in orgEvent) {
                deltaX = orgEvent.deltaX;
                if (deltaY === 0) { delta = deltaX * -1; }
            }

            if (deltaY === 0 && deltaX === 0) { return; }

            if (orgEvent.deltaMode === 1) {
                var lineHeight = $.data(this, 'mousewheel-line-height');

                delta *= lineHeight;
                deltaY *= lineHeight;
                deltaX *= lineHeight;
            }
            else if (orgEvent.deltaMode === 2) {
                var pageHeight = $.data(this, 'mousewheel-page-height');

                delta *= pageHeight;
                deltaY *= pageHeight;
                deltaX *= pageHeight;
            }

            absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

            if (!lowestDelta || absDelta < lowestDelta) {
                lowestDelta = absDelta;

                if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
                    lowestDelta /= 40;
                }
            }

            if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
                delta /= 40;
                deltaX /= 40;
                deltaY /= 40;
            }

            delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
            deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
            deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta);

            if (special.settings.normalizeOffset && this.getBoundingClientRect) {
                var boundingRect = this.getBoundingClientRect();

                offsetX = event.clientX - boundingRect.left;
                offsetY = event.clientY - boundingRect.top;
            }

            event.deltaX = deltaX;
            event.deltaY = deltaY;
            event.deltaFactor = lowestDelta;
            event.offsetX = offsetX;
            event.offsetY = offsetY;
            event.deltaMode = 0;

            args.unshift(event, delta, deltaX, deltaY);

            if (nullLowestDeltaTimeout) {
                clearTimeout(nullLowestDeltaTimeout);
            }

            nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

            return ($.event.dispatch || $.event.handle).apply(this, args);
        }

        function nullLowestDelta() {
            lowestDelta = null;
        }

        function shouldAdjustOldDeltas(orgEvent, absDelta) {
            return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
        }
    }();

    /**
     * 補間事件
     * 
     * @todo 全小寫
     * @todo animation
     * @function
     */
    !function () {
        var plugin = 'transitionEnd';

        function getOrigEventType(handleObj) {
            var found = null,
                transitions = {
                    'transition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'MsTransition': 'transitionend'
                }

            $.each(transitions, function (property, type) {
                if (document.documentElement.style[property] !== undefined) {
                    found = type + '.' + plugin;
                    if (handleObj.namespace) {
                        found += '.' + handleObj.namespace;
                    }
                    return false;
                }
            });

            return found;
        }

        $.event.special[plugin] = {
            add: function (handleObj) {
                var that = this,
                    $that = $(that),
                    timer = null,
                    args = [getOrigEventType(handleObj)];

                if (handleObj.selector !== undefined || handleObj.selector !== null) {
                    args.push(handleObj.selector);
                }

                if (handleObj.data !== undefined || handleObj.data !== null) {
                    args.push(handleObj.data);
                }

                if (handleObj.handler && $.isFunction(handleObj.handler)) {
                    args.push(handleObj.wrapper = function (event) {
                        clearTimeout(timer);
                        timer = setTimeout(function () {
                            event.type = plugin;
                            event.handleObj = handleObj;
                            handleObj.handler.call(that, event);
                        });
                    });
                }

                $that.on.apply($that, args);
            },
            remove: function (handleObj) {
                var that = this,
                    $that = $(that),
                    args = [getOrigEventType(handleObj)];

                if (handleObj.selector !== undefined || handleObj.selector !== null) {
                    args.push(handleObj.selector);
                }

                if (handleObj.handler && $.isFunction(handleObj.handler)) {
                    args.push(handleObj.wrapper);
                }

                $that.off.apply($that, args);
            },
        };

        $.fn[plugin] = function (callback) {
            return this.on(getOrigEventType(), callback);
        };
    }();

    /**
     * 熱鍵事件
     * 
     * @function
     */
    !function () {
        var plugin = 'hotkey',
            configs = {
                filter: true,
                event: 'keydown',
                separator: '+',
                specialKeys: {
                    8: 'backspace',
                    9: 'tab',
                    10: 'return',
                    13: 'return',
                    16: 'shift',
                    17: 'ctrl',
                    18: 'alt',
                    19: 'pause',
                    20: 'capslock',
                    27: 'esc',
                    32: 'space',
                    33: 'pageup',
                    34: 'pagedown',
                    35: 'end',
                    36: 'home',
                    37: 'left',
                    38: 'up',
                    39: 'right',
                    40: 'down',
                    45: 'insert',
                    46: 'del',
                    59: ';',
                    61: '=',
                    96: '0',
                    97: '1',
                    98: '2',
                    99: '3',
                    100: '4',
                    101: '5',
                    102: '6',
                    103: '7',
                    104: '8',
                    105: '9',
                    106: '*',
                    107: '+',
                    109: '-',
                    110: '.',
                    111: '/',
                    112: 'f1',
                    113: 'f2',
                    114: 'f3',
                    115: 'f4',
                    116: 'f5',
                    117: 'f6',
                    118: 'f7',
                    119: 'f8',
                    120: 'f9',
                    121: 'f10',
                    122: 'f11',
                    123: 'f12',
                    144: 'numlock',
                    145: 'scroll',
                    173: '-',
                    186: ';',
                    187: '=',
                    188: ',',
                    189: '-',
                    190: '.',
                    191: '/',
                    192: '`',
                    219: '[',
                    220: '\\',
                    221: ']',
                    222: "'"
                },
                shiftNums: {
                    '`': '~',
                    '1': '!',
                    '2': '@',
                    '3': '#',
                    '4': '$',
                    '5': '%',
                    '6': '^',
                    '7': '&',
                    '8': '*',
                    '9': '(',
                    '0': ')',
                    '-': '_',
                    '=': '+',
                    ';': ': ',
                    "'": "'",
                    ',': '<',
                    '.': '>',
                    '/': '?',
                    '\\': '|'
                },
                specialNodes: [
                    'textarea',
                    'select'
                ],
                specialInputs: [
                    'text',
                    'password',
                    'number',
                    'email',
                    'url',
                    'range',
                    'date',
                    'month',
                    'week',
                    'time',
                    'datetime',
                    'datetime-local',
                    'search',
                    'color',
                    'tel'
                ],
            };

        function handleKeys(keys, separator) {
            var regexp = new RegExp(youweb.quoteRegex(separator) + '+', 'g');
            keys = keys.replace(/\s+/g, separator);
            keys = keys.replace(regexp, separator);
            keys = keys.toLowerCase();
            keys = keys.split(separator);
            keys = keys.sort();
            keys = keys.join(separator);
            return keys;
        }

        $.each(['keydown', 'keyup', 'keypress'], function () {
            $.event.special[this] = {
                add: function (handleObj) {
                    var handler = handleObj.handler,
                        data = handleObj.data,
                        settings = $.extend({}, configs),
                        listens = [];

                    if (youweb.isType(data, 'string, array')) {
                        settings.keys = data;
                    }

                    if ($.isPlainObject(data)) {
                        $.extend(settings, data);
                    }

                    if (settings.keys) {
                        var keys = $.isArray(settings.keys) ? settings.keys : [settings.keys];

                        $.each(keys, function (index, part) {
                            if (part && typeof part === 'string') {
                                part = handleKeys(part, settings.separator);
                                listens.push(part);
                            }
                        });
                    }

                    if (!listens.length) {
                        return true;
                    }

                    handleObj.handler = function (event) {
                        var target = $(event.target),
                            modifier = '',
                            possible = {},
                            special = event.type !== 'keypress' && settings.specialKeys[event.which],
                            character = String.fromCharCode(event.which).toLowerCase();

                        if (settings.filter) {
                            if (target.prop('isContentEditable')
                                || target.is(settings.specialNodes.join(','))
                                || (target.is('input') && $.inArray(
                                       youweb.stringify(target.prop('type')).toLowerCase(),
                                       settings.specialInputs) > -1
                                    )
                            ) {
                                return true;
                            }
                        }

                        $.each(['alt', 'ctrl', 'shift', 'meta'], function (index, specialKey) {
                            if (event[specialKey + 'Key'] && special !== specialKey) {
                                if (specialKey === 'meta' && event.ctrlKey) return;
                                modifier += specialKey + settings.separator;
                            }
                        });

                        if (special) {
                            possible[modifier + special] = true;
                        }
                        else {
                            possible[modifier + character] = true;
                            possible[modifier + settings.shiftNums[character]] = true;

                            if (modifier === 'shift' + settings.separator) {
                                possible[settings.shiftNums[character]] = true;
                            }
                        }

                        $.each(possible, function (key, val) {
                            possible[handleKeys(key, settings.separator)] = val;
                        });

                        for (var i = 0, length = listens.length; i < length; i++) {
                            if (possible[listens[i]]) {
                                return handler.apply(this, arguments);
                            }
                        }
                    }
                }
            };
        });

        $.fn[plugin] = function (keys, callback, options) {
            options = options || {};

            if (typeof keys === 'object') {
                options = keys;
            }

            if (typeof keys === 'string') {
                options.keys = keys;
            }

            if (typeof callback === 'function') {
                options.callback = callback;
            }

            var eventType = (options.event || configs.event) + '.' + plugin,
                callback = options.callback,
                data = options,
                selector = null;

            return this.on(eventType, selector, data, callback);
        };

        // Global function
        $[plugin] = function () {
            var target = $(window);
            return target[plugin].apply(target, arguments);
        };

        // Default settings
        $[plugin].configs = $.fn[plugin].configs = configs;
    }();

    /**
     * 事件偵測
     *
     * @todo native event?
     * @param string event
     * @param string selector
     * @param function handler
     * @return jQuery
     */
    $.fn.hasEvent = function (event, selector, handler) {
        var result = false;

        this.each(function () {
            var events = $._data(this, 'events') || {},
                eventParts = event.split(/\./),
                eventType = eventParts[0],
                namespace = eventParts.slice(1).join('.'),
                matches = events[eventType];

            if (selector && $.isFunction(selector)) {
                handler = selector;
                selector = undefined;
            }

            result = !!matches;

            if (matches && (namespace || selector || handler)) {
                $.each(matches, function (index, handleObj) {
                    if (namespace && namespace !== handleObj.namespace) {
                        return (result = false);
                    }
                    if (selector && selector !== handleObj.selector) {
                        return (result = false);
                    }
                    if (handler && handler !== handleObj.handler) {
                        return (result = false);
                    }
                });
            }
        });

        return result;
    };
    
    /**
     * 除錯器
     *
     * @class
     */
    this.debugger = (function () {
        var self = this,
            internal = {},
            external = {},
            duration = 300;

        // Build
        internal.build = function () {
            var id = 'youweb-helper-debugger',
                container = $('#' + id);

            if (!container.exists()) {
                return $('<div id="' + id + '"/>').css({
                    display: 'block',
                    position: 'fixed',
                    zIndex: 2147483647,
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    padding: '5px 10px',
                    borderTop: '1px solid rgba(0, 0, 0, 0.8)',
                    backgroundColor: 'rgba(0, 0, 0, 0.7)',
                    whiteWpace: 'pre-wrap',
                    fontFamily: 'Tahoma',
                    color: '#fff',
                })
                .hide()
                .on('click touchend', function () {
                    if (!self.getSelectedText()) {
                        $(this).not(':hidden').finish().slideUp(duration);
                    }
                })
                .appendTo(document.body);
            }
            return container;
        };

        // Format
        internal.format = function (args) {
            for (var i in args) {
                try {
                    args[i] = JSON.stringify(args[i], null, 4);
                }
                catch (ex) {
                    args[i] = '[' + youweb.type(args[i]) + ']';
                }
            }
            return args.join(' ');
        };

        // Log
        internal.log =
        external.log = function () {
            var args = Array.prototype.slice.call(arguments),
                output = internal.format(args),
                container = internal.build();

            if (window.console) {
                console.log.apply(console, arguments);
            }

            if (youweb.env.device.mobile) {
                container.text(output).not(':visible').finish().slideDown(duration);
            }

            return external;
        };

        // Top
        internal.top =
        external.top = function () {
            internal.build().css({
                top: 0,
                bottom: 'auto'
            });
            return external;
        };

        // Bottom
        internal.bottom =
        external.bottom = function () {
            internal.build().css({
                top: 'auto',
                bottom: 0
            });
            return external;
        };

        youweb.log = external.log;
        return external;
    }.call(this));

    /**
     * 應用程式
     *
     * @namespace
     */
    this.App = function (app)
    {
        var self = this, external = self, internal = {};

        /**
         * 延遲載入元件
         *
         * @param {String}|{Array} pattern
         * @param {String} id
         * @returns {Object}
         */
        internal.lazy =
        external.lazy = function (pattern, id) {
            return internal.load(pattern, id, true);
        };

        /**
         * 載入元件
         *
         * @param {String}|{Array} pattern
         * @param {String} id
         * @param {Boolean} lazy
         * @returns {Object}
         */
        internal.load =
        external.load = function (pattern, id, lazy) {
            var callee = arguments.callee,
                extensions = {
                    lazy: internal.lazy,
                    load: internal.load,
                    wait: internal.wait
                };

            if (typeof id === 'boolean') {
                lazy = id;
                id = null;
            }

            id = youweb.stringify(id || pattern);
            lazy = lazy || false;

            if (lazy) {
                callee.lazy = callee.lazy || {};
                callee.lazy[id] = { pattern: pattern, id: id, lazy: !lazy };

                return youweb.extend({
                    process: id,
                    complete: function (callback) {
                        return internal.wait(id).complete(callback);
                    },
                    success: function (callback) {
                        return internal.wait(id).success(callback);
                    },
                    error: function (callback) {
                        return internal.wait(id).error(callback);
                    }
                }, extensions);
            }

            var loader = youweb.loader.include.library(pattern, id);
            return youweb.extend(loader, extensions);
        };

        /**
         * 等候元件載入
         *
         * @returns {void}
         */
        internal.wait =
        external.wait = function (process, successHandler, csvToArray) {
            var callee = arguments.callee,
                callback = function (process, successHandler) {
                    var response = youweb.loader.response(process);
                    if (youweb.isFunction(successHandler)) {
                        response.success(successHandler);
                    }
                    return youweb.extend(response, {
                        load: internal.load,
                        lazy: internal.lazy,
                        wait: internal.wait
                    });
                };

            if (typeof successHandler === 'boolean') {
                csvToArray = successHandler;
                successHandler = null;
            }

            csvToArray = csvToArray || false;

            if (typeof (process) === 'string') {
                process = csvToArray ? process.split(/\s*,\s*/) : [process];
            }

            if (youweb.isArray(process)) {
                youweb.each(process, function (key, val) {
                    if (typeof val === 'object' && val.process) {
                        process[key] = val.process;
                    }
                });

                if (internal.load.lazy) {
                    var cloned = youweb.extend({}, internal.load.lazy),
                        loaders = [];

                    for (var p in process) {
                        for (var id in cloned) {
                            if (process[p] === id) {
                                var params = cloned[id],
                                    loader = internal.load(params.pattern, params.id, params.lazy);

                                loaders.push(loader);
                                delete internal.load.lazy[id];
                            }
                        }
                    }

                    if (loaders.length) {
                        return callback(loaders, successHandler);
                    }
                }
            }

            return callback(process, successHandler);
        };

        /**
         * 視窗尺寸變更
         *
         * @return void
         */
        internal.resize = function () {

            $(window).resized({
                callback: function () {
                    internal.trigger.call(app, 'resizing');
                },
                delay: false,
            });

            $(window).resized({
                callback: function () {
                    internal.trigger.call(app, 'resized');
                }
            });
        };

        /**
         * 設定可辨識標誌
         *
         * @return void
         */
        internal.tokens = function () {
            var env = youweb.env;

            $.each($.extend({}, env.os, env.device, env.browser), function (name, value) {
                if (value) app.html.that.addClass(name);
                if (name === 'msie') app.html.that.addClass(name + value);
            });
        };

        /**
         * 可視窗口物件
         *
         * @todo from server by cookis
         * @return void
         */
        internal.viewport =
        external.viewport = function () {
            var prefix = 'viewport-detection',
                detection = $('<span/>'),
                settings = { list: {}},
                methods = {};

            // Setting
            methods.setting = function (name) {
                return settings[name];
            };

            // Size
            methods.size = function (type) {
                return settings.list[type] || 0;
            };

            // Match
            methods.match = function (type) {
                var keys = Object.keys(settings.list),
                    index = keys.indexOf(type),
                    width = $(window).width(),
                    prev = index - 1,
                    next = index + 1,
                    isMost = index == 0,
                    isLeast = index >= keys.length - 1;

                if (index < 0) return false;

                if (isLeast) {
                    var size = settings.list[keys[prev]];
                    return size ? width < size : false;
                }
                else if (isMost) {
                    return width >= settings.list[type]
                }
                else {
                    return width >= settings.list[type]
                        && width < settings.list[keys[prev]];
                }
            };

            // Up
            methods.up = function (type) {
                var size = settings.list[type];
                return size ? $(window).width() >= size : false;
            };

            // Down
            methods.down = function (type) {
                var size = settings.list[type];
                return size ? $(window).width() <= size : false;
            };

            // Is
            methods.is = function (type) {
                var isVisible = detection.hide()
                    .attr('class', 'shown-' + type)
                    .appendTo(document.body)
                    .is(':visible');

                detection.remove();
                return isVisible;
            };

            // Content
            methods.content = function (name, value) {
                var viewport = app.html.that.find('meta[name="viewport"]'),
                    content = viewport.attr('content') || '',
                    parts = content.split(/\s*,\s*/),
                    settings = {},
                    builds = [];

                $.each(parts, function (index, part) {
                    var info = part.split(/\s*\=\s*/);
                    settings[info[0]] = info[1];
                });

                if (arguments.length === 0) return content;
                if (arguments.length === 1) return settings[name];

                settings[name] = value;
                $.each(settings, function (name, value) {
                    builds.push(name + '=' + value);
                });

                viewport.attr('content', builds.join(', '));
                return this;
            };

            // Initialize
            !function () {
                var content, parts;

                detection.attr('class', prefix).appendTo(document.body);
                content = getComputedStyle(detection[0], ':before').content;
                parts = youweb.trim(content, '""').split(/\s*;\s*/);

                $.each(parts, function (index, part) {
                    var info = part.split(/\s*\:\s*/);
                    if (info.length >= 2) {
                        settings[info[0]] = info[1].indexOf(',') > -1
                            ? info[1].split(/\s*,\s*/)
                            : info[1];
                    }
                });

                $.each(settings.types, function (index, type) {
                    var className = prefix + '-' + type,
                        width = detection.attr('class', className).width();

                    if (width) settings.list[type] = width;
                });

                detection.remove();
                external.viewport = internal.viewport = methods;
            }();

            return methods;
        };

        /**
         * 桌面模擬器
         *
         * @returns {void}
         */
        internal.simulate = function () {
            if (youweb.env.device.mobile
                && youweb.env.simulate.status
                && youweb.env.simulate.desktop) {
                var width = internal.viewport.setting('simulate')
                    || internal.viewport.size('mini');

                if (width) {
                    app.html.that.addClass('simulate desktop');
                    internal.viewport.content('user-scalable', 'yes');
                    internal.viewport.content('width', parseInt(width, 10));
                    $(window).trigger('resize');
                }
            }
        };

        /**
         * 停用無效的連結
         *
         * @returns {void}
         */
        internal.disableInvalidLinks = function () {
            setTimeout(function () {
                app.html.that.find('a, area').click(function (event) {
                    var link = $(this),
                        href = link.attr('href');

                    if (/^javascript(\:;|\/\/)$/i.test(href)) {
                        link.attr('href', href = '#');
                    }

                    if (href === '#') {
                        event.preventDefault();
                    }
                });
            });
        };

        /**
         * 取得作用空間
         *
         * @return void
         */
        internal.workspace =
        external.workspace = function (node) {

            if (node.html
                && node.html.that
                && node.html.that.length) {
                return node.html.that;
            }

            if (node.parent) {
                return node.parent.html
                    && node.parent.html.that
                    && node.parent.html.that.length
                        ? node.parent.html.that
                        : arguments.callee(node.parent);
            }

            return arguments.callee(node.parent);
        };

        /**
         * 傳回是否為有效成員
         *
         * @returns {Boolean}
         */
        internal.isValidMember = function (name, context) {
            var isValidName = /^[a-z0-9]*$/i.test(name)
                && !internal.isReservedWord(name);

            if (arguments.length >= 2) {
                return isValidName
                    && internal.hasRequiredMember(context);
            }

            return isValidName;
        };

        /**
         * 傳回是否使用保留字
         *
         * @returns {Boolean}
         */
        internal.isReservedWord = function (word) {
            var words = ['root', 'parent', 'name', 'selector', 'html', 'window', 'init', 'ready'];
            return youweb.inArray(word, words) > -1;
        };

        /**
         * 傳回是否包含必要成員
         *
         * @returns {Boolean}
         */
        internal.hasRequiredMember = function (context) {
            if (youweb.isPlainObject(context)) {
                return $.isFunction(context.init)
                    || $.isFunction(context.ready);
            }
            return false;
        };

        /**
         * 傳回是否為推遲器
         *
         * @returns {void}
         */
        internal.isDeferred = function (value) {
            return youweb.isSet(value)
                && $.isPlainObject(value)
                && $.isFunction(value.promise);
        };

        /**
         * 推遲器工廠
         *
         * @returns {void}
         */
        internal.defer =
        external.defer = function () {
            return $.Deferred();
        };

        /**
         * 觸發事件
         *
         * @returns {void}
         */
        internal.trigger = function (event, scope, args, then) {
            var returned;

            if ($.isFunction(args)) {
                then = args;
                args = [];
            }

            if ($.isFunction(scope[event])) {
                args = youweb.ifSet(args, []);
                returned = scope[event].apply(scope, args);
            }

            if ($.isFunction(then) && returned !== false) {
                if (internal.isDeferred(returned)) {
                    returned.done(then);
                }
                else then();
            }

            return returned;
        };

        /**
         * 解析方法
         *
         * @return void
         */
        internal.parse = function (scope, name, skipReady) {
            var member = scope[name], returned;

            if (internal.isValidMember(name, member)) {

                member.root = app;
                member.parent = scope;
                member.name = member.name || name;
                member.selector = member.selector;
                member.html = member.html || {};
                member.window = app.window;

                if (internal.trigger('init', member, ready) === false) return;

                function ready()
                {
                    if (!skipReady) {
                        if (!member.html.that) {
                            var workspace = internal.workspace(member);

                            if (member.selector) {
                                member.html.that = workspace.find(member.selector);
                            }
                            else {
                                var uniqueTags = ['title', 'head', 'body', 'main'],
                                    isUniqueTag = youweb.inArray(member.name, uniqueTags) > -1,
                                    selectors = ['#' + member.name, '.' + member.name];

                                if (isUniqueTag) {
                                    // selectors.push(member.name);
                                }
                                
                                youweb.each(selectors, function (index, selector) {
                                    member.html.that = workspace.find(selector);
                                    if (member.html.that.exists()) {
                                        member.selector = selector;
                                        return false;
                                    }
                                });
                            }
                        }

                        if (member.html.that.exists()) {
                            if (internal.trigger('ready', member, parse) === false) return;
                        }
                        else {
                            skipReady = true;
                        }
                    }

                    skipReady && parse();

                    function parse() {
                        youweb.each(member, function (key) {
                            internal.parse(member, key, skipReady);
                        });
                    }
                }
            }
        };

        /**
         * 建構子
         *
         * @constructor
         */
        (function (skipReady) {

            if (!youweb.isPlainObject(app)) {
                throw new Error('This app is not valid.');
            }

            if (internal.hasRequiredMember(app)) {
                app = youweb.extend(external, app);
                app.root = youweb.helper;
                app.parent = external;
                app.name = app.name || 'html';
                app.selector = app.selector || app.name;
                app.html = app.html || {};
                app.window = $(window);

                if (internal.trigger('init', app, ready) === false) return;

                function ready() {
                    youweb.helper.ready(function () {

                        if (!app.html.that) {
                            app.html.that = $(app.selector);
                        }

                        if (app.html.that.exists()) {
                            internal.tokens();
                            internal.viewport();
                            internal.simulate();
                            internal.disableInvalidLinks();

                            if (internal.trigger('ready', app, parse) === false) return;
                        }
                        else {
                            skipReady = true;
                            parse();
                        }

                        function parse() {
                            youweb.each(app, function (name) {
                                internal.parse(app, name, skipReady);
                            });
                        }
                    });
                }
            }
        }());
    };
}
.call(youweb.helper));
