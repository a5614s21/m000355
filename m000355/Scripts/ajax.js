$(document).ready(function () {

    //FB分享
    $(".fb_s").click(function (e) {
        var thisURL = window.document.location.href;

        window.open('http://www.facebook.com/share.php?u='.concat(thisURL), 'pop', config = 'height=500,width=500');
    });

    //twitter分享
    $(".tt_s").click(function (e) {
        var pageTitle = encodeURIComponent(document.title);
        var thisURL = window.document.location.href;
        window.open('http://twitter.com/home?status=' + pageTitle + "+".concat(thisURL), 'pop', config = 'height=500,width=500');
    });

    //line分享
    $(".line_s").click(function (e) {
        var pageTitle = encodeURIComponent(document.title);
        var thisURL = encodeURIComponent(window.document.location.href);
       // window.open('http://line.naver.jp/R/msg/text/?' + "+".concat(thisURL), 'pop', config = 'height=500,width=500');
      
        window.open('https://social-plugins.line.me/lineit/share?url=' + thisURL, 'pop', config = 'height=500,width=500');
    });


    //重製驗證碼

    $('#captcha').click(function () {

        var src = $('#captcha').attr('src').split('?');

        $('#captcha').attr('src', src[0] + "?d=" + Date.now());

    });
});